/* -------------------------------- IdEntity --------------------------------
id              bigint          NOT NULL    auto_increment          COMMENT '编号',
company_id      bigint          NOT NULL                            COMMENT '数据所属公司的ID,用于公司之间的数据隔离',
org_id          bigint          NOT NULL                            COMMENT '数据直属机构的ID',
create_by       bigint          NOT NULL                            COMMENT '创建者',
create_date     datetime        NOT NULL                            COMMENT '创建时间',
update_by       bigint          NOT NULL                            COMMENT '更新者',
update_date     datetime        NOT NULL                            COMMENT '更新时间',
remarks         varchar(255)                                        COMMENT '备注信息',
del_flag        char(1)         NOT NULL    DEFAULT '1'             COMMENT '删除标记（1：正常；2：删除；3：审核）,以字典表sys_dict.dict_key=‘删除标记’为准',
uuid            varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',

PRIMARY KEY (id)
-------------------------------- IdEntity -------------------------------- */

/* ====================================================================================================================
    fileupload -- 文件上传
==================================================================================================================== */

CREATE TABLE fileupload_fileinfo
(
    id              bigint          NOT NULL    auto_increment          COMMENT '编号',
    company_id      bigint          NOT NULL                            COMMENT '数据所属公司的ID,用于公司之间的数据隔离',
    org_id          bigint          NOT NULL                            COMMENT '数据直属机构的ID',
    create_by       bigint          NOT NULL                            COMMENT '创建者',
    create_date     datetime        NOT NULL                            COMMENT '创建时间',
    update_by       bigint          NOT NULL                            COMMENT '更新者',
    update_date     datetime        NOT NULL                            COMMENT '更新时间',
    remarks         varchar(255)                                        COMMENT '备注信息',
    del_flag        char(1)         NOT NULL    DEFAULT '1'             COMMENT '删除标记（1：正常；2：删除；3：审核）,以字典表sys_dict.dict_key=‘删除标记’为准',
    uuid            varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',

    stored_type     char(1)         NOT NULL                            COMMENT '上传文件的存储类型（1：当前服务器硬盘；2：FTP服务器；3：；FastDFS服务器）',
    file_path       varchar(255)    NOT NULL                            COMMENT '上传文件存放路径',
    digest          varchar(255)    NOT NULL                            COMMENT '文件签名，用于判断是否是同一文件',
    digest_type     char(1)         NOT NULL                            COMMENT '文件签名算法类型（1：MD5；2：SHA-1；）',
    file_size       bigint          NOT NULL                            COMMENT '文件大小，单位：byte(1KB = 1024byte)',
    file_name       varchar(255)    NOT NULL                            COMMENT '文件原名称，用户上传时的名称',
    new_name        varchar(255)    NOT NULL                            COMMENT '文件当前名称（UUID + 后缀名）',
    upload_time     bigint          NOT NULL                            COMMENT '文件上传所用时间',
    stored_time     bigint          NOT NULL                            COMMENT '文件存储用时，单位：毫秒（此时间只包含服务器端存储文件所用的时间，不包括文件上传所用的时间）',
    PRIMARY KEY (id)
) COMMENT = '上传文件信息表';
CREATE INDEX fileupload_fileinfo_digest     ON  fileupload_fileinfo     (digest ASC);


/* ====================================================================================================================
    dbscript -- 数据库脚本 SQL查询语句、HQL查询语句
==================================================================================================================== */

CREATE TABLE qlscript_qlscript
(
    id              bigint          NOT NULL    auto_increment          COMMENT '编号',
    company_id      bigint          NOT NULL                            COMMENT '数据所属公司的ID,用于公司之间的数据隔离',
    org_id          bigint          NOT NULL                            COMMENT '数据直属机构的ID',
    create_by       bigint          NOT NULL                            COMMENT '创建者',
    create_date     datetime        NOT NULL                            COMMENT '创建时间',
    update_by       bigint          NOT NULL                            COMMENT '更新者',
    update_date     datetime        NOT NULL                            COMMENT '更新时间',
    remarks         varchar(255)                                        COMMENT '备注信息',
    del_flag        char(1)         NOT NULL    DEFAULT '1'             COMMENT '删除标记（1：正常；2：删除；3：审核）,以字典表sys_dict.dict_key=‘删除标记’为准',
    uuid            varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',

    script_type     varchar(10)     NOT NULL    DEFAULT 'SQL'           COMMENT '脚本类型，可取："SQL"、"HQL"',
    script          varchar(2000)   NOT NULL                            COMMENT '脚本，可以使用模版技术拼接',
    name            varchar(100)    NOT NULL    UNIQUE                  COMMENT '脚本名称，使用包名称+类名+方法名',
    description     varchar(1000)   NOT NULL                            COMMENT '脚本功能说明',
    PRIMARY KEY (id)
) COMMENT = '数据库脚本';
CREATE INDEX qlscript_qlscript_name         ON  qlscript_qlscript (name ASC);


/* ====================================================================================================================
    ftp_user -- ftp服务器用户
==================================================================================================================== */

CREATE TABLE ftp_user (
    userid          varchar(64)     NOT NULL                            COMMENT '用户ID',
    userpassword    varchar(64)                                         COMMENT '用户密码',
    homedirectory   varchar(128)    NOT NULL                            COMMENT '主目录',
    enableflag      boolean                     DEFAULT true            COMMENT '当前用户可用',
    writepermission boolean                     DEFAULT false           COMMENT '具有上传权限',
    idletime        int                         DEFAULT 0               COMMENT '空闲时间',
    uploadrate      int                         DEFAULT 0               COMMENT '上传速率限制（字节每秒）',
    downloadrate    int                         DEFAULT 0               COMMENT '下载速率限制（字节每秒）',
    maxloginnumber  int                         DEFAULT 0               COMMENT '最大登陆用户数',
    maxloginperip   int                         DEFAULT 0               COMMENT '同IP登陆用户数',
    PRIMARY KEY (userid)
) COMMENT = 'FTP服务器用户';






