/* -------------------------------- IdEntity --------------------------------
id              bigint          NOT NULL    auto_increment          COMMENT '编号',
company_code    varchar(255)    NOT NULL                            COMMENT '数据所属公司的机构编码',
office_code     varchar(255)    NOT NULL                            COMMENT '数据直属机构的编码',
create_by       bigint          NOT NULL                            COMMENT '创建者',
create_date     datetime        NOT NULL                            COMMENT '创建时间',
update_by       bigint          NOT NULL                            COMMENT '更新者',
update_date     datetime        NOT NULL                            COMMENT '更新时间',
remarks         varchar(255)                                        COMMENT '备注信息',
del_flag        char(1)         NOT NULL    DEFAULT '0'             COMMENT '删除标记（0：正常；1：删除；2：审核）',
uuid            varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',

PRIMARY KEY (id)
-------------------------------- IdEntity -------------------------------- */





/* -------------------------------- 删除所有表 --------------------------------

DROP TABLE common_qlscript;


-------------------------------- 删除所有表 -------------------------------- */


/* -------------------------------- 系统基础数据 -------------------------------- */



SET character_set_client = utf8 ;
SET character_set_connection = utf8 ;
SET character_set_database = utf8 ;
SET character_set_results = utf8 ;
SET character_set_server = utf8 ;
FLUSH PRIVILEGES ;


-- 日志表
-- MVC URL说明表

/* 
处理完成 搜索不到 webframe Office companyCode officeCode


*/


















