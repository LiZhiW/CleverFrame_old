/* -------------------------------- IdEntity --------------------------------
id              bigint          NOT NULL    auto_increment          COMMENT '编号',
company_code    varchar(255)    NOT NULL                            COMMENT '数据所属公司的机构编码',
office_code     varchar(255)    NOT NULL                            COMMENT '数据直属机构的编码',
create_by       bigint          NOT NULL                            COMMENT '创建者',
create_date     datetime        NOT NULL                            COMMENT '创建时间',
update_by       bigint          NOT NULL                            COMMENT '更新者',
update_date     datetime        NOT NULL                            COMMENT '更新时间',
remarks         varchar(255)                                        COMMENT '备注信息',
del_flag        char(1)         NOT NULL    DEFAULT '0'             COMMENT '删除标记（0：正常；1：删除；2：审核）',
uuid            varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',

PRIMARY KEY (id)
-------------------------------- IdEntity -------------------------------- */


CREATE TABLE test_table
(
    id              bigint          NOT NULL    auto_increment          COMMENT '编号',
    company_code    varchar(255)    NOT NULL                            COMMENT '数据所属公司的机构编码',
    office_code     varchar(255)    NOT NULL                            COMMENT '数据直属机构的编码',
    create_by       bigint          NOT NULL                            COMMENT '创建者',
    create_date     datetime        NOT NULL                            COMMENT '创建时间',
    update_by       bigint          NOT NULL                            COMMENT '更新者',
    update_date     datetime        NOT NULL                            COMMENT '更新时间',
    remarks         varchar(255)                                        COMMENT '备注信息',
    del_flag        char(1)         NOT NULL    DEFAULT '0'             COMMENT '删除标记（0：正常；1：删除；2：审核）',
    uuid            varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',

    int_field       int                                                 COMMENT '整数',
    double_field    double(30,10)                                       COMMENT '小数',
    timestamp_field timestamp                                           COMMENT '时间戳',
    text_field      text                                                COMMENT '长字符串',
    blob_field      blob                                                COMMENT '二进制数据',
    PRIMARY KEY (id)
) COMMENT = '测试使用的表';


















