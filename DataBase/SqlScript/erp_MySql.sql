/*
原料管理
    原料库统计
    登记入库界面
        登记入库界面-
        财务审核
    车间加工出库
        车间加工出库申请-
        车间加工财务申请审核出库
    外发加工出库
        原料外发加工-
        原料外发加工财务审核-
        原料外发加工返回情况-
    半成品外发加工
        外发加工录入-
        外发加工财务审核-
        外发加工返回情况-
    供应商管理
        供应商录入-
        




基础资料管理
    原料资料管理
    供应商管理
原料管理
    原料入库申请
    原料库存查询
    原料出库申请-车间加工
    原料外发加工
        原料出库申请-外发加工
        原料外发加工返回情况
    半成品外发加工
        半成品外发加工申请
        半成品外发加工返回情况
流程管理
    原料入库审核
    原料外发加工审核
    半成品外发加工审核

订单管理
    订单登记 ——> A生产订单、A样板订单、H生产订单、H样板订单、X生产订单、X样板订单
        1.订单登记-业务登记 ——> ?
            
        2.订单登记-工艺登记 ——> ?
            
        3.订单登记-(工艺)审核(厂长、厂长助理审核)  ——> ?
    
生产管理
    经编车间(A)
        订单分配(1-14机器)
        下布记录 ——> (关联订单数据，填写下布数据)
        剖布 ——> (关联订单数据，填写剖布结果)
        产量报表 ——> (关联订单数据，填写产量数据)
    大圆机车间(H)
        产量报表 ——> (关联订单数据，填写产量数据)
    小圆机车间(X)
        产量报表 ——> (关联订单数据，填写产量数据)
    定型机车间
        蒸箱报表 ——> 只记录蒸箱数据
        定型机蒸布报表 ——> 只记录蒸布数据
        烘干机蒸布 ——> 只记录蒸布数据
        定型 ——> (关联订单数据，填写定型数据)
    剪毛机车间(1-4机器) ——> 只记录剪毛机数据
成品管理
    包装入库 ——> (关联订单数据，填写包装结果，打印条形码，扫描入库)
    入库审核 ——> (统计员审核)
    (订单)成品库存查询 ——> 按照订单分组
    成品调拨 ——> 订单对应的成品之间调拨，可以修改订单数量
退货管理
    退货登记 ——> 报废退货，非报废退货，新下订单 重新进入 "生产管理"
五金房管理
    
财务管理
    原料入库 ——> 记录原材料应付货款
    原料外发加工 ——> 记录原材料外发加工应付货款
    半成品外发加工 ——> 记录半成品外发加工应付货款
    成品出库应收 ——> 记录成品出库应收货款
    采购五金 ——> 记录应付货款
    
·










入库：
原料入库单：

明细




入库表单：
批号
名称
规格
类型
色号
数量
供应商
入库日期
备注
附件

批号自动生成按：年月-（数字顺序）的规则、名称、规格、数量、供应商、日期必填，其余可填。

---------------------------------------------------

审核表单：
支付方式+
单价+
总额(+)

---------------------------------------------------


供应商表单：

编号                  supplier_no
供应商名称            supplier_name
供应商法人            legal_people
供应商法人联系方式    legal_people_contact
供应商对接人          docking_people
供应商对接人联系方式  docking_people_contact
供应商开始供应日期    start_supply_date
备注                  remark
上传附件              attachment


*/


CREATE TABLE erp_rawmaterial_info
(
    id                  bigint          NOT NULL    auto_increment          COMMENT '编号',
    company_id          bigint          NOT NULL                            COMMENT '数据所属公司的ID,用于公司之间的数据隔离',
    org_id              bigint          NOT NULL                            COMMENT '数据直属机构的ID',
    create_by           bigint          NOT NULL                            COMMENT '创建者',
    create_date         datetime        NOT NULL                            COMMENT '创建时间',
    update_by           bigint          NOT NULL                            COMMENT '更新者',
    update_date         datetime        NOT NULL                            COMMENT '更新时间',
    remarks             varchar(255)                                        COMMENT '备注信息',
    del_flag            char(1)         NOT NULL    DEFAULT '1'             COMMENT '删除标记（1：正常；2：删除；3：审核）,以字典表sys_dict.dict_key=‘删除标记’为准',
    uuid                varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',
    
    rawmaterial_code    varchar(100)    NOT NULL    UNIQUE                  COMMENT '原料编码',
    name                varchar(100)    NOT NULL                            COMMENT '原料名称',
    specification       varchar(100)    NOT NULL                            COMMENT '原料规格',
    rawmaterial_type    varchar(100)                                        COMMENT '原料类型',
    color_no            varchar(100)                                        COMMENT '原料色号',
    PRIMARY KEY (id)
) COMMENT = '原料基础数据表';
/*------------------------------------------------------------------------------------------------------------------------

增加原料：验证“原料名称、原料规格、原料类型、原料色号”组合值不能重复，生成唯一的“原料编码”
更新原料：不能修改“原料编码”，验证“原料名称、原料规格、原料类型、原料色号”组合值不能重复
删除原料：原料如果被其他地方引用(比如：库存)，则不能删除

--------------------------------------------------------------------------------------------------------------------------*/


CREATE TABLE erp_supplier
(
    id                      bigint          NOT NULL    auto_increment          COMMENT '编号',
    company_id              bigint          NOT NULL                            COMMENT '数据所属公司的ID,用于公司之间的数据隔离',
    org_id                  bigint          NOT NULL                            COMMENT '数据直属机构的ID',
    create_by               bigint          NOT NULL                            COMMENT '创建者',
    create_date             datetime        NOT NULL                            COMMENT '创建时间',
    update_by               bigint          NOT NULL                            COMMENT '更新者',
    update_date             datetime        NOT NULL                            COMMENT '更新时间',
    remarks                 varchar(255)                                        COMMENT '备注信息',
    del_flag                char(1)         NOT NULL    DEFAULT '1'             COMMENT '删除标记（1：正常；2：删除；3：审核）,以字典表sys_dict.dict_key=‘删除标记’为准',
    uuid                    varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',
    
    supplier_no             varchar(100)    NOT NULL    UNIQUE                  COMMENT '供应商编号',
    supplier_name           varchar(255)    NOT NULL                            COMMENT '供应商名称',
    legal_people            varchar(100)                                        COMMENT '供应商法人',
    legal_people_contact    varchar(255)                                        COMMENT '供应商法人联系方式',
    docking_people          varchar(100)    NOT NULL                            COMMENT '供应商对接人',
    docking_people_contact  varchar(255)    NOT NULL                            COMMENT '供应商对接人联系方式',
    start_supply_date       datetime                                            COMMENT '供应商开始供应日期',
    PRIMARY KEY (id)
) COMMENT = '供应商信息表';
/*------------------------------------------------------------------------------------------------------------------------

增加供应商：供应商名称不能重复、供应商编号自动生成
更新供应商：供应商名称不能重复、不能修改供应商编号
删除供应商：被其他地方引用不能删除

--------------------------------------------------------------------------------------------------------------------------*/


CREATE TABLE erp_supplier_rawmaterial
(
    supplier_id             bigint          NOT NULL                            COMMENT '供应商ID',
    rawmaterial_id          bigint          NOT NULL                            COMMENT '原料基础数据ID',
    PRIMARY KEY (supplier_id, rawmaterial_id)
) COMMENT = '供应商所供应的原料表';
/*------------------------------------------------------------------------------------------------------------------------

供应商供应原料

--------------------------------------------------------------------------------------------------------------------------*/


CREATE TABLE erp_rawmaterial_warehouse
(
    id                  bigint          NOT NULL    auto_increment          COMMENT '编号',
    company_id          bigint          NOT NULL                            COMMENT '数据所属公司的ID,用于公司之间的数据隔离',
    org_id              bigint          NOT NULL                            COMMENT '数据直属机构的ID',
    create_by           bigint          NOT NULL                            COMMENT '创建者',
    create_date         datetime        NOT NULL                            COMMENT '创建时间',
    update_by           bigint          NOT NULL                            COMMENT '更新者',
    update_date         datetime        NOT NULL                            COMMENT '更新时间',
    remarks             varchar(2000)                                       COMMENT '备注信息',
    del_flag            char(1)         NOT NULL    DEFAULT '1'             COMMENT '删除标记（1：正常；2：删除；3：审核）,以字典表sys_dict.dict_key=‘删除标记’为准',
    uuid                varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',
    
    warehouse_no        varchar(100)    NOT NULL    UNIQUE                  COMMENT '原料入库申请单编号',
    rawmaterial_code    varchar(100)                                        COMMENT '原料编码',
    name                varchar(100)    NOT NULL                            COMMENT '原料名称',
    specification       varchar(100)    NOT NULL                            COMMENT '原料规格',
    rawmaterial_type    varchar(100)                                        COMMENT '原料类型',
    color_no            varchar(100)                                        COMMENT '原料色号',
    quantity            decimal(15,2)   NOT NULL                            COMMENT '入库数量',
    supplier_id         bigint          NOT NULL                            COMMENT '供应商ID',
    warehouse_date      datetime        NOT NULL                            COMMENT '入库日期',
    auditor_id          bigint          NOT NULL                            COMMENT '审核人ID（ID=-1 表示：已审核完成）',
    auditor_state       char(1)         NOT NULL    DEFAULT '1'             COMMENT '审核状态（1：等待审核；2：审核中-已经审核过但是没有完成；3：审核完成-通过；4：审核完成-驳回）',
    
    unit_price          decimal(15,2)   NOT NULL    DEFAULT '0'             COMMENT '单价',
    aggregate_amount    decimal(15,2)   NOT NULL    DEFAULT '0'             COMMENT '总额',
    is_invoice          char(1)         NOT NULL    DEFAULT '1'             COMMENT '是否开票（1：开票；2：不开票）',
    
    is_history          char(1)         NOT NULL    DEFAULT '1'             COMMENT '是否是历史记录（1：不是历史记录；2：是历史记录）',
    is_change           char(1)         NOT NULL    DEFAULT '1'             COMMENT '是否是变更后的数据（1：不是；2：是）',
    PRIMARY KEY (id)
) COMMENT = '原料入库申请单';
/*------------------------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------------------------*/



CREATE TABLE erp_rawmaterial_out
(
    id                  bigint          NOT NULL    auto_increment          COMMENT '编号',
    company_id          bigint          NOT NULL                            COMMENT '数据所属公司的ID,用于公司之间的数据隔离',
    org_id              bigint          NOT NULL                            COMMENT '数据直属机构的ID',
    create_by           bigint          NOT NULL                            COMMENT '创建者',
    create_date         datetime        NOT NULL                            COMMENT '创建时间',
    update_by           bigint          NOT NULL                            COMMENT '更新者',
    update_date         datetime        NOT NULL                            COMMENT '更新时间',
    remarks             varchar(2000)                                       COMMENT '备注信息',
    del_flag            char(1)         NOT NULL    DEFAULT '1'             COMMENT '删除标记（1：正常；2：删除；3：审核）,以字典表sys_dict.dict_key=‘删除标记’为准',
    uuid                varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',
    
    out_no              varchar(100)    NOT NULL    UNIQUE                  COMMENT '原料出库申请单编号',
    rawmaterial_code    varchar(100)                                        COMMENT '原料编码',
    name                varchar(100)    NOT NULL                            COMMENT '原料名称',
    specification       varchar(100)    NOT NULL                            COMMENT '原料规格',
    rawmaterial_type    varchar(100)                                        COMMENT '原料类型',
    color_no            varchar(100)                                        COMMENT '原料色号',
    quantity            decimal(15,2)   NOT NULL                            COMMENT '出库数量',
    out_date            datetime        NOT NULL                            COMMENT '出库日期',
    out_type            char(1)         NOT NULL                            COMMENT '出库类型（1：车间加工；2：外发加工）',
    order_no            varchar(100)                                        COMMENT '订单编号',
    is_first_order      char(1)         NOT NULL                            COMMENT '是否先下订单后原料出库（1：是；2：不是）',
    auditor_id          bigint          NOT NULL                            COMMENT '审核人ID（ID=-1 表示：已审核完成）',
    auditor_state       char(1)         NOT NULL    DEFAULT '1'             COMMENT '审核状态（1：等待审核；2：审核中-已经审核过但是没有完成；3：审核完成-通过；4：审核完成-驳回）',
    
    unit_price          decimal(15,2)   NOT NULL    DEFAULT '0'             COMMENT '单价',
    aggregate_amount    decimal(15,2)   NOT NULL    DEFAULT '0'             COMMENT '总额',
    is_invoice          char(1)         NOT NULL    DEFAULT '1'             COMMENT '是否开票（1：开票；2：不开票）',
    
    is_history          char(1)         NOT NULL    DEFAULT '1'             COMMENT '是否是历史记录（1：不是历史记录；2：是历史记录）',
    is_change           char(1)         NOT NULL    DEFAULT '1'             COMMENT '是否是变更后的数据（1：不是；2：是）',
    PRIMARY KEY (id)
) COMMENT = '原料出库申请单';
/*------------------------------------------------------------------------------------------------------------------------

原料出库申请单

--------------------------------------------------------------------------------------------------------------------------*/



CREATE TABLE erp_rawmaterial_return
(
    id                  bigint          NOT NULL    auto_increment          COMMENT '编号',
    company_id          bigint          NOT NULL                            COMMENT '数据所属公司的ID,用于公司之间的数据隔离',
    org_id              bigint          NOT NULL                            COMMENT '数据直属机构的ID',
    create_by           bigint          NOT NULL                            COMMENT '创建者',
    create_date         datetime        NOT NULL                            COMMENT '创建时间',
    update_by           bigint          NOT NULL                            COMMENT '更新者',
    update_date         datetime        NOT NULL                            COMMENT '更新时间',
    remarks             varchar(2000)                                       COMMENT '备注信息',
    del_flag            char(1)         NOT NULL    DEFAULT '1'             COMMENT '删除标记（1：正常；2：删除；3：审核）,以字典表sys_dict.dict_key=‘删除标记’为准',
    uuid                varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',
    
    out_id              bigint          NOT NULL                            COMMENT '原料出库申请单ID',
    rawmaterial_code    varchar(100)                                        COMMENT '原料编码',
    name                varchar(100)    NOT NULL                            COMMENT '原料名称',
    specification       varchar(100)    NOT NULL                            COMMENT '原料规格',
    rawmaterial_type    varchar(100)                                        COMMENT '原料类型',
    color_no            varchar(100)                                        COMMENT '原料色号',
    return_quantity     decimal(15,2)                                       COMMENT '返回数量',
    loss_quantity       decimal(15,2)                                       COMMENT '损耗数量',
    PRIMARY KEY (id)
) COMMENT = '原料外发加工返回原料信息';
/*------------------------------------------------------------------------------------------------------------------------

原料外发加工返回原料信息

--------------------------------------------------------------------------------------------------------------------------*/


CREATE TABLE erp_order
(
    id                      bigint          NOT NULL    auto_increment          COMMENT '编号',
    company_id              bigint          NOT NULL                            COMMENT '数据所属公司的ID,用于公司之间的数据隔离',
    org_id                  bigint          NOT NULL                            COMMENT '数据直属机构的ID',
    create_by               bigint          NOT NULL                            COMMENT '创建者',
    create_date             datetime        NOT NULL                            COMMENT '创建时间',
    update_by               bigint          NOT NULL                            COMMENT '更新者',
    update_date             datetime        NOT NULL                            COMMENT '更新时间',
    remarks                 varchar(2000)                                       COMMENT '备注信息',
    del_flag                char(1)         NOT NULL    DEFAULT '1'             COMMENT '删除标记（1：正常；2：删除；3：审核）,以字典表sys_dict.dict_key=‘删除标记’为准',
    uuid                    varchar(36)     NOT NULL                            COMMENT '数据全局标识UUID',
                            
    order_code              varchar(100)    NOT NULL                            COMMENT '订单号',
    order_no                varchar(100)    NOT NULL                            COMMENT '批号',
    order_type              char(1)         NOT NULL                            COMMENT '订单类型（1：A生产单；2：A生产样板单；3：X生产单；4：X生产样板单；5：H生产单；6：H生产样板单；）',
    order_date              datetime        NOT NULL                            COMMENT '登记日期',
    customer                varchar(100)                                        COMMENT '客户',
    name                    varchar(100)                                        COMMENT '名称',
    specification           varchar(100)                                        COMMENT '规格',
    order_quantity          decimal(15,2)                                       COMMENT '订单数量',
    cycling                 varchar(100)                                        COMMENT '循环',
    real_quantity           decimal(15,2)                                       COMMENT '实产数量',
    is_brush                char(1)                                             COMMENT '是否刷毛（1：刷毛；2：不刷毛）',
    delivery_date           datetime                                            COMMENT '交货日期',
    delivery_claim          varchar(255)                                        COMMENT '交货要求',
    tabulation_people       bigint                                              COMMENT '制表人',
    yarn_audit              varchar(100)                                        COMMENT '排纱审核',
    workshop_class          varchar(100)                                        COMMENT '车间班长',
    auditor                 varchar(100)                                        COMMENT '审核',
    has_contracts           char(1)                                             COMMENT '是否有合同（1：有合同；2：无合同）',   
    order_enrollment        bigint                                              COMMENT '订单登记',
    color_no                varchar(100)                                        COMMENT '色号',
    white_gauze             varchar(100)                                        COMMENT '白纱',
    colored_yarn            varchar(100)                                        COMMENT '色纱',
    yarn_type               varchar(100)                                        COMMENT '纱种',
    yarn_type_no            varchar(100)                                        COMMENT '原料纱种批号',
    is_first_order          char(1)                                             COMMENT '是否先下订单后原料出库（1：是；2：不是）',
    stereotypes_width       varchar(100)                                        COMMENT '定型幅宽',
    stereotypes_freeness    varchar(100)                                        COMMENT '定型浆度',
    director_audit          bigint                                              COMMENT '厂长审核', 
    business_audit          bigint                                              COMMENT '业务审核', 
    
    production              bigint                                              COMMENT '生产部（H）',
    material                varchar(100)                                        COMMENT '材料（H）',
    percentage              decimal(15,4)                                       COMMENT '百分比（H）',
    quantity                decimal(15,2)                                       COMMENT '数量/公斤（H）',
    
    auditor_id              bigint          NOT NULL                            COMMENT '审核人ID（ID=-1 表示：已审核完成）',
    auditor_state           char(1)         NOT NULL                            COMMENT '流程状态（1：业务登记；2：工艺登记；3：厂长审核；4：审核完成-通过；5：审核完成-驳回）',

    is_history              char(1)         NOT NULL    DEFAULT '1'             COMMENT '是否是历史记录（1：不是历史记录；2：是历史记录）',
    is_change               char(1)         NOT NULL    DEFAULT '1'             COMMENT '是否是变更后的数据（1：不是；2：是）',
    PRIMARY KEY (id)
) COMMENT = '系统订单表';

--------------------------------------------------------------------- A X
-----------------------业务登记
订单号 varchar(100) -
订单类型 char(1) -
登记日期 datetime -
客户 varchar(100) -
批号 varchar(100) -
名称 varchar(100) -
规格 varchar(100) -
订单数量 decimal(15,2) -
循环 varchar(100) -
实产数量 decimal(15,2) -
是否刷毛 char(1) -
交货日期 datetime -
交货要求 
备注 varchar(2000) -
制表 bigint -
排纱审核 varchar(100) -
车间班长 varchar(100) -
审核 varchar(100) -
是否有合同 char(1) -
订单登记 bigint -

后续流程

-----------------------工艺登记

色号 varchar(100) -
白纱 varchar(100) -
色纱 varchar(100) -
纱种 varchar(100) -
原料纱种批号 varchar(100) -
是否先订单后原料出库 char(1) -


-----------------------厂长审核
定型幅宽 varchar(100) -
定型浆度 varchar(100) -
厂长审核 bigint -


--------------------------------------------------------------------- H
-----------------------业务登记
订单号 varchar(100) -
订单类型 char(1) -
登记日期 datetime -
客户 varchar(100) -
批号 varchar(100) -
名称 varchar(100) -
规格 varchar(100) -
订单数量 decimal(15,2) -
实产数量 decimal(15,2) -
交货日期 datetime - 
备注 varchar(2000) -
制表 bigint -
后续流程 -


-----------------------工艺登记
定型幅宽 varchar(100) -
定型浆度 varchar(100) -

生产部 bigint -

色号 varchar(100) -
材料 varchar(100) -
原料规格 varchar(100) -
百分比 decimal(15,4) -
数量/公斤 decimal(15,2) -

-----------------------厂长审核







1.KMS全文搜索新功能开发
    搜索增加集团分级授权支持
    内新增elasticsearch初始化mapping设置
    新增知识智能推荐功能
    高级搜索时新增完全匹配选项
    搜索结果列表增加快照功能
    
2.KMS全文搜索的优化以及缺陷的修改
    索引任务异常后锁无法释放，导致索引任务无法进行
    索引任务解析zip文件里的文档时，会解压zip文件到临时文件，索引完成之后不会清除临时文件，导致Tomcat的tmp目录里有大量临时文件占用硬盘空间
    搜索及结果显示页面采用新ui，提升用户体验
    全系统搜索页面搜索异常时弹出错误提示
    索引员工信息重复或者不全

 

3.处理项目服务单
    各种搜索问题：搜索不到结果、搜索结果不准确、搜索结果权限过滤
    由于索引引起的宕机问题




































