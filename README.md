### oschina上的CleverFrame的项目已经废弃，我已经在github上重构此项目，地址 https://github.com/Lzw2016/cleverframe.git

## 内置功能

1.    数据库脚本管理：数据库SQL查询脚本存储在数据库中，在线修改立即生效，支持SQL、HQL两种脚本，并且脚本支持Freemarker模版脚本动态拼接。

2.    字典管理：对系统中经常使用的一些较为固定的数据进行维护，如：是否、男女、类别、级别等。

3.    机构管理：公司(或集团)的组织机构管理，支持集团下面有多级分公司的情况。

4.    用户管理：用户是系统操作者，该功能主要完成系统用户配置。

5.    角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。

6.    菜单管理：配置系统菜单，操作权限，按钮权限标识等。

7.    权限管理：系统权限的管理，如菜单权限，UI权限(按钮权限，数据表格查看权限)。

8.    多级字典管理：配置属性结构的多级字典数据，如：国家、省市、地市、区县数据。

9.    用户角色权限管理：管理用户的角色，管理角色的权限。

10.    连接池监视：使用druid数据库连接池，监视当期系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

11.    上传文件管理：文件上传使用了MD5文件签名，对于已经上传过的文件支持文件秒传。

12.    工作流引擎：使用了轻量级工作流引擎Snaker,实现业务工单流转、在线流程设计器。



## 技术选型

1、后端

* 核心框架：Spring Framework 4.0

* 安全框架：Apache Shiro 1.2

* 视图框架：Spring MVC 4.0

* 服务端验证：Hibernate Validator 5.1

* 工作流引擎：Snaker

* 任务调度：Spring Task 4.0

* 持久层框架：Hibernate 4.3.11

* 数据库连接池：Alibaba Druid 1.0

* 缓存框架：Ehcache 2.6

* 日志管理：SLF4J、Log4j

* 工具类：Apache Commons、Jackson、Xstream、POI等



2、前端

* JS框架：jQuery、jQuery EasyUI

* CSS框架： Bootstrap

* 树结构控件：zTree



3、平台

* 服务器中间件：在Java EE 5规范（Servlet 2.5、JSP 2.1）下开发，支持应用服务器中间件有Tomcat 6、Jboss 7、WebLogic 10、WebSphere 8。

* 数据库支持：目前仅提供MySql和Oracle数据库的支持，但不限于数据库，平台留有其它数据库支持接口，可方便更改为其它数据库，如：SqlServer 2008、MySql 5.5、H2等

* 开发环境：Java EE、Eclipse、Maven、Git



## 系统设计

1. 开发语言：系统采用Java语言开发，具有卓越的通用性、高效性、平台移植性和安全性。

2. 分层设计：（数据库层，数据访问层，业务逻辑层，展示层）层次清楚，低耦合，各层必须通过接口才能接入并进行参数校验（如：在展示层不可直接操作数据库），保证数据操作的安全。

3. 双重验证：用户表单提交双验证：包括服务器端验证及客户端验证，防止用户通过浏览器恶意修改（如不可写文本域、隐藏变量篡改、上传非法文件等），跳过客户端验证操作数据库。

4. 安全编码：用户表单提交所有数据，在服务器端都进行安全编码，防止用户提交非法脚本及SQL注入获取敏感数据等，确保数据安全。

5. 密码加密：登录用户密码进行SHA1散列加密，此加密方法是不可逆的。保证密文泄露后的安全问题。

6. 强制访问：系统对所有管理端链接都进行用户身份权限验证(使用Shiro)，防止用户直接填写url进行访问。



## 系统截图演示
![数据库脚本管理](http://git.oschina.net/uploads/images/2016/0217/221435_ae3d7977_339280.png "数据库脚本管理页面")
![组织机构管理](http://git.oschina.net/uploads/images/2016/0217/221504_68260918_339280.png "组织机构管理页面")
![用户管理](http://git.oschina.net/uploads/images/2016/0217/221556_57a40895_339280.png "用户管理页面")
![菜单管理](http://git.oschina.net/uploads/images/2016/0217/221625_6f2f1051_339280.png "菜单管理页面")
![用户角色权限管理](http://git.oschina.net/uploads/images/2016/0217/221703_7f50846f_339280.png "用户角色权限管理页面")
![工作流引擎](http://git.oschina.net/uploads/images/2016/0217/221725_5906d340_339280.png "工作流引擎页面")
![ERP模块-1](http://git.oschina.net/uploads/images/2016/0217/221839_86a65017_339280.png "ERP模块-1页面")
![ERP模块-2](http://git.oschina.net/uploads/images/2016/0217/222031_74b4faad_339280.png "ERP模块-2")
![ERP模块-3](http://git.oschina.net/uploads/images/2016/0217/222200_5b8b2efd_339280.png "ERP模块-3")