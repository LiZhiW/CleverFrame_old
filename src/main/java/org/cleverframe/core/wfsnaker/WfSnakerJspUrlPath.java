package org.cleverframe.core.wfsnaker;

/**
 * 当前wfsnaker模块对应的JSP文件URL路径<br>
 * 1.注意：此类只保存JSP文件的URL<br>
 * @author LiZW
 * @version 2016年1月3日 下午1:06:25
 */
public class WfSnakerJspUrlPath
{
    /** Snaker工作流程WEB设计界面 */
    public static final String ProcessDesigner = "core/wfsnaker/ProcessDesigner";

    /** 所有流程列表 */
    public static final String ProcessList = "core/wfsnaker/ProcessList";

    /** 根据上传文件部署流程 */
    public static final String ProcessDeploy = "core/wfsnaker/ProcessDeploy";

    /** 编辑流程定义所有字段 */
    public static final String ProcessEdit = "core/wfsnaker/ProcessEdit";

    /** 显示流程实例运行状态和审核历史 */
    public static final String ProcessView = "core/wfsnaker/ProcessView";

    /** 流程实例状态图 */
    public static final String Diagram = "core/wfsnaker/Diagram";

    /** 流程实例管理 */
    public static final String OrderList = "core/wfsnaker/OrderList";
    
    /** 活动的任务:待办任务、协办任务、抄送任务... */
    public static final String ActiveTask = "core/wfsnaker/ActiveTask";
    
    /** 所有代办任务:待办任务、协办任务 */
    public static final String ActiveTaskList = "core/wfsnaker/ActiveTaskList";
    
    /** 所有抄送任务 */
    public static final String ActiveCCList = "core/wfsnaker/ActiveCCList";
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
