package org.cleverframe.core.wfsnaker.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.core.wfsnaker.WfSnakerBeanNames;
import org.cleverframe.core.wfsnaker.engine.SnakerEngineFacets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 流程处理Controller<br>
 * @author LiZW
 * @version 2016年1月4日 下午10:09:13
 */
@Controller
@RequestMapping("/${mvcPath}/wfsnaker")
@Transactional(readOnly = true)
public class FlowController extends BaseController
{
    /** 日志对象 */
    // private final static Logger logger = LoggerFactory.getLogger(FlowController.class);

    @Autowired
    @Qualifier(WfSnakerBeanNames.SnakerEngineFacets)
    private SnakerEngineFacets facets;
    
    /**
     * 通用的流程处理方法<br>
     * 1.存储流程附属变量<br>
     * 2.执行流程Task或启动流程实例Order<br>
     */
    @RequestMapping("/process")
    public ModelAndView process(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView();

        return mav;
    }
}
