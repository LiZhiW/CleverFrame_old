package org.cleverframe.core.wfsnaker.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.core.wfsnaker.WfSnakerBeanNames;
import org.cleverframe.core.wfsnaker.WfSnakerJspUrlPath;
import org.cleverframe.core.wfsnaker.engine.SnakerEngineFacets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 流程任务Controller<br>
 * @author LiZW
 * @version 2016年1月4日 下午9:58:45
 */
@Controller
@RequestMapping("/${mvcPath}/wfsnaker")
@Transactional(readOnly = true)
public class TaskController extends BaseController
{
    /** 日志对象 */
    // private final static Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    @Qualifier(WfSnakerBeanNames.SnakerEngineFacets)
    private SnakerEngineFacets facets;
    
    /**
     * 活动的任务:待办任务、协办任务、抄送任务... <br>
     */
    @RequestMapping("/getActiveTaskJsp")
    public ModelAndView getActiveTaskJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(WfSnakerJspUrlPath.ActiveTask);
        return mav;
    }
    
    /**
     * 所有代办任务:待办任务、协办任务<br>
     */
    @RequestMapping("/getActiveTaskListJsp")
    public ModelAndView getActiveTaskListJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(WfSnakerJspUrlPath.ActiveTaskList);
        return mav;
    }
    
    /**
     * 所有抄送任务 <br>
     */
    @RequestMapping("/getActiveCCListJsp")
    public ModelAndView getActiveCCListJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(WfSnakerJspUrlPath.ActiveCCList);
        return mav;
    }
    
    // 动态添加参与者
}
