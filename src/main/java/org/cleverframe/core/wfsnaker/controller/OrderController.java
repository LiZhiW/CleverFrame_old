package org.cleverframe.core.wfsnaker.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.core.wfsnaker.WfSnakerBeanNames;
import org.cleverframe.core.wfsnaker.WfSnakerJspUrlPath;
import org.cleverframe.core.wfsnaker.engine.SnakerEngineFacets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 流程实例Controller<br>
 * @author LiZW
 * @version 2016年1月4日 下午9:56:29
 */
@Controller
@RequestMapping("/${mvcPath}/wfsnaker")
@Transactional(readOnly = true)
public class OrderController extends BaseController
{

    /** 日志对象 */
    // private final static Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    @Qualifier(WfSnakerBeanNames.SnakerEngineFacets)
    private SnakerEngineFacets facets;

    /**
     * 返回到流程实例管理页面<br>
     */
    @RequestMapping("/getOrderListJsp")
    public ModelAndView getOrderListJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(WfSnakerJspUrlPath.OrderList);
        return mav;
    }

}
