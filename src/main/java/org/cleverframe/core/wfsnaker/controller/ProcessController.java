package org.cleverframe.core.wfsnaker.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.core.wfsnaker.WfSnakerBeanNames;
import org.cleverframe.core.wfsnaker.WfSnakerJspUrlPath;
import org.cleverframe.core.wfsnaker.engine.SnakerEngineFacets;
import org.cleverframe.core.wfsnaker.engine.SnakerHelper;
import org.cleverframe.core.wfsnaker.utils.ConvertModel;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snaker.engine.access.Page;
import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.Process;
import org.snaker.engine.helper.AssertHelper;
import org.snaker.engine.helper.StreamHelper;
import org.snaker.engine.helper.StringHelper;
import org.snaker.engine.model.ProcessModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 * 流程定义Controller<br>
 * @author LiZW
 * @version 2016年1月3日 下午1:04:50
 */
@Controller
@RequestMapping("/${mvcPath}/wfsnaker")
@Transactional(readOnly = true)
public class ProcessController extends BaseController
{
    /** 日志对象 */
    private final static Logger logger = LoggerFactory.getLogger(ProcessController.class);
    
    @Autowired
    @Qualifier(WfSnakerBeanNames.SnakerEngineFacets)
    private SnakerEngineFacets facets;

    /**
     * 初始化流程定义<br>
     * @return
     */
    @RequestMapping("/processInit")
    @Transactional(readOnly = false)
    public AjaxMessage processInit(HttpServletRequest request, HttpServletResponse response)
    {
        AjaxMessage message = new AjaxMessage(true);
        facets.initFlows();
        message.setMessage("系统流程初始化成功");
        return message;
    }
    
    /**
     * 新建(或编辑)流程定义[web流程设计器]<br>
     * @param processId 如果编辑流程，需要传入流程ID
     */
    @RequestMapping("/getProcessDesignerJsp")
    public ModelAndView getProcessDesignerJsp(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value = "processId", required = false, defaultValue = "") String processId)
    {
        ModelAndView mav = new ModelAndView(WfSnakerJspUrlPath.ProcessDesigner);
        if (StringUtils.isNotEmpty(processId))
        {
            Process process = facets.getEngine().process().getProcessById(processId);
            AssertHelper.notNull(process);
            ProcessModel processModel = process.getModel();
            if (processModel != null)
            {
                String json = SnakerHelper.getModelJson(processModel);
                request.setAttribute("process", json);
            }
            request.setAttribute("processId", processId);
        }
        return mav;
    }
    
    /**
     * 返回到流程列表页面<br>
     */
    @RequestMapping("/getProcessListJsp")
    public ModelAndView getProcessListJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(WfSnakerJspUrlPath.ProcessList);
        return mav;
    }
    
    /**
     * 根据上传文件部署流程页面 <br>
     */
    @RequestMapping("/getProcessDeployJsp")
    public ModelAndView getProcessDeployJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(WfSnakerJspUrlPath.ProcessDeploy);
        return mav;
    }
    
    /**
     * 显示流程实例运行状态和审核历史<br>
     */
    @RequestMapping("/getProcessViewJsp")
    public ModelAndView getProcessViewJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(WfSnakerJspUrlPath.ProcessView);
        return mav;
    }
    
    /**
     * 流程实例状态图 <br>
     */
    @RequestMapping("/getDiagramJsp")
    public ModelAndView getDiagramJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(WfSnakerJspUrlPath.Diagram);
        return mav;
    }
    
    /**
     * 编辑流程定义所有字段<br>
     * */
    @RequestMapping("/getProcessEditJsp")
    public ModelAndView getProcessEditJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(WfSnakerJspUrlPath.ProcessEdit);
        return mav;
    }
    
    /**
     * 流程定义查询列表
     * @param displayName 流程显示名称
     */
    @ResponseBody
    @RequestMapping("/findProcessByPage")
    public DataGridJson<Map<String,Object>> findProcessByPage(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "displayName", required = false, defaultValue = "") String displayName)
    {
        DataGridJson<Map<String, Object>> json = new DataGridJson<Map<String, Object>>();
        QueryFilter filter = new QueryFilter();
        if (StringHelper.isNotEmpty(displayName))
        {
            filter.setDisplayName(displayName);
        }
        String pageSize = request.getParameter("rows");
        String pageNo = request.getParameter("page");
        Page<Process> page = new Page<Process>(NumberUtils.toInt(pageSize, 10));
        page.setPageNo(NumberUtils.toInt(pageNo, 1));
        List<Process> processList = facets.getEngine().process().getProcesss(page, filter);
        for (Process process : processList)
        {
            Map<String, Object> row = ConvertModel.processToMap(process);
            json.addRow(row);
        }
        json.setTotal(page.getTotalCount());
        return json;
    }

    /**
     * 根据流程ID 获取流程定义数据<br>
     * */
    @ResponseBody
    @RequestMapping("/getProcess/{id}")
    public AjaxMessage getProcess(HttpServletRequest request, HttpServletResponse response, @PathVariable(value = "id") String id)
    {
        AjaxMessage message = new AjaxMessage(true, "成功获取流程定义数据");
        Process process = facets.getEngine().process().getProcessById(id);
        if (process == null)
        {
            message.setMessage("获取流程数据失败，流程不存在");
            message.setSuccess(false);
            return message;
        }
        Map<String, Object> data = ConvertModel.processToMap(process);
        message.setObject(data);
        return message;
    }
    
    /**
     * 根据流程定义ID，卸载流程定义<br>
     */
    @ResponseBody
    @RequestMapping("processUndeploy/{id}")
    @Transactional(readOnly = false)
    public AjaxMessage processUndeploy(HttpServletRequest request, HttpServletResponse response, @PathVariable(value = "id") String id)
    {
        AjaxMessage message = new AjaxMessage(true, "流程卸载成功");
        facets.getEngine().process().undeploy(id);
        return message;
    }
    
    /**
     * 部署(或重新部署)流程定义[web流程设计器]<br>
     * @param model 流程定义的XML内容
     * @param id 流程定义的ID，如果不为空重新部署流程
     */
    @ResponseBody
    @RequestMapping(value = "deployXml", method = RequestMethod.POST)
    @Transactional(readOnly = false)
    public AjaxMessage processDeploy(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value = "model", required = true) String model, 
            @RequestParam(value = "processId", required = false, defaultValue = "") String processId)
    {
        AjaxMessage message = new AjaxMessage(false);
        InputStream input = null;
        try
        {
            String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" + SnakerHelper.convertXml(model);
            input = StreamHelper.getStreamFromString(xml);
            if (StringUtils.isNotEmpty(processId))
            {
                facets.getEngine().process().redeploy(processId, input);
            }
            else
            {
                facets.getEngine().process().deploy(input);
            }
        }
        catch (Exception e)
        {
            logger.error("部署流程错误", e);
            message.setMessage("部署流程失败");
            return message;
        }
        finally
        {
            if (input != null)
            {
                try
                {
                    input.close();
                }
                catch (IOException e)
                {
                    logger.error("关闭输入流异常", e);
                }
            }
        }
        message.setMessage("部署流程成功");
        message.setSuccess(true);
        return message;
    }
    
    /**
     * 上传文件部署流程<br>
     * @param snakerFile 上传的流程部署文件
     * @param id 流程ID
     */
    @ResponseBody
    @RequestMapping(value = "processDeploy", method = RequestMethod.POST)
    @Transactional(readOnly = false)
    public AjaxMessage processDeploy(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "snakerFile", required = true) MultipartFile snakerFile,
            @RequestParam(value = "id", required = true) String id)
    {
        AjaxMessage message = new AjaxMessage(true);
        InputStream input = null;
        try
        {
            input = snakerFile.getInputStream();
            if (StringUtils.isNotEmpty(id))
            {
                facets.getEngine().process().redeploy(id, input);
            }
            else
            {
                facets.getEngine().process().deploy(input);
            }
        }
        catch (IOException e)
        {
            logger.error("部署流程失败", e);
            message.setMessage("部署流程失败");
            message.setSuccess(false);
            return message;
        }
        finally
        {
            if (input != null)
            {
                try
                {
                    input.close();
                }
                catch (IOException e)
                {
                    logger.error("关闭输入流失败", e);
                }
            }
        }
        return message;
    }
    

}
