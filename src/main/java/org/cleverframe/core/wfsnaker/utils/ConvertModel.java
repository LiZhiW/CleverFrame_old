package org.cleverframe.core.wfsnaker.utils;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snaker.engine.entity.Process;
import org.snaker.engine.helper.StringHelper;

/**
 * 由于snaker的实体类不能直接序列化所以需要使用此工具类转换成Map<br>
 * @author LiZW
 * @version 2016年1月5日 上午9:50:26
 */
public class ConvertModel
{
    /** 日志对象 */
    private final static Logger logger = LoggerFactory.getLogger(ConvertModel.class);

    /**
     * 把流程定义转换成Map<br>
     */
    public static Map<String, Object> processToMap(Process process)
    {
        Map<String, Object> map = new HashMap<String, Object>();
        if (process == null)
        {
            return map;
        }
        map.put("id", process.getId());
        map.put("name", process.getName());
        map.put("displayName", process.getDisplayName());
        map.put("type", process.getType());
        map.put("instanceUrl", process.getInstanceUrl());
        map.put("state", process.getState());
        if (process.getContent() != null)
        {
            String xml = null;
            try
            {
                xml = StringHelper.textXML(new String(process.getDBContent(), "UTF-8"));
            }
            catch (Exception e)
            {
                logger.error("读取流程定义的XML数据失败", e);
            }
            map.put("content", xml);
        }
        map.put("version", process.getVersion());
        map.put("createTime", process.getCreateTime());
        map.put("creator", process.getCreator());
        return map;
    }

}
