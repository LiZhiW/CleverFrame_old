package org.cleverframe.core.doc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.core.doc.DocJspUrlPath;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/${mvcPath}/doc")
@Transactional(readOnly = true)
public class DocController extends BaseController
{
    // private final static Logger logger = LoggerFactory.getLogger(DocController.class);

    /**
     * 系统功能页面列表页面 
     */
    @RequestMapping("/functionList")
    public ModelAndView getFunctionListJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(DocJspUrlPath.FunctionList);
        return mav;
    }

    /**
     * 系统功能页面列表页面 
     */
    @RequestMapping("/includeDemo")
    public ModelAndView getIncludeDemoJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(DocJspUrlPath.IncludeDemo);
        return mav;
    }
}
