package org.cleverframe.core.doc;

/**
 * 当前doc模块对应的JSP文件URL路径<br>
 * 1.注意：此类只保存JSP文件的URL<br>
 * 
 * @author LiZW
 * @version 2015年11月28日 下午3:07:10
 */
public class DocJspUrlPath
{
    /** 系统功能页面列表页面 */
    public static final String FunctionList = "core/doc/FunctionList";
    
    /** 系统引用JS、CSS等文件示例页面 */
    public static final String IncludeDemo = "core/doc/IncludeDemo";
}
