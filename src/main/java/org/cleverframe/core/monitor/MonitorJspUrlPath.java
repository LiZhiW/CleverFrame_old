package org.cleverframe.core.monitor;

/**
 * 当前monitor模块对应的JSP文件URL路径<br>
 * 1.注意：此类只保存JSP文件的URL<br>
 * @author LiZW
 * @version 2015年9月25日 下午11:29:19
 */
public class MonitorJspUrlPath
{
    /** 系统监控主页 */
    public static final String MonitorMain = "core/monitor/MonitorMain";
    
    /** Application属性范围值监控页面 */
    public static final String ApplicationAttribute = "core/monitor/ApplicationAttribute";
    
    /** Session属性范围值监控页面 */
    public static final String SessionAttribute = "core/monitor/SessionAttribute";
    
    /** 系统配置信息监控页面 */
    public static final String ServerProperties = "core/monitor/ServerProperties";
    
    /** Spring容器Bean值监控页面 */
    public static final String SpringBeans = "core/monitor/SpringBeans";
    
    /** FastDFS集群监控页面 */
    public static final String FastDFSMonitor = "core/monitor/FastDFSMonitor";
    
    /** Redis图形监控页面 */
    public static final String RedisChartsMonitor = "core/monitor/RedisChartsMonitor";
    
    /** Redis Info监控页面 */
    public static final String RedisInfoMonitor = "core/monitor/RedisInfoMonitor";
    
    /** Redis Keys监控页面 */
    public static final String RedisKeysMonitor = "core/monitor/RedisKeysMonitor";
    
    /** Redis Conf监控页面 */
    public static final String RedisConfMonitor = "core/monitor/RedisConfMonitor";
    
    /** Memcached Stats监控页面 */
    public static final String MemcachedStatsMonitor = "core/monitor/MemcachedStatsMonitor";
    
    /** Memcached Stats监控页面 */
    public static final String MemcachedChartsMonitor = "core/monitor/MemcachedChartsMonitor";
    
}
