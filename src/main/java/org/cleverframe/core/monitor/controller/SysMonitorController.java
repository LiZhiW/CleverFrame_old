package org.cleverframe.core.monitor.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.core.monitor.MonitorBeanNames;
import org.cleverframe.core.monitor.MonitorJspUrlPath;
import org.cleverframe.core.monitor.service.SysMonitorService;
import org.cleverframe.core.monitor.vo.ServerAttribute;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 系统基础监控Controller<br>
 * @author LiZW
 * @version 2015年9月26日 上午12:04:07
 */
@Controller
@RequestMapping("/${mvcPath}/monitor/sys")
public class SysMonitorController extends BaseController
{
    @Autowired
    @Qualifier(MonitorBeanNames.SysMonitorService)
    private SysMonitorService sysMonitorService;

    /** 返回Application属性范围值监控页面 */
    @RequestMapping("/applicationAttribute")
    public ModelAndView getApplicationAttributeJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(MonitorJspUrlPath.ApplicationAttribute);
        return mav;
    }

    /** 返回Session属性范围值监控页面 */
    @RequestMapping("/sessionAttribute")
    public ModelAndView getSessionAttributeJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(MonitorJspUrlPath.SessionAttribute);
        return mav;
    }

    /** 返回系统配置信息监控页面 */
    @RequestMapping("/serverProperties")
    public ModelAndView getServerPropertiesJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(MonitorJspUrlPath.ServerProperties);
        return mav;
    }

    /**
     * 返回所有 session属性范围值或application属性范围里的值<br>
     * */
    @ResponseBody
    @RequestMapping(value = "/getAttribute")
    public DataGridJson<ServerAttribute> getAttributes(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value = "scope", required = true) String scope)
    {
        DataGridJson<ServerAttribute> json = new DataGridJson<ServerAttribute>();
        List<ServerAttribute> attributeList = null;
        if ("session".equals(scope))
        {
            attributeList = sysMonitorService.getSessionAttribute(request);
        }
        else if ("application".equals(scope))
        {
            attributeList = sysMonitorService.getApplicationAttribute(request);
        }
        json.setRows(attributeList);
        return json;
    }

    /**
     * 移除 session属性范围值或application属性范围里的值<br>
     * */
    @RequestMapping(value = "/removeAttribute")
    public ModelAndView removeAttribute(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value = "scope", required = true) String scope,
            @RequestParam(value = "name", required = true) String name)
    {
        ModelAndView mav = new ModelAndView("mappingJackson2JsonView");
        Map<String, Object> map = null;
        if ("session".equals(scope))
        {
            map = this.sysMonitorService.removeSessionAttribute(request, name);
        }
        else if ("application".equals(scope))
        {
            map = this.sysMonitorService.removeApplicationAttribute(request, name);
        }
        else
        {
            map = new HashMap<String, Object>();
            map.put("success", false);
            map.put("message", "scope参数错误，scope参数值只能选择：session、application");
        }
        mav.addAllObjects(map);
        return mav;
    }

    /**
     * 获取系统所有配置信息<br>
     * */
    @ResponseBody
    @RequestMapping(value = "/getProperties")
    public DataGridJson<ServerAttribute> getProperties(HttpServletRequest request, HttpServletResponse response)
    {
        DataGridJson<ServerAttribute> json = new DataGridJson<ServerAttribute>();
        List<ServerAttribute> propertiesList = this.sysMonitorService.getProperties();
        json.setRows(propertiesList);
        return json;
    }
}
