package org.cleverframe.core.monitor.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.core.monitor.MonitorBeanNames;
import org.cleverframe.core.monitor.MonitorJspUrlPath;
import org.cleverframe.core.monitor.service.MonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 服务端监控Controller<br>
 * @author LiZW
 * @version 2015年5月25日 下午5:02:36
 */
@Controller
@RequestMapping("/${mvcPath}/monitor")
public class MonitorController extends BaseController
{
    @Autowired
    @Qualifier(MonitorBeanNames.MonitorService)
    private MonitorService monitorService;

    /** 返回系统监控主页 */
    @RequestMapping("/monitorMain")
    public ModelAndView getMonitorMainJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(MonitorJspUrlPath.MonitorMain);
        return mav;
    }
}
