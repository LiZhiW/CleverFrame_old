package org.cleverframe.core.monitor.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.core.monitor.MonitorBeanNames;
import org.cleverframe.core.monitor.MonitorJspUrlPath;
import org.cleverframe.core.monitor.service.SpringMonitorService;
import org.cleverframe.core.monitor.vo.ServerAttribute;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Spring监控Controller
 * @author LiZW
 * @version 2015年9月26日 上午12:35:56
 */
@Controller
@RequestMapping("/${mvcPath}/monitor/spring")
public class SpringMonitorController extends BaseController
{
    @Autowired
    @Qualifier(MonitorBeanNames.SpringMonitorService)
    private SpringMonitorService springMonitorService;
    
    /** 返回Spring容器Bean值监控页面 */
    @RequestMapping("/springBeans")
    public ModelAndView getApplicationAttributeJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(MonitorJspUrlPath.SpringBeans);
        return mav;
    }
    
    /**
     * 返回所有Spring容器中的Bean<br>
     * */
    @ResponseBody
    @RequestMapping(value = "/getSpringBean")
    public DataGridJson<ServerAttribute> getBeans(HttpServletRequest request, HttpServletResponse response)
    {
        DataGridJson<ServerAttribute> json = new DataGridJson<ServerAttribute>();
        List<ServerAttribute> beanList = this.springMonitorService.getSpringBeans();
        json.setRows(beanList);
        return json;
    }
}
