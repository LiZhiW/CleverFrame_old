package org.cleverframe.core.monitor.vo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Servlet容器属性值<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月1日 下午8:54:09
 */
@JsonInclude(Include.NON_NULL)
public class ServerAttribute implements Serializable
{
	private static final long serialVersionUID = 1L;
	/** 属性名称 */
	private String name;
	/** 属性类型 */
	private String type;
	/** 属性值(Json字符串) */
	private String value;

	public ServerAttribute()
	{

	}

	/**
	 * @param name 属性名称
	 * @param type 属性类型
	 * @param value 属性值(Json字符串)
	 * */
	public ServerAttribute(String name, String type, String value)
	{
		this.name = name;
		this.type = type;
		this.value = value;
	}

	/*--------------------------------------------------------------
	 * 			getter、setter
	 * -------------------------------------------------------------*/
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}
}
