package org.cleverframe.core.monitor.service;

import org.cleverframe.common.service.BaseService;
import org.cleverframe.core.monitor.MonitorBeanNames;
import org.springframework.stereotype.Service;

/**
 * 系统监控Service<br>
 * 
 * @author LiZhiWei
 * @version 2015年5月30日 上午11:41:57
 */
@Service(MonitorBeanNames.MonitorService)
public class MonitorService extends BaseService
{

}
