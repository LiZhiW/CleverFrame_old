package org.cleverframe.core.monitor.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.cleverframe.common.config.GlobalConfig;
import org.cleverframe.common.mapper.JsonMapper;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.core.monitor.MonitorBeanNames;
import org.cleverframe.core.monitor.vo.ServerAttribute;
import org.springframework.stereotype.Service;

/**
 * 系统基础监控Service<br>
 * @author LiZW
 * @version 2015年9月26日 下午1:12:48
 */
@Service(MonitorBeanNames.SysMonitorService)
public class SysMonitorService extends BaseService
{
    /**
     * 获取Session属性范围值<br>
     * */
    public List<ServerAttribute> getSessionAttribute(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        List<ServerAttribute> attributeList = new ArrayList<ServerAttribute>();
        Enumeration<String> attributes = session.getAttributeNames();
        while (attributes.hasMoreElements())
        {
            String name = attributes.nextElement();
            Object object = session.getAttribute(name);
            String type = object.getClass().getName();
            String value = null;
            if (object instanceof Serializable)
            {
                value = JsonMapper.nonEmptyMapper().toJson(object);
            }
            attributeList.add(new ServerAttribute(name, type, value));
        }
        return attributeList;
    }

    /**
     * 获取application属性范围值<br>
     * */
    public List<ServerAttribute> getApplicationAttribute(HttpServletRequest request)
    {
        ServletContext context = request.getServletContext();
        List<ServerAttribute> attributeList = new ArrayList<ServerAttribute>();
        Enumeration<String> attributes = context.getAttributeNames();
        while (attributes.hasMoreElements())
        {
            String name = attributes.nextElement();
            Object object = context.getAttribute(name);
            String type = object.getClass().getName();
            String value = null;
            if (object instanceof Serializable)
            {
                value = JsonMapper.nonEmptyMapper().toJson(object);
            }
            attributeList.add(new ServerAttribute(name, type, value));
        }
        return attributeList;
    }

    /**
     * 移除Session中的属性<br>
     * */
    public Map<String, Object> removeSessionAttribute(HttpServletRequest request, String name)
    {
        Map<String, Object> map = new HashMap<String, Object>();
        HttpSession session = request.getSession();
        session.removeAttribute(name);
        map.put("success", true);
        map.put("message", "已从Session中移除属性：" + name);
        return map;
    }

    /**
     * 移除Application中的属性<br>
     * */
    public Map<String, Object> removeApplicationAttribute(HttpServletRequest request, String name)
    {
        Map<String, Object> map = new HashMap<String, Object>();
        ServletContext context = request.getServletContext();
        context.removeAttribute(name);
        map.put("success", true);
        map.put("message", "已从Application中移除属性：" + name);
        return map;
    }

    /**
     * 得到系统配置信息<br>
     * */
    public List<ServerAttribute> getProperties()
    {
        List<ServerAttribute> list = new ArrayList<ServerAttribute>();
        Map<String, String> properties = GlobalConfig.getAllConfig();
        Set<String> names = properties.keySet();
        for (String name : names)
        {
            String value = properties.get(name);
            list.add(new ServerAttribute(name, String.class.toString(), value));
        }
        return list;
    }
}
