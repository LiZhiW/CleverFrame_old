package org.cleverframe.core.monitor.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.cleverframe.common.mapper.JsonMapper;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.common.spring.SpringContextHolder;
import org.cleverframe.core.monitor.MonitorBeanNames;
import org.cleverframe.core.monitor.vo.ServerAttribute;
import org.springframework.stereotype.Service;

/**
 * Spring监控Service<br>
 * @author LiZW
 * @version 2015年9月26日 下午1:18:11
 */
@Service(MonitorBeanNames.SpringMonitorService)
public class SpringMonitorService extends BaseService
{
    /**
     * 返回Spring容器中的所有Bean<br>
     * */
    public List<ServerAttribute> getSpringBeans()
    {
        // TODO Spring Bean 应有特定的vo
        List<ServerAttribute> beans = new ArrayList<ServerAttribute>();
        String[] beanNames = SpringContextHolder.getApplicationContext().getBeanDefinitionNames();
        for (String name : beanNames)
        {
            Object object = SpringContextHolder.getBean(name);
            if (object == null)
            {
                beans.add(new ServerAttribute(name, "", ""));
                continue;
            }
            String type = object.getClass().getName();
            String value = null;
            if (object instanceof Serializable)
            {
                value = JsonMapper.nonEmptyMapper().toJson(object);
            }
            beans.add(new ServerAttribute(name, type, value));
        }
        return beans;
    }
}
