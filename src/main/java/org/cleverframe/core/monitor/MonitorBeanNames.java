package org.cleverframe.core.monitor;

/**
 * 定义当前monitor模块定义的Spring Bean名称<br>
 * @author LiZW
 * @version 2015年9月25日 下午11:29:15
 */
public class MonitorBeanNames
{
    // -------------------------------------------------------------------------------------------//
    // Dao
    // -------------------------------------------------------------------------------------------//
    public static final String FastDFSMonitorDao = "monitor_FastDFSMonitorDao";
    
    public static final String RedisMonitorDao = "monitor_RedisMonitorDao";
    
    public static final String MemcachedMonitorDao = "monitor_MemcachedMonitorDao";
    
    // -------------------------------------------------------------------------------------------//
    // Service
    // -------------------------------------------------------------------------------------------//
    public static final String MonitorService = "monitor_MonitorService";
    
    public static final String SysMonitorService = "monitor_SysMonitorService";
    
    public static final String SpringMonitorService = "monitor_SpringMonitorService";
    
    public static final String FastDFSMonitorService = "monitor_FastDFSMonitorService";
    
    public static final String RedisMonitorService = "monitor_RedisMonitorService";
    
    public static final String MemcachedMonitorService = "monitor_MemcachedMonitorService";
    
    // -------------------------------------------------------------------------------------------//
    // Other
    // -------------------------------------------------------------------------------------------//
    
}
