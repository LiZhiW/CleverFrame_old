package org.cleverframe.core.fileupload.servlet;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * 自定义的处理文件上传的解析器<br>
 * 1.注册文件上传时的上传进度监听器<br>
 * @see CommonsMultipartResolver
 * 
 * @author LiZW
 * @version 2015年9月2日 下午2:01:21
 */
public class CustomMultipartResolver extends CommonsMultipartResolver
{

    /* （非 Javadoc）
     * 自定义创建DiskFileItemFactory
     */
    @Override
    protected DiskFileItemFactory newFileItemFactory()
    {
        DiskFileItemFactory diskFileItemFactory = super.newFileItemFactory();
        // diskFileItemFactory.setFileCleaningTracker(pTracker);
        return diskFileItemFactory;
    }

    /* （非 Javadoc）
     * 解析上传的文件，此方法必定会被执行
     * 重写此方法的主要目的是注册文件上传时的上传进度监听器，其他代码参考父类实现
     */
    @Override
    protected MultipartParsingResult parseRequest(HttpServletRequest request) throws MultipartException
    {
        String encoding = determineEncoding(request);
        FileUpload fileUpload = prepareFileUpload(encoding);
        // 注册文件上传时的上传进度监听器，并向监听器
        fileUpload.setProgressListener(new LocalProgressListener(request.getSession()));
        try
        {
            List<FileItem> fileItems = ((ServletFileUpload) fileUpload).parseRequest(request);
            return parseFileItems(fileItems, encoding);
        }
        catch (FileUploadBase.SizeLimitExceededException ex)
        {
            throw new MaxUploadSizeExceededException(fileUpload.getSizeMax(), ex);
        }
        catch (FileUploadException ex)
        {
            throw new MultipartException("Could not parse multipart servlet request", ex);
        }
    }
}
