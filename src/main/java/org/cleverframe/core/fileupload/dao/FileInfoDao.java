package org.cleverframe.core.fileupload.dao;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.fileupload.FileuploadBeanNames;
import org.cleverframe.core.fileupload.entity.FileInfo;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.springframework.stereotype.Repository;

/**
 * 上传文件信息Dao<br>
 * 
 * @author LiZW
 * @version 2015年8月31日 下午5:58:25
 */
@Repository(FileuploadBeanNames.FileInfoDao)
public class FileInfoDao extends BaseDao<FileInfo>
{
    /**
     * 根据文件签名，到数据库查找相同的文件<br>
     * 
     * @param fileDigest 文件签名
     * @param digestType 文件签名类型
     * @return 上传的文件信息，未找到返回null
     */
    public FileInfo findFileInfoByDigest(String fileDigest, Character digestType)
    {
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.core.fileupload.dao.FileInfoDao.findFileInfoByDigest");
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("fileDigest", fileDigest);
        param.put("digestType", digestType);
        List<FileInfo> fileInfoList = this.getHibernateDao().findBySql(sql, param);
        if (fileInfoList == null || fileInfoList.size() <= 0)
        {
            return null;
        }
        else
        {
            return fileInfoList.get(0);
        }
    }
    
    /**
     * 查询同一文件在数据库里的引用数量<br>
     * <b>注意：文件只要存在引用就不能删除</b>
     * @param filePath 文件路径
     * @param newName 服务器端的文件名
     * @return 文件被引用的数量
     */
    public int findRepeatFile(String filePath, String newName)
    {
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.core.fileupload.dao.FileInfoDao.findRepeatFile");
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("filePath", filePath);
        param.put("newName", newName);
        BigInteger count = (BigInteger) this.getHibernateDao().getBySql(null, sql, param);
        return count.intValue();
    }
    
    /**
     * 分页查询上传文件信息<br>
     * @param page 分页数据
     * @param digest 查询参数：文件签名
     * @param fileName 查询参数：文件名称
     * @param newName 查询参数：服务器端文件名
     * @param startTime 查询参数：开始时间
     * @param endTime 查询参数：结束时间
     * @return
     */
    public Page<FileInfo> findFileInfoByPage(Page<FileInfo> page, String digest, String fileName, String newName, Date startTime, Date endTime)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("digest", digest);
        param.put("fileName", fileName);
        param.put("newName", newName);
        param.put("startTime", startTime);
        param.put("endTime", endTime);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.core.fileupload.dao.FileInfoDao.findFileInfoByPage");
        this.getHibernateDao().findBySql(page, sql, param);
        return page;
    }
    
    /**
     * 根据UUID查询文件信息
     * @param uuid 查询参数：数据UUID
     * @return
     */
    public FileInfo getFileInfoByUuid(Serializable uuid)
    {
        Parameter param = new Parameter();
        param.put("uuid", uuid);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.core.fileupload.dao.FileInfoDao.getFileInfoByUuid");
        return getHibernateDao().getBySql(sql, param);
    }
}
