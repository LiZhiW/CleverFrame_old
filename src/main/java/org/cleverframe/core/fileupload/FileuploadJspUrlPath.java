package org.cleverframe.core.fileupload;

/**
 * 当前fileupload模块对应的JSP文件URL路径<br>
 * 1.注意：此类只保存JSP文件的URL<br>
 * @author LiZW
 * @version 2015年8月27日 下午10:31:27
 */
public class FileuploadJspUrlPath
{
    /** 文件上传示例页面 */
    public static final String FileuploadDemo = "core/fileupload/FileuploadDemo";

    /** 上传文件管理页面 */
    public static final String FileManager = "core/fileupload/FileManager";

}
