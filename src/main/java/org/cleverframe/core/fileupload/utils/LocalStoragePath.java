package org.cleverframe.core.fileupload.utils;

import java.io.File;

import org.cleverframe.common.utils.DateUtils;

/**
 * 使用当前服务器硬盘存储上传文件时，用于生成存放文件夹路径工具<br>
 * 
 * @author LiZW
 * @version 2015年9月2日 下午7:44:58
 */
public class LocalStoragePath
{
    /**
     * 根据当前时间生成文件存储路径，生成策略：basePath/yyyy/yyyy-MM/yyyy-MM-dd <br>
     * 
     * @param basePath 存储文件基路径
     * @return 返回存储文件完整路径，类似：basePath + /yyyy/yyyy-MM/yyyy-MM-dd
     */
    public static String createFilePathByDate(String basePath)
    {
        long dateTime = System.currentTimeMillis();
        String dateStr = DateUtils.getDate(dateTime, "yyyy-MM-dd");
        String[] array = dateStr.split("-");
        StringBuffer path = new StringBuffer();
        path.append(basePath)
            .append(File.separator)
            .append(array[0])
            .append(File.separator)
            .append(array[0] + "-" + array[1])
            .append(File.separator)
            .append(dateStr);
        return path.toString();
    }
}
