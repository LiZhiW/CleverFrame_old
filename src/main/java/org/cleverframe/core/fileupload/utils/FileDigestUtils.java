package org.cleverframe.core.fileupload.utils;

import java.io.IOException;
import java.io.InputStream;

import org.cleverframe.common.utils.Digests;
import org.cleverframe.common.utils.Encodes;

/**
 * 用于处理文件签名的工具类，文件签名统一处理<br>
 * 
 * @author LiZW
 * @version 2015年9月3日 上午12:19:20
 */
public class FileDigestUtils
{
    /**
     * 获取文件的MD5签名字符串<br>
     * 
     * @param data 文件二进制数据
     * @return 文件的MD5签名字符串
     */
    public static String FileDigestByMD5(byte[] data)
    {
        String digest = Encodes.encodeHex(Digests.md5(data));
        return digest;
    }
    
    /**
     * 获取文件的SHA1签名字符串<br>
     * 
     * @param data 文件二进制数据
     * @return 文件的SHA1签名字符串
     */
    public static String FileDigestBySHA1(byte[] data)
    {
        String digest = Encodes.encodeHex(Digests.sha1(data));
        return digest;
    }
    
    /**
     * 获取文件的MD5签名字符串<br>
     * 
     * @param stream 文件流
     * @return 文件的MD5签名字符串
     * @throws IOException 操作失败
     */
    public static String FileDigestByMD5(InputStream stream) throws IOException
    {
        String digest = Encodes.encodeHex(Digests.md5(stream));
        return digest;
    }
    
    /**
     * 获取文件的SHA1签名字符串<br>
     * 
     * @param stream 文件流
     * @return 文件的SHA1签名字符串
     * @throws IOException 操作失败
     */
    public static String FileDigestBySHA1(InputStream stream) throws IOException
    {
        String digest = Encodes.encodeHex(Digests.sha1(stream));
        return digest;
    }
}
