package org.cleverframe.core.fileupload.vo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 文件上传进度信息对象<br>
 * 
 * @author LiZW
 * @version 2015年9月2日 下午4:04:49
 */
@JsonInclude(Include.NON_NULL)
public class FileuploadProgress implements Serializable
{
    private static final long serialVersionUID = 1L;
    /** 已经上传到服务器的字节数 */
    private long bytesRead;
    /** 所有文件的总大小 */
    private long contentLength;
    /** 表示第几个文件 */
    private int itemIndex = -1;
    /** 完成百分比 */
    private long percentage;
    /** 是否上传完成 */
    private boolean complete;

    /*--------------------------------------------------------------
     *          getter、setter
     * -------------------------------------------------------------*/
    public float getBytesRead()
    {
        return bytesRead;
    }

    public void setBytesRead(long bytesRead)
    {
        this.bytesRead = bytesRead;
    }

    public long getContentLength()
    {
        return contentLength;
    }

    public void setContentLength(long contentLength)
    {
        this.contentLength = contentLength;
    }

    public int getItemIndex()
    {
        return itemIndex;
    }

    public void setItemIndex(int itemIndex)
    {
        this.itemIndex = itemIndex;
    }

    public long getPercentage()
    {
        percentage = (bytesRead * 100) / contentLength;
        return percentage;
    }

    public void setPercentage(long percentage)
    {
        this.percentage = percentage;
    }

    public boolean isComplete()
    {
        return complete;
    }

    public void setComplete(boolean complete)
    {
        this.complete = complete;
    }
}
