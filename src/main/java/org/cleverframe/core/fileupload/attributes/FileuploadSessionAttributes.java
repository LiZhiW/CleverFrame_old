package org.cleverframe.core.fileupload.attributes;

/**
 * 记录fileupload模块向ServletContext中加入的Seesion范围的属性值的名称
 * 
 * @author LiZW
 * @version 2015年9月2日 下午3:51:01
 */
public class FileuploadSessionAttributes
{
    /** 文件上传进度状态,类型：FileuploadProgress */
    public static final String FILEUPLOAD_PROGRESS = "Fileupload_Fileupload_Progress";
}
