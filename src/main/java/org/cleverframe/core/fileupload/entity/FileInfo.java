package org.cleverframe.core.fileupload.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * 上传文件的文件信息<br>
 * 
 * @author LiZW
 * @version 2015年8月31日 下午3:41:01
 */
@Entity
@Table(name = "fileupload_fileinfo")
@DynamicInsert
@DynamicUpdate
public class FileInfo extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 上传文件的存储类型（1：当前服务器硬盘；2：FTP服务器；3：FastDFS服务器） */
    public static final char LOCAL_STORAGE = '1';
    /** 上传文件的存储类型（1：当前服务器硬盘；2：FTP服务器；3：FastDFS服务器） */
    public static final char FTP_STORAGE = '2';
    /** 上传文件的存储类型（1：当前服务器硬盘；2：FTP服务器；3：FastDFS服务器） */
    public static final char FASTDFS_STORAGE = '3';

    /** 文件签名算法类型（1：MD5；2：SHA-1；） */
    public static final char MD5_DIGEST = '1';
    /** 文件签名算法类型（1：MD5；2：SHA-1；） */
    public static final char SHA1_DIGEST = '2';

    /** 上传文件的存储类型（1：当前服务器硬盘；2：FTP服务器；3：FastDFS服务器） */
    private Character storedType;

    /** 上传文件存放路径 */
    private String filePath;

    /** 文件签名，用于判断是否是同一文件 */
    private String digest;

    /** 文件签名算法类型（1：MD5；2：SHA-1；） */
    private Character digestType;

    /** 文件大小 */
    private Long fileSize;

    /** 文件原名称，用户上传时的名称 */
    private String fileName;

    /** 文件当前名称(UUID + 后缀名) */
    private String newName;

    /** 文件上传所用时间 */
    private Long uploadTime;

    /** 文件存储用时(此时间只包含服务器端存储文件所用的时间，不包括文件上传所用的时间) */
    private Long storedTime;

    /*--------------------------------------------------------------
     *          getter、setter
     * -------------------------------------------------------------*/
    public Character getStoredType()
    {
        return storedType;
    }

    public void setStoredType(Character storedType)
    {
        this.storedType = storedType;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    public String getDigest()
    {
        return digest;
    }

    public void setDigest(String digest)
    {
        this.digest = digest;
    }

    public Long getFileSize()
    {
        return fileSize;
    }

    public void setFileSize(Long fileSize)
    {
        this.fileSize = fileSize;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getNewName()
    {
        return newName;
    }

    public void setNewName(String newName)
    {
        this.newName = newName;
    }

    public Long getUploadTime()
    {
        return uploadTime;
    }

    public void setUploadTime(Long uploadTime)
    {
        this.uploadTime = uploadTime;
    }

    public Long getStoredTime()
    {
        return storedTime;
    }

    public void setStoredTime(Long storedTime)
    {
        this.storedTime = storedTime;
    }

    public Character getDigestType()
    {
        return digestType;
    }

    public void setDigestType(Character digestType)
    {
        this.digestType = digestType;
    }
}
