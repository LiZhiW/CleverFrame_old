package org.cleverframe.core.fileupload.service;

import java.io.OutputStream;
import java.io.Serializable;

import org.cleverframe.core.fileupload.entity.FileInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 上传文件存储接口<br>
 * 
 * @author LiZW
 * @version 2015年8月31日 下午5:29:50
 */
public interface IStorageService
{
    /**
     * 根据文件签名保存文件，实现文件秒传<br>
     * 
     * @param fileName 文件名称
     * @param fileDigest 文件签名
     * @param digestType 签名类型
     * @return 保存成功返回文件信息，失败返回null
     * @throws Exception 操作失败
     */
    public FileInfo lazySaveFile(String fileName, String fileDigest, Character digestType) throws Exception;

    /**
     * 保存文件，当文件较大时此方法会占用磁盘IO，因为common-fileupload会将上传文件写入硬盘的临时文件<br>
     * <b>注意：如果上传的文件在服务器端存在(通过文件签名判断)，就不会存储文件只会新增文件引用</b>
     * @param uploadTime 文件上传所用时间
     * @param multipartFile 上传的文件信息
     * @return 返回存储后的文件信息
     * @throws Exception 保存失败抛出异常
     */
    public FileInfo saveFile(long uploadTime, MultipartFile multipartFile) throws Exception;
    
    /**
     * 删除服务器端的文件<br>
     * @param fileInfoUuid 文件信息UUID
     * @param lazy 如果值为true表示：只删除当前文件引用；值为false表示：如果没有其他引用就删除服务器端文件
     * @return 1：成功删除fileInfo和服务器端文件；2：只删除了fileInfo；3：fileInfo不存在
     */
    public int deleteFile(Serializable fileInfoUuid, boolean lazy) throws Exception;
    
    /**
     * 判断文件在服务端是否存在<br>
     * <b>注意：此方法的返回值与数据库中是否存在fileInfo无关系</b>
     * @param fileInfoUuid 文件信息UUID
     * @return 不存在返回null，存在返回文件信息
     */
    public FileInfo isExists(Serializable fileInfoUuid) throws Exception;
    
    /**
     * 打开文件到OutputStream<br>
     * @param fileInfoUuid 文件信息UUID
     * @param outputStream 输出流，用于打开文件
     * @return FileInfo(文件信息)。 文件不存在返回null
     * @throws Exception
     */
    public FileInfo openFile(Serializable fileInfoUuid, OutputStream outputStream) throws Exception;
}
