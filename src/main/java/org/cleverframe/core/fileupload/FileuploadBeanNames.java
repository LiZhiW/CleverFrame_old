package org.cleverframe.core.fileupload;

/**
 * 定义当前fileupload模块定义的Spring Bean名称<br>
 * 
 * @author LiZW
 * @version 2015年9月1日 下午12:21:07
 */
public class FileuploadBeanNames
{
    // -------------------------------------------------------------------------------------------//
    // Dao
    // -------------------------------------------------------------------------------------------//
    public static final String FileInfoDao = "fileupload_FileInfoDao";

    // -------------------------------------------------------------------------------------------//
    // Service
    // -------------------------------------------------------------------------------------------//
    public static final String LocalStorageService = "fileupload_LocalStorageService";

    public static final String FtpStorageService = "fileupload_FtpStorageService";
    
    public static final String FastDFSStorageService = "fileupload_FastDFSStorageService";
    
    public static final String FileManagerService = "fileupload_FileManagerService";
    
    // -------------------------------------------------------------------------------------------//
    // Other
    // -------------------------------------------------------------------------------------------//
}
