package org.cleverframe.core.qlscript.utils;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.apache.commons.lang3.StringUtils;
import org.cleverframe.core.qlscript.QLScriptBeanNames;
import org.cleverframe.core.qlscript.entity.QLScript;
import org.cleverframe.core.qlscript.service.IQLScriptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import freemarker.cache.TemplateLoader;

/**
 * 实现freemarker的模版加载器，从数据库加载"数据库脚本"模版<br>
 * 1.实现参考：StringTemplateLoader
 * 
 * @see freemarker.cache.StringTemplateLoader
 * @see freemarker.cache.FileTemplateLoader
 * @author LiZW
 * @version 2015年6月24日 上午10:29:38
 */
@Component(QLScriptBeanNames.QLScriptTemplateLoader)
public class QLScriptTemplateLoader implements TemplateLoader
{
    /** 日志对象 */
    private final static Logger logger = LoggerFactory.getLogger(QLScriptTemplateLoader.class);
    
    @Autowired
    @Qualifier(QLScriptBeanNames.MemoryQLScriptService)
    private IQLScriptService qLScriptService;

    /** FreeMarker配置的区域信息，如 ：“zh_CN”、“en_US” */
    private String locale;

    /**
     * 根据名称返回指定的模版资源
     * */
    @Override
    public Object findTemplateSource(String name) throws IOException
    {
        name = StringUtils.replace(name, "_" + locale, "");
        QLScript qLScript = null;
        try
        {
            qLScript = qLScriptService.getQLScriptByName(name);
        }
        catch (Exception e)
        {
            logger.error("获取脚本异常，脚本名称：" + name, e);
        }
        if (qLScript == null)
        {
            throw new IOException("脚本不存在，脚本名称：" + name);
        }
        return qLScript;
    }

    /**
     * 返回模版资源最后一次修改的时间
     * */
    @Override
    public long getLastModified(Object templateSource)
    {
        QLScript qLScript = (QLScript) templateSource;
        if (qLScript.getUpdateDate() != null)
        {
            return qLScript.getUpdateDate().getTime();
        }
        else
        {
            return qLScript.getCreateDate().getTime();
        }
    }

    /**
     * 返回读取模版资源的 Reader
     * */
    @Override
    public Reader getReader(Object templateSource, String encoding) throws IOException
    {
        QLScript qLScript = (QLScript) templateSource;
        StringReader reader = new StringReader(qLScript.getScript());
        return reader;
    }

    /**
     * 关闭模板源
     * */
    @Override
    public void closeTemplateSource(Object templateSource) throws IOException
    {

    }

    /*--------------------------------------------------------------
     *          getter、setter
     * -------------------------------------------------------------*/
    public String getLocale()
    {
        return locale;
    }

    public void setLocale(String locale)
    {
        this.locale = locale;
    }
}
