package org.cleverframe.core.qlscript.dao;

import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.QLScriptBeanNames;
import org.cleverframe.core.qlscript.entity.QLScript;
import org.springframework.stereotype.Repository;

/**
 * 数据库脚本Dao<br>
 * 
 * @author LiZW
 * @version 2015年6月24日 上午11:18:10
 */
@Repository(QLScriptBeanNames.QLScriptDao)
public class QLScriptDao extends BaseDao<QLScript>
{
	/** 
	 * 根据name查询QLScript，没有按公司隔离数据，参数: <br>
	 * 1.:delFlag delFlag 删除标识<br>
	 * 2.:name name 脚本名称<br>
	 * */
	public static final String SQL_GET_QLSCRIPT_BY_NAME = "SELECT * FROM qlscript_qlscript WHERE del_flag=:delFlag AND name=:name";

	/** 
	 * 查询QLScript，没有按公司隔离数据，参数:<br>
	 * 1.:delFlag delFlag 删除标识<br>
	 * 2.:name name 脚本名称
	 * 3.:script_type scriptType 脚本类型
	 * 4.:id id 脚本ID
	 * 5.:uuid uuid 数据UUID
	 * */
	public static final String SQL_FIND_ALL_QLSCRIPT = "SELECT * FROM qlscript_qlscript WHERE del_flag=:delFlag "
			+ " AND (name LIKE :name OR :name = '' ) "
			+ " AND (script_type=:scriptType OR :scriptType='' ) "
			+ " AND (id=:id OR :id=-1) "
			+ " AND (uuid=:uuid OR :uuid='') " 
			+ " ORDER BY name ";

	/**
	 * 根据脚本名称获取脚本对象<br>
	 * 
	 * @param name 查询参数：脚本名称(使用包名称+类名+方法名)
	 */
	public QLScript getQLScriptByname(String name)
	{
	    Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
	    param.put("name", name);
		return hibernateDao.getBySql(SQL_GET_QLSCRIPT_BY_NAME, param);
	}

	/**
	 * 得到所有数据库脚本
	 */
	public List<QLScript> findAllScript()
	{
		return hibernateDao.findBySql(SQL_FIND_ALL_QLSCRIPT);
	}

	/**
	 * 获取所有的数据库脚本，使用分页
	 * 
	 * @param page 分页对象
	 * @param name 查询参数：脚本名称(使用包名称+类名+方法名)
	 * @param scriptType 查询参数：脚本类型
	 * @param id 查询参数：脚本ID
	 * @param uuid 查询参数：UUID
	 * @return 分页数据
	 */
    public Page<QLScript> findAllQLScript(Page<QLScript> page, String name, String scriptType, Long id, String uuid)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("name", name);
        param.put("scriptType", scriptType);
        param.put("id", id);
        param.put("uuid", uuid);
        return hibernateDao.findBySql(page, SQL_FIND_ALL_QLSCRIPT, param);
    }
}
