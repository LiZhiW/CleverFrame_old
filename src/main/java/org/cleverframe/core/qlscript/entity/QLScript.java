package org.cleverframe.core.qlscript.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

/**
 * 数据库脚本<br>
 * 
 * @author LiZW
 * @version 2015年6月24日 上午11:09:30
 */
@Entity
@Table(name = "qlscript_qlscript")
@DynamicInsert
@DynamicUpdate
public class QLScript extends IdEntity
{
    private static final long serialVersionUID = 1L;
    /** 脚本类型:SQL */
    public static final String TYPE_SQL = "SQL";
    /** 脚本类型:HQL */
    public static final String TYPE_HQL = "HQL";

    /** 脚本类型，可取："SQL"、"HQL" */
    @NotNull(message = "脚本类型不能为空")
    @Length(min = 1, max = 10, message = "脚本类型长度必须是1——10个字符")
    private String scriptType;

    /** 查询脚本，可以使用模版技术拼接 */
    @NotNull(message = "数据库脚本不能为空")
    @Length(min = 1, max = 2000, message = "数据库脚本长度必须是1——2000个字符")
    private String script;

    /** 脚本名称，使用包名称+类名+方法名 */
    @NotNull(message = "脚本名称不能为空")
    @Length(min = 1, max = 100, message = "脚本名称长度必须是1——100个字符")
    private String name;

    /** 脚本功能说明 */
    @NotNull(message = "脚本功能说明不能为空")
    @Length(min = 1, max = 1000, message = "脚本功能说明长度必须是1——1000个字符")
    private String description;

    public QLScript()
    {
        super();
    }

    /*--------------------------------------------------------------
     *          getter、setter
     * -------------------------------------------------------------*/
    public String getScriptType()
    {
        return scriptType;
    }

    public void setScriptType(String scriptType)
    {
        this.scriptType = scriptType;
    }
    
    public String getScript()
    {
        return script;
    }

    public void setScript(String script)
    {
        this.script = script;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
