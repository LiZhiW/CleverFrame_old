package org.cleverframe.core.qlscript;

/**
 * 定义当前qlscript模块定义的Spring Bean名称<br>
 * 
 * @author LiZW
 * @version 2015年9月1日 下午12:24:31
 */
public class QLScriptBeanNames
{
    // -------------------------------------------------------------------------------------------//
    // Dao
    // -------------------------------------------------------------------------------------------//
    public static final String QLScriptDao = "qlscript_QLScriptDao";

    // -------------------------------------------------------------------------------------------//
    // Service
    // -------------------------------------------------------------------------------------------//
    /*QLScriptService 只能使用一种不能不能混用！*/
    public static final String MemoryQLScriptService = "qlscript_MemoryQLScriptService";
    public static final String RedisQLScriptService = "qlscript_RedisQLScriptService";
    public static final String EhCacheQLScriptService = "qlscript_EhCacheQLScriptService";
    public static final String MemcachedQLScriptService = "qlscript_MemcachedQLScriptService";
    
    // -------------------------------------------------------------------------------------------//
    // Other
    // -------------------------------------------------------------------------------------------//
    public static final String QLScriptTemplateLoader = "qlscript_QLScriptTemplateLoader";

}
