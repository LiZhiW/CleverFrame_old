package org.cleverframe.core.qlscript;

/**
 * 当前qlscript模块对应的JSP文件URL路径<br>
 * 1.注意：此类只保存JSP文件的URL<br>
 * @author LiZhiWei
 * @version 2015年6月30日 下午8:29:04
 */
public class QLScriptJspUrlPath
{
    /** 数据库脚本管理页面 */
    public static final String QLScript = "core/qlscript/QLScript";
}
