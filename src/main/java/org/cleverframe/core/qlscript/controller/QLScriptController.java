package org.cleverframe.core.qlscript.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.utils.Encodes;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.core.qlscript.QLScriptBeanNames;
import org.cleverframe.core.qlscript.QLScriptJspUrlPath;
import org.cleverframe.core.qlscript.entity.QLScript;
import org.cleverframe.core.qlscript.service.IQLScriptService;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 数据库脚本Controller<br>
 * 
 * @author LiZW
 * @version 2015年6月25日 下午3:54:29
 */
@Controller
@RequestMapping("/${mvcPath}/common")
@Transactional(readOnly = true)
public class QLScriptController extends BaseController
{
	@Autowired
	@Qualifier(QLScriptBeanNames.MemoryQLScriptService)
	private IQLScriptService qLScriptService;

	/**
	 * 跳转到数据库脚本管理页面<br>
	 */
	// @RequiresRoles("root")
	@RequestMapping("/getQLScriptJsp")
	public ModelAndView getQLScriptJsp(HttpServletRequest request, HttpServletResponse response)
	{
		ModelAndView mav = new ModelAndView(QLScriptJspUrlPath.QLScript);
		return mav;
	}

	/**
	 * 查询数据库脚本，使用分页
	 * @return EasyUI DataGrid控件的json数据
	 */
	// @RequiresRoles("root")
	@ResponseBody
	@RequestMapping("/findQLScriptByPage")
	public DataGridJson<QLScript> findQLScriptByPage(
			HttpServletRequest request, 
			HttpServletResponse response,
			@RequestParam(value = "type-search", required = false,defaultValue="") String scriptType,
			@RequestParam(value = "name-search", required = false,defaultValue="") String name,
			@RequestParam(value = "id-search", required = false,defaultValue="-1") Long id,
			@RequestParam(value = "uuid-search", required = false,defaultValue="") String uuid)
	{
        name = "%" + name + "%";
        DataGridJson<QLScript> json = new DataGridJson<QLScript>();
        Page<QLScript> qLScriptPage = null;
        try
        {
            qLScriptPage = qLScriptService.findAllQLScript(new Page<QLScript>(request, response), name, scriptType, id, uuid);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        json.setRows(qLScriptPage.getList());
        json.setTotal(qLScriptPage.getCount());
        return json;
	}

	/**
	 * 保存数据库脚本对象<br>
	 */
	// @RequiresRoles("root")
	@ResponseBody
	@RequestMapping("/addQLScript")
	@Transactional(readOnly = false)
	public AjaxMessage addQLScript(HttpServletRequest request, HttpServletResponse response, @Valid QLScript qLScript, BindingResult bindingResult)
	{
	    String qlStr = Encodes.unescapeHtml(qLScript.getScript());// HTML转码
	    qLScript.setScript(qlStr);
		AjaxMessage message = new AjaxMessage();
		if (beanValidator(bindingResult, message))
		{
			try
            {
                qLScriptService.saveQLScript(qLScript);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
			message.setMessage("数据库脚本保存成功");
		}
		return message;
	}

	/**
	 * 更新数据库脚本对象<br>
	 */
	// @RequiresRoles("root")
	@ResponseBody
	@RequestMapping("/updateQLScript")
	@Transactional(readOnly = false)
	public AjaxMessage updateQLScript(HttpServletRequest request, HttpServletResponse response, @Valid QLScript qLScript, BindingResult bindingResult)
	{
        String qlStr = Encodes.unescapeHtml(qLScript.getScript());// HTML转码
        qLScript.setScript(qlStr);
		AjaxMessage message = new AjaxMessage();
		if (beanValidator(bindingResult, message))
		{
			try
            {
                qLScriptService.updateQLScript(qLScript);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
			QLScriptUtils.removeTemplateCache(qLScript.getName());
			message.setMessage("数据库脚本更新成功");
		}
		return message;
	}

	/**
	 * 删除数据库脚本对象<br>
	 */
	// @RequiresRoles("root")
	@ResponseBody
	@RequestMapping("/deleteQLScript")
	@Transactional(readOnly = false)
	public AjaxMessage deleteQLScript(HttpServletRequest request, HttpServletResponse response, @Valid QLScript qLScript, BindingResult bindingResult)
	{
		try
        {
            qLScriptService.deleteQLScript(qLScript);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
		QLScriptUtils.removeTemplateCache(qLScript.getName());
		AjaxMessage message = new AjaxMessage(true, "删除成功");
		return message;
	}
}
