package org.cleverframe.core.qlscript.service.impl;

import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.core.qlscript.QLScriptBeanNames;
import org.cleverframe.core.qlscript.entity.QLScript;
import org.cleverframe.core.qlscript.service.IQLScriptService;
import org.springframework.stereotype.Service;

/**
 * @author LiZW
 * @version 2015年9月22日 下午10:03:14
 */
@Service(QLScriptBeanNames.EhCacheQLScriptService)
public class EhCacheQLScriptService extends BaseService implements IQLScriptService
{

    @Override
    public QLScript getQLScriptByName(String name) throws Exception
    {
        // TODO 自动生成的方法存根
        return null;
    }

    @Override
    public boolean saveQLScript(QLScript qLScript) throws Exception
    {
        // TODO 自动生成的方法存根
        return false;
    }

    @Override
    public boolean updateQLScript(QLScript qLScript) throws Exception
    {
        // TODO 自动生成的方法存根
        return false;
    }

    @Override
    public boolean deleteQLScript(QLScript qLScript) throws Exception
    {
        // TODO 自动生成的方法存根
        return false;
    }

    @Override
    public QLScript refreshQLScript(String name) throws Exception
    {
        // TODO 自动生成的方法存根
        return null;
    }

    @Override
    public List<QLScript> findAllQLScript() throws Exception
    {
        // TODO 自动生成的方法存根
        return null;
    }

    @Override
    public Page<QLScript> findAllQLScript(Page<QLScript> page, String name, String scriptType, Long id, String uuid) throws Exception
    {
        // TODO 自动生成的方法存根
        return null;
    }

}
