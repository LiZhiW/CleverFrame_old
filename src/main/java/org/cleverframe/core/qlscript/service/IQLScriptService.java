package org.cleverframe.core.qlscript.service;

import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.core.qlscript.entity.QLScript;

/**
 * 数据库脚本Service接口<br>
 * <b>注意：只能使用一种实现，不能混用</b>
 * @author LiZW
 * @version 2015年9月22日 下午9:35:37
 */
public interface IQLScriptService
{
    /**
     * 根据脚本名称获取脚本对象，优先到缓存中获取<br>
     * 1.到QLScript缓存中获取<br>
     * 2.QLScript缓存中没有才到数据库里获取并存到缓存中<br>
     * 
     * @param name 数据库脚本名称(使用包名称+类名+方法名)
     * @return 数据库脚本信息(QLScript),不存在返回null
     * @throws Exception
     */
    public QLScript getQLScriptByName(String name) throws Exception;

    /**
     * 保存数据库脚本，也会保存到QLScript缓存中<br>
     * @param qLScript
     * @return 成功返回true，失败返回false
     * @throws Exception
     */
    public boolean saveQLScript(QLScript qLScript) throws Exception;

    /**
     * 更新数据库脚本，也会刷新QLScript缓存数据<br>
     * @param qLScript 数据库脚本信息(QLScript)
     * @return 成功返回true，失败返回false
     * @throws Exception
     */
    public boolean updateQLScript(QLScript qLScript) throws Exception;

    /**
     * 逻辑删除数据库脚本，也会从QLScript缓存中删除<br>
     * @param qLScript 数据库脚本信息(QLScript)
     * @return 成功返回true，失败返回false
     * @throws Exception
     */
    public boolean deleteQLScript(QLScript qLScript) throws Exception;

    /**
     * 获取数据库脚本，直接到数据库里获取，并覆盖到QLScript缓存<br>
     * 
     * @param name 脚本名称(使用包名称+类名+方法名)
     * @return 刷新缓存成功返回新的QLScript，否则返回null
     * @throws Exception
     */
    public QLScript refreshQLScript(String name) throws Exception;

    /**
     * 从数据库查询所有的QL脚本，并存到QLScript缓存中<br>
     * @return 所有数据库脚本
     * @throws Exception
     */
    public List<QLScript> findAllQLScript() throws Exception;

    /**
     * 获取数据库脚本，使用分页，同时把查询到的数据放入QLScript缓存<br>
     * 
     * @param page 分页对象
     * @param name 查询参数：脚本名称(使用包名称+类名+方法名)
     * @param scriptType 查询参数：脚本类型
     * @param id 查询参数：脚本ID
     * @param uuid 查询参数：UUID
     * @return 分页数据
     * @throws Exception
     */
    public Page<QLScript> findAllQLScript(Page<QLScript> page, String name, String scriptType, Long id, String uuid) throws Exception;
}
