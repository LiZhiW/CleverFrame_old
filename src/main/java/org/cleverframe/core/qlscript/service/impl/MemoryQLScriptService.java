package org.cleverframe.core.qlscript.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.core.qlscript.QLScriptBeanNames;
import org.cleverframe.core.qlscript.dao.QLScriptDao;
import org.cleverframe.core.qlscript.entity.QLScript;
import org.cleverframe.core.qlscript.service.IQLScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 数据库脚本Service，直接使用内存来缓存数据库脚本<br>
 * 
 * @author LiZW
 * @version 2015年9月22日 下午9:57:09
 */
@Service(QLScriptBeanNames.MemoryQLScriptService)
public class MemoryQLScriptService extends BaseService implements IQLScriptService
{
    @Autowired
    @Qualifier(QLScriptBeanNames.QLScriptDao)
    private QLScriptDao qLScriptDao;
    
    /** 
     * 缓存的数据库脚本对象，QLScript缓存<br>
     * */
    private Map<String, QLScript> qLScriptCahe = new HashMap<String, QLScript>();
    
    @Override
    public QLScript getQLScriptByName(String name)
    {
        QLScript qLScript = qLScriptCahe.get(name);
        if (qLScript == null)
        {
            qLScript = qLScriptDao.getQLScriptByname(name);
            if (qLScript != null)
            {
                qLScriptCahe.put(name, qLScript);
            }
        }
        return qLScript;
    }

    @Override
    public boolean saveQLScript(QLScript qLScript) throws Exception
    {
        if (qLScript != null)
        {
            qLScriptDao.getHibernateDao().save(qLScript);
            qLScriptCahe.put(qLScript.getName(), qLScript);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateQLScript(QLScript qLScript) throws Exception
    {
        if (qLScript != null)
        {
            qLScriptDao.getHibernateDao().update(qLScript);
            qLScriptCahe.put(qLScript.getName(), qLScript);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteQLScript(QLScript qLScript) throws Exception
    {
        qLScriptDao.getHibernateDao().delete(qLScript);
        qLScriptCahe.remove(qLScript.getName());
        return true;
    }

    @Override
    public QLScript refreshQLScript(String name) throws Exception
    {
        QLScript qLScript = qLScriptDao.getQLScriptByname(name);
        if (qLScript != null)
        {
            qLScriptCahe.put(name, qLScript);
        }
        return qLScript;
    }

    @Override
    public List<QLScript> findAllQLScript() throws Exception
    {
        return qLScriptDao.findAllScript();
    }

    @Override
    public Page<QLScript> findAllQLScript(Page<QLScript> page, String name, String scriptType, Long id, String uuid) throws Exception
    {
        page = qLScriptDao.findAllQLScript(page, name, scriptType, id, uuid);
        for (QLScript script : page.getList())
        {
            if (qLScriptCahe.get(script.getName()) == null)
            {
                qLScriptCahe.put(script.getName(), script);
            }
        }
        return page;
    }
}
