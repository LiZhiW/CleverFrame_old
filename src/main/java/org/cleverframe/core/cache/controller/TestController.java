package org.cleverframe.core.cache.controller;

import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.rubyeye.xmemcached.exception.MemcachedException;

import org.cleverframe.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @author LiZW
 * @version 2015年9月23日 下午2:00:29
 */
@Controller
@RequestMapping("/${mvcPath}/test")
@Transactional(readOnly = true)
public class TestController extends BaseController
{
    @RequestMapping("/01")
    public ModelAndView test01(HttpServletRequest request, HttpServletResponse response)
    {
        return null;
    }
    
    @RequestMapping("/02")
    public ModelAndView test02(HttpServletRequest request, HttpServletResponse response) throws TimeoutException, InterruptedException, MemcachedException
    {
        return null;
    }
    
}
