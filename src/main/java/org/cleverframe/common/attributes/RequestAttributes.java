package org.cleverframe.common.attributes;

/**
 * 记录common模块向ServletContext中加入的Request范围的属性值的名称<br>
 * 
 * @author LiZW
 * @version 2015年6月23日 下午1:46:26
 */
public class RequestAttributes
{
	/** 请求开始时间,类型：long */
	public static final String REQUEST_START_TIME = "Request_Start_Time";
}
