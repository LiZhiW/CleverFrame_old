package org.cleverframe.common.attributes;

/**
 * 记录common模块向ServletContext中加入的Seesion范围的属性值的名称<br>
 * 
 * @author LiZW
 * @version 2015年6月19日 下午2:12:34
 */
public class SessionAttributes
{
	/**
	 * 验证码参数名,类型:ValidateCode<br>
	 * 1.用于客户端请求验证码参数名<br>
	 * 2.用于HttpSession中的验证码属性名<br>
	 * */
	public static final String VALIDATE_CODE = "validateCode";
	
	
}
