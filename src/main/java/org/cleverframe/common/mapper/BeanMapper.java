package org.cleverframe.common.mapper;

import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JavaBean与Map<String,Object>互转 工具类(org.apache.commons.beanutils.BeanUtils实现)<br>
 * 
 * @author LiZW
 * @version 2015年5月28日 下午5:14:24
 */
public class BeanMapper
{
    /** 日志记录器 */
    private final static Logger logger = LoggerFactory.getLogger(JsonMapper.class);
    
    /**
     * 把Map转换成JavaBean对象<br>
     * 
     * @param bean JavaBean对象
     * @param properties Map集合
     */
    public static boolean toObject(Object bean, Map<String, Object> properties)
    {
        try
        {
            BeanUtils.populate(bean, properties);
        }
        catch (Exception e)
        {
            logger.error("把Map转换成JavaBean对象出错", e);
            return false;
        }
        return true;
    }
    
    /**
     * 把JavaBean对象转换成Map<String, String>
     * @param bean JavaBean对象
     * @return Map
     */
    public static Map<String, String> toMap(Object bean)
    {
        Map<String, String> map = null;
        try
        {
            map = BeanUtils.describe(bean);
        }
        catch (Exception e)
        {
            logger.error("把JavaBean对象转换成Map出错", e);
        }
        return map;
    }
}
