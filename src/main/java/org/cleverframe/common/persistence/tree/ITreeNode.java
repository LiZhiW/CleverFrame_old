package org.cleverframe.common.persistence.tree;

import java.io.Serializable;
import java.util.List;

/**
 * 系统树节点接口，实现此接口
 * @author LiZhiWei
 * @version 2015年6月27日 上午10:33:47
 */
public interface ITreeNode extends Serializable
{
    /** 节点ID */
    Long getId();

    /** 父节点ID */
    Long getParentId();

    /** 节点全路径 */
    String getFullPath();

    /** 判断当前节点是否被构建到树中了 */
    boolean isBulid();

    /** 设置当前节点是否构建到树中 */
    void setBulid(boolean isBulid);

    /** 返回所有子节点，必须是List否则顺序会不一致 */
    List<ITreeNode> getChildren();

    /**
     * 增加子节点
     * @param node 子节点
     */
    void addChildren(ITreeNode node);
}
