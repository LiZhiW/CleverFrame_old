package org.cleverframe.common.persistence.dao;

import java.io.Serializable;

import org.cleverframe.common.utils.IUserUtils;
import org.cleverframe.common.utils.Reflections;
import org.cleverframe.modules.sys.SysBeanNames;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Dao基类，包含HibernateDao工具类<br>
 * <b>注意：若继承该类并重写构造方法的话一定要调用super()</b>
 * 
 * @param <T> 实体类类型，特定Dao继承此类时必须指明泛型T的具体类型
 * @author LiZW
 * @version 2015年8月31日 下午9:43:11
 */
public abstract class BaseDao<T extends Serializable>
{
    /** HibernateDao工具类 */
    protected HibernateDao<T> hibernateDao;

    /** IUserUtils方便获取当前用户的组织架构信息 */
    @Autowired
    @Qualifier(SysBeanNames.UserUtils)
    protected IUserUtils userUtils;

    /**
     * 默认构造，初始化HibernateDao工具类
     */
    public BaseDao()
    {
        Class<T> entityClass = Reflections.getClassGenricType(getClass());
        hibernateDao = new HibernateDao<T>(entityClass);
    }

    /**
     * @return 获取HibernateDao工具类
     */
    public HibernateDao<T> getHibernateDao()
    {
        return hibernateDao;
    }
}
