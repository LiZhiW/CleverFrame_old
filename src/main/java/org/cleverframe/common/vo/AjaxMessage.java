package org.cleverframe.common.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.FieldError;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * ajax异步请求的响应消息<br>
 * 
 * @author LiZW
 * @version 2015年6月11日 上午9:44:11
 */
/**
 * @author LiZW
 * @version 2015年9月2日 下午4:02:45
 */
@JsonInclude(Include.NON_NULL)
public class AjaxMessage implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 下次请求是否需要验证码 */
    private boolean isNeedValidateCode = false;

    /** 请求结果是否成功 */
    private boolean success = false;

    /** 请求响应消息 */
    private String message;

    /** 异常类型名称 */
    private String exceptionName;

    /** 抛出的异常消息 */
    private String exceptionMessage;

    /** 请求成功后跳转地址 */
    private String successUrl;

    /** 表单数据验证的错误消息 */
    private List<ValidMessage> validMessageList;

    /** 请求相应数据 */
    private Object object;

    public AjaxMessage()
    {

    }

    /**
     * @param success 请求结果是否成功
     */
    public AjaxMessage(boolean success)
    {
        this(success, null, null);
    }
    
    /**
     * @param success 请求结果是否成功
     * @param message 请求响应消息
     * */
    public AjaxMessage(boolean success, String message)
    {
        this(success, message, null);
    }

    /**
     * @param success 请求结果是否成功
     * @param message 请求响应消息
     * @param e 异常对象
     * */
    public AjaxMessage(boolean success, String message, Exception e)
    {
        this.success = success;
        this.message = message;
        if (e != null)
        {
            this.exceptionName = e.getClass().toString();
            this.exceptionMessage = e.getMessage();
        }
    }

    /**
     * 增加验证错误消息<br>
     * @param validMessage 服务端数据验证错误消息
     * @return 当前对象
     */
    public AjaxMessage addValidMessage(ValidMessage validMessage)
    {
        if (this.validMessageList == null)
        {
            this.validMessageList = new ArrayList<ValidMessage>();
        }
        this.validMessageList.add(validMessage);
        return this;
    }

    /**
     * 增加验证错误消息<br>
     * @param fieldError Spring的验证错误消息
     * @return 当前对象
     */
    public AjaxMessage addValidMessage(FieldError fieldError)
    {
        ValidMessage validMessage = new ValidMessage(fieldError);
        return addValidMessage(validMessage);
    }

    /*--------------------------------------------------------------
     * 			getter、setter
     * -------------------------------------------------------------*/
    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public void setExceptionName(String exceptionName)
    {
        this.exceptionName = exceptionName;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage)
    {
        this.exceptionMessage = exceptionMessage;
    }

    public String getSuccessUrl()
    {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl)
    {
        this.successUrl = successUrl;
    }

    public List<ValidMessage> getValidMessageList()
    {
        return validMessageList;
    }

    public void setValidMessageList(List<ValidMessage> validMessageList)
    {
        this.validMessageList = validMessageList;
    }

    public boolean isNeedValidateCode()
    {
        return isNeedValidateCode;
    }

    public void setNeedValidateCode(boolean isNeedValidateCode)
    {
        this.isNeedValidateCode = isNeedValidateCode;
    }

    public Object getObject()
    {
        return object;
    }

    public void setObject(Object object)
    {
        this.object = object;
    }
}
