package org.cleverframe.common.vo;

import java.io.Serializable;
import java.util.Date;

import org.cleverframe.common.utils.DateUtils;

/**
 * 记录请求信息<br>
 * 
 * @author LiZW
 * @version 2015年6月23日 上午10:40:13
 */
public class RequestInfo implements Serializable
{
	private static final long serialVersionUID = 1L;

	/** 请求IP地址 */
	private String ip;

	/** 请求Url */
	private String url;

	/** 请求时间 */
	private Date requestTime;

	/** 请求处理时间 */
	private long processTime;

	/** 请求用户ID */
	private Long userId;

	/**
	 * @param ip 请求IP地址
	 * @param url 请求Url
	 * @param requestTime 请求时间 
	 * @param processTime 请求处理时间
	 * @param userId 请求用户ID
	 */
	public RequestInfo(String ip, String url, Date requestTime, long processTime, Long userId)
	{
		this.ip = ip;
		this.url = url;
		this.requestTime = requestTime;
		this.processTime = processTime;
		this.userId = userId;
	}

	/*--------------------------------------------------------------
	 * 			getter、setter
	 * -------------------------------------------------------------*/
	public String getIp()
	{
		return ip;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public Date getRequestTime()
	{
		return requestTime;
	}

	public void setRequestTime(Date requestTime)
	{
		this.requestTime = requestTime;
	}

	public long getProcessTime()
	{
		return processTime;
	}

	public void setProcessTime(long processTime)
	{
		this.processTime = processTime;
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

    @Override
    public String toString()
    {
        return "RequestInfo [ip=" + ip + ", url=" + url + ", requestTime=" + DateUtils.formatDateTime(requestTime) + ", processTime=" + processTime + ", userId=" + userId + "]";
    }
}
