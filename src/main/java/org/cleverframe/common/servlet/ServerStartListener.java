package org.cleverframe.common.servlet;

import java.util.Date;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.cleverframe.common.attributes.ApplicationAttributes;
import org.cleverframe.common.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 系统启动或关闭的监听器
 * 
 * @author LiZW
 * @version 2015年5月26日 下午5:36:08
 */
public class ServerStartListener implements ServletContextListener
{
    /** 日志对象 */
    private final static Logger logger = LoggerFactory.getLogger(ServerStartListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce)
    {
        // 必须调用一下：ApplicationAttributes.SERVER_START_TIME，初始化服务器启动时间
        logger.info("服务器启动时间：" + DateUtils.formatDateTime(new Date(ApplicationAttributes.SERVER_START_TIME)));

        // TODO 系统启动时的任务
        logger.info("系统启动时的任务");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce)
    {
        // TODO 系统关闭时的任务
        logger.info("系统关闭时的任务");
    }
}
