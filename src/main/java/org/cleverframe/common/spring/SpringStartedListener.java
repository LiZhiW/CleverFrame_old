package org.cleverframe.common.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * Spring容器初始化完毕事件<br>
 * 
 * @author LiZW
 * @version 2015年6月24日 下午1:27:06
 */
public class SpringStartedListener implements ApplicationListener<ContextRefreshedEvent>
{
    private final static Logger logger = LoggerFactory.getLogger(SpringStartedListener.class);
    
    
    
//    ContextRefreshedEvent   当ApplicationContext初始化或者刷新时触发该事件
//    ContextClosedEvent      当ApplicationContext被关闭时触发该事件。容器被关闭时，其管理的所有单例Bean都被销毁
//    RequestHandleEvent      在Web应用中，当一个http请求（request）结束触发该事件
//    ContestStartedEvent     Spring2.5新增的事件，当容器调用ConfigurableApplicationContext的Start()方法开始/重新开始容器时触发该事件
//    ContestStopedEvent      Spring2.5新增的事件，当容器调用ConfigurableApplicationContext的Stop()方法停止容器时触发该事件
    
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event)
    {
        // TODO Spring初始化完成后处理
        logger.info("Spring初始化完成后处理");
    }
}
