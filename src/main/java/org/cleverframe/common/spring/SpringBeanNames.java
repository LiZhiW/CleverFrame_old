package org.cleverframe.common.spring;

/**
 * 定义Spring的配置文件中配置的Bean名称，使用静态字符串<br>
 * 1.若修改Spring的配置文件，则需要同步此类<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月18日 下午9:29:12
 */
public class SpringBeanNames
{
    // -------------------------------------------------------------------------------------------//
    // spring-context.xml配置文件的Bean名称
    // -------------------------------------------------------------------------------------------//
    /** 数据库连接池Bean名称 */
    public static final String DATA_SOURCE = "dataSource";
    /** Hibernate Session工厂Bean名称 */
    public static final String SESSION_FACTORY = "sessionFactory";
    /** HibernateTemplate Bean名称 */
    public static final String HIBERNATE_TEMPLATE = "hibernateTemplate";
    /** LocalValidatorFactoryBean Bean名称 */
    public static final String VALIDATOR = "validator";
    /** EhCacheManagerFactoryBean Bean名称 */
    public static final String CACHE_MANAGER = "cacheManager";

    // -------------------------------------------------------------------------------------------//
    // spring-context-shiro.xml配置文件的Bean名称
    // -------------------------------------------------------------------------------------------//

    // -------------------------------------------------------------------------------------------//
    // spring-context-freemarker.xml配置文件的Bean名称
    // -------------------------------------------------------------------------------------------//
    /** Spring整合FreeMarker的Bean名称 */
    public static final String FREEMARKER_CONFIG = "freemarkerConfig";

    // -------------------------------------------------------------------------------------------//
    // spring-context-jedis.xml配置文件的Bean名称
    // -------------------------------------------------------------------------------------------//
    /** Redis连接池Bean名称 */
    public static final String JEDIS_POOL_CONFIG = "jedisPoolConfig";
    /** Redis连接工厂Bean名称 */
    public static final String JEDIS_CONNECTION_FACTORY = "jedisConnectionFactory";
    /** Spring提供的Redis模版Bean名称 */
    public static final String JEDIS_TEMPLATE = "jedisTemplate";
    
    // -------------------------------------------------------------------------------------------//
    // spring-context-memcached.xml配置文件的Bean名称
    // -------------------------------------------------------------------------------------------//
    /** Memcached客户端工厂Bean名称，通过此Bean名称直接得到Memcached客户端对象 */
    public static final String MEMCACHED_CLIENT = "memcachedClient";
    
    
}
