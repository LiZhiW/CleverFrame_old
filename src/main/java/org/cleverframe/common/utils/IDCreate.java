package org.cleverframe.common.utils;

import java.util.UUID;

/**
 * 封装各种生成唯一性ID算法的工具类<br>
 * 1.数据库主键值或UUID字段值的生成<br>
 * 
 * @author LiZW
 * @version 2015年5月28日 下午1:15:19
 */
public class IDCreate
{
	/**
	 * 封装JDK自带的UUID, 通过Random数字生成, 中间无"-"分割.<br>
	 * 例如：57d7058dbc79444db7e57a5d0b955cc8<br>
	 */
	public static String uuidNotSplit()
	{
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * 封装JDK自带的UUID<br>
	 * 例如：57d7058d-bc79-444d-b7e5-7a5d0b955cc8<br>
	 * */
	public static String uuid()
	{
		return UUID.randomUUID().toString();
	}
}
