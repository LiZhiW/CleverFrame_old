package org.cleverframe.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * HQL解析工具，获取Count-HQL<br>
 * 
 * @author LiZW
 * @version 2015年7月7日 下午3:40:34
 */
public class HqlParserUtils
{
    /**
     * 将hql查询转换为count查询，需要改进
     * @return 转换后的HQL字符串
     * */
    public static String hqlToCount(String hqlQuery)
    {
        if (hqlQuery != null && StringUtils.isNotBlank(hqlQuery))
        {
            hqlQuery = "select count(*) " + removeSelect(removeOrders(hqlQuery));
        }
        return hqlQuery;
    }

    /**
     * 去除HQL的select子句。
     * 
     * @param qlString
     * @return
     */
    private static String removeSelect(String qlString)
    {
        int beginPos = qlString.toLowerCase().indexOf("from");
        return qlString.substring(beginPos);
    }

    /**
     * 去除HQL的orderBy子句。
     * 
     * @param qlString
     * @return
     */
    private static String removeOrders(String qlString)
    {
        Pattern p = Pattern.compile("order\\s*by[\\w|\\W|\\s|\\S]*", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(qlString);
        StringBuffer sb = new StringBuffer();
        while (m.find())
        {
            m.appendReplacement(sb, "");
        }
        m.appendTail(sb);
        return sb.toString();
    }
}
