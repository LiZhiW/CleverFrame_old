package org.cleverframe.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * 日期工具类, 继承org.apache.commons.lang3.time.DateUtils类<br>
 * 
 * @author LiZW
 * @version 2015年5月28日 下午4:48:56
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils
{
    /** 定义可能出现的时间日期格式 */
    private static String[] parsePatterns = { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm" };

    /**
     * 得到当前系统日期字符串，格式（yyyy-MM-dd）<br>
     */
    public static String getDate()
    {
        return getDate("yyyy-MM-dd");
    }

    /**
     * 得到当前日期字符串<br>
     * @param pattern 日期格式字符串，如："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String getDate(String pattern)
    {
        return DateFormatUtils.format(new Date(), pattern);
    }

    /**
     * 得到日期字符串 默认格式（yyyy-MM-dd）
     * @param date 日期对象
     * @param pattern 日期格式，如："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String formatDate(Date date, Object... pattern)
    {
        String formatDate = null;
        if (pattern != null && pattern.length > 0)
        {
            formatDate = DateFormatUtils.format(date, pattern[0].toString());
        }
        else
        {
            formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
        }
        return formatDate;
    }

    /**
     * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
     * @param date 日期对象
     * @return 日期格式字符串，如：2015-03-01 10:21:14
     */
    public static String formatDateTime(Date date)
    {
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前时间字符串 格式（HH:mm:ss）
     * @return 当前时间字符串，如：12:14:21
     */
    public static String getTime()
    {
        return formatDate(new Date(), "HH:mm:ss");
    }

    /**
     * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
     * @return 当前时间字符串，如：2014-01-02 10:14:10
     */
    public static String getDateTime()
    {
        return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 根据时间数，得到日期字符串<br>
     * @param dateTime 时间数，可通过System.currentTimeMillis()得到
     * @param pattern 时间格式字符串，如："yyyy-MM-dd HH:mm:ss"
     * @return 时间字符串
     * */
    public static String getDate(long dateTime, String pattern)
    {
        return formatDate(new Date(dateTime), pattern);
    }

    /**
     * 根据时间数，得到日期字符串<br>
     * @param dateTime 时间数，可通过System.currentTimeMillis()得到
     * @return 时间字符串，如：2014-03-02 03:12:03
     * */
    public static String getDate(long dateTime)
    {
        return formatDate(new Date(dateTime), "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前年份字符串 格式（yyyy）
     * @return 当前年字符串，如：2014
     */
    public static String getYear()
    {
        return formatDate(new Date(), "yyyy");
    }

    /**
     * 得到当前月份字符串 格式（MM）
     * @return 当前月字符串，如：02
     */
    public static String getMonth()
    {
        return formatDate(new Date(), "MM");
    }

    /**
     * 得到当天字符串 格式（dd）
     * @return 当前天字符串，如：21
     */
    public static String getDay()
    {
        return formatDate(new Date(), "dd");
    }

    /**
     * 得到当前星期字符串 格式（E）星期几
     * @return 当前日期是星期几，如：5
     */
    public static String getWeek()
    {
        return formatDate(new Date(), "E");
    }

    /**
     * 日期型字符串转化为日期,支持格式如下：<br>
     *  "yyyy-MM-dd"<br>
     *  "yyyy-MM-dd HH:mm:ss"<br>
     *  "yyyy-MM-dd HH:mm"<br>
     *  "yyyy/MM/dd"<br>
     *  "yyyy/MM/dd HH:mm:ss"<br>
     *  "yyyy/MM/dd HH:mm"<br>
     *  
     *  @param str 日期字符串，如：2014/03/01 12:15:10
     */
    public static Date parseDate(Object str)
    {
        if (str == null)
        {
            return null;
        }
        try
        {
            return parseDate(str.toString(), parsePatterns);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    /**
     * 获取过去的天数
     * @param date 时间
     * @return "当前系统时间 - 参数时间" 的天数
     */
    public static long pastDays(Date date)
    {
        long t = new Date().getTime() - date.getTime();
        return t / (24 * 60 * 60 * 1000);
    }

    /**
     * 得到指定时间当天的开始时间<br>
     * 例如：传入"2014-01-03 08:36:21" 返回 "2014-01-03 00:00:00"
     * */
    public static Date getDayStartTime(Date date)
    {
        if (date == null)
        {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            date = sdf.parse(formatDate(date, "yyyy-MM-dd") + " 00:00:00");
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 得到指定时间当天的截止时间<br>
     * 例如：传入"2014-01-03 08:36:21" 返回 "2014-01-03 23:59:59"
     * */
    public static Date getDayEndTime(Date date)
    {
        if (date == null)
        {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            date = sdf.parse(formatDate(date, "yyyy-MM-dd") + " 23:59:59");
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 得到指定时间当小时的开始时间<br>
     * 例如：传入"2014-01-03 08:36:21" 返回 "2014-01-03 08:00:00"
     * */
    public static Date getHourStartTime(Date date)
    {
        if (date == null)
        {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            date = sdf.parse(formatDate(date, "yyyy-MM-dd HH") + ":00:00");
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 得到指定时间当小时的截止时间<br>
     * 例如：传入"2014-01-03 08:36:21" 返回 "2014-01-03 08:59:59"
     * */
    public static Date getHourEndTime(Date date)
    {
        if (date == null)
        {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            date = sdf.parse(formatDate(date, "yyyy-MM-dd HH") + ":59:59");
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

}
