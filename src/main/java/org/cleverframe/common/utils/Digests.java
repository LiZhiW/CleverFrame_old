package org.cleverframe.common.utils;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.SecureRandom;

import org.apache.commons.lang3.Validate;

/**
 * 支持SHA-1/MD5消息摘要的工具类<br>
 * 返回ByteSource，可进一步被编码为Hex, Base64或UrlSafeBase64<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月17日 下午9:12:17
 */
public class Digests
{
    public static final String SHA1 = "SHA-1";
    public static final String MD5 = "MD5";

    private static SecureRandom random = new SecureRandom();

    /**
     * 对数据进行sha1散列<br>
     * @param data 数据
     * @return sha1散列后的数据
     */
    public static byte[] sha1(byte[] data)
    {
        return digest(data, SHA1, null, 1);
    }

    /**
     * 对数据进行sha1散列<br>
     * @param data 数据
     * @param salt 散列盐
     * @return sha1散列后的数据
     */
    public static byte[] sha1(byte[] data, byte[] salt)
    {
        return digest(data, SHA1, salt, 1);
    }

    /**
     * 对数据进行sha1散列<br>
     * @param data 数据
     * @param salt 散列盐
     * @param iterations 迭代次数
     * @return sha1散列后的数据
     */
    public static byte[] sha1(byte[] data, byte[] salt, int iterations)
    {
        return digest(data, SHA1, salt, iterations);
    }

    /**
     * 对文件进行sha1散列<br>
     * @param input 输入流
     * @return sha1散列值
     * @throws IOException 
     */
    public static byte[] sha1(InputStream input) throws IOException
    {
        return digest(input, SHA1);
    }
    
    /**
     * 对数据进行md5散列<br>
     * @param data 数据
     * @return sha1散列后的数据
     */
    public static byte[] md5(byte[] data)
    {
        return digest(data, MD5, null, 1);
    }

    /**
     * 对数据进行md5散列<br>
     * @param data 数据
     * @param salt 散列盐
     * @return sha1散列后的数据
     */
    public static byte[] md5(byte[] data, byte[] salt)
    {
        return digest(data, MD5, salt, 1);
    }

    /**
     * 对数据进行md5散列<br>
     * @param data 数据
     * @param salt 散列盐
     * @param iterations 迭代次数
     * @return sha1散列后的数据
     */
    public static byte[] md5(byte[] data, byte[] salt, int iterations)
    {
        return digest(data, MD5, salt, iterations);
    }
    
    /**
     * 对文件进行md5散列<br>
     * @param input 输入流
     * @return md5散列值
     * @throws IOException 
     */
    public static byte[] md5(InputStream input) throws IOException
    {
        return digest(input, MD5);
    }

    /**
     * 对文件进行散列<br>
     * @param input 输入流
     * @param algorithm 散列算法
     * @return 散列值
     * @throws IOException
     */
    private static byte[] digest(InputStream input, String algorithm) throws IOException
    {
        try
        {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            int bufferLength = 8 * 1024;
            byte[] buffer = new byte[bufferLength];
            int read = input.read(buffer, 0, bufferLength);

            while (read > -1)
            {
                messageDigest.update(buffer, 0, read);
                read = input.read(buffer, 0, bufferLength);
            }

            return messageDigest.digest();
        }
        catch (GeneralSecurityException e)
        {
            throw Exceptions.unchecked(e);
        }
    }

    /**
     * 对字符串进行散列, 支持md5与sha1算法<br>
     * @param input 输入数据
     * @param algorithm 散列算法
     * @param salt 散列盐
     * @param iterations 迭代次数
     * @return 散列后的数据
     */
    private static byte[] digest(byte[] input, String algorithm, byte[] salt, int iterations)
    {
        try
        {
            MessageDigest digest = MessageDigest.getInstance(algorithm);

            if (salt != null)
            {
                digest.update(salt);
            }

            byte[] result = digest.digest(input);

            for (int i = 1; i < iterations; i++)
            {
                digest.reset();
                result = digest.digest(result);
            }
            return result;
        }
        catch (GeneralSecurityException e)
        {
            throw Exceptions.unchecked(e);
        }
    }

    /**
     * 生成随机的Byte[]作为salt<br>
     * @param numBytes byte数组的大小
     */
    public static byte[] generateSalt(int numBytes)
    {
        Validate.isTrue(numBytes > 0, "numBytes argument must be a positive integer (1 or larger)", numBytes);

        byte[] bytes = new byte[numBytes];
        random.nextBytes(bytes);
        return bytes;
    }
}
