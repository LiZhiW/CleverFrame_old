package org.cleverframe.common.utils;

/**
 * 用户工具接口，用于获取当前登入用户的相关信息，如当前用户、当前用户所属公司、当前用户所属机构<br>
 * 
 * @author LiZW
 * @version 2015年7月2日 上午9:55:56
 */
public interface IUserUtils
{
    /** 获取当前登入用户的登入名 */
    String getCurrentUserLoginName(); 
    
    /** 获取当前登入用户的ID */
    Long getCurrentUserId(); 
    
    /** 获取当前登入用户的所属公司ID */
    Long getCurrentCompanyId();
    
    /** 获取当前登入用户的所属机构ID */
    Long getCurrentOrgId();
}
