package org.cleverframe.common.config;

/**
 * 根据webframe.properties文件，定义全局配置属性名称以及默认值<br>
 * 1.供程序访问，以免程序访问全局配置属性值时直接传字符串不易维护<br>
 * 2.此类的成员依赖webframe.properties文件，若webframe.properties文件改变要相应的修改此类<br>
 * 
 * @author LiZW
 * @version 2015年5月25日 下午3:39:41
 */
public class GlobalPropertiesNames
{
	// -------------------------------------------------------------------------------------------//
	// 数据库配置
	// -------------------------------------------------------------------------------------------//
	/** 数据库类型，如：mysql、Oracle、h2... */
	public final static String JDBC_TYPE = "jdbc.type";

	/** 数据库类型值，默认值：mysql */
	public final static String JDBC_TYPE_DEFAULT = "mysql";

	/** 数据库驱动 */
	public final static String JDBC_DRIVER = "jdbc.driver";

	/** 数据库驱动值，默认值： com.mysql.jdbc.Driver*/
	public final static String JDBC_DRIVER_DEFAULT = "com.mysql.jdbc.Driver";

	/** 数据库地址 */
	public final static String JDBC_URL = "jdbc.url";

	/** 数据库地址值，默认值：jdbc:mysql://127.0.0.1:3306/mysql?useUnicode=true&characterEncoding=utf-8 */
	public final static String JDBC_URL_DEFAULT = "jdbc:mysql://127.0.0.1:3306/mysql?useUnicode=true&characterEncoding=utf-8";

	/** 数据库用户名 */
	public final static String JDBC_USERNAME = "jdbc.username";

	/** 数据库用户名值，默认值： root*/
	public final static String JDBC_USERNAME_DEFAULT = "root";

	/** 数据库密码 */
	public final static String JDBC_PASSWORD = "jdbc.password";

	/** 数据库密码值，默认值：root */
	public final static String JDBC_PASSWORD_DEFAULT = "root";

	/** 数据库连接池初始数 */
	public final static String JDBC_POOL_INIT = "jdbc.pool.init";
	
	/** 数据库连接池初始数，默认值：5 */
	public final static String JDBC_POOL_INIT_DEFAULT = "5";
	
	/** 数据库连接池最小大小 */
	public final static String JDBC_POOL_MINIDLE = "jdbc.pool.minIdle";

	/** 数据库连接池最小大小值，默认值：3 */
	public final static String JDBC_POOL_MINIDLE_DEFAULT = "3";

	/** 数据库连接池最大大小 */
	public final static String JDBC_POOL_MAXACTIVE = "jdbc.pool.maxActive";

	/** 数据库连接池最小大小值，默认值：20 */
	public final static String JJDBC_POOL_MAXACTIVE_DEFAULT = "20";

	/** 数据库测试使用的SQL */
	public final static String JDBC_TESTSQL = "jdbc.testSql";
	
	/** 数据库测试使用的SQL，默认值：SELECT 'x' FROM DUAL */
	public final static String JDBC_TESTSQL_DEFAULT = "SELECT 'x' FROM DUAL";
	
	// -------------------------------------------------------------------------------------------//
	// 框架配置
	// -------------------------------------------------------------------------------------------//
	/** Hibernate是否显示sql语句 */
	public final static String HIBERNATE_SHOW_SQL = "hibernate.show_sql";

	/** Hibernate是否显示sql语句值，默认值：true */
	public final static String HIBERNATE_SHOW_SQL_DEFAULT = "true";
	
	/** 最大文件上传限制 */
	public final static String WEB_MAX_UPLOAD_SIZE = "web.maxUploadSize";
	
    /** 最大文件上传限制，单位字节。默认值： 10M=10*1024*1024(B)=10485760 */
    public final static String WEB_MAX_UPLOAD_SIZE_DEFAULT = "10485760";
    
    /** 上传文件的存储路径，当storedType=1时使用，（1：当前服务器硬盘；2：FTP服务器；3：；FastDFS服务器） */
    public final static String WEB_FILE_STORAGE_PATH = "web.fileStoragePath";
    
    /** 储路上传文件的存径，默认值 */
    public final static String WEB_FILE_STORAGE_PATH_DEFAULT = "F:\\fileStoragePath";
    
    /** 上传文件到FTP的存储路径 */
    public final static String WEB_FILE_STORAGE_PATH_BY_FTP = "web.fileStoragePathByFTP";
    
    /** 存储上传文件的FTP服务器地址 */
    public final static String FTP_HOST = "FTP.host";

    /** 存储上传文件的FTP服务器端口号 */
    public final static String FTP_PORT = "FTP.port";
    
    /** 存储上传文件的FTP服务器用户名 */
    public final static String FTP_USER_NAME = "FTP.username";
    
    /** 存储上传文件的FTP服务器用户密码 */
    public final static String FTP_PASSWORD = "FTP.password";
    
    /** FastDFS 配置文件地址 */
    public final static String FASTDFS_CONF_FILE_NAME = "FastDFS.conf.filename";
    
    /** FastDFS 配置文件地址，默认值：fdfs_client.conf */
    public final static String FASTDFS_CONF_FILE_NAME_DEFAULT = "fdfs_client.conf";
    
	// -------------------------------------------------------------------------------------------//
	// 系统配置
	// -------------------------------------------------------------------------------------------//
	/** 系统名称 */
	public final static String PRODUCTNAME = "productName";
	
	/** 系统名称，默认值：WebFrame 快速开发平台 */
	public final static String PRODUCTNAME_DEFAULT = "WebFrame 快速开发平台";
	
	/** 版权年份 */
	public final static String COPYRIGHTYEAR = "copyrightYear";
	
	/** 版权年份，默认值：2014 */
	public final static String COPYRIGHTYEAR_DEFAULT = "2014";
	
	/** 系统版本号 */
	public final static String VERSION = "version";
	
	/** 系统版本号，默认值：V1.0 */
	public final static String VERSION_DEFAULT = "V1.0";
	
	/** 静态资源基路径 */
	public final static String STATIC_PATH="staticPath";
	
	/** 静态资源基路径，默认值：static */
	public final static String STATIC_PATH_DEFAULT="static";
	
	/** 系统HTML、Jsp、Freemarker等系统页面基路径 */
	public final static String VIEWS_PATH="viewsPath";
	
	/** 系统HTML、Jsp、Freemarker等系统页面基路径，默认值：views */
	public final static String VIEWS_PATH_DEFAULT="views";

	/** 系统文档基路径 */
	public final static String DOC_PATH="docPath";
	
	/** 系统文档基路径，默认值：doc */
	public final static String DOC_PATH_DEFAULT="doc";
	
	/** MVC框架的请求映射基路径 */
	public final static String MVC_PATH="mvcPath";
	
	/** MVC框架的请求映射基路径，默认值：mvc */
	public final static String MVC_PATH_DEFAULT="mvc";
	
	/** JSP视图文件存放路径 */
	public final static String JSP_VIEW_PREFIX = "web.view.prefix";

	/** JSP视图文件存放路径，默认值：/WEB-INF/views/ */
	public final static String JSP_VIEW_PREFIX_DEFAULT = "/WEB-INF/views/";

	/** JSP视图路径后缀名 */
	public final static String JSP_VIEW_SUFFIX = "web.view.suffix";

	/** JSP视图路径后缀名值，默认值：.jsp */
	public final static String JSP_VIEW_SUFFIX_DEFAULT = ".jsp";

	/** 分页查询的每页的数据条数，“-1”表示不分页 */
	public final static String PAGE_PAGESIZE = "page.pageSize";

	/** 分页查询的每页的数据条数，默认值：10 */
	public final static String PAGE_PAGESIZE_DEFAULT = "10";

	/**
	 * 得到默认的配置文件信息
	 * */
	public static String getDefaultConfig()
	{
		StringBuffer config = new StringBuffer();
		config.append("");

		return config.toString();
	}
}
