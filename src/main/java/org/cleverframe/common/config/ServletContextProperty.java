package org.cleverframe.common.config;

import javax.servlet.ServletContext;

import org.springframework.web.context.ServletContextAware;

/**
 * 管理Application范围的属性值：<br>
 * 1.设置系统版本号<br>
 * 2.设置前台页面需要引入的css、js文件的统一路径<br>
 * 
 * @author LiZhiWei
 * @version 2015年5月23日 下午2:00:15
 * */
/**
 * @author LiZhiWei
 * @version 2015年5月23日 下午2:24:18
 */
public class ServletContextProperty implements ServletContextAware
{
	/** ServletContext对象,用于管理Application范围的属性值 */
	private ServletContext servletContext;

	/** 系统静态资源的基路径 */
	public String resourceBasePath;

	/** 系统版本号 */
	public String systemVersion;

	/**
	 * 初始化系统属性值,在系统启动时调用，加入一些系统级别的属性值。
	 * */
	public void init()
	{
		// 系统版本号
		this.systemVersion = "1.0";
		this.getServletContext().setAttribute("systemVersion", systemVersion);

		this.resourceBasePath = this.getServletContext().getContextPath() + "/resource" + systemVersion;
		// 系统静态资源的基路径
		this.getServletContext().setAttribute("resourceBasePath", resourceBasePath);
	}

	@Override
	public void setServletContext(ServletContext servletContext)
	{
		this.servletContext = servletContext;
	}

	public ServletContext getServletContext()
	{
		return servletContext;
	}

	public String getResourceBasePath()
	{
		return resourceBasePath;
	}

	public String getSystemVersion()
	{
		return systemVersion;
	}
}
