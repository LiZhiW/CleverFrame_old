package org.cleverframe.modules.sys.utils;

import org.apache.shiro.SecurityUtils;
import org.cleverframe.common.utils.IUserUtils;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.shiro.ShiroPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 用户工具接口，用于获取当前登入用户的相关信息，如当前用户、当前用户所属公司、当前用户所属机构<br>
 * 
 * @author LiZW
 * @version 2015年7月2日 上午10:38:48
 */
@Component(SysBeanNames.UserUtils)
public class UserUtils implements IUserUtils
{
    /** 日志对象 */
    private final static Logger logger = LoggerFactory.getLogger(UserUtils.class);

    /**
     * 获取当前登入用户Shiro授权信息<br>
     */
    private ShiroPrincipal getCurrentShiroPrincipal()
    {
        Object object = SecurityUtils.getSubject().getPrincipal();
        if (!(object instanceof ShiroPrincipal))
        {
            logger.error("获取ShiroPrincipal失败！");
            throw new RuntimeException("获取ShiroPrincipal失败！");
        }
        return (ShiroPrincipal) object;
    }

    @Override
    public String getCurrentUserLoginName()
    {
        return getCurrentShiroPrincipal().getUser().getLoginName();
    }

    @Override
    public Long getCurrentUserId()
    {
        return getCurrentShiroPrincipal().getUser().getId();
    }

    @Override
    public Long getCurrentCompanyId()
    {
        return getCurrentShiroPrincipal().getHomeCompany().getId();
    }

    @Override
    public Long getCurrentOrgId()
    {
        return getCurrentShiroPrincipal().getHomeOrg().getId();
    }
}
