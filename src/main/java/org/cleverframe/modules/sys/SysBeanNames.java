package org.cleverframe.modules.sys;

/**
 * 定义当前sys模块定义的Spring Bean名称<br>
 * 
 * @author LiZW
 * @version 2015年11月28日 下午4:03:54
 */
public class SysBeanNames
{
    // -------------------------------------------------------------------------------------------//
    // Dao
    // -------------------------------------------------------------------------------------------//
    public static final String OrganizationDao = "sys_OrganizationDao";
    public static final String UserDao = "sys_UserDao";
    public static final String RoleDao = "sys_RoleDao";
    public static final String MenuDao = "sys_MenuDao";
    public static final String PermissionDao = "sys_PermissionDao";
    public static final String DictDao = "sys_DictDao";
    public static final String MDictDao = "sys_MDictDao";
    public static final String TemplateFileDao = "sys_TemplateFileDao";
    public static final String LogDao = "sys_LogDao";

    // -------------------------------------------------------------------------------------------//
    // Service
    // -------------------------------------------------------------------------------------------//
    public static final String OrganizationService = "sys_OrganizationService";
    public static final String UserService = "sys_UserService";
    public static final String RoleService = "sys_RoleService";
    public static final String MenuService = "sys_MenuService";
    public static final String PermissionService = "sys_PermissionService";
    public static final String DictService = "sys_DictService";
    public static final String MDictService = "sys_MDictService";
    public static final String TemplateFileService = "sys_TemplateFileService";
    public static final String LogService = "sys_LogService";
    public static final String AuthorizingRealmService = "sys_AuthorizingRealmService";

    // -------------------------------------------------------------------------------------------//
    // Other
    // -------------------------------------------------------------------------------------------//
    public static final String UserUtils = "sys_UserUtils";
}
