package org.cleverframe.modules.sys.service;

import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.dao.PermissionDao;
import org.cleverframe.modules.sys.dao.RoleDao;
import org.cleverframe.modules.sys.entity.Menu;
import org.cleverframe.modules.sys.entity.Permission;
import org.cleverframe.modules.sys.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 角色Service<br>
 * @author LiZW
 * @version 2015年6月26日 下午4:32:49
 */
@Service(SysBeanNames.RoleService)
public class RoleService extends BaseService
{
    @Autowired
    @Qualifier(SysBeanNames.RoleDao)
    private RoleDao roleDao;

    @Autowired
    @Qualifier(SysBeanNames.PermissionDao)
    private PermissionDao permissionDao;

    /**
     * 查询字典数据，使用分页<br>
     * */
    public Page<Role> findRoleByPage(Page<Role> page, String name, Long id, String uuid)
    {
        roleDao.findRoleByPage(page, name, id, uuid);
        return page;
    }

    /**
     * 保存角色对象<br>
     */
    public void saveRole(Role role)
    {
        roleDao.getHibernateDao().save(role);
    }

    /**
     * 更新角色对象<br>
     */
    public void updateRole(Role role)
    {
        roleDao.getHibernateDao().update(role);
    }

    /**
     * 删除角色对象<br>
     */
    public void deleteRole(Role role)
    {
        roleDao.getHibernateDao().delete(role);
    }

    /**
     * 查询某个公司下的的角色<br>
     * @param companyId 公司ID
     */
    public List<Role> findRoleByCompany(Long companyId)
    {
        return roleDao.findRoleByCompany(companyId);
    }

    /**
     * 查询某个公司下的的角色<br>
     * @param userId 用户ID
     */
    public List<Role> findRoleByUserId(Long userId)
    {
        return roleDao.findRoleByUserId(userId);
    }

    /**
     * 为角色增加菜单<br>
     * @param roleId 角色ID
     * @param menuId 菜单ID
     * @return 成功返回true
     */
    public boolean roleAddMenu(Long roleId, Long menuId, AjaxMessage message)
    {
        Role role = roleDao.getHibernateDao().get(roleId);
        if (role == null)
        {
            message.setMessage("角色不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != role.getDelFlag().charValue())
        {
            message.setMessage("角色已被删除");
            return false;
        }
        Menu menu = roleDao.getHibernateDao().getEntity(Menu.class, menuId);
        if (menu == null)
        {
            message.setMessage("菜单不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != menu.getDelFlag().charValue())
        {
            message.setMessage("菜单已被删除");
            return false;
        }
        if (roleDao.roleHasMenu(roleId, menuId))
        {
            message.setMessage("角色已拥有此菜单");
            return true;
        }
        roleDao.roleAddMenu(roleId, menuId);
        return true;
    }

    /**
     * 为角色删除菜单<br>
     * @param roleId 角色ID
     * @param menuId 菜单ID
     * @return 成功返回true
     */
    public boolean roleDelMenu(Long roleId, Long menuId, AjaxMessage message)
    {
        Role role = roleDao.getHibernateDao().get(roleId);
        if (role == null)
        {
            message.setMessage("角色不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != role.getDelFlag().charValue())
        {
            message.setMessage("角色已被删除");
            return false;
        }
        Menu menu = roleDao.getHibernateDao().getEntity(Menu.class, menuId);
        if (menu == null)
        {
            message.setMessage("菜单不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != menu.getDelFlag().charValue())
        {
            message.setMessage("菜单已被删除");
            return false;
        }
        if (roleDao.roleHasMenu(roleId, menuId) == false)
        {
            message.setMessage("角色不拥有此菜单");
            return true;
        }
        roleDao.roleDelMenu(roleId, menuId);
        return true;
    }

    /**
     * 为角色增加权限<br>
     * @param roleId 角色ID
     * @param permissionId 权限ID
     * @return 成功返回true
     */
    public boolean roleAddPermission(Long roleId, Long permissionId, AjaxMessage message)
    {
        Role role = roleDao.getHibernateDao().get(roleId);
        if (role == null)
        {
            message.setMessage("角色不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != role.getDelFlag().charValue())
        {
            message.setMessage("角色已被删除");
            return false;
        }
        Permission permission = roleDao.getHibernateDao().getEntity(Permission.class, permissionId);
        if (permission == null)
        {
            message.setMessage("权限不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != permission.getDelFlag().charValue())
        {
            message.setMessage("权限已被删除");
            return false;
        }
        if (roleDao.roleHasPermission(roleId, permissionId))
        {
            message.setMessage("角色已拥有权限");
            return true;
        }
        roleDao.roleAddPermission(roleId, permissionId);
        return true;
    }

    /**
     * 为角色增加所有权限<br>
     * @param roleId 角色ID
     * @param permissionType 权限类型
     * @param message
     * @return
     */
    public boolean roleAddAllPermission(Long roleId, char permissionType, AjaxMessage message)
    {
        Role role = roleDao.getHibernateDao().get(roleId);
        if (role == null)
        {
            message.setMessage("角色不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != role.getDelFlag().charValue())
        {
            message.setMessage("角色已被删除");
            return false;
        }
        List<Permission> permissionList = permissionDao.findAllPermissionByType(permissionType);
        for (Permission permission : permissionList)
        {
            if (roleDao.roleHasPermission(roleId, permission.getId()) == false)
            {
                roleDao.roleAddPermission(roleId, permission.getId());
            }
        }
        return true;
    }

    /**
     * 为角色删除权限<br>
     * @param roleId 角色ID
     * @param permissionId 权限ID
     * @return 成功返回true
     */
    public boolean roleDelPermission(Long roleId, Long permissionId, AjaxMessage message)
    {
        Role role = roleDao.getHibernateDao().get(roleId);
        if (role == null)
        {
            message.setMessage("角色不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != role.getDelFlag().charValue())
        {
            message.setMessage("角色已被删除");
            return false;
        }
        Permission permission = roleDao.getHibernateDao().getEntity(Permission.class, permissionId);
        if (permission == null)
        {
            message.setMessage("权限不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != permission.getDelFlag().charValue())
        {
            message.setMessage("权限已被删除");
            return false;
        }
        if (roleDao.roleHasPermission(roleId, permissionId) == false)
        {
            message.setMessage("角色不拥有权限");
            return true;
        }
        roleDao.roleDelPermission(roleId, permissionId);
        return true;
    }

    /**
     * 为角色删除所有权限<br>
     * @param roleId 角色ID
     * @param permissionType 权限类型
     * @param message
     * @return
     */
    public boolean roleDelAllPermission(Long roleId, char permissionType, AjaxMessage message)
    {
        Role role = roleDao.getHibernateDao().get(roleId);
        if (role == null)
        {
            message.setMessage("角色不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != role.getDelFlag().charValue())
        {
            message.setMessage("角色已被删除");
            return false;
        }
        List<Permission> permissionList = permissionDao.findAllPermissionByType(permissionType);
        for (Permission permission : permissionList)
        {
            if (roleDao.roleHasPermission(roleId, permission.getId()))
            {
                roleDao.roleDelPermission(roleId, permission.getId());
            }
        }
        return true;
    }
    
    /**
     * 为角色增加某个菜单下的所有UI权限<br>
     * @param roleId 角色ID
     * @param menuId 菜单ID
     * @param message
     * @return
     */
    public boolean roleAddAllUiPermissionByMenuId(Long roleId, Long menuId, AjaxMessage message)
    {
        Role role = roleDao.getHibernateDao().get(roleId);
        if (role == null)
        {
            message.setMessage("角色不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != role.getDelFlag().charValue())
        {
            message.setMessage("角色已被删除");
            return false;
        }
        List<Permission> permissionList = permissionDao.findPermissionByMenu(menuId, Permission.PERMISSION_TYPE_UI);
        for (Permission permission : permissionList)
        {
            if (roleDao.roleHasPermission(roleId, permission.getId()) == false)
            {
                roleDao.roleAddPermission(roleId, permission.getId());
            }
        }
        return true;
    }
    
    /**
     * 为角色删除某个菜单下的所有UI权限<br>
     * @param roleId 角色ID
     * @param menuId 菜单ID
     * @param message
     * @return
     */
    public boolean roleDelAllUiPermissionByMenuId(Long roleId, Long menuId, AjaxMessage message)
    {
        Role role = roleDao.getHibernateDao().get(roleId);
        if (role == null)
        {
            message.setMessage("角色不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != role.getDelFlag().charValue())
        {
            message.setMessage("角色已被删除");
            return false;
        }
        List<Permission> permissionList = permissionDao.findPermissionByMenu(menuId, Permission.PERMISSION_TYPE_UI);
        for (Permission permission : permissionList)
        {
            if (roleDao.roleHasPermission(roleId, permission.getId()))
            {
                roleDao.roleDelPermission(roleId, permission.getId());
            }
        }
        return true;
    }
}
