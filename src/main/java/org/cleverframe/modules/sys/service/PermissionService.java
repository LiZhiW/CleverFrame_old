package org.cleverframe.modules.sys.service;

import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.dao.PermissionDao;
import org.cleverframe.modules.sys.entity.Menu;
import org.cleverframe.modules.sys.entity.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 权限Service<br>
 * @author LiZW
 * @version 2015年6月26日 下午4:29:22
 */
@Service(SysBeanNames.PermissionService)
public class PermissionService extends BaseService
{
    @Autowired
    @Qualifier(SysBeanNames.PermissionDao)
    private PermissionDao permissionDao;
    
    /**
     * 查询权限数据<br>
     * @param page 分页对象
     * @param param 参数
     * @return 分页数据
     */
    public Page<Permission> findPermissionByPage(Page<Permission> page, Long menuId, String permission, Long id, String uuid)
    {
        permissionDao.findPermissionByPage(page, menuId, permission, id, uuid);
        return page;
    }
	
	/**
	 * 保存权限对象<br>
	 * 权限新增验证：URL权限menu_id=-1；UI权限menu_id必须存在<br>
	 * @param permission 权限对象
	 * @param message 验证失败消息
	 */
    public boolean savePermission(Permission permission, AjaxMessage message)
	{
        /* ----------------------------------权限新增验证---------------------------------- */
        if (new Character(Permission.PERMISSION_TYPE_URL).equals(permission.getPermissionType()))
        {
            if (permission.getMenuId() != null && permission.getMenuId() != -1)
            {
                message.setMessage("权限类型是“URL权限”，上级菜单不能取值(menu_id=-1)");
                return false;
            }
            permission.setMenuId(-1L);
        }
        else if (new Character(Permission.PERMISSION_TYPE_UI).equals(permission.getPermissionType()))
        {
            if(permission.getMenuId() == null)
            {
                message.setMessage("权限类型是“UI权限”，上级菜单不能为空");
                return false; 
            }
            else
            {
                Menu menu = permissionDao.getHibernateDao().getEntity(Menu.class, permission.getMenuId());
                if (menu == null || new Character(BaseEntity.DEL_FLAG_NORMAL).equals(menu.getDelFlag()) == false)
                {
                    message.setMessage("权限类型是“UI权限”，上级菜单必须存在");
                    return false;
                }
            }
        }
        else
        {
            message.setMessage("权限类型取值错误");
            return false;
        }
        /* ----------------------------------权限保存---------------------------------- */
		permissionDao.getHibernateDao().save(permission);
		return true;
	}
	
	/**
	 * 更新权限对象<br>
	 * 权限更新验证：URL权限menu_id=-1；UI权限menu_id必须存在<br>
	 * @param permission 权限对象
	 * @param message 验证失败消息
	 */
	public boolean updatePermission(Permission permission, AjaxMessage message)
	{
	    /* ----------------------------------权限更新验证---------------------------------- */
        if (new Character(Permission.PERMISSION_TYPE_URL).equals(permission.getPermissionType()))
        {
            if (permission.getMenuId() != null && permission.getMenuId() != -1)
            {
                message.setMessage("权限类型是“URL权限”，上级菜单不能取值(menu_id=-1)");
                return false;
            }
            permission.setMenuId(-1L);
        }
        else if (new Character(Permission.PERMISSION_TYPE_UI).equals(permission.getPermissionType()))
        {
            if(permission.getMenuId() == null)
            {
                message.setMessage("权限类型是“UI权限”，上级菜单不能为空");
                return false; 
            }
            else
            {
                Menu menu = permissionDao.getHibernateDao().getEntity(Menu.class, permission.getMenuId());
                if (menu == null || new Character(BaseEntity.DEL_FLAG_NORMAL).equals(menu.getDelFlag()) == false)
                {
                    message.setMessage("权限类型是“UI权限”，上级菜单必须存在");
                    return false;
                }
            }
        }
        else
        {
            message.setMessage("权限类型取值错误");
            return false;
        }
	    /* ----------------------------------权限更新---------------------------------- */
		permissionDao.getHibernateDao().update(permission);
		return true;
	}
	
	/**
	 * 删除权限对象<br>
	 * @param permission 权限对象
	 */
	public void delete(Permission permission)
	{
	    // TODO 权限是否都可以删除，如：管理员权限！！
		permissionDao.getHibernateDao().delete(permission);
	}
    
    /**
     * 获取某个菜单的权限<br>
     * @param menuId 查询参数：菜单ID
     */
    public List<Permission> findPermissionByMenu(Long menuId, char permissionType)
    {
        return permissionDao.findPermissionByMenu(menuId, permissionType);
    }

    /**
     * 获取所有的的URL权限<br>
     */
    public List<Permission> findAllPermissionByType(char permissionType)
    {
        return permissionDao.findAllPermissionByType(permissionType);
    }
    
    /**
     * 查找某个角色拥有某个菜单下的权限,根据权限类型过滤<br>
     * @return 权限集合
     */
    public List<Permission> findPermissionByRoleAndMenu(char permissionType, Long roleId, Long menuId)
    {
        return permissionDao.findPermissionByRoleAndMenu(permissionType, roleId, menuId);
    }
    
    /**
     * 查找某个角色的所有权限<br>
     */
    public List<Permission> findPermissionByRoleId(char permissionType, Long roleId)
    {
        return permissionDao.findPermissionByRoleId(permissionType, roleId);
    }
}
