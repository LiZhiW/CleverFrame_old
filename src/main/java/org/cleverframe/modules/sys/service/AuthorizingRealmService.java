package org.cleverframe.modules.sys.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.cleverframe.common.service.BaseService;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.dao.OrganizationDao;
import org.cleverframe.modules.sys.dao.PermissionDao;
import org.cleverframe.modules.sys.dao.RoleDao;
import org.cleverframe.modules.sys.dao.UserDao;
import org.cleverframe.modules.sys.entity.Organization;
import org.cleverframe.modules.sys.entity.Permission;
import org.cleverframe.modules.sys.entity.Role;
import org.cleverframe.modules.sys.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 为ShiroAuthorizingRealm提供的Service，用于到数据库查询用户、角色、权限等信息<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月20日 下午4:31:40
 */
@Service(SysBeanNames.AuthorizingRealmService)
public class AuthorizingRealmService extends BaseService
{
    @Autowired
    @Qualifier(SysBeanNames.UserDao)
    private UserDao userDao;

    @Autowired
    @Qualifier(SysBeanNames.RoleDao)
    private RoleDao roleDao;

    @Autowired
    @Qualifier(SysBeanNames.PermissionDao)
    private PermissionDao permissionDao;

    @Autowired
    @Qualifier(SysBeanNames.OrganizationDao)
    private OrganizationDao organizationDao;

    /**
     * 根据机构ID获取机构信息<br>
     * @param id 机构ID
     */
    public Organization getOrgById(Serializable id)
    {
        return organizationDao.getHibernateDao().get(id);
    }

    /**
     * 根据用户登入名查询用户<br>
     * 
     * @param loginName 用户登入名
     * */
    public User findUserByLoginName(String loginName)
    {
        return userDao.findUserByLoginName(loginName);
    }

    /**
     * 根据用户ID得到用户的所有角色信息<br>
     * 
     * @param id 用户ID
     */
    public List<Role> findRoleByUserId(Serializable userId)
    {
        return roleDao.findRoleByUserId(userId);
    }

    /**
     * 根据角色ID集合查询角色集合下所有的权限信息<br>
     * 
     * @param roleIdList 角色ID集合
     */
    public List<Permission> findPermissionByUserId(List<Serializable> roleIdList)
    {
        // TODO 根据用户ID 查询该用户拥有的所有权限
        // return permissionDao.findPermissionByRoleIds(roleIdList);
        return new ArrayList<Permission>();
    }
}
