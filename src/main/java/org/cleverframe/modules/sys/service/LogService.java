package org.cleverframe.modules.sys.service;

import org.cleverframe.common.service.BaseService;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.dao.LogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 日志Service<br>
 * @author LiZW
 * @version 2015年6月26日 下午4:36:32
 */
@Service(SysBeanNames.LogService)
public class LogService extends BaseService
{
    @Autowired
    @Qualifier(SysBeanNames.LogDao)
    private LogDao logDao;
}
