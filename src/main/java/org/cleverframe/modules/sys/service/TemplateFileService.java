package org.cleverframe.modules.sys.service;

import java.util.Map;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.core.fileupload.FileuploadBeanNames;
import org.cleverframe.core.fileupload.entity.FileInfo;
import org.cleverframe.core.fileupload.service.IStorageService;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.dao.TemplateFileDao;
import org.cleverframe.modules.sys.entity.TemplateFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 模版文件实体Service<br>
 * 
 * @author LiZW
 * @version 2015年12月25日 下午2:56:50
 */
@Service(SysBeanNames.TemplateFileService)
public class TemplateFileService extends BaseService
{
    /** 日志对象 */
    private final static Logger logger = LoggerFactory.getLogger(TemplateFileService.class);
    
    @Autowired
    @Qualifier(SysBeanNames.TemplateFileDao)
    private TemplateFileDao templateFileDao;

    @Autowired
    @Qualifier(FileuploadBeanNames.LocalStorageService)
    private IStorageService storageService;
    
    /**
     * 查询模板文件信息，使用分页<br>
     * @param page 分页数据
     * @param name 查询参数：模版名称
     * @param fileName 查询参数：文件名称
     * @param id 查询参数：数据ID
     * @param uuid 查询参数：数据UUID
     * @return 
     */
    public Page<Map<Object, Object>> findTemplateFileByPage(Page<Map<Object, Object>> page, String name, String fileName, Long id, String uuid)
    {
        return templateFileDao.findTemplateFileByPage(page, name, fileName, id, uuid);
    }

    /**
     * 增加模版文件<br>
     * 1.验证对应的上传文件(fileinfo_id)存不存在<br>
     * 2.模版名称不能重复<br>
     * @return
     */
    public boolean addTemplateFile(TemplateFile templateFile, AjaxMessage message)
    {
        /* ----------------------------------验证模版文件信息---------------------------------- */
        FileInfo fileInfo = null;
        try
        {
            fileInfo = templateFileDao.getHibernateDao().getEntity(FileInfo.class, templateFile.getFileinfoId());
            if (fileInfo != null)
            {
                fileInfo = storageService.isExists(fileInfo.getUuid());
            }
        }
        catch (Exception e)
        {
            logger.error("判断服务器端模版文件是否存在异常", e);
            message.setMessage("判断服务器端模版文件是否存在异常，请联系系统维护人员");
            return false;
        }
        if (fileInfo == null)
        {
            message.setMessage("模版文件在服务器端不存在!");
            return false;
        }
        
        if (templateFileDao.templateNameIsRepeat(null, templateFile.getName()))
        {
            message.setMessage("模版名称不能重复");
            return false;
        }
        
        /* ----------------------------------保存模版---------------------------------- */
        templateFileDao.getHibernateDao().save(templateFile);
        return true;
    }
    
    /**
     * 更新模版文件<br>
     * 1.验证对应的上传文件(fileinfo_id)存不存在<br>
     * 2.模版名称不能重复<br>
     * @return
     */
    public boolean updateTemplateFile(TemplateFile templateFile, AjaxMessage message)
    {
        /* ----------------------------------验证模版文件信息---------------------------------- */
        TemplateFile oldTemplateFile = templateFileDao.getHibernateDao().get(templateFile.getId());
        if (oldTemplateFile == null)
        {
            message.setMessage("模版不存在");
            return false;
        }

        FileInfo fileInfo = null;
        try
        {
            fileInfo = templateFileDao.getHibernateDao().getEntity(FileInfo.class, templateFile.getFileinfoId());
            if (fileInfo != null)
            {
                fileInfo = storageService.isExists(fileInfo.getUuid());
            }
        }
        catch (Exception e)
        {
            logger.error("判断服务器端模版文件是否存在异常", e);
            message.setMessage("判断服务器端模版文件是否存在异常，请联系系统维护人员");
            return false;
        }
        if (fileInfo == null)
        {
            message.setMessage("模版文件在服务器端不存在!");
            return false;
        }

        if (oldTemplateFile.getName().equals(templateFile.getName()) == false)
        {
            if (templateFileDao.templateNameIsRepeat(templateFile.getId(), templateFile.getName()))
            {
                message.setMessage("模版名称不能重复");
                return false;
            }
        }
        
        /* ----------------------------------更新模版文件信息---------------------------------- */
        templateFileDao.getHibernateDao().getSession().evict(oldTemplateFile);
        templateFileDao.getHibernateDao().update(templateFile);
        return true;
    }
    
    /**
     * 删除模板文件<br>
     * 1.一同删除关联的"上传文件信息表"数据<br>
     * @return
     */
    public boolean deleteTemplateFile(TemplateFile templateFile, AjaxMessage message)
    {
        TemplateFile oldTemplateFile = templateFileDao.getHibernateDao().get(templateFile.getId());
        if (oldTemplateFile == null)
        {
            message.setMessage("模版不存在");
            return false;
        }
        
        templateFileDao.getHibernateDao().delete(oldTemplateFile);
        FileInfo fileInfo = null;
        try
        {
            fileInfo = templateFileDao.getHibernateDao().getEntity(FileInfo.class, oldTemplateFile.getFileinfoId());
            if (fileInfo != null)
            {
                fileInfo = storageService.isExists(fileInfo.getUuid());
                if (fileInfo != null)
                {
                    storageService.deleteFile(fileInfo.getUuid(), false);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("操作服务端上传文件出错", e);
        }
        return true;
    }
    
    /**
     * 根据模版名称查询模版<br>
     * @param name 查询参数：模版名称
     * @return 模版文件信息,未找到返回null
     */
    public FileInfo getTemplateFileByName(String name)
    {
        FileInfo fileInfo = null;
        TemplateFile templateFile = templateFileDao.getTemplateFileByName(name);
        if (templateFile != null)
        {
            fileInfo = templateFileDao.getHibernateDao().getEntity(FileInfo.class, templateFile.getFileinfoId());
            try
            {
                if (fileInfo != null)
                {
                    fileInfo = storageService.isExists(fileInfo.getUuid());
                }
            }
            catch (Exception e)
            {
                logger.error("操作服务端上传文件出错", e);
            }
        }
        return fileInfo;
    }
}
