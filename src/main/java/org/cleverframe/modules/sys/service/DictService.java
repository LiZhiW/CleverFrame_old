package org.cleverframe.modules.sys.service;

import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.dao.DictDao;
import org.cleverframe.modules.sys.entity.Dict;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 系统字典Service<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月6日 上午10:01:56
 */
@Service(SysBeanNames.DictService)
public class DictService extends BaseService
{
    @Autowired
    @Qualifier(SysBeanNames.DictDao)
    private DictDao dictDao;

    /**
     * 分页查询字典<br>
     * */
    public Page<Dict> findDictByPage(Page<Dict> page, String dictKey, String dictType, String dictValue, Long id, String uuid)
    {
        dictDao.findDictByPage(page, dictKey, dictType, dictValue, id, uuid);
        return page;
    }
    
    /**
     * 保存字典对象
     * @param dict 字典对象
     */
    public void saveDict(Dict dict)
    {
        dictDao.getHibernateDao().save(dict);
    }
    
    /**
     * 更新字典对象
     * @param dict 字典对象
     */
    public void updateDict(Dict dict)
    {
        dictDao.getHibernateDao().update(dict);
    }
    
    /**
     * 删除字典对象
     * @param dict 字典对象
     */
    public void deleteDict(Dict dict)
    {
        dictDao.getHibernateDao().delete(dict);
    }
    
    /**
     * 根据字典类别查询字典对象
     * @return 字典对象集合
     */
    public List<Dict> findDictByType(String dictType)
    {
        return dictDao.findDictByType(dictType);
    }
    
    
    /**
     * 查询字典的所有类型
     */
    public List<String> findDictAllType()
    {
        return dictDao.findDictAllType();
    }
}
