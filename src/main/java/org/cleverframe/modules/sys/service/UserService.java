package org.cleverframe.modules.sys.service;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.dao.RoleDao;
import org.cleverframe.modules.sys.dao.UserDao;
import org.cleverframe.modules.sys.entity.Organization;
import org.cleverframe.modules.sys.entity.Role;
import org.cleverframe.modules.sys.entity.User;
import org.cleverframe.modules.sys.shiro.ShiroPasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 用户登入处理逻辑
 * 
 * @author LiZhiWei
 * @version 2015年5月23日 下午5:10:46
 */
@Service(SysBeanNames.UserService)
public class UserService extends BaseService
{
    @Autowired
    @Qualifier(SysBeanNames.UserDao)
    private UserDao userDao;

    @Autowired
    @Qualifier(SysBeanNames.RoleDao)
    private RoleDao roleDao;
    
    /**
     * 分页查询用户数据<br>
     * @param page 分页对象
     * @param param 查询参数
     * @return 分页数据
     */
    public Page<User> findUserByPage(Page<User> page, String name,String logiName,String userType,Long id,String uuid)
    {
        userDao.findUserByPage(page, name, logiName, userType, id, uuid);
        return page;
    }

    /**
     * 保存用户对象，同步方法<br>
     * 1.用户密码处理<br>
     * 2.用户工号生成，需要同步<br>
     */
    public synchronized boolean addUser(User user, AjaxMessage message)
    {
        /* ----------------------------------验证用户“归属公司”和“直属机构”关系---------------------------------- */
        Organization homeCompany = userDao.getHibernateDao().getEntity(Organization.class, user.getHomeCompany());
        Organization homeorg = userDao.getHibernateDao().getEntity(Organization.class, user.getHomeOrg());
        if (homeorg.getFullPath().startsWith(homeCompany.getFullPath()) == false)
        {
            message.setMessage("直属机构[" + homeorg.getName() + "]不属于归属公司[" + homeCompany.getName() + "]");
            return false;
        }
        
        /* ----------------------------------保存用户---------------------------------- */
        // 用户密码处理
        user.setPassword(ShiroPasswordUtils.Encryption(user.getPassword()));
        // 用户工号生成，格式：前缀 + 固定长度数字 + 后缀
        String maxJobNo = userDao.getMaxUserJobNo();
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < User.NO_NUMBER_LENGTH; i++)
        {
            str.append('0');
        }
        if (StringUtils.isBlank(maxJobNo))
        {
            // 第一个员工号
            str.append('1');
        }
        else
        {
            maxJobNo = maxJobNo.replace(User.NO_PREFIX, "").replace(User.NO_SUFFIX, "");
            int max = NumberUtils.toInt(maxJobNo) + 1;
            str.append(max);
        }
        maxJobNo = str.toString().substring(str.length() - User.NO_NUMBER_LENGTH);
        maxJobNo = User.NO_PREFIX + maxJobNo + User.NO_SUFFIX;
        user.setJobNo(maxJobNo);
        userDao.getHibernateDao().save(user);
        return true;
    }

    /**
     * 更新用户对象<br>
     * 用户更新不能修改的字段：login_name、job_no、login_ip、login_date、<br>
     */
    public boolean updateUser(User user, AjaxMessage message)
    {
        /* ----------------------------------验证更新---------------------------------- */
        User oldUser = userDao.getHibernateDao().get(user.getId());
        if (!oldUser.getLoginName().equals(user.getLoginName()))
        {
            message.setMessage("不能修改登录名");
            return false;
        }
        if (!oldUser.getJobNo().equals(user.getJobNo()))
        {
            message.setMessage("不能修改工号");
            return false;
        }
        if (!oldUser.getLoginIp().equals(user.getLoginIp()))
        {
            message.setMessage("不能修改最后登陆IP");
            return false;
        }
        if (user.getLoginDate() == null || oldUser.getLoginDate().getTime() != user.getLoginDate().getTime())
        {
            message.setMessage("不能修改最后登陆时间");
            return false;
        }
        /* ----------------------------------更新数据---------------------------------- */
        userDao.getHibernateDao().getSession().evict(oldUser);
        userDao.getHibernateDao().update(user);
        return true;
    }

    /**
     * 删除用户对象<br>
     */
    public boolean deleteUser(User user, AjaxMessage message)
    {
        // TODO 删除其他正在登录使用系统的用户？？
        if (userUtils.getCurrentUserId().equals(user.getId()))
        {
            message.setMessage("不能删除当前用户(自己)");
            return false;
        }
        user.setAccountState(User.ACCOUNT_STATE_DELETE);
        userDao.getHibernateDao().update(user);
        return true;
    }
    
    /**
     * 查询某机构下的直属用户<br>
     * @param param 查询参数：直属机构ID
     * @return 返回某机构下的所有直属用户
     */
    public List<User> findUserByHomeOrg(Long homeOrg)
    {
        return userDao.findUserByHomeOrg(homeOrg);
    }
    
    /**
     * 为用户增加一个角色<br>
     * @param userId 用户ID
     * @param roleId 角色ID
     * @return 成功返回true
     */
    public boolean userAddRole(Long userId, Long roleId, AjaxMessage message)
    {
        User user = userDao.getHibernateDao().get(userId);
        if (user == null)
        {
            message.setMessage("用户不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != user.getDelFlag().charValue())
        {
            message.setMessage("用户已被删除");
            return false;
        }
        if (User.ACCOUNT_STATE_NORMAL != user.getAccountState().charValue())
        {
            message.setMessage("用户帐号状态不可用");
            return false;
        }
        if (User.USER_STATE_LEAVE == user.getUserState().charValue())
        {
            message.setMessage("用户已离职");
            return false;
        }
        Role role = userDao.getHibernateDao().getEntity(Role.class, roleId);
        if (role == null)
        {
            message.setMessage("角色不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != role.getDelFlag().charValue())
        {
            message.setMessage("角色已被删除");
            return false;
        }
        if (role.getCompanyId() != user.getHomeCompany())
        {
            message.setMessage("用户与角色不在同一个公司");
            return false;
        }
        if (userDao.userHasRole(userId, roleId))
        {
            message.setMessage("用户已经拥有此角色");
            return true;
        }
        userDao.userAddRole(userId, roleId);
        return true;
    }
    
    /**
     * 为用户增加所有角色<br>
     * @param userId 用户ID
     * @return 成功返回true
     */
    public boolean userAddAllRole(Long userId, AjaxMessage message)
    {
        User user = userDao.getHibernateDao().get(userId);
        if (user == null)
        {
            message.setMessage("用户不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != user.getDelFlag().charValue())
        {
            message.setMessage("用户已被删除");
            return false;
        }
        if (User.ACCOUNT_STATE_NORMAL != user.getAccountState().charValue())
        {
            message.setMessage("用户帐号状态不可用");
            return false;
        }
        if (User.USER_STATE_LEAVE == user.getUserState().charValue())
        {
            message.setMessage("用户已离职");
            return false;
        }
        if (userUtils.getCurrentCompanyId() != user.getHomeCompany())
        {
            message.setMessage("用户与角色不在同一个公司");
            return false;
        }
        List<Role> roleList = roleDao.findRoleByCompany(userUtils.getCurrentCompanyId());
        for (Role role : roleList)
        {
            if (userDao.userHasRole(userId, role.getId()) == false)
            {
                userDao.userAddRole(userId, role.getId());
            }
        }
        return true;
    }
    
    /**
     * 为用户增加所有角色<br>
     * @param userId 用户ID
     * @return 成功返回true
     */
    public boolean userDelAllRole(Long userId, AjaxMessage message)
    {
        User user = userDao.getHibernateDao().get(userId);
        if (user == null)
        {
            message.setMessage("用户不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != user.getDelFlag().charValue())
        {
            message.setMessage("用户已被删除");
            return false;
        }
        if (User.ACCOUNT_STATE_NORMAL != user.getAccountState().charValue())
        {
            message.setMessage("用户帐号状态不可用");
            return false;
        }
        if (User.USER_STATE_LEAVE == user.getUserState().charValue())
        {
            message.setMessage("用户已离职");
            return false;
        }
        if (userUtils.getCurrentCompanyId() != user.getHomeCompany())
        {
            message.setMessage("用户与角色不在同一个公司");
            return false;
        }
        List<Role> roleList = roleDao.findRoleByCompany(userUtils.getCurrentCompanyId());
        for (Role role : roleList)
        {
            if (userDao.userHasRole(userId, role.getId()))
            {
                userDao.userDelRole(userId, role.getId());
            }
        }
        return true;
    }
    
    /**
     * 为用户删除一个角色<br>
     * @param userId 用户ID
     * @param roleId 角色ID
     * @return 成功返回true
     */
    public boolean userDelRole(Long userId, Long roleId, AjaxMessage message)
    {
        User user = userDao.getHibernateDao().get(userId);
        if (user == null)
        {
            message.setMessage("用户不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != user.getDelFlag().charValue())
        {
            message.setMessage("用户已被删除");
            return false;
        }
        if (User.ACCOUNT_STATE_NORMAL != user.getAccountState().charValue())
        {
            message.setMessage("用户帐号状态不可用");
            return false;
        }
        if (User.USER_STATE_LEAVE == user.getUserState().charValue())
        {
            message.setMessage("用户已离职");
            return false;
        }
        Role role = userDao.getHibernateDao().getEntity(Role.class, roleId);
        if (role == null)
        {
            message.setMessage("角色不存在");
            return false;
        }
        if (BaseEntity.DEL_FLAG_NORMAL != role.getDelFlag().charValue())
        {
            message.setMessage("角色已被删除");
            return false;
        }
        if (role.getCompanyId() != user.getHomeCompany())
        {
            message.setMessage("用户与角色不在同一个公司");
            return false;
        }
        if (userDao.userHasRole(userId, roleId) == false)
        {
            message.setMessage("用户不拥有此角色");
            return true;
        }
        userDao.userDelRole(userId, roleId);
        return true;
    }
    
    /**
     * 模糊查询用户信息，使用分页<br>
     * */
    public Page<User> findUserByParam(Page<User> page, String q)
    {
        return userDao.findUserByParam(page, q);
    }
    
    /**
     * 根据ID获取用户信息<br>
     * @param id 用户id
     */
    public User getUserById(Serializable id)
    {
        return userDao.getHibernateDao().get(id);
    }
}
