package org.cleverframe.modules.sys;

/**
 * 当前sys模块对应的JSP文件URL路径<br>
 * 1.注意：此类只保存JSP文件的URL<br>
 * @author LiZhiWei
 * @version 2015年6月25日 下午10:03:42
 */
public class SysJspUrlPath
{
    /** 用户登入页面 */
    public static final String SysLogin = "modules/sys/Login";
    
    /** sys模块字典管理页面 */
    public static final String SysDict = "modules/sys/Dict";
    
    /** sys模块管理页面 */
    public static final String SysMain = "modules/sys/SysMain";

    /** sys模块机构管理页面 */
    public static final String SysOrganization = "modules/sys/Organization";
    
    /** sys模块用户管理页面 */
    public static final String SysUser = "modules/sys/User";
    
    /** sys模块角色管理页面 */
    public static final String SysRole = "modules/sys/Role";
    
    /** sys模块菜单管理页面 */
    public static final String SysMenu = "modules/sys/Menu";
    
    /** sys模块权限管理页面 */
    public static final String SysPermission = "modules/sys/Permission";
    
    /** sys模块多级字典管理页面 */
    public static final String SysMDict = "modules/sys/MDict";
    
    /** sys模块用户、角色、菜单、权限管理页面 */
    public static final String SysUserRoleMenuPermission = "modules/sys/UserRoleMenuPermission";
    
    /** 系统模版管理页面 */
    public static final String SysTemplateFile = "modules/sys/TemplateFile";
    
}
