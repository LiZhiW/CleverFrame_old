package org.cleverframe.modules.sys.attributes;

/**
 * 记录sys模块向ServletContext中加入的Seesion范围的属性值的名称
 * 
 * @author LiZhiWei
 * @version 2015年6月10日 下午9:42:59
 */
public class SysSessionAttributes
{
	/** 用户连续登入失败次数,类型:int */
	public final static String LOGIN_FAILED_COUNT = "Sys_Login_Failed_Count";

	/** 当前登入的用户,类型:User */
	public final static String LOGIN_USER = "Sys_Login_User";

	/** 当前登入用户的所属公司,类型:Organization */
	public final static String CURRENT_COMPANY = "Sys_Current_Company";

	/** 当前登入用户的归属机构,类型:Organization */
	public final static String CURRENT_ORG = "Sys_Current_Org";

	// ......
}
