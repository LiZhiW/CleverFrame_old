package org.cleverframe.modules.sys.dao;

import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.entity.Permission;
import org.springframework.stereotype.Repository;

/**
 * 权限Dao<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月21日 下午8:28:05
 */
@Repository(SysBeanNames.PermissionDao)
public class PermissionDao extends BaseDao<Permission>
{
    /**
     * 查询权限数据<br>
     * @param page 分页对象
     * @param param 参数
     * @return 分页数据
     */
    public Page<Permission> findPermissionByPage(Page<Permission> page, Long menuId, String permission, Long id, String uuid)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("menuId", menuId);
        param.put("permission", permission);
        param.put("id", id);
        param.put("uuid", uuid);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.PermissionDao.findPermissionByPage");
        hibernateDao.findBySql(page, sql, param);
        return page;
    }
    
    /**
     * 获取某个菜单的权限<br>
     * @param menuId 查询参数：菜单ID
     * @param permissionType 查询参数：权限类型
     * @return 权限集合
     */
    public List<Permission> findPermissionByMenu(Long menuId, char permissionType)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("menuId", menuId);
        param.put("permissionType", permissionType);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.PermissionDao.findPermissionByMenu");
        return hibernateDao.findBySql(sql, param);
    }
    
    /**
     * 获取所有权限,根据权限类型过滤<br>
     * @param permissionType 查询参数：权限类型
     * @return 权限集合
     */
    public List<Permission> findAllPermissionByType(char permissionType)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("permissionType", permissionType);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.PermissionDao.findAllPermissionByType");
        return hibernateDao.findBySql(sql, param);
    }
    
    /**
     * 查找某个角色拥有某个菜单下的权限,根据权限类型过滤<br>
     * @param roleId 查询参数：角色ID
     * @param menuId 查询参数：菜单ID
     * @param permissionType 查询参数：权限类型
     * @return UI权限集合
     */
    public List<Permission> findPermissionByRoleAndMenu(char permissionType, Long roleId, Long menuId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("permissionType", permissionType);
        param.put("roleId", roleId);
        param.put("menuId", menuId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.PermissionDao.findPermissionByRoleAndMenu");
        return hibernateDao.findBySql(sql, param);
    }
    

    /**
     * 查找某个角色的所有权限<br>
     * @param permissionType 查询参数：权限类型
     * @param roleId 查询参数：角色ID
     * @return 权限集合
     */
    public List<Permission> findPermissionByRoleId(char permissionType, Long roleId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("permissionType", permissionType);
        param.put("roleId", roleId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.PermissionDao.findPermissionByRoleId");
        return hibernateDao.findBySql(sql, param);
    }
}
