package org.cleverframe.modules.sys.dao;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.entity.TemplateFile;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 * 模版文件实体Dao<br>
 * 
 * @author LiZW
 * @version 2015年12月25日 下午2:54:21
 */
@Repository(SysBeanNames.TemplateFileDao)
public class TemplateFileDao extends BaseDao<TemplateFile>
{

    /**
     * 查询模板文件信息，使用分页<br>
     * @param page 分页数据
     * @param name 查询参数：模版名称
     * @param fileName 查询参数：文件名称
     * @param id 查询参数：数据ID
     * @param uuid 查询参数：数据UUID
     * @return 
     */
    public Page<Map<Object, Object>> findTemplateFileByPage(Page<Map<Object, Object>> page, String name, String fileName, Long id, String uuid)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("name", name);
        param.put("fileName", fileName);
        param.put("id", id);
        param.put("uuid", uuid);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.TemplateFileDao.findTemplateFileByPage");
        hibernateDao.findMapBySql(page, sql, param);
        return page;
    }
    
    /**
     * 判断模版名称是否重复<br>
     * @param id 查询参数：排除的数据ID，若不排除任何数据使用null
     * @param name 模版名称
     * @return 模版名称重复返回true
     */
    public boolean templateNameIsRepeat(Serializable id, String name)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("id", id);
        param.put("name", name);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.TemplateFileDao.templateNameIsRepeat");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        int count = NumberUtils.toInt(object.toString(), 0);
        return count > 0;
    }
    
    /**
     * 根据模版名称查询模版<br>
     * @param name 查询参数：模版名称
     * @return 模版数据
     */
    public TemplateFile getTemplateFileByName(String name)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("name", name);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.TemplateFileDao.getTemplateFileByName");
        return hibernateDao.getBySql(sql, param);
    }
}
