package org.cleverframe.modules.sys.dao;

import java.util.List;

import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.entity.MDict;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

/**
 * 多级字典Dao<br>
 * @author LiZhiWei
 * @version 2015年6月21日 下午8:25:50
 */
@Repository(SysBeanNames.MDictDao)
public class MDictDao extends BaseDao<MDict>
{
    /**
     * 查询多级字典，不分页<br>
     * @param type 查询参数：字典类型
     * @param id 查询参数：字典ID
     * @param uuid 查询参数：数据UUID
     * @return 多级字典集合
     */
    public List<MDict> findMDictByType(String mdictType , Long id, String uuid)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("mdictType", mdictType);
        param.put("id", id);
        param.put("uuid", uuid);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MDictDao.findMDictByType");
        return hibernateDao.findBySql(sql, param);
    }
    
    /**
     * 获取某一类型的字典数据，排除当前字典以及当前字典的所有下级字典<br>
     * @param mdictType 查询参数：字典类型
     * @param fullPath 查询参数：字典全路径
     */
    public List<MDict> getMDictTreeExcludeOneself(String mdictType, String fullPath)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("mdictType", mdictType);
        param.put("fullPath", fullPath);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MDictDao.getMDictTreeExcludeOneself");
        return hibernateDao.findBySql(sql, param);
    }
    
    /**
     * 删除字典，以及其下级字典<br>
     * @param fullPath 全路径 + %
     * @return 删除数据数
     */
    public int deleteMDict(String fullPath)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_DELETE, userUtils.getCurrentCompanyId());
        param.put("fullPath", fullPath);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MDictDao.deleteMDict");
        SQLQuery query = hibernateDao.createSqlQuery(sql, param);
        return query.executeUpdate();
    }
    
    /**
     * 查询多级字典所有字典类型<br>
     * @return 所有字典类型集合
     */
    public List<String> findMDictAllType()
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MDictDao.findMDictAllType");
        SQLQuery query =  hibernateDao.createSqlQuery(sql, param);
        @SuppressWarnings("unchecked")
        List<String> list = query.list();
        return list;
    }
    
    /**
     * 更新多级字典字典类型，会同时更新其所有子节点的字典类型<br>
     * @param fullPath 查询参数：字典全路径
     * @param mdictType 更新参数：新的字典类型
     * @return 更新数量
     */
    public int updateMDictType(String fullPath, String mdictType)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("fullPath", fullPath);
        param.put("mdictType", mdictType);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MDictDao.updateMDictType");
        SQLQuery query = hibernateDao.createSqlQuery(sql, param);
        return query.executeUpdate();
    }

    /**
     * 当某个节点位置发生变化，需要更新它的FullPath和它所有子节点的FullPath属性<br>
     * FullPath更新： full_path = CONCAT( :parentFullPath, SUBSTRING( full_path, :fullPathSubstringBegin ) )<br>
     * @param fullPath 查询参数：菜单路径 + %
     * @param parentFullPath 更新参数：新的子节点路径前缀
     * @param fullPathSubstringBegin 更新参数：截取原数据的FullPath的起始位置
     * @return 更新数量
     */
    public int updateFullPath(String fullPath, String parentFullPath, int fullPathSubstringBegin)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("fullPath", fullPath);
        param.put("parentFullPath", parentFullPath);
        param.put("fullPathSubstringBegin", fullPathSubstringBegin);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MDictDao.updateFullPath");
        SQLQuery query = hibernateDao.createSqlQuery(sql, param);
        return query.executeUpdate();
    }
}
