package org.cleverframe.modules.sys.dao;

import java.util.List;

import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.entity.Menu;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

/**
 * 系统菜单DAO<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月4日 下午9:26:58
 */
@Repository(SysBeanNames.MenuDao)
public class MenuDao extends BaseDao<Menu>
{
	/**
	 * 返回所有系统菜单<br>
	 * */
    public List<Menu> getAllMenu()
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MenuDao.getAllMenu");
        return hibernateDao.findBySql(sql, param);
    }
	
	/**
	 * 查询系统菜单<br>
	 * @param category 查询参数：菜单类别
	 * @param menuType 查询参数：菜单类型
	 * @param name 查询参数：菜单名称
	 * @param id 查询参数：数据ID
	 * @param uuid 查询参数：数据UUID
	 * @return 返回菜单集合
	 */
    public List<Menu> findMenu(String category, String menuType, String name, Long id, String uuid)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("category", category);
        param.put("menuType", menuType);
        param.put("name", name);
        param.put("id", id);
        param.put("uuid", uuid);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MenuDao.findMenu");
        return getHibernateDao().findBySql(sql, param);
    }
	
    /**
     * 获取所有菜单信息，排除当前菜单以及当前菜单的所有下级菜单<br>
     * @param fullPath 当前菜单全路径
     */
    public List<Menu> getMenuTreeExcludeOneself(String fullPath)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("fullPath", fullPath);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MenuDao.getMenuTreeExcludeOneself");
        return hibernateDao.findBySql(sql, param);
    }
    
	/**
	 * 获取特定类别的菜单<br>
	 * @param category 查询参数：菜单类别
	 */
    public List<Menu> findMenuByCategory(String category)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("category", category);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MenuDao.findMenuByCategory");
        return hibernateDao.findBySql(sql, param);
    }
	
	/**
	 * 删除菜单对象<br>
	 * @param fullPath 全路径 + %
	 */
    public int deleteMenu(String fullPath)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_DELETE);
        param.put("fullPath", fullPath);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MenuDao.deleteMenu");
        SQLQuery query = hibernateDao.createSqlQuery(sql, param);
        return query.executeUpdate();
    }
    
    /**
     * 更新当前菜单和其所有子菜单的“菜单类别、菜单类型”<br>
     * @param fullPath 查询参数：菜单路径 + %
     * @param category 更新参数：菜单类别
     * @param menuType 更新参数：菜单类型
     * @return 更新数量
     */
    public int updateMenuCategoryAndType(String fullPath, String category, char menuType)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("fullPath", fullPath);
        param.put("category", category);
        param.put("menuType", menuType);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MenuDao.updateMenuCategoryAndType");
        SQLQuery query = hibernateDao.createSqlQuery(sql, param);
        return query.executeUpdate();
    }
    
    /**
     * 当某个节点位置发生变化，需要更新它的FullPath和它所有子节点的FullPath属性<br>
     * FullPath更新： full_path = CONCAT( :parentFullPath, SUBSTRING( full_path, :fullPathSubstringBegin ) )<br>
     * @param fullPath 查询参数：菜单路径 + %
     * @param parentFullPath 更新参数：新的子节点路径前缀
     * @param fullPathSubstringBegin 更新参数：截取原数据的FullPath的起始位置
     * @return 更新数量
     */
    public int updateFullPath(String fullPath, String parentFullPath, int fullPathSubstringBegin)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("fullPath", fullPath);
        param.put("parentFullPath", parentFullPath);
        param.put("fullPathSubstringBegin", fullPathSubstringBegin);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MenuDao.updateFullPath");
        SQLQuery query = hibernateDao.createSqlQuery(sql, param);
        return query.executeUpdate();
    }
    
    /**
     * 获取角色拥有的菜单<br>
     * @param roleId 角色ID
     * @return 菜单集合
     */
    public List<Menu> findMenuByRoleId(Long roleId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("roleId", roleId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MenuDao.findMenuByRoleId");
        return hibernateDao.findBySql(sql, param);
    }
    
    /**
     * 获取用户拥有的所有菜单<br>
     * @param userId 查询参数：用户ID
     * @return 菜单集合
     */
    public List<Menu> findMenuByUserId(Long userId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("userId", userId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MenuDao.findMenuByUserId");
        return hibernateDao.findBySql(sql, param);
    }
    
    /**
     * 获取用户拥有的菜单，根据菜单类别过滤<br>
     * @param userId 查询参数：用户ID
     * @param category 查询参数：菜单类别
     * @return 菜单集合
     */
    public List<Menu> findMenuByUserIdAndCategory(Long userId, String category)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("userId", userId);
        param.put("category", category);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.MenuDao.findMenuByUserIdAndCategory");
        return hibernateDao.findBySql(sql, param);
    }
}
