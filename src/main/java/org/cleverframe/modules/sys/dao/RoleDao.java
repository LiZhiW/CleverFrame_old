package org.cleverframe.modules.sys.dao;

import java.io.Serializable;
import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.entity.Role;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

/**
 * 角色Dao<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月21日 下午8:29:51
 */
@Repository(SysBeanNames.RoleDao)
public class RoleDao extends BaseDao<Role>
{
    /**
     * 根据用户ID得到用户的所有角色信息<br>
     * @param userId 查询参数：用户ID
     * @return 用户的所有角色
     */
    public List<Role> findRoleByUserId(Serializable userId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("userId", userId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.RoleDao.findRoleByUserId");
        return hibernateDao.findBySql(sql, param);
    }
	
    /**
     * 查询字典数据，使用分页<br>
     * @param page 分页数据
     * @param name 查询参数：角色名称
     * @param id 查询参数：数据ID
     * @param uuid 查询参数：数据UUID
     * */
    public Page<Role> findRoleByPage(Page<Role> page, String name, Long id, String uuid)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("name", name);
        param.put("id", id);
        param.put("uuid", uuid);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.RoleDao.findRoleByPage");
        hibernateDao.findBySql(page, sql, param);
        return page;
    }
    
    /**
     * 查询某个公司下的的角色<br>
     * @param companyId 查询参数：公司ID
     * @param 角色集合
     */
    public List<Role> findRoleByCompany(Long companyId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("companyId", companyId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.RoleDao.findRoleByCompany");
        return hibernateDao.findBySql(sql, param);
    }
    
    /**
     * 判断角色是否拥有菜单<br>
     * @param roleId 角色ID
     * @param menuId 菜单ID
     * @return 角色拥有菜单返回true
     */
    public boolean roleHasMenu(Long roleId, Long menuId)
    {
        Parameter param = new Parameter();
        param.put("roleId", roleId);
        param.put("menuId", menuId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.RoleDao.roleHasMenu");
        SQLQuery sQLQuery = hibernateDao.createSqlQuery(sql, param);
        List<?> list = sQLQuery.list();
        return (list.size() > 0);
    }
    
    /**
     * 为角色增加菜单<br>
     * @param roleId 角色ID
     * @param menuId 菜单ID
     * @return 返回更新数据数量
     */
    public int roleAddMenu(Long roleId, Long menuId)
    {
        Parameter param = new Parameter();
        param.put("roleId", roleId);
        param.put("menuId", menuId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.RoleDao.roleAddMenu");
        SQLQuery sQLQuery = hibernateDao.createSqlQuery(sql, param);
        return sQLQuery.executeUpdate();
    }
    
    /**
     * 为角色删除菜单<br>
     * @param roleId 角色ID
     * @param menuId 菜单ID
     * @return 返回更新数据数量
     */
    public int roleDelMenu(Long roleId, Long menuId)
    {
        Parameter param = new Parameter();
        param.put("roleId", roleId);
        param.put("menuId", menuId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.RoleDao.roleDelMenu");
        SQLQuery sQLQuery = hibernateDao.createSqlQuery(sql, param);
        return sQLQuery.executeUpdate();
    }
    
    /**
     * 判断角色是否拥有权限<br>
     * @param roleId 角色ID
     * @param permissionId 权限ID
     * @return 角色拥有权限返回true
     */
    public boolean roleHasPermission(Long roleId, Long permissionId)
    {
        Parameter param = new Parameter();
        param.put("roleId", roleId);
        param.put("permissionId", permissionId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.RoleDao.roleHasPermission");
        SQLQuery sQLQuery = hibernateDao.createSqlQuery(sql, param);
        List<?> list = sQLQuery.list();
        return (list.size() > 0);
    }
    
    /**
     * 为角色增加权限<br>
     * @param roleId 角色ID
     * @param permissionId 权限ID
     * @return 更新数据数量
     */
    public int roleAddPermission(Long roleId, Long permissionId)
    {
        Parameter param = new Parameter();
        param.put("roleId", roleId);
        param.put("permissionId", permissionId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.RoleDao.roleAddPermission");
        SQLQuery sQLQuery = hibernateDao.createSqlQuery(sql, param);
        return sQLQuery.executeUpdate();
    }
    
    /**
     * 为角色删除权限<br>
     * @param roleId 角色ID
     * @param permissionId 权限ID
     * @return 更新数据数量
     */
    public int roleDelPermission(Long roleId, Long permissionId)
    {
        Parameter param = new Parameter();
        param.put("roleId", roleId);
        param.put("permissionId", permissionId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.RoleDao.roleDelPermission");
        SQLQuery sQLQuery = hibernateDao.createSqlQuery(sql, param);
        return sQLQuery.executeUpdate();
    }
}
