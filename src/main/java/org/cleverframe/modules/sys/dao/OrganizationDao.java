package org.cleverframe.modules.sys.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.entity.Organization;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 * 组织机构Dao<br>
 * @author LiZhiWei
 * @version 2015年6月21日 下午8:27:39
 */
@Repository(SysBeanNames.OrganizationDao)
public class OrganizationDao extends BaseDao<Organization>
{
    /**
     * 查询所有机构对象
     */
    public List<Organization> findAllOrganization()
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.OrganizationDao.findAllOrganization");
        return hibernateDao.findBySql(sql, param);
    }

    /**
     * 获取当前机构的直接子机构数量，不包含软删除数据
     * @param orgId 机构ID
     * @return 
     */
    public int getChildrenOrgCount(Long orgId)
    {
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.OrganizationDao.getChildrenOrgCount");
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("parentId", orgId);
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object result = query.uniqueResult();
        return NumberUtils.toInt(result.toString(), 0);
    }

    /**
     * 获取所有机构信息，排除当前机构以及当前机构的所有下级机构<br>
     * @param fullPath 查询参数：当前机构全路径
     */
    public List<Organization> getOrgTreeExcludeOneself(String fullPath)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("fullPath", fullPath);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.OrganizationDao.getOrgTreeExcludeOneself");
        return hibernateDao.findBySql(sql, param);
    }

    /**
     * 查询机构层级的最大机构编码<br>
     * @param parentId 查询参数：父级机构ID
     * @return 机构层级的最大机构编码
     */
    public String getMaxCode(Long parentId)
    {
        Parameter param = new Parameter();
        param.put("parentId", parentId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.OrganizationDao.getMaxCode");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object result = query.uniqueResult();
        if (result == null)
        {
            return "";
        }
        else
        {
            return result.toString();
        }
    }

    /**
     * 获取组织架构中的所有公司<br>
     * @return 公司集合，包括集团和公司
     */
    public List<Organization> getAllCompany()
    {
        List<Character> orgTypeList = new ArrayList<Character>();
        orgTypeList.add(Organization.TYPE_GROUP);
        orgTypeList.add(Organization.TYPE_COMPANY);
        Map<String, Object> dataModel = new HashMap<String, Object>();
        dataModel.put("orgTypeList", orgTypeList);
        
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.OrganizationDao.getAllCompany", dataModel);
        return hibernateDao.findBySql(sql, param);
    }

    /**
     * 获取某机构下的的所有机构(包括该机构)<br>
     * @param fullPath 机构路径 + %
     * @return 某机构下的所有机构集合
     */
    public List<Organization> getChildrenOrg(String fullPath)
    {
        Parameter parameter = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        parameter.put("fullPath", fullPath);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.OrganizationDao.getOrgByCompany");
        return hibernateDao.findBySql(sql, parameter);
    }
}
