package org.cleverframe.modules.sys.dao;

import org.cleverframe.common.persistence.dao.HibernateDao;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.entity.Log;
import org.springframework.stereotype.Repository;

/**
 * 日志Dao<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月21日 下午8:30:22
 */
@Repository(SysBeanNames.LogDao)
public class LogDao extends HibernateDao<Log>
{

}
