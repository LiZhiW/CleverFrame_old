package org.cleverframe.modules.sys.dao;

import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.entity.User;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

/**
 * 用户登入DAO
 * 
 * @author LiZhiWei
 * @version 2015年5月23日 下午5:12:54
 */
@Repository(SysBeanNames.UserDao)
public class UserDao extends BaseDao<User>
{
	/**
	 * 根据用户登入名查询用户<br>
	 * @param loginName 用户登入名
	 * @return 用户信息
	 * */
    public User findUserByLoginName(String loginName)
    {
        Parameter param = new Parameter();
        param.put("loginName", loginName);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.UserDao.findUserByLoginName");
        User user = hibernateDao.getBySql(sql, param);
        return user;
    }
	
    /**
     * 分页查询用户数据<br>
     * @param page 分页对象
     * @param name 姓名
     * @param logiName 登录名
     * @param userType 用户类型
     * @param id 用户ID
     * @param uuid 数据UUID
     * @return 分页数据
     */
    public Page<User> findUserByPage(Page<User> page, String name, String logiName, String userType, Long id, String uuid)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("name", name);
        param.put("logiName", logiName);
        param.put("userType", userType);
        param.put("id", id);
        param.put("uuid", uuid);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.UserDao.findUserByPage");
        hibernateDao.findBySql(page, sql, param);
        return page;
    }
	
    /**
     * 获取当前最大员工号<br>
     */
	public String getMaxUserJobNo()
	{
		String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.UserDao.getMaxUserJobNo");
		Object object = hibernateDao.createSqlQuery(sql, null).uniqueResult();
		if (object == null)
		{
			return "";
		}
		return object.toString();
	}
	
    /**
     * 查询某机构下的直属用户<br>
     * @param homeOrg 查询参数:直属机构ID
     * @return 返回某机构下的所有直属用户
     */
    public List<User> findUserByHomeOrg(Long homeOrg)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL);
        param.put("homeOrg", homeOrg);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.UserDao.findUserByHomeOrg");
        return hibernateDao.findBySql(sql, param);
    }
    
    /**
     * 用户是否拥有此角色<br>
     * @param userId
     * @param roleId
     */
    public boolean userHasRole(Long userId, Long roleId)
    {
        Parameter param = new Parameter();
        param.put("userId", userId);
        param.put("roleId", roleId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.UserDao.userHasRole");
        SQLQuery sQLQuery = hibernateDao.createSqlQuery(sql, param);
        List<?> list = sQLQuery.list();
        return (list.size() > 0);
    }
    
    /**
     * 为用户增加一个角色<br>
     * @param userId 用户ID
     * @param roleId 角色ID
     * @return 更新数据数量
     */
    public int userAddRole(Long userId, Long roleId)
    {
        Parameter param = new Parameter();
        param.put("userId", userId);
        param.put("roleId", roleId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.UserDao.userAddRole");
        SQLQuery sQLQuery = hibernateDao.createSqlQuery(sql, param);
        return sQLQuery.executeUpdate();
    }
    
    /**
     * 为用户删除一个角色<br>
     * @param userId 用户ID
     * @param roleId 角色ID
     * @return 更新数据数量
     */
    public int userDelRole(Long userId, Long roleId)
    {
        Parameter param = new Parameter();
        param.put("userId", userId);
        param.put("roleId", roleId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.UserDao.userDelRole");
        SQLQuery sQLQuery = hibernateDao.createSqlQuery(sql, param);
        return sQLQuery.executeUpdate();
    }
    
    /**
     * 模糊查询用户信息，使用分页<br>
     * */
    public Page<User> findUserByParam(Page<User> page, String q)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("q", q);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.UserDao.findUserByParam");
        hibernateDao.findBySql(page, sql, param);
        return page;
    }
}
