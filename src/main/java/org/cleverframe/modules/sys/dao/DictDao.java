package org.cleverframe.modules.sys.dao;

import java.util.List;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.entity.Dict;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

/**
 * 系统字典DAO<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月6日 上午9:59:12
 */
@Repository(SysBeanNames.DictDao)
public class DictDao extends BaseDao<Dict>
{
    /**
     * 分页查询字典
     * @param page 分页对象
     * @param dictKey 查询参数：字典名称
     * @param dictType 查询参数：字典类型
     * @param dictValue 查询参数：字典值
     * @param id 查询参数：字典ID
     * @param uuid 查询参数：数据UUID
     * @return 分页数据
     */
    public Page<Dict> findDictByPage(Page<Dict> page, String dictKey, String dictType, String dictValue, Long id, String uuid)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("dictKey", dictKey);
        param.put("dictType", dictType);
        param.put("dictValue", dictValue);
        param.put("id", id);
        param.put("uuid", uuid);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.DictDao.findDictByPage");
        hibernateDao.findBySql(page, sql, param);
        return page;
    }
    
    /**
     * 根据字典类别查询字典对象
     * @param dictType 查询参数：字典类型
     * @return 字典对象集合
     */
    public List<Dict> findDictByType(String dictType)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("dictType", dictType);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.DictDao.findDictByType");
        return hibernateDao.findBySql(sql, param);
    }
    
    /**
     * 根据字典类别和字典名称查询字典
     * @param dictType 查询参数：字典类型
     * @param dictKey 查询参数：字典名称
     * @return 一个字典对象
     */
    public Dict getDictByTypeAndkey(String dictType, String dictKey)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("dictType", dictType);
        param.put("dictKey", dictKey);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.DictDao.findDictByTypeAndkey");
        return hibernateDao.getBySql(sql, param);
    }
    
    /**
     * 根据字典类别和字典值查询字典
     * @param dictType 查询参数：字典类型
     * @param dictValue 查询参数：字典值
     * @return 一个字典对象
     */
    public Dict getDictByTypeAndValue(String dictType, String dictValue)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("dictType", dictType);
        param.put("dictValue", dictValue);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.DictDao.getDictByTypeAndValue");
        return hibernateDao.getBySql(sql, param);
    }
    
    /**
     * 查询字典的所有类型
     */
    public List<String> findDictAllType()
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.sys.dao.DictDao.findDictAllType");
        SQLQuery query =  hibernateDao.createSqlQuery(sql, param);
        @SuppressWarnings("unchecked")
        List<String> list = query.list();
        return list;
    }
}
