package org.cleverframe.modules.sys.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.SysJspUrlPath;
import org.cleverframe.modules.sys.entity.Dict;
import org.cleverframe.modules.sys.service.DictService;
import org.cleverframe.webui.easyui.data.ComboBoxJson;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author LiZhiWei
 * @version 2015年6月6日 下午12:35:01
 */
@Controller
@RequestMapping("/${mvcPath}/sys")
@Transactional(readOnly = true)
public class DictController extends BaseController
{
    @Autowired
    @Qualifier(SysBeanNames.DictService)
    private DictService dictService;

    /** 跳转到系统字典管理页面 */
    @RequestMapping("/getDictJsp")
    public ModelAndView getDictJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(SysJspUrlPath.SysDict);
        return mav;
    }
    
    /** 
     * 查询字典数据，使用分页<br>
     * @return EasyUI DataGrid控件的json数据
     *  */
    @ResponseBody
    @RequestMapping("/findDictByPage")
    public DataGridJson<Dict> findDictByPage(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "dictKey-search", required = false,defaultValue="") String dictKey,
            @RequestParam(value = "dictType-search", required = false,defaultValue="") String dictType,
            @RequestParam(value = "dictValue-search", required = false,defaultValue="") String dictValue,
            @RequestParam(value = "id-search", required = false,defaultValue="-1") Long id,
            @RequestParam(value = "uuid-search", required = false,defaultValue="") String uuid)
    {
        dictValue = dictValue + "%";
        DataGridJson<Dict> json = new DataGridJson<Dict>();
        Page<Dict> dictPage = dictService.findDictByPage(new Page<Dict>(request, response), dictKey, dictType, dictValue, id, uuid);
        json.setRows(dictPage.getList());
        json.setTotal(dictPage.getCount());
        return json;
    }
    
    /** 新增字典数据 */
    @ResponseBody
    @RequestMapping("/addDict")
    @Transactional(readOnly = false)
    public AjaxMessage addDict(HttpServletRequest request, HttpServletResponse response, @Valid Dict dict, BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
            dictService.saveDict(dict);
            message.setMessage("字典数据保存成功");
        }
        return message;
    }
    
    /** 更新字典数据 */
    @ResponseBody
    @RequestMapping("/updateDict")
    @Transactional(readOnly = false)
    public AjaxMessage updateDict(HttpServletRequest request, HttpServletResponse response, @Valid Dict dict, BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
            dictService.updateDict(dict);
            message.setMessage("字典数据更新成功");
        }
        return message;
    }
    
    /**
     * 删除数据库脚本对象<br>
     */
    @ResponseBody
    @RequestMapping("/deleteDict")
    @Transactional(readOnly = false)
    public AjaxMessage deleteDict(HttpServletRequest request, HttpServletResponse response, @Valid Dict dict, BindingResult bindingResult)
    {
        dictService.deleteDict(dict);
        AjaxMessage message = new AjaxMessage(true, "删除成功");
        return message;
    }
    
    /** 根据字典类别查询字典对象 */
    @ResponseBody
    @RequestMapping("/findDictByType")
    public List<ComboBoxJson> findDictByType(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value = "dict-type") String dictType)
	{
		List<ComboBoxJson> comboBoxList = new ArrayList<ComboBoxJson>();
		if (StringUtils.isBlank(dictType))
		{
			return comboBoxList;
		}
		List<Dict> dictList = dictService.findDictByType(dictType);
        for (Dict dict : dictList)
        {
            ComboBoxJson comboBox = new ComboBoxJson(false, dict.getDictKey(), dict.getDictValue());
            comboBoxList.add(comboBox);
        }
		return comboBoxList;
	}
    
	/** 查询字典的所有类型 */
	@ResponseBody
	@RequestMapping("/findDictAllType")
    public List<ComboBoxJson> findDictAllType(HttpServletRequest request, HttpServletResponse response)
    {
        List<ComboBoxJson> comboBoxList = new ArrayList<ComboBoxJson>();
        List<String> list = dictService.findDictAllType();
        for (String type : list)
        {
            ComboBoxJson comboBox = new ComboBoxJson(false, type, type);
            comboBoxList.add(comboBox);
        }
        return comboBoxList;
    }
}
