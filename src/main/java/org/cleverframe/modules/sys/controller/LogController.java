package org.cleverframe.modules.sys.controller;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 日志Controller<br>
 * @author LiZW
 * @version 2015年6月26日 下午4:37:40
 */
@Controller
@RequestMapping("/${mvcPath}/sys")
public class LogController extends BaseController
{
    @Autowired
    @Qualifier(SysBeanNames.LogService)
    private LogService logService;
}
