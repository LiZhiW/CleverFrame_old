package org.cleverframe.modules.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.ExpiredCredentialsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.SysJspUrlPath;
import org.cleverframe.modules.sys.attributes.SysSessionAttributes;
import org.cleverframe.modules.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 用户登录处理的Controller
 * 
 * @author LiZhiWei
 * @version 2015年5月23日 下午4:58:20
 */
@Controller
@RequestMapping("/${mvcPath}/sys")
public class LoginController extends BaseController
{
	@Autowired
	@Qualifier(SysBeanNames.UserService)
	private UserService userService;
	
	/**
	 * 用户登入入口，跳转到登入页面<br>
	 * */
	@ResponseBody
	@RequestMapping(value = "/loginJsp")
	public ModelAndView loginJsp(HttpServletRequest request)
	{
		ModelAndView mav=new ModelAndView(SysJspUrlPath.SysLogin);
		return mav;
	}
	
	/**
	 * 用户登入失败处理，真正的用户登入验证交给Shiro处理了<br>
	 * 1.用户第一次登入成功并不会调用此方法<br>
	 * */
	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public AjaxMessage longin(HttpServletRequest request)
	{
		// TODO 用户重复登入，必须判断用户是否想切换账号
		Subject subject = SecurityUtils.getSubject();
		if (subject.isAuthenticated())
		{
			return new AjaxMessage(true, "登入成功");
		}
		
		// 解析登录错误 TODO 生产环境下解析登录错误不能太详细
		String exceptionClassName = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
		String error = null;
		if (UnknownAccountException.class.getName().equals(exceptionClassName))
		{
			error = "帐号不存在";
		}
		else if (IncorrectCredentialsException.class.getName().equals(exceptionClassName))
		{
			error = "密码错误";
		}
		else if (DisabledAccountException.class.getName().equals(exceptionClassName))
		{
			error = "禁用的帐号";
		}
		else if (LockedAccountException.class.getName().equals(exceptionClassName))
		{
			error = "锁定的帐号";
		}
		else if (ExpiredCredentialsException.class.getName().equals(exceptionClassName))
		{
			error = "过期的凭证";
		}
		else if (ExcessiveAttemptsException.class.getName().equals(exceptionClassName))
		{
			error = "登录失败次数过多";
		}
		else if (exceptionClassName != null)
		{
			error = exceptionClassName;
		}
		else
		{
			error = "未知的错误！";
		}
		
		AjaxMessage ajaxMessage = new AjaxMessage(false, error);
		// 获取登入失败次数
        Object temp = request.getSession().getAttribute(SysSessionAttributes.LOGIN_FAILED_COUNT);
        int count = NumberUtils.toInt(temp == null ? null : temp.toString(), 0);
		if(count >= 3)
		{
		    ajaxMessage.setNeedValidateCode(true);
		}
		return ajaxMessage;
	}
	
	/**
	 * Shiro登入成功跳转到此方法<br>
	 * */
	@ResponseBody
	@RequestMapping(value = "/loginSuccess")
	public AjaxMessage loginSuccess(HttpServletRequest request, HttpServletResponse response)
	{
		Subject subject = SecurityUtils.getSubject();
		if (subject.isAuthenticated())
		{
			return new AjaxMessage(true, "登入成功");
		}
		else
		{
			return new AjaxMessage(false, "登入失败");
		}
	}
}
