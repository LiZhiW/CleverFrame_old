package org.cleverframe.modules.sys.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.persistence.tree.BuildTreeUtil;
import org.cleverframe.common.persistence.tree.ITreeNode;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.SysJspUrlPath;
import org.cleverframe.modules.sys.entity.MDict;
import org.cleverframe.modules.sys.service.MDictService;
import org.cleverframe.webui.easyui.data.ComboBoxJson;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.cleverframe.webui.easyui.data.TreeGridNodeJson;
import org.cleverframe.webui.easyui.data.TreeNodeJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 多级字典Controller<br>
 * @author LiZW
 * @version 2015年6月26日 下午4:21:43
 */
@Controller
@RequestMapping("/${mvcPath}/sys")
@Transactional(readOnly = true)
public class MDictController extends BaseController
{
    @Autowired
    @Qualifier(SysBeanNames.MDictService)
    private MDictService mDictService;
    
    /**
     * 跳转到多级字典管理页面<br>
     */
    @RequestMapping("/getSysMDictJsp")
    public ModelAndView getSysMDictJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(SysJspUrlPath.SysMDict);
        return mav;
    }
    
    /**
     * 查询多级字典对象<br>
     * @return 返回EasyUI TreeGrid控件格式数据
     * */
    @ResponseBody
    @RequestMapping("/findMDictByType")
    public DataGridJson<TreeGridNodeJson<MDict>> findMDictByType(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "type-search", required = false, defaultValue = "") String mdictType,
            @RequestParam(value = "id-search", required = false,defaultValue="-1") Long id,
            @RequestParam(value = "uuid-search", required = false,defaultValue="") String uuid)
    {
        DataGridJson<TreeGridNodeJson<MDict>> treeGrid = new DataGridJson<TreeGridNodeJson<MDict>>();
        List<MDict> mdictList = mDictService.findMDictByType(mdictType, id, uuid);
        for (MDict mdict : mdictList)
        {
            TreeGridNodeJson<MDict> node = new TreeGridNodeJson<MDict>(mdict.getParentId(), mdict);
            treeGrid.addRow(node);
        }
        return treeGrid;
    }
    
    /**
     * 获取某一类型的字典数据，排除当前字典以及当前字典的所有下级字典<br>
     * @return 返回EasyUI Tree控件格式数据
     */
    @ResponseBody
    @RequestMapping("/getMDictTreeExcludeOneself")
    public List<ITreeNode> getMDictTreeExcludeOneself(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value="type" ,required=false,defaultValue="") String mdictType,
            @RequestParam(value="fullPath" ,required=false,defaultValue="XXX") String fullPath)
    {
        fullPath = fullPath + "%";
        List<MDict> mdictList = mDictService.getMDictTreeExcludeOneself(mdictType, fullPath);
        List<ITreeNode> nodes = new ArrayList<ITreeNode>();
        for (MDict mdict : mdictList)
        {
            TreeNodeJson node = new TreeNodeJson(mdict.getParentId(), mdict.getId(), mdict.getFullPath(), mdict.getMdictKey(), "", false, "open");
            nodes.add(node);
        }
        return BuildTreeUtil.bulidTree(nodes);
    }
    
    /**
     * 保存多级字典<br>
     */
    @ResponseBody
    @RequestMapping("/addMDict")
    @Transactional(readOnly = false)
    public AjaxMessage addMDict(HttpServletRequest request, HttpServletResponse response, @Valid MDict mdict, BindingResult bindingResult)
    {
        AjaxMessage message=new AjaxMessage();
        if(beanValidator(bindingResult, message))
        {
            if(mDictService.addMDict(mdict, message))
            {
                message.setMessage("多级字典数据保存成功");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 更新多级字典<br>
     */
    @ResponseBody
    @RequestMapping("/updateMDict")
    @Transactional(readOnly = false)
    public AjaxMessage updateMDict(HttpServletRequest request, HttpServletResponse response, @Valid MDict mdict, BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
            if (mDictService.updateMDict(mdict, message))
            {
                message.setMessage("多级字典数据更新成功");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 删除多级字典<br>
     */
    @ResponseBody
    @RequestMapping("/deleteMDict")
    @Transactional(readOnly = false)
    public AjaxMessage deleteMDict(HttpServletRequest request, HttpServletResponse response, @Valid MDict mdict, BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
            String fullPath = mdict.getFullPath() + "%";
            int deleteCount = mDictService.deleteMDict(fullPath);
            message.setMessage("多级字典数据删除成功，删除多级字典数：" + deleteCount);
        }
        return message;
    }
    
    /**
     * 查询多级字典所有字典类型<br>
     * @return 返回EasyUI ComboBox数据绑定的Json格式数据
     */
    @ResponseBody
    @RequestMapping("/findMDictAllType")
    public List<ComboBoxJson> findMDictAllType(HttpServletRequest request, HttpServletResponse response)
    {
        List<ComboBoxJson> comboBoxList = new ArrayList<ComboBoxJson>();
        List<String> list = mDictService.findMDictAllType();
        for (String type : list)
        {
            ComboBoxJson comboBox = new ComboBoxJson(false, type, type);
            comboBoxList.add(comboBox);
        }
        return comboBoxList;
    }
}
