package org.cleverframe.modules.sys.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.SysJspUrlPath;
import org.cleverframe.modules.sys.entity.Permission;
import org.cleverframe.modules.sys.service.PermissionService;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 权限Controller<br>
 * @author LiZW
 * @version 2015年6月26日 下午4:31:19
 */
@Controller
@RequestMapping("/${mvcPath}/sys")
@Transactional(readOnly = true)
public class PermissionController extends BaseController
{
    @Autowired
    @Qualifier(SysBeanNames.PermissionService)
    private PermissionService permissionService;
    
    /**
     * 跳转到权限管理页面<br>
     */
    @RequestMapping("/getSysPermissionJsp")
    public ModelAndView getSysPermissionJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(SysJspUrlPath.SysPermission);
        return mav;
    }
    
    /** 
     * 查询字典数据，使用分页<br>
     * @return EasyUI DataGrid控件的json数据
     *  */
    @ResponseBody
    @RequestMapping("/findPermissionByPage")
    public DataGridJson<Permission> findPermissionByPage(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "menuId-search", required = false, defaultValue = "-1") Long menuId,
            @RequestParam(value = "permission-search", required = false, defaultValue = "") String permission,
            @RequestParam(value = "id-search", required = false, defaultValue = "-1") Long id,
            @RequestParam(value = "uuid-search", required = false, defaultValue = "") String uuid)
    {
        permission = permission + "%";
        DataGridJson<Permission> json = new DataGridJson<Permission>();
        Page<Permission> dictPage = permissionService.findPermissionByPage(new Page<Permission>(request, response), menuId, permission, id, uuid);
        json.setRows(dictPage.getList());
        json.setTotal(dictPage.getCount());
        return json;
    }
    
	/**
	 * 新增权限数据数据
	 * */
	@ResponseBody
	@RequestMapping("/addPermission")
	@Transactional(readOnly = false)
	public AjaxMessage addPermission(HttpServletRequest request, HttpServletResponse response, @Valid Permission permission, BindingResult bindingResult)
	{
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
            if (permissionService.savePermission(permission, message))
            {
                message.setMessage("权限数据保存成功");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
	}
	
	/**
	 * 更新权限对象<br>
	 * */
	@ResponseBody
	@RequestMapping("/updatePermission")
	@Transactional(readOnly = false)
    public AjaxMessage updatePermission(HttpServletRequest request, HttpServletResponse response, @Valid Permission permission, BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
            if (permissionService.updatePermission(permission, message))
            {
                message.setMessage("权限数据更新成功");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
	
	/**
	 * 删除权限对象<br>
	 * */
	@ResponseBody
	@RequestMapping("/deletePermission")
	@Transactional(readOnly = false)
	public AjaxMessage deletePermission(HttpServletRequest request, HttpServletResponse response, @Valid Permission permission, BindingResult bindingResult)
	{
		AjaxMessage message = new AjaxMessage();
		if (beanValidator(bindingResult, message))
		{
			permissionService.delete(permission);
			message.setMessage("权限数据删除成功");
		}
		return message;
	}
	
    /**
     * 获取某个菜单的UI权限<br>
     * @param menuId 菜单ID
     * @return 权限集合  EasyUI DataGrid控件的json数据
     */
    @ResponseBody
    @RequestMapping("/findUiPermissionByMenu")
	public DataGridJson<Permission> findUiPermissionByMenu(
	        HttpServletRequest request, 
	        HttpServletResponse response,
	        @RequestParam(value = "menuId", required = true) Long menuId)
    {
        DataGridJson<Permission> json = new DataGridJson<Permission>();
        List<Permission> permissionList = permissionService.findPermissionByMenu(menuId, Permission.PERMISSION_TYPE_UI);
        json.setRows(permissionList);
        return json;
    }
    
    /**
     * 获取所有的的URL权限<br>
     * @return 权限集合  EasyUI DataGrid控件的json数据
     */
    @ResponseBody
    @RequestMapping("/findAllUrlPermission")
    public DataGridJson<Permission> findAllUrlPermission(HttpServletRequest request, HttpServletResponse response)
    {
        DataGridJson<Permission> json = new DataGridJson<Permission>();
        List<Permission> permissionList = permissionService.findAllPermissionByType(Permission.PERMISSION_TYPE_URL);
        json.setRows(permissionList);
        return json;
    }
    
    /**
     * 获取所有的的UI权限<br>
     * @return 权限集合  EasyUI DataGrid控件的json数据
     */
    @ResponseBody
    @RequestMapping("/findAllUiPermission")
    public DataGridJson<Permission> findAllUiPermission(HttpServletRequest request, HttpServletResponse response)
    {
        DataGridJson<Permission> json = new DataGridJson<Permission>();
        List<Permission> permissionList = permissionService.findAllPermissionByType(Permission.PERMISSION_TYPE_UI);
        json.setRows(permissionList);
        return json;
    }
    
    /**
     * 查找某个角色拥有某个菜单下的UI权限<br>
     * @return 权限集合
     */
    @ResponseBody
    @RequestMapping("/findUiPermissionByRoleAndMenu")
    public List<Permission> findUiPermissionByRoleAndMenu(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value = "roleId", required = true) Long roleId, 
            @RequestParam(value = "menuId", required = true) Long menuId)
    {
        List<Permission> permissionList = permissionService.findPermissionByRoleAndMenu(Permission.PERMISSION_TYPE_UI, roleId, menuId);
        return permissionList;
    }
    
    /**
     * 查找某个角色拥有的所有URL权限<br>
     * @param roleId 角色ID
     * @return 权限集合
     */
    @ResponseBody
    @RequestMapping("/findUrlPermissionByRoleId")
    public List<Permission> findUrlPermissionByRoleId(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value = "roleId", required = true) Long roleId)
    {
        List<Permission> permissionList = permissionService.findPermissionByRoleId(Permission.PERMISSION_TYPE_URL, roleId);
        return permissionList;
    }
    
    /**
     * 查找某个角色拥有的所有UI权限<br>
     * @param roleId 角色ID
     * @return 权限集合
     */
    @ResponseBody
    @RequestMapping("/findUiPermissionByRoleId")
    public List<Permission> findUiPermissionByRoleId(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value = "roleId", required = true) Long roleId)
    {
        List<Permission> permissionList = permissionService.findPermissionByRoleId(Permission.PERMISSION_TYPE_UI, roleId);
        return permissionList;
    }
}
