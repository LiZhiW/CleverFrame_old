package org.cleverframe.modules.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.SysJspUrlPath;
import org.cleverframe.modules.sys.entity.Permission;
import org.cleverframe.modules.sys.service.RoleService;
import org.cleverframe.modules.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 用户、角色、菜单、权限管理<br>
 * 1.给用户分配角色<br>
 * 2.给角色分配菜单<br>
 * 3.给菜单分配功能<br>
 * 
 * @author LiZW
 * @version 2015年7月23日 下午4:14:42
 */
@Controller
@RequestMapping("/${mvcPath}/sys")
@Transactional(readOnly = true)
public class UserRoleMenuPermissionController extends BaseController
{
    @Autowired
    @Qualifier(SysBeanNames.UserService)
    private UserService userService;
    
    @Autowired
    @Qualifier(SysBeanNames.RoleService)
    private RoleService roleService;
    
    /**
     * 跳转到用户、角色、菜单、权限管理管理页面<br>
     */
    @RequestMapping("/getSysUserRoleMenuPermission")
    public ModelAndView getSysUserJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(SysJspUrlPath.SysUserRoleMenuPermission);
        return mav;
    }
    
    /**
     * 为用户增加一个角色<br>
     * @param userId 用户ID
     * @param roleId 角色ID
     */
    @ResponseBody
    @RequestMapping("/userAddRole")
    @Transactional(readOnly = false)
    public AjaxMessage userAddRole(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "userId", required = true) Long userId, 
            @RequestParam(value = "roleId", required = true) Long roleId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (userService.userAddRole(userId, roleId, message))
        {
            if (StringUtils.isBlank(message.getMessage()))
            {
                message.setMessage("用户增加角色成功");
            }
        }
        else
        {
            message.setSuccess(false);
        }
        return message;
    }
    
    /**
     * 为用户增加所有角色
     * @param userId 用户ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/userAddAllRole")
    @Transactional(readOnly = false)
    public AjaxMessage userAddAllRole(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "userId", required = true) Long userId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (userService.userAddAllRole(userId, message))
        {
            message.setMessage("为用户增加所有角色成功");
        }
        return message;
    }
    
    /**
     * 为用户删除一个角色<br>
     * @param userId 用户ID
     * @param roleId 角色ID
     */
    @ResponseBody
    @RequestMapping("/userDelRole")
    @Transactional(readOnly = false)
    public AjaxMessage userDelRole(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "userId", required = true) Long userId, 
            @RequestParam(value = "roleId", required = true) Long roleId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (userService.userDelRole(userId, roleId, message))
        {
            if (StringUtils.isBlank(message.getMessage()))
            {
                message.setMessage("用户删除角色成功");
            }
        }
        else
        {
            message.setSuccess(false);
        }
        return message;
    }
    
    /**
     * 为用户增加所有角色
     * @param userId 用户ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/userDelAllRole")
    @Transactional(readOnly = false)
    public AjaxMessage userDelAllRole(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "userId", required = true) Long userId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (userService.userDelAllRole(userId, message))
        {
            message.setMessage("删除用户所有角色成功");
        }
        return message;
    }
    
    /**
     * 为角色增加菜单<br>
     * @param userId 角色ID
     * @param menuId 菜单ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/roleAddMenu")
    @Transactional(readOnly = false)
    public AjaxMessage roleAddMenu(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "roleId", required = true) Long roleId, 
            @RequestParam(value = "menuId", required = true) Long menuId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (roleService.roleAddMenu(roleId, menuId, message))
        {
            if (StringUtils.isBlank(message.getMessage()))
            {
                message.setMessage("角色增加菜单成功");
            }
        }
        else
        {
            message.setSuccess(false);
        }
        return message;
    }
    
    /**
     * 为角色删除菜单<br>
     * @param userId 角色ID
     * @param menuId 菜单ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/roleDelMenu")
    @Transactional(readOnly = false)
    public AjaxMessage roleDelMenu(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "roleId", required = true) Long roleId, 
            @RequestParam(value = "menuId", required = true) Long menuId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (roleService.roleDelMenu(roleId, menuId, message))
        {
            if (StringUtils.isBlank(message.getMessage()))
            {
                message.setMessage("角色删除菜单成功");
            }
        }
        else
        {
            message.setSuccess(false);
        }
        return message;
    }
    
    /**
     * 为角色增加权限<br>
     * @param roleId 角色ID
     * @param permissionId 权限ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/roleAddPermission")
    @Transactional(readOnly = false)
    public AjaxMessage roleAddPermission(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "roleId", required = true) Long roleId, 
            @RequestParam(value = "permissionId", required = true) Long permissionId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (roleService.roleAddPermission(roleId, permissionId, message))
        {
            if (StringUtils.isBlank(message.getMessage()))
            {
                message.setMessage("为角色增加权限成功");
            }
        }
        else
        {
            message.setSuccess(false);
        }        
        return message;
    }
    
    /**
     * 为角色增加所有UI权限<br>
     * @param roleId 角色ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/roleAddAllUiPermission")
    @Transactional(readOnly = false)
    public AjaxMessage roleAddAllUiPermission(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "roleId", required = true) Long roleId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (roleService.roleAddAllPermission(roleId, Permission.PERMISSION_TYPE_UI, message))
        {
            message.setMessage("为角色增加所有UI权限成功");
        }
        return message;
    }
    
    /**
     * 为角色增加所有UI权限<br>
     * @param roleId 角色ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/roleAddAllUrlPermission")
    @Transactional(readOnly = false)
    public AjaxMessage roleAddAllUrlPermission(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "roleId", required = true) Long roleId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (roleService.roleAddAllPermission(roleId, Permission.PERMISSION_TYPE_URL, message))
        {
            message.setMessage("为角色增加所有URL权限成功");
        }
        return message;
    }
    
    /**
     * 为角色删除权限<br>
     * @param roleId 角色ID
     * @param permissionId 权限ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/roleDelPermission")
    @Transactional(readOnly = false)
    public AjaxMessage roleDelPermission(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "roleId", required = true) Long roleId, 
            @RequestParam(value = "permissionId", required = true) Long permissionId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (roleService.roleDelPermission(roleId, permissionId, message))
        {
            if (StringUtils.isBlank(message.getMessage()))
            {
                message.setMessage("角色删除权限成功");
            }
        }
        else
        {
            message.setSuccess(false);
        } 
        return message;
    }
    
    /**
     * 为角色删除所有UI权限<br>
     * @param roleId 角色ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/roleDelAllUiPermission")
    @Transactional(readOnly = false)
    public AjaxMessage roleDelAllUiPermission(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "roleId", required = true) Long roleId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (roleService.roleDelAllPermission(roleId, Permission.PERMISSION_TYPE_UI, message))
        {
            message.setMessage("删除角色所有UI权限成功");
        }
        return message;
    }
    
    /**
     * 为角色删除所有UI权限<br>
     * @param roleId 角色ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/roleDelAllUrlPermission")
    @Transactional(readOnly = false)
    public AjaxMessage roleDelAllUrlPermission(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "roleId", required = true) Long roleId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (roleService.roleDelAllPermission(roleId, Permission.PERMISSION_TYPE_URL, message))
        {
            message.setMessage("删除角色所有URL权限成功");
        }
        return message;
    }
    
    /**
     * 为角色增加某个菜单下的所有UI权限<br>
     * @param roleId 角色ID
     * @param menuId 菜单ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/roleAddAllUiPermissionByMenuId")
    @Transactional(readOnly = false)
    public AjaxMessage roleAddAllUiPermissionByMenuId(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "roleId", required = true) Long roleId,
            @RequestParam(value = "menuId", required = true) Long menuId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (roleService.roleAddAllUiPermissionByMenuId(roleId, menuId, message))
        {
            message.setMessage("为角色增加UI权限成功");
        }
        return message;
    }
    
    /**
     * 为角色删除某个菜单下的所有UI权限<br>
     * @param roleId 角色ID
     * @param menuId 菜单ID
     * @return
     */
    @ResponseBody
    @RequestMapping("/roleDelAllUiPermissionByMenuId")
    @Transactional(readOnly = false)
    public AjaxMessage roleDelAllUiPermissionByMenuId(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "roleId", required = true) Long roleId,
            @RequestParam(value = "menuId", required = true) Long menuId)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (roleService.roleDelAllUiPermissionByMenuId(roleId, menuId, message))
        {
            message.setMessage("删除角色UI权限成功");
        }
        return message;
    }
}
