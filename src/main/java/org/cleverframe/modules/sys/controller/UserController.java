package org.cleverframe.modules.sys.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.SysJspUrlPath;
import org.cleverframe.modules.sys.entity.User;
import org.cleverframe.modules.sys.service.UserService;
import org.cleverframe.webui.easyui.data.ComboBoxJson;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 用户Controller
 * @author LiZW
 * @version 2015年7月13日 下午5:14:34
 */
@Controller
@RequestMapping("/${mvcPath}/sys")
@Transactional(readOnly = true)
public class UserController extends BaseController
{
	@Autowired
	@Qualifier(SysBeanNames.UserService)
	private UserService userService;

	/**
	 * 跳转到用户管理页面<br>
	 */
	@RequestMapping("/getSysUserJsp")
	public ModelAndView getSysUserJsp(HttpServletRequest request, HttpServletResponse response)
	{
		ModelAndView mav = new ModelAndView(SysJspUrlPath.SysUser);
		return mav;
	}

	/** 
	 * 查询用户数据，使用分页<br>
	 * @return EasyUI DataGrid控件的json数据
	 *  */
	@ResponseBody
	@RequestMapping("/findUserByPage")
	public DataGridJson<User> findUserByPage(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "name-search", required = false, defaultValue = "") String name,
			@RequestParam(value = "logiName-search", required = false, defaultValue = "") String logiName,
			@RequestParam(value = "userType-search", required = false, defaultValue = "") String userType,
			@RequestParam(value = "id-search", required = false, defaultValue = "-1") Long id,
			@RequestParam(value = "uuid-search", required = false, defaultValue = "") String uuid)
	{
        DataGridJson<User> json = new DataGridJson<User>();
        Page<User> userPage = userService.findUserByPage(new Page<User>(request, response), name, logiName, userType, id, uuid);
        json.setRows(userPage.getList());
        json.setTotal(userPage.getCount());
        return json;
	}

	/**
	 * 新增用户<br>
	 */
	@ResponseBody
	@RequestMapping("/addUser")
	@Transactional(readOnly = false)
	public AjaxMessage addUser(
	        HttpServletRequest request, 
	        HttpServletResponse response, 
	        @Valid User user, 
	        BindingResult bindingResult)
	{
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
            if (userService.addUser(user, message))
            {
                message.setMessage("用户数据保存成功");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
	}

	/**
	 * 更新用户<br>
	 */
	@ResponseBody
	@RequestMapping("/updateUser")
	@Transactional(readOnly = false)
	public AjaxMessage updateUser(HttpServletRequest request, HttpServletResponse response, @Valid User user, BindingResult bindingResult)
	{
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
            if (userService.updateUser(user, message))
            {
                message.setMessage("用户数据保存成功");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
	}

	/**
	 * 删除用户<br>
	 * */
	@ResponseBody
	@RequestMapping("/deleteUser")
	@Transactional(readOnly = false)
	public AjaxMessage deleteUser(HttpServletRequest request, HttpServletResponse response, @Valid User user, BindingResult bindingResult)
	{
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
            if (userService.deleteUser(user, message))
            {
                message.setMessage("用户帐号已被禁用");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
	}
	
    /** 
     * 查询某个机构下的用户<br>
     * @return EasyUI DataGrid控件的json数据
     *  */
    @ResponseBody
    @RequestMapping("/findUserByHomeOrg")
	public DataGridJson<User> findUserByHomeOrg(
	        HttpServletRequest request, 
	        HttpServletResponse response,
	        @RequestParam(value = "homeOrg", required = true) Long homeOrg)
    {
        DataGridJson<User> json = new DataGridJson<User>();
        List<User> userList = userService.findUserByHomeOrg(homeOrg);
        json.setRows(userList);
        return json;
    }
    
    /**
     * 模糊查询用户信息，使用分页<br>
     * */
    @ResponseBody
    @RequestMapping("/findUserByParam")
    public List<ComboBoxJson> findUserByParam(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "q", required = false, defaultValue = "") String q)
    {
        q = "%" + q + "%";
        List<ComboBoxJson> comboBoxList = new ArrayList<ComboBoxJson>();
        Page<User> page = new Page<User>(1, 50);
        userService.findUserByParam(page, q);
        for (User user : page.getList())
        {
            String text = user.getName();
            String value = user.getId().toString();
            ComboBoxJson comboBox = new ComboBoxJson(false, text, value, user);
            comboBoxList.add(comboBox);
        }
        return comboBoxList;
    }
    
    /**
     * 根据用户ID查询用户<br>
     * */
    @ResponseBody
    @RequestMapping("/getUserById")
    public AjaxMessage getUserById(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "id", required = true) Long id)
    {
        AjaxMessage message = new AjaxMessage(true);
        User user = userService.getUserById(id);
        if (user == null)
        {
            message.setSuccess(false);
            message.setMessage("用户不存在");
        }
        else
        {
            message.setObject(user);
        }
        return message;
    }
}
