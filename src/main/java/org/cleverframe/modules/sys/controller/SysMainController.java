package org.cleverframe.modules.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.SysJspUrlPath;
import org.cleverframe.modules.sys.entity.User;
import org.cleverframe.modules.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * sys模块Controller<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月25日 下午9:58:32
 */
@Controller
@RequestMapping("/${mvcPath}/sys")
public class SysMainController extends BaseController
{
    @Autowired
    @Qualifier(SysBeanNames.UserService)
    private UserService userService;
    
    /**
     * 跳转到后台管理主页<br>
     */
    @RequestMapping("/getSysMainJsp")
    public ModelAndView getSysMainJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(SysJspUrlPath.SysMain);
        return mav;
    }
    
    /**
     * 获取当前登录的用户<br>
     * */
    @ResponseBody
    @RequestMapping("/getCurrentUser")
    public AjaxMessage getCurrentUser(HttpServletRequest request, HttpServletResponse response)
    {
        AjaxMessage message = new AjaxMessage(true);
        User user = userService.getUserById(userUtils.getCurrentUserId());
        message.setObject(user);
        return message;
    }
}
