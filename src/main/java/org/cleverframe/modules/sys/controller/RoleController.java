package org.cleverframe.modules.sys.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.SysJspUrlPath;
import org.cleverframe.modules.sys.entity.Role;
import org.cleverframe.modules.sys.service.RoleService;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 角色Controller<br>
 * @author LiZW
 * @version 2015年6月26日 下午4:33:46
 */
@Controller
@RequestMapping("/${mvcPath}/sys")
@Transactional(readOnly = true)
public class RoleController extends BaseController
{
    @Autowired
    @Qualifier(SysBeanNames.RoleService)
    private RoleService roleService;
    
    /**
     * 跳转到角色管理页面<br>
     */
    @RequestMapping("/getSysRoleJsp")
    public ModelAndView getSysRoleJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(SysJspUrlPath.SysRole);
        return mav;
    }
    
    /** 
     * 查询角色数据，使用分页<br>
     * @return EasyUI DataGrid控件的json数据
     *  */
    @ResponseBody
    @RequestMapping("/findRoleByPage")
    public DataGridJson<Role> findRoleByPage(
    		HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "name-search", required = false,defaultValue="") String name,
            @RequestParam(value = "id-search", required = false,defaultValue="-1") Long id,
            @RequestParam(value = "uuid-search", required = false,defaultValue="") String uuid)
    {
        name = name + "%";
        DataGridJson<Role> json = new DataGridJson<Role>();
        Page<Role> dictPage = roleService.findRoleByPage(new Page<Role>(request, response), name, id, uuid);
        json.setRows(dictPage.getList());
        json.setTotal(dictPage.getCount());
        return json;
    }
    
    /**
     * 新增角色<br>
     * */
    @ResponseBody
    @RequestMapping("/addRole")
    @Transactional(readOnly = false)
    public AjaxMessage addRole(HttpServletRequest request, HttpServletResponse response, @Valid Role role, BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
        	roleService.saveRole(role);
            message.setMessage("角色数据保存成功");
        }
        return message;
    }
    
    /**
     * 更新角色<br>
     * */
    @ResponseBody
    @RequestMapping("/updateRole")
    @Transactional(readOnly = false)
    public AjaxMessage updateRole(HttpServletRequest request, HttpServletResponse response, @Valid Role role, BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage();
        if (beanValidator(bindingResult, message))
        {
        	roleService.updateRole(role);
            message.setMessage("角色数据更新成功");
        }
        return message;
    }
    
    /**
     * 删除角色<br>
     * */
    @ResponseBody
    @RequestMapping("/deleteRole")
    @Transactional(readOnly = false)
    public AjaxMessage deleteRole(HttpServletRequest request, HttpServletResponse response, @Valid Role role, BindingResult bindingResult)
    {
    	roleService.deleteRole(role);
        AjaxMessage message = new AjaxMessage(true, "删除成功");
        return message;
    }
    
    /**
     * 查询当前公司下面的角色<br>
     * 
     * @return EasyUI DataGrid控件的json数据
     */
    @ResponseBody
    @RequestMapping("/findRoleByCurrentCompany")
    public DataGridJson<Role> findRoleByCurrentCompany(HttpServletRequest request, HttpServletResponse response)
    {
        DataGridJson<Role> json = new DataGridJson<Role>();
        List<Role> roleList = roleService.findRoleByCompany(userUtils.getCurrentCompanyId());
        json.setRows(roleList);
        return json;
    }
    
    /**
     * 根据用户ID得到用户的所有角色信息<br>
     */
    @ResponseBody
    @RequestMapping("/findRoleByUserId")
    public List<Role> findRoleByUserId(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "userId", required = true) Long userId)
    {
        List<Role> roleList = roleService.findRoleByUserId(userId);
        return roleList;
    }
}
