package org.cleverframe.modules.sys.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * SpringMVC拦截器，拦截未登入的用户<br>
 * 1.检查Session中是否有用户信息，若没有则跳转到登入界面
 * 
 * @author LiZhiWei
 * @version 2015年5月26日 下午9:02:21
 */
@Deprecated
public class LoginInterceptor implements HandlerInterceptor
{
	/**
	 * 请求到Handler之前的处理<br>
	 * */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
	{
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
	{
		// TODO 拦截未登入的用户
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception
	{
		// TODO 拦截未登入的用户
		
	}

}
