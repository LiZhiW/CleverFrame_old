package org.cleverframe.modules.sys.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.cleverframe.common.persistence.entity.BaseEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 系统日志信息<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月5日 下午10:26:44
 */
@Entity
@Table(name = "sys_log")
@DynamicInsert
@DynamicUpdate
public class Log extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	@Id
	/** 编号 */
	private Long id;
	/** 日志类型（1：接入日志；2：错误日志） */
	private Character logType;
	/** 创建者 */
	private Long createBy;
	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	private Date createDate;
	/** 操作IP地址 */
	private String remoteAddr;
	/** 所属公司的机构编码 */
	private String companyId;
	/** 用户代理 */
	private String userAgent;
	/** 请求URI */
	private String requestUri;
	/** 操作方式 */
	private String method;
	/** 操作提交的数据 */
	private String params;
	/** 异常信息 */
	private String exceptionInfo;

	/*--------------------------------------------------------------
	 * 			getter、setter
	 * -------------------------------------------------------------*/
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Character getLogType()
    {
        return logType;
    }

    public void setLogType(Character logType)
    {
        this.logType = logType;
    }

    public Long getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(Long createBy)
	{
		this.createBy = createBy;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public String getRemoteAddr()
	{
		return remoteAddr;
	}

	public void setRemoteAddr(String remoteAddr)
	{
		this.remoteAddr = remoteAddr;
	}
	
	public String getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(String companyId)
    {
        this.companyId = companyId;
    }

    public String getUserAgent()
	{
		return userAgent;
	}

	public void setUserAgent(String userAgent)
	{
		this.userAgent = userAgent;
	}

	public String getRequestUri()
	{
		return requestUri;
	}

	public void setRequestUri(String requestUri)
	{
		this.requestUri = requestUri;
	}

	public String getMethod()
	{
		return method;
	}

	public void setMethod(String method)
	{
		this.method = method;
	}

	public String getParams()
	{
		return params;
	}

	public void setParams(String params)
	{
		this.params = params;
	}

    public String getExceptionInfo()
    {
        return exceptionInfo;
    }

    public void setExceptionInfo(String exceptionInfo)
    {
        this.exceptionInfo = exceptionInfo;
    }
}
