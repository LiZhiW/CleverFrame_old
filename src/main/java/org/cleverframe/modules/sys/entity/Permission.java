package org.cleverframe.modules.sys.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * 系统权限<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月21日 下午3:53:27
 */
@Entity
@Table(name = "sys_permission")
@DynamicInsert
@DynamicUpdate
public class Permission extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 权限类型（1：URL权限；2：UI权限） */
    public static final char PERMISSION_TYPE_URL = '1';
    /** 权限类型（1：URL权限；2：UI权限） */
    public static final char PERMISSION_TYPE_UI = '2';
    
    /** 所属菜单,系统模块菜单ID */
    private Long menuId;

    /** 权限标识字符串 */
    private String permission;

    /** 权限类型（1：URL权限；2：UI权限） */
    private Character permissionType;
    
    /** 权限名称 */
    private String name;

    /** 权限对应的请求地址 */
    private String url;

    /*--------------------------------------------------------------
     * 			getter、setter
     * -------------------------------------------------------------*/
    public Permission()
    {
        super();
    }

    public Long getMenuId()
    {
        return menuId;
    }

    public void setMenuId(Long menuId)
    {
        this.menuId = menuId;
    }

    public String getPermission()
    {
        return permission;
    }

    public void setPermission(String permission)
    {
        this.permission = permission;
    }

    public Character getPermissionType()
    {
        return permissionType;
    }

    public void setPermissionType(Character permissionType)
    {
        this.permissionType = permissionType;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
