package org.cleverframe.modules.sys.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

/**
 * 系统数据字典<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月5日 下午9:58:49
 */
@Entity
@Table(name = "sys_dict")
@DynamicInsert
@DynamicUpdate
public class Dict extends IdEntity
{
	private static final long serialVersionUID = 1L;

	/** 字典键 */
	@NotNull(message = "字典名称不能为空")
	@Length(min = 1, max = 100, message = "字典名称长度必须是1——100个字符")
	private String dictKey;
	
	/** 字典数据值 */
	@NotNull(message = "字典值不能为空")
	@Length(min = 1, max = 255, message = "字典值长度必须是1——255个字符")
	private String dictValue;
	
	/** 字典分类 */
	@NotNull(message = "字典类型不能为空")
	@Length(min = 1, max = 100, message = "字典类型度必须是1——100个字符")
	private String dictType;
	
	/** 字典描述 */
	@NotNull(message = "字典描述不能为空")
	@Length(min = 1, max = 500, message = "字典描述度必须是1——500个字符")
	private String description;
	
	/** 排序 */
	@NotNull(message = "字典排序不能为空")
	private Integer sort;

    /*--------------------------------------------------------------
    *          getter、setter
    * -------------------------------------------------------------*/
	
    public String getDictKey()
    {
        return dictKey;
    }

    public void setDictKey(String dictKey)
    {
        this.dictKey = dictKey;
    }

    public String getDictValue()
    {
        return dictValue;
    }

    public void setDictValue(String dictValue)
    {
        this.dictValue = dictValue;
    }

    public String getDictType()
    {
        return dictType;
    }

    public void setDictType(String dictType)
    {
        this.dictType = dictType;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Integer getSort()
    {
        return sort;
    }

    public void setSort(Integer sort)
    {
        this.sort = sort;
    }
}
