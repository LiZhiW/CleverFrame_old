package org.cleverframe.modules.sys.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

/**
 * 系统多级字典<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月5日 下午10:19:35
 */
@Entity
@Table(name = "sys_mdict")
@DynamicInsert
@DynamicUpdate
public class MDict extends IdEntity
{
    private static final long serialVersionUID = 1L;
    
    /** 父级编号 */
    // @NotNull(message = "上级字典不能为空")
    private Long parentId;

    /** 树结构的全路径用“-”隔开 */
    // @NotNull(message = "结构路径不能为空")
    // @Length(min = 1, max = 255, message = "结构路径长度必须在1——255之间")
    private String fullPath;

    /** 字典名称 */
    @NotNull(message = "字典名称不能为空")
    @Length(min = 1, max = 100, message = "字典名称长度必须是1——100个字符")
    private String mdictKey;

    /** 字典值 */
    @NotNull(message = "字典值不能为空")
    @Length(min = 1, max = 255, message = "字典值长度必须是1——255个字符")
    private String mdictValue;

    /** 字典类型 */
    @NotNull(message = "字典类型不能为空")
    @Length(min = 1, max = 100, message = "字典类型度必须是1——100个字符")
    private String mdictType;

    /** 描述 */
    @NotNull(message = "字典描述不能为空")
    @Length(min = 1, max = 500, message = "字典描述度必须是1——500个字符")
    private String description;

    /** 排序 */
    @NotNull(message = "字典排序不能为空")
    private Integer sort;

    /**
     * 判断当前字典是否是根字典<br>
     * @return 当前字典是根字典返回True
     */
    public boolean isRootMDict()
    {
        if (getParentId() == null || getParentId() == -1L)
        {
            return true;
        }
        return false;
    }
    
    /*--------------------------------------------------------------
     *          getter、setter
     * -------------------------------------------------------------*/
    
    public Long getParentId()
    {
        return parentId;
    }

    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public String getFullPath()
    {
        return fullPath;
    }

    public void setFullPath(String fullPath)
    {
        this.fullPath = fullPath;
    }

    public String getMdictKey()
    {
        return mdictKey;
    }

    public void setMdictKey(String mdictKey)
    {
        this.mdictKey = mdictKey;
    }

    public String getMdictValue()
    {
        return mdictValue;
    }

    public void setMdictValue(String mdictValue)
    {
        this.mdictValue = mdictValue;
    }

    public String getMdictType()
    {
        return mdictType;
    }

    public void setMdictType(String mdictType)
    {
        this.mdictType = mdictType;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Integer getSort()
    {
        return sort;
    }

    public void setSort(Integer sort)
    {
        this.sort = sort;
    }
}
