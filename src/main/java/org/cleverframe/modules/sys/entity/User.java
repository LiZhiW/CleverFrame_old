package org.cleverframe.modules.sys.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 系统用户
 * 
 * @author LiZhiWei
 * @version 2015年5月23日 下午5:08:18
 */
@Entity
@Table(name = "sys_user")
@DynamicInsert
@DynamicUpdate
public class User extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 帐号状态：正常 (1：正常；2：锁定；3：删除)*/
    public static final char ACCOUNT_STATE_NORMAL = '1';
    /** 帐号状态：锁定 (1：正常；2：锁定；3：删除)*/
    public static final char ACCOUNT_STATE_LOCKED = '2';
    /** 帐号状态：锁定 (1：正常；2：锁定；3：删除)*/
    public static final char ACCOUNT_STATE_DELETE = '3';

    /** 用户状态(1：试用；2：在职；3：离职)*/
    public static final char USER_STATE_TRIAL = '1';
    /** 用户状态(1：试用；2：在职；3：离职)*/
    public static final char USER_STATE_SERVICE = '2';
    /** 用户状态(1：试用；2：在职；3：离职)*/
    public static final char USER_STATE_LEAVE = '3';

    /** 用户类型（1：内部用户；2：外部用户） */
    public static final char USER_TYPE_INNER = '1';
    /** 用户类型（1：内部用户；2：外部用户） */
    public static final char USER_TYPE_OUT = '2';
    
    /** 员工号前缀 */
    public static final String NO_PREFIX = "LZW";
    /** 员工号后缀 */
    public static final String NO_SUFFIX = "";
    /** 员工号数字部分长度 */
    public static final int NO_NUMBER_LENGTH = 6;
    
    /** 归属公司 */
    @NotNull(message = "归属公司不能为空")
    private Long homeCompany;

    /** 直属机构 */
    @NotNull(message = "直属机构不能为空")
    private Long homeOrg;

    /** 登录名，不能修改 */
    @NotNull(message = "登录名不能为空")
    @Length(min = 3, max = 20, message = "登录名长度必须在6——20之间")
    private String loginName;

    /** 密码 */
    @NotNull(message = "密码不能为空")
    @Length(min = 6, max = 100, message = "密码长度必须在6——100之间")
    private String password;

    /** 工号 */
    // @NotNull(message = "工号不能为空")
    // @Length(min = 6, max = 30, message = "工号长度必须在6——30之间")
    private String jobNo;

    /** 姓名 */
    @NotNull(message = "姓名不能为空")
    @Length(min = 3, max = 30, message = "姓名长度必须在6——30之间")
    private String name;

    /** 邮箱 */
    @Email(message = "邮箱格式不正确")
    @Length(max = 100, message = "邮箱长度必须在0——100之间")
    private String email;

    /** 电话 */
    @Length(max = 100, message = "电话长度必须在0——100之间")
    private String phone;

    /** 手机 */
    @Length(max = 100, message = "手机长度必须在0——100之间")
    private String mobile;

    /** 用户类型 */
    @NotNull(message = "用户类型不能为空")
    private Character userType;

    /** 最后登陆IP */
    @Length(max = 100, message = "最后登陆IP长度必须在0——100之间")
    private String loginIp;

    /** 最后登陆日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date loginDate;

    /** 帐号状态：正常 (1：正常；2：锁定) */
    // @NotNull(message = "帐号状态不能为空")
    private Character accountState = User.ACCOUNT_STATE_NORMAL;

    /** 用户状态(1：试用；2：在职；3：离职) */
    // @NotNull(message = "帐号状态不能为空")
    private Character userState;
    
	public User()
	{
		super();
	}

    /*--------------------------------------------------------------
    *          getter、setter
    * -------------------------------------------------------------*/
	
    public Long getHomeCompany()
    {
        return homeCompany;
    }

    public void setHomeCompany(Long homeCompany)
    {
        this.homeCompany = homeCompany;
    }

    public Long getHomeOrg()
    {
        return homeOrg;
    }

    public void setHomeOrg(Long homeOrg)
    {
        this.homeOrg = homeOrg;
    }

    public String getLoginName()
    {
        return loginName;
    }

    public void setLoginName(String loginName)
    {
        this.loginName = loginName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getJobNo()
    {
        return jobNo;
    }

    public void setJobNo(String jobNo)
    {
        this.jobNo = jobNo;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public Character getUserType()
    {
        return userType;
    }

    public void setUserType(Character userType)
    {
        this.userType = userType;
    }

    public String getLoginIp()
    {
        return loginIp;
    }

    public void setLoginIp(String loginIp)
    {
        this.loginIp = loginIp;
    }

    public Date getLoginDate()
    {
        return loginDate;
    }

    public void setLoginDate(Date loginDate)
    {
        this.loginDate = loginDate;
    }

    public Character getAccountState()
    {
        return accountState;
    }

    public void setAccountState(Character accountState)
    {
        this.accountState = accountState;
    }

    public Character getUserState()
    {
        return userState;
    }

    public void setUserState(Character userState)
    {
        this.userState = userState;
    }
}
