package org.cleverframe.modules.sys.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

/**
 * 系统菜单<br>
 * 
 * @author LiZW
 * @version 2015年5月29日 下午12:23:09
 */
@Entity
@Table(name = "sys_menu")
@DynamicInsert
@DynamicUpdate
public class Menu extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 菜单类型（1：系统模块菜单，2：个人快捷菜单） */
    public static final Character MENU_TYPE_SYSTEM = '1';
    /** 菜单类型（1：系统模块菜单，2：个人快捷菜单） */
    public static final Character MENU_TYPE_PERSONAL = '2';

    /** 个人快捷菜单的菜单类别值的前缀 */
    public static final String SHORTCUT_MENU_PREFIX = "ShortcutMenu-";

    /** 父级编号 */
    // @NotNull(message="上级菜单不能为空")
    private Long parentId;

    /** 树结构的全路径用“-”隔开 */
    // @NotNull(message="菜单树路径不能为空")
    // @Length(min = 1, max = 255, message = "菜单树路径长度必须在1——255之间")
    private String fullPath;

    /** 菜单类别,如：系统不同模块的菜单(模块名)、个人快捷菜单(login_name) */
    @NotNull(message = "菜单类别不能为空")
    @Length(min = 1, max = 100, message = "菜单类别长度必须在1——100之间")
    private String category;

    /** 菜单类型（1：系统模块菜单，2：个人快捷菜单） */
    @NotNull(message = "菜单类型不能为空")
    private Character menuType;

    /** 菜单名称 */
    @NotNull(message = "菜单名称不能为空")
    @Length(min = 1, max = 50, message = "菜单名称长度必须在1——50之间")
    private String name;

    /** 链接 */
    @NotNull(message = "菜单地址不能为空")
    @Length(min = 1, max = 255, message = "菜单地址长度必须在1——255之间")
    private String href;

    /** 图标 */
    @NotNull(message = "菜单图标不能为空")
    @Length(min = 1, max = 50, message = "菜单图标长度必须在1——50之间")
    private String icon;

    /** 排序(升序) */
    @NotNull(message = "菜单排序不能为空")
    private Integer sort;

    /**
     * 判断当前菜单是否是根菜单<br>
     * @return 当前菜单是根菜单返回True
     */
    public boolean isRootMenu()
    {
        if (getParentId() == null || getParentId() == -1L)
        {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------
     *          getter、setter
     * -------------------------------------------------------------*/
    public Long getParentId()
    {
        return parentId;
    }

    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public String getFullPath()
    {
        return fullPath;
    }

    public void setFullPath(String fullPath)
    {
        this.fullPath = fullPath;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public Character getMenuType()
    {
        return menuType;
    }

    public void setMenuType(Character menuType)
    {
        this.menuType = menuType;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getHref()
    {
        return href;
    }

    public void setHref(String href)
    {
        this.href = href;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public Integer getSort()
    {
        return sort;
    }

    public void setSort(Integer sort)
    {
        this.sort = sort;
    }
}
