package org.cleverframe.modules.sys.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

/**
 * 角色信息<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月21日 下午4:15:24
 */
@Entity
@Table(name = "sys_role")
@DynamicInsert
@DynamicUpdate
public class Role extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 角色名称 */
    @NotNull(message = "角色名称不能为空")
    @Length(min = 1, max = 50, message = "角色名称长度必须在1——50之间")
    private String name;

    public Role()
    {
        super();
    }

    /*--------------------------------------------------------------
     * 			getter、setter
     * -------------------------------------------------------------*/

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
