package org.cleverframe.modules.sys.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

/**
 * 组织机构<br>
 * 
 * @author LiZhiWei
 * @version 2015年6月21日 下午7:32:45
 */
@Entity
@Table(name = "sys_organization")
@DynamicInsert
@DynamicUpdate
public class Organization extends IdEntity
{
    private static final long serialVersionUID = 1L;
    
    /** （1: 集团；2：区域；3：公司；4：部门；5：小组） */
    public static final char TYPE_GROUP = '1';
    /** （1: 集团；2：区域；3：公司；4：部门；5：小组） */
    public static final char TYPE_AREA = '2';
    /** （1: 集团；2：区域；3：公司；4：部门；5：小组） */
    public static final char TYPE_COMPANY = '3';
    /** （1: 集团；2：区域；3：公司；4：部门；5：小组） */
    public static final char TYPE_DEPARTMENT = '4';
    /** （1: 集团；2：区域；3：公司；4：部门；5：小组） */
    public static final char TYPE_TEAM = '5';
    
    /** 机构编码层级串，一个层级的长度 */
    public static final int CODE_LEVEL_LENGTH = 3;
    
    /** 父级编号 */
    // @NotNull(message="父级编号不能为空")
    private Long parentId;

    /** 树结构的全路径用“-”隔开 */
    // @NotNull(message="树结构的全路径不能为空")
    // @Length(min=1,max=255,message="树结构的全路径长度必须在1——255之间")
    private String fullPath;

    /** 机构编码 */
    // @NotNull(message="机构编码不能为空")
    // @Length(min=1,max=255,message="机构编码长度必须在1——255之间")
    private String code;

    /** 机构名称 */
    @NotNull(message = "机构名称不能为空")
    @Length(min = 1, max = 100, message = "机构名称长度必须在1——100之间")
    private String name;

    /** 机构类型（1: 集团；2：区域；3：公司；4：部门；5：小组）  */
    @NotNull(message = "机构类型不能为空")
    private Character orgType;

    /** 联系地址 */
    @Length(max = 255, message = "联系地址长度必须在0——255之间")
    private String address;

    /** 邮政编码 */
    @Length(max = 50, message = "邮政编码长度必须在0——50之间")
    private String zipCode;

    /** 负责人 */
    @Length(max = 100, message = "负责人长度必须在0——100之间")
    private String master;

    /** 电话 */
    @Length(max = 100, message = "电话长度必须在0——100之间")
    private String phone;

    /** 传真 */
    @Length(max = 100, message = "传真长度必须在0——100之间")
    private String fax;

    /** 邮箱 */
    @Length(max = 100, message = "邮箱 长度必须在0——100之间")
    private String email;

    public Organization()
    {
        super();
    }

    /**
     * 判断当前机构是否是根机构<br>
     * @return 当前机构是根机构返回True
     */
    public boolean isRootOrg()
    {
        if (getParentId() == null || getParentId() == -1L)
        {
            return true;
        }
        return false;
    }

    /*--------------------------------------------------------------
     *          getter、setter
     * -------------------------------------------------------------*/
    
    public Long getParentId()
    {
        return parentId;
    }

    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public String getFullPath()
    {
        return fullPath;
    }

    public void setFullPath(String fullPath)
    {
        this.fullPath = fullPath;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Character getOrgType()
    {
        return orgType;
    }

    public void setOrgType(Character orgType)
    {
        this.orgType = orgType;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getMaster()
    {
        return master;
    }

    public void setMaster(String master)
    {
        this.master = master;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
