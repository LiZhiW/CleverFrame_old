package org.cleverframe.modules.sys.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

/**
 * 模版文件实体<br>
 * 
 * @author LiZW
 * @version 2015年12月25日 下午2:48:27
 */
@Entity
@Table(name = "sys_template_file")
@DynamicInsert
@DynamicUpdate
public class TemplateFile extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 文件信息ID */
    @NotNull(message = "文件信息ID不能为空")
    private Long fileinfoId;

    /** 模版名称 */
    @NotNull(message = "模版名称不能为空")
    @Length(min = 1, max = 100, message = "模版名称长度必须是1——100个字符")
    private String name;

    /*--------------------------------------------------------------
    *          getter、setter
    * -------------------------------------------------------------*/
    
    public Long getFileinfoId()
    {
        return fileinfoId;
    }

    public void setFileinfoId(Long fileinfoId)
    {
        this.fileinfoId = fileinfoId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
