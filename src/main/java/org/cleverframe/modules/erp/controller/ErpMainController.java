package org.cleverframe.modules.erp.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.ErpJspUrlPath;
import org.cleverframe.modules.erp.service.RawmaterialWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * erp模块Controller<br>
 * 
 * @author LiZW
 * @version 2015年12月21日 下午9:45:16
 */
@Controller
@RequestMapping("/${mvcPath}/erp")
public class ErpMainController extends BaseController
{
    @Autowired
    @Qualifier(ErpBeanNames.RawmaterialWarehouseService)
    private RawmaterialWarehouseService rawmaterialWarehouseService;
    
    /**
     * 跳转到ERP模块管理主页<br>
     */
    @RequestMapping("/getErpMainJsp")
    public ModelAndView getErpMainJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.ErpMain);
        return mav;
    }
    
    /**
     * 获取当前用户代办消息数量<br>
     */
    @ResponseBody
    @RequestMapping("/getErpToDo")
    public AjaxMessage getErpToDo(HttpServletRequest request, HttpServletResponse response)
    {
        AjaxMessage message = new AjaxMessage(true);
        Map<String, String> map = new HashMap<String, String>();
        int sum = 0;
        // 原料入库订单
        int count = rawmaterialWarehouseService.getCountByAuditorId(userUtils.getCurrentUserId());
        sum += count;
        map.put("RawmaterialWarehouse", "原料入库订单审核数：" + count);
        map.put("RawmaterialWarehouseCount", "" + count);
        
        // 其他
        message.setMessage(sum + "");
        message.setObject(map);
        return message;
    }
}
