package org.cleverframe.modules.erp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.ErpJspUrlPath;
import org.cleverframe.modules.erp.entity.Supplier;
import org.cleverframe.modules.erp.service.SupplierService;
import org.cleverframe.webui.easyui.data.ComboBoxJson;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @author LiZW
 * @version 2015年12月30日 下午9:15:36
 */
@Controller
@RequestMapping("/${mvcPath}/erp")
@Transactional(readOnly = true)
public class SupplierController extends BaseController
{
    @Autowired
    @Qualifier(ErpBeanNames.SupplierService)
    private SupplierService supplierService;

    /** 跳转到供应商管理页面 */
    @RequestMapping("/getSupplierJsp")
    public ModelAndView getSupplierJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.Supplier);
        return mav;
    }

    /** 跳转到供应商供应原料管理页面 */
    @RequestMapping("/getSupplierRawMaterialJsp")
    public ModelAndView getSupplierRawMaterialJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.SupplierRawMaterial);
        return mav;
    }
    
    /** 跳转到供应商供应原料查询页面 */
    @RequestMapping("/getSupplierRawMaterialQueryJsp")
    public ModelAndView getSupplierRawMaterialQueryJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.SupplierRawMaterialQuery);
        return mav;
    }
    
    /**
     *查询供应商信息，使用分页<br>
     * */
    @ResponseBody
    @RequestMapping("/findSupplierByPage")
    public DataGridJson<Supplier> findSupplierByPage(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "supplierNo", required = false, defaultValue = "") String supplierNo,
            @RequestParam(value = "supplierName", required = false, defaultValue = "") String supplierName,
            @RequestParam(value = "legalPeople", required = false, defaultValue = "") String legalPeople,
            @RequestParam(value = "dockingPeople", required = false, defaultValue = "") String dockingPeople,
            @RequestParam(value = "startDate", required = false, defaultValue = "") Date startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = "") Date endDate)
    {
        supplierName = "%" + supplierName + "%";
        DataGridJson<Supplier> json = new DataGridJson<Supplier>();
        Page<Supplier> page = supplierService.findSupplierByPage(new Page<Supplier>(request, response), supplierNo, supplierName, legalPeople, dockingPeople, startDate, endDate);
        json.setRows(page.getList());
        json.setTotal(page.getCount());
        return json;
    }
    
    /**
     * 增加供应商信息<br>
     * */
    @ResponseBody
    @RequestMapping("/addSupplier")
    @Transactional(readOnly = false)
    public AjaxMessage addSupplier(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @Valid Supplier supplier,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (beanValidator(bindingResult, message))
        {
            if (supplierService.addSupplier(supplier, message))
            {
                message.setMessage("供应商信息保存成功");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 更新供应商信息<br>
     * */
    @ResponseBody
    @RequestMapping("/updateSupplier")
    @Transactional(readOnly = false)
    public AjaxMessage updateSupplier(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @Valid Supplier supplier,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (beanValidator(bindingResult, message))
        {
            if (supplierService.updateSupplier(supplier, message))
            {
                message.setMessage("更新供应商成功");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 更新供应商信息<br>
     * */
    @ResponseBody
    @RequestMapping("/deleteSupplier")
    @Transactional(readOnly = false)
    public AjaxMessage deleteSupplier(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @Valid Supplier supplier,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (beanValidator(bindingResult, message))
        {
            if (supplierService.deleteSupplier(supplier, message))
            {
                message.setMessage("删除供应商成功");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 模糊查询供应商信息，使用分页<br>
     * */
    @ResponseBody
    @RequestMapping("/findSupplierByParam")
    public List<ComboBoxJson> findSupplierByParam(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "q", required = false, defaultValue = "") String q)
    {
        q = "%" + q + "%";
        List<ComboBoxJson> comboBoxList = new ArrayList<ComboBoxJson>();
        Page<Supplier> page = new Page<Supplier>(1, 50);
        supplierService.findSupplierByParam(page, q);
        for (Supplier supplier : page.getList())
        {
            String text = supplier.getSupplierName();
            String value = supplier.getId().toString();
            ComboBoxJson comboBox = new ComboBoxJson(false, text, value, supplier);
            comboBoxList.add(comboBox);
        }
        return comboBoxList;
    }
    
    /**
     * 增加供应商供应的原料<br>
     * */
    @ResponseBody
    @RequestMapping("/addSupplierRawmaterial")
    @Transactional(readOnly = false)
    public AjaxMessage addSupplierRawmaterial(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value = "supplierId", required = true) Long supplierId,
            @RequestParam(value = "rawmaterialId", required = true) Long rawmaterialId)
    {
        AjaxMessage message = new AjaxMessage(true, "供应商供应原料增加成功");
        if (supplierService.addSupplierRawmaterial(supplierId, rawmaterialId, message) == false)
        {
            message.setSuccess(false);
        }
        return message;
    }
    
    /**
     * 删除供应商供应的原料<br>
     * */
    @ResponseBody
    @RequestMapping("/deleteSupplierRawmaterial")
    @Transactional(readOnly = false)
    public AjaxMessage deleteSupplierRawmaterial(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @RequestParam(value = "supplierId", required = true) Long supplierId,
            @RequestParam(value = "rawmaterialId", required = true) Long rawmaterialId)
    {
        AjaxMessage message = new AjaxMessage(true, "供应商供应原料移除成功");
        if (supplierService.deleteSupplierRawmaterial(supplierId, rawmaterialId, message) == false)
        {
            message.setSuccess(false);
        }
        return message;
    }
}
