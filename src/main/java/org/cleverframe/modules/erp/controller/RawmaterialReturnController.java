package org.cleverframe.modules.erp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.ErpJspUrlPath;
import org.cleverframe.modules.erp.entity.RawmaterialReturn;
import org.cleverframe.modules.erp.service.RawmaterialReturnService;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author LiZW
 * @version 2016年1月19日 下午9:21:26
 */
@Controller
@RequestMapping("/${mvcPath}/erp")
@Transactional(readOnly = true)
public class RawmaterialReturnController extends BaseController
{
    @Autowired
    @Qualifier(ErpBeanNames.RawmaterialReturnService)
    private RawmaterialReturnService rawmaterialReturnService;

    /** 原料外发加工返回情况 */
    @RequestMapping("/getRawmaterialOutgoingReturnJsp")
    public ModelAndView getRawmaterialOutgoingReturnJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.RawmaterialOutgoingReturn);
        return mav;
    }

    /**
     * 查询原料出库单<br>
     * */
    @ResponseBody
    @RequestMapping("/findRawmaterialReturnByOutId")
    public DataGridJson<RawmaterialReturn> findRawmaterialReturnByOutId(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "outId", required = true) Long outId)
    {
        DataGridJson<RawmaterialReturn> json = new DataGridJson<RawmaterialReturn>();
        List<RawmaterialReturn> list = rawmaterialReturnService.findRawmaterialReturnByOutId(outId);
        json.setRows(list);
        return json;
    }

    /**
     * 增加原料外发加工返回原料信息<br>
     */
    @ResponseBody
    @RequestMapping("/addRawmaterialReturn")
    @Transactional(readOnly = false)
    public AjaxMessage addRawmaterialReturn(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialReturn rawmaterialReturn,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "增加原料外发加工返回原料信息成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialReturnService.addRawmaterialReturn(rawmaterialReturn, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 更新原料外发加工返回原料信息<br>
     */
    @ResponseBody
    @RequestMapping("/updateRawmaterialReturn")
    @Transactional(readOnly = false)
    public AjaxMessage updateRawmaterialReturn(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialReturn rawmaterialReturn,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "更新原料外发加工返回原料信息成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialReturnService.updateRawmaterialReturn(rawmaterialReturn, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 删除原料外发加工返回原料信息<br>
     */
    @ResponseBody
    @RequestMapping("/deleteRawmaterialReturn")
    @Transactional(readOnly = false)
    public AjaxMessage deleteRawmaterialReturn(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialReturn rawmaterialReturn,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料外发加工返回原料信息删除成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialReturnService.deleteRawmaterialReturn(rawmaterialReturn, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
}
