package org.cleverframe.modules.erp.controller;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.ErpJspUrlPath;
import org.cleverframe.modules.erp.entity.ErpOrder;
import org.cleverframe.modules.erp.service.ErpOrderService;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 订单Controller<br>
 * 
 * @author LiZW
 * @version 2016年2月14日 下午8:14:36
 */
@Controller
@RequestMapping("/${mvcPath}/erp")
@Transactional(readOnly = true)
public class ErpOrderController extends BaseController
{
    /** 日志对象 */
    // private final static Logger logger = LoggerFactory.getLogger(ErpOrderController.class);

    @Autowired
    @Qualifier(ErpBeanNames.ErpOrderService)
    private ErpOrderService erpOrderService;

    /** A生产订单-业务登记 */
    @RequestMapping("/getA_BusinessRegistrationJsp")
    public ModelAndView getA_BusinessRegistrationJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.A_BusinessRegistration);
        return mav;
    }

    /** A生产订单-工艺登记 */
    @RequestMapping("/getA_CraftsRegistrationJsp")
    public ModelAndView getA_CraftsRegistrationJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.A_CraftsRegistration);
        return mav;
    }

    /** A生产订单-厂长审核 */
    @RequestMapping("/getA_DirectorAuditJsp")
    public ModelAndView getA_DirectorAuditJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.A_DirectorAudit);
        return mav;
    }
    
    /**
     * 查询原料出库单<br>
     * */
    @ResponseBody
    @RequestMapping("/findOrderByPage")
    public DataGridJson<Map<Object, Object>> findOrderByPage(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "customer", required = false, defaultValue = "") String customer,
            @RequestParam(value = "orderNo", required = false, defaultValue = "") String orderNo,
            @RequestParam(value = "name", required = false, defaultValue = "") String name,
            @RequestParam(value = "specification", required = false, defaultValue = "") String specification,
            @RequestParam(value = "orderType", required = false, defaultValue = "") String orderType,
            @RequestParam(value = "auditorState", required = false, defaultValue = "") String auditorState,
            @RequestParam(value = "startDate", required = false, defaultValue = "") Date startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = "") Date endDate)
    {
        DataGridJson<Map<Object, Object>> json = new DataGridJson<Map<Object, Object>>();
        Page<Map<Object, Object>> page = new Page<Map<Object, Object>>(request, response);
        erpOrderService.findOrderByPage(page, customer, orderNo, name, specification, orderType, auditorState, startDate, endDate);
        json.setRows(page.getList());
        json.setTotal(page.getCount());
        return json;
    }
    
    /**
     * 增加订单
     * @return
     */
    @ResponseBody
    @RequestMapping("/addOrder")
    @Transactional(readOnly = false)
    public AjaxMessage addOrder(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @Valid ErpOrder erpOrder,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (beanValidator(bindingResult, message))
        {
            if (erpOrderService.addOrder(erpOrder, message))
            {
                message.setMessage("A生产订单-业务登记[成功]!");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 更新订单<br>
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateOrder")
    @Transactional(readOnly = false)
    public AjaxMessage updateOrder(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @Valid ErpOrder erpOrder,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (beanValidator(bindingResult, message))
        {
            if (erpOrderService.updateOrder(erpOrder, message))
            {
                message.setMessage("A生产订单-业务更新[成功]!");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
    } 
    
    /**
     * 删除订单<br>
     * @return
     */
    @ResponseBody
    @RequestMapping("/deleteOrder")
    @Transactional(readOnly = false)
    public AjaxMessage deleteOrder(
            HttpServletRequest request, 
            HttpServletResponse response, 
            @Valid ErpOrder erpOrder,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true);
        if (beanValidator(bindingResult, message))
        {
            if (erpOrderService.updateOrder(erpOrder, message))
            {
                message.setMessage("A生产订单-业务删除[成功]!");
            }
            else
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
}
