package org.cleverframe.modules.erp.controller;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.ErpJspUrlPath;
import org.cleverframe.modules.erp.entity.RawmaterialWarehouse;
import org.cleverframe.modules.erp.service.RawmaterialWarehouseService;
import org.cleverframe.modules.sys.entity.User;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 原料入库申请单Controller<br>
 * @author LiZW
 * @version 2016年1月6日 下午2:12:09
 */
@Controller
@RequestMapping("/${mvcPath}/erp")
@Transactional(readOnly = true)
public class RawmaterialWarehouseController extends BaseController
{
    @Autowired
    @Qualifier(ErpBeanNames.RawmaterialWarehouseService)
    private RawmaterialWarehouseService rawmaterialWarehouseService;

    /** 跳转到原料入库申请页面 */
    @RequestMapping("/getRawmaterialWarehouseJsp")
    public ModelAndView getRawmaterialWarehouseJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.RawmaterialWarehouse);
        return mav;
    }
    
    /** 跳转到原料入库审核页面 */
    @RequestMapping("/getRawmaterialWarehouseAuditJsp")
    public ModelAndView getRawmaterialWarehouseAuditJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.RawmaterialWarehouseAudit);
        return mav;
    }
    
    /**
     * 查询原料入库单<br>
     * */
    @ResponseBody
    @RequestMapping("/findRawmaterialWarehouseByPage")
    public DataGridJson<Map<Object, Object>> findRawmaterialWarehouseByPage(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "warehouseNo", required = false, defaultValue = "") String warehouseNo,
            @RequestParam(value = "rawmaterialCode", required = false, defaultValue = "") String rawmaterialCode,
            @RequestParam(value = "name", required = false, defaultValue = "") String name,
            @RequestParam(value = "auditorState", required = false, defaultValue = "") String auditorState,
            @RequestParam(value = "startDate", required = false, defaultValue = "") Date startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = "") Date endDate,
            @RequestParam(value = "supplierName", required = false, defaultValue = "") String supplierName)
    {
        DataGridJson<Map<Object, Object>> json = new DataGridJson<Map<Object, Object>>();
        Page<Map<Object, Object>> page = new Page<Map<Object, Object>>(request, response);
        rawmaterialWarehouseService.findRawmaterialWarehouseByPage(page, warehouseNo, rawmaterialCode, name, auditorState, startDate, endDate, supplierName);
        json.setRows(page.getList());
        json.setTotal(page.getCount());
        return json;
    }
    
    /**
     * 查询当前用户需要审核的原料入库单<br>
     * */
    @ResponseBody
    @RequestMapping("/findAuditRawmaterialWarehouseByPage")
    public DataGridJson<Map<Object, Object>> findAuditRawmaterialWarehouseByPage(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "warehouseNo", required = false, defaultValue = "") String warehouseNo,
            @RequestParam(value = "rawmaterialCode", required = false, defaultValue = "") String rawmaterialCode,
            @RequestParam(value = "name", required = false, defaultValue = "") String name,
            @RequestParam(value = "startDate", required = false, defaultValue = "") Date startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = "") Date endDate,
            @RequestParam(value = "supplierName", required = false, defaultValue = "") String supplierName)
    {
        DataGridJson<Map<Object, Object>> json = new DataGridJson<Map<Object, Object>>();
        Page<Map<Object, Object>> page = new Page<Map<Object, Object>>(request, response);
        Long userId = userUtils.getCurrentUserId();
        rawmaterialWarehouseService.findAuditRawmaterialWarehouseByPage(page, warehouseNo, rawmaterialCode, name, startDate, endDate, supplierName, userId);
        json.setRows(page.getList());
        json.setTotal(page.getCount());
        return json;
    }
    
    
    /**
     * 新建原料入库申请单<br>
     * */
    @ResponseBody
    @RequestMapping("/addRawmaterialWarehouse")
    @Transactional(readOnly = false)
    public AjaxMessage addRawmaterialWarehouse(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialWarehouse rawmaterialWarehouse,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料入库申请单保存成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialWarehouseService.addRawmaterialWarehouse(rawmaterialWarehouse, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 更新原料入库申请单<br>
     * */
    @ResponseBody
    @RequestMapping("/updateRawmaterialWarehouse")
    @Transactional(readOnly = false)
    public AjaxMessage updateRawmaterialWarehouse(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialWarehouse rawmaterialWarehouse,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料入库申请单更新成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialWarehouseService.updateRawmaterialWarehouse(rawmaterialWarehouse, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 删除原料入库申请单<br>
     * */
    @ResponseBody
    @RequestMapping("/deleteRawmaterialWarehouse")
    @Transactional(readOnly = false)
    public AjaxMessage deleteRawmaterialWarehouse(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialWarehouse rawmaterialWarehouse,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料入库申请单删除成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialWarehouseService.deleteRawmaterialWarehouse(rawmaterialWarehouse, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 原料入库申请单审核--通过<br>
     * */
    @ResponseBody
    @RequestMapping("/auditResultPass")
    @Transactional(readOnly = false)
    public AjaxMessage auditResultPass(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialWarehouse rawmaterialWarehouse,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料入库申请单已审核通过");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialWarehouseService.auditResultPass(rawmaterialWarehouse, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 原料入库申请单审核--驳回<br>
     * */
    @ResponseBody
    @RequestMapping("/auditResultReject")
    @Transactional(readOnly = false)
    public AjaxMessage auditResultReject(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialWarehouse rawmaterialWarehouse,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料入库申请单已审核驳回");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialWarehouseService.auditResultReject(rawmaterialWarehouse, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 变更原料申请单<br>
     * */
    @ResponseBody
    @RequestMapping("/changeRawmaterialWarehouse")
    @Transactional(readOnly = false)
    public AjaxMessage changeRawmaterialWarehouse(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialWarehouse rawmaterialWarehouse,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料入库申请单变更成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialWarehouseService.changeRawmaterialWarehouse(rawmaterialWarehouse, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 根据原料入库申请单编号查询申请单
     */
    @ResponseBody
    @RequestMapping("/getRawmaterialWarehouseByNo")
    public AjaxMessage getRawmaterialWarehouseByNo(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "warehouseNo", required = false, defaultValue = "") String warehouseNo)
    {
        AjaxMessage message = new AjaxMessage(true);
        Object object = rawmaterialWarehouseService.getRawmaterialWarehouseByNo(warehouseNo);
        if (object == null)
        {
            message.setSuccess(false);
            message.setMessage("原料入库申请单不存在");
        }
        else
        {
            message.setObject(object);
        }
        return message;
    }
    
    /**
     * 获取原料入库审核人<br>
     * */
    @ResponseBody
    @RequestMapping("/getAuditorByRawmaterialWarehouse")
    public AjaxMessage getAuditorByRawmaterialWarehouse(HttpServletRequest request, HttpServletResponse response)
    {
        AjaxMessage message = new AjaxMessage(true);
        User user = rawmaterialWarehouseService.getAuditorByRawmaterialWarehouse();
        if (user == null)
        {
            message.setSuccess(false);
            message.setMessage("获取原料入库审核人出错！请在配置字典[type=" + RawmaterialWarehouseService.AUDITOR_DICT_TYPE + ",key="
                    + RawmaterialWarehouseService.AUDITOR_DICT_KEY + "]的值是可用的用户ID！");
        }
        else
        {
            message.setObject(user);
        }
        return message;
    }
    
    
    
}
