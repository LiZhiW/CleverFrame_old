package org.cleverframe.modules.erp.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.ErpJspUrlPath;
import org.cleverframe.modules.erp.entity.RawmaterialOut;
import org.cleverframe.modules.erp.service.RawmaterialOutService;
import org.cleverframe.modules.sys.entity.User;
import org.cleverframe.webui.easyui.data.DataGridJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author LiZW
 * @version 2016年1月19日 下午9:21:26
 */
@Controller
@RequestMapping("/${mvcPath}/erp")
@Transactional(readOnly = true)
public class RawmaterialOutController extends BaseController
{
    @Autowired
    @Qualifier(ErpBeanNames.RawmaterialOutService)
    private RawmaterialOutService rawmaterialOutService;
    
    /** 跳转到原料出库申请-车间加工页面 */
    @RequestMapping("/getRawmaterialOutWorkshopJsp")
    public ModelAndView getRawmaterialOutWorkshopJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.RawmaterialOutWorkshop);
        return mav;
    }

    /** 跳转到原料出库-车间审核页面 */
    @RequestMapping("/getRawmaterialOutWorkshopAuditJsp")
    public ModelAndView getRawmaterialOutWorkshopAuditJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.RawmaterialOutWorkshopAudit);
        return mav;
    }

    /** 跳转到原料出库申请-外发加工页面 */
    @RequestMapping("/getRawmaterialOutgoingJsp")
    public ModelAndView getRawmaterialOutgoingJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.RawmaterialOutgoing);
        return mav;
    }

    /** 跳转到原料外发加工审核页面 */
    @RequestMapping("/getRawmaterialOutgoingAuditJsp")
    public ModelAndView getRawmaterialOutgoingAuditJsp(HttpServletRequest request, HttpServletResponse response)
    {
        ModelAndView mav = new ModelAndView(ErpJspUrlPath.RawmaterialOutgoingAudit);
        return mav;
    }

    /**
     * 查询原料出库单<br>
     * */
    @ResponseBody
    @RequestMapping("/findRawmaterialOutByPage")
    public DataGridJson<Map<Object, Object>> findRawmaterialOutByPage(
        HttpServletRequest request, 
        HttpServletResponse response,
        @RequestParam(value = "rawmaterialCode", required = false, defaultValue = "") String rawmaterialCode,
        @RequestParam(value = "outType", required = false, defaultValue = "") String outType,
        @RequestParam(value = "outNo", required = false, defaultValue = "") String outNo,
        @RequestParam(value = "name", required = false, defaultValue = "") String name,
        @RequestParam(value = "specification", required = false, defaultValue = "") String specification,
        @RequestParam(value = "auditorState", required = false, defaultValue = "") String auditorState,
        @RequestParam(value = "startDate", required = false, defaultValue = "") Date startDate,
        @RequestParam(value = "endDate", required = false, defaultValue = "") Date endDate)
    {
        DataGridJson<Map<Object, Object>> json = new DataGridJson<Map<Object, Object>>();
        Page<Map<Object, Object>> page = new Page<Map<Object, Object>>(request, response);
        rawmaterialOutService.findRawmaterialOutByPage(page, rawmaterialCode, outType, outNo, name, specification, auditorState, startDate, endDate);
        json.setRows(page.getList());
        json.setTotal(page.getCount());
        return json;
    }
    
    /**
     * 根据原料出库单编号或原料名称，模糊查询原料出库单<br>
     * */
    @ResponseBody
    @RequestMapping("/findRawmaterialOutByCombobox")
    public List<Map<Object, Object>> findRawmaterialOutByCombobox(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "q", required = false, defaultValue = "") String q)
    {
        q = "%" + q + "%";
        Page<Map<Object, Object>> page = new Page<Map<Object, Object>>(1, 50);
        rawmaterialOutService.findRawmaterialOutByCombobox(page, q);
        return page.getList();
    }
    
    
    /**
     * 查询当前用户需要审核的原料出库单<br>
     * */
    @ResponseBody
    @RequestMapping("/findAuditRawmaterialOutByPage")
    public DataGridJson<Map<Object, Object>> findAuditRawmaterialOutByPage(
        HttpServletRequest request, 
        HttpServletResponse response,
        @RequestParam(value = "outType", required = false, defaultValue = "") String outType,
        @RequestParam(value = "outNo", required = false, defaultValue = "") String outNo,
        @RequestParam(value = "name", required = false, defaultValue = "") String name,
        @RequestParam(value = "specification", required = false, defaultValue = "") String specification,
        @RequestParam(value = "auditorState", required = false, defaultValue = "") String auditorState,
        @RequestParam(value = "startDate", required = false, defaultValue = "") Date startDate,
        @RequestParam(value = "endDate", required = false, defaultValue = "") Date endDate)
    {
        DataGridJson<Map<Object, Object>> json = new DataGridJson<Map<Object, Object>>();
        Page<Map<Object, Object>> page = new Page<Map<Object, Object>>(request, response);
        Long userId = userUtils.getCurrentUserId();
        rawmaterialOutService.findAuditRawmaterialOutByPage(page, outType, outNo, name, specification, auditorState, startDate, endDate, userId);
        json.setRows(page.getList());
        json.setTotal(page.getCount());
        return json;
    }
    
    /**
     * 新建原料出库申请单<br>
     * */
    @ResponseBody
    @RequestMapping("/addRawmaterialOut")
    @Transactional(readOnly = false)
    public AjaxMessage addRawmaterialOut(
        HttpServletRequest request, 
        HttpServletResponse response,
        @Valid RawmaterialOut rawmaterialOut,
        BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料出库申请单保存成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialOutService.addRawmaterialOut(rawmaterialOut, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 更新原料出库申请单<br>
     * */
    @ResponseBody
    @RequestMapping("/updateRawmaterialOut")
    @Transactional(readOnly = false)
    public AjaxMessage updateRawmaterialOut(
        HttpServletRequest request, 
        HttpServletResponse response,
        @Valid RawmaterialOut rawmaterialOut,
        BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料出库申请单更新成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialOutService.updateRawmaterialOut(rawmaterialOut, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 删除原料出库申请单<br>
     * */
    @ResponseBody
    @RequestMapping("/deleteRawmaterialOut")
    @Transactional(readOnly = false)
    public AjaxMessage deleteRawmaterialOut(
        HttpServletRequest request, 
        HttpServletResponse response,
        @Valid RawmaterialOut rawmaterialOut,
        BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料出库申请单删除成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialOutService.deleteRawmaterialOut(rawmaterialOut, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 原料出库申请单审核--通过<br>
     * */
    @ResponseBody
    @RequestMapping("/rawmaterialOut/auditResultPass")
    @Transactional(readOnly = false)
    public AjaxMessage auditResultPass(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialOut rawmaterialOut,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料出库申请单已审核通过");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialOutService.auditResultPass(rawmaterialOut, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 原料出库申请单审核--驳回<br>
     * */
    @ResponseBody
    @RequestMapping("/rawmaterialOut/auditResultReject")
    @Transactional(readOnly = false)
    public AjaxMessage auditResultReject(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialOut rawmaterialOut,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料出库申请单已审核驳回");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialOutService.auditResultReject(rawmaterialOut, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 变更原料出库申请单<br>
     * */
    @ResponseBody
    @RequestMapping("/changeRawmaterialOut")
    @Transactional(readOnly = false)
    public AjaxMessage changeRawmaterialOut(
            HttpServletRequest request, 
            HttpServletResponse response,
            @Valid RawmaterialOut rawmaterialOut,
            BindingResult bindingResult)
    {
        AjaxMessage message = new AjaxMessage(true, "原料出库申请单变更成功");
        if (beanValidator(bindingResult, message))
        {
            if (rawmaterialOutService.changeRawmaterialOut(rawmaterialOut, message) == false)
            {
                message.setSuccess(false);
            }
        }
        return message;
    }
    
    /**
     * 根据原料出库申请单编号查询申请单
     */
    @ResponseBody
    @RequestMapping("/getRawmaterialOutByNo")
    public AjaxMessage getRawmaterialOutByNo(
            HttpServletRequest request, 
            HttpServletResponse response,
            @RequestParam(value = "outNo", required = true) String outNo)
    {
        AjaxMessage message = new AjaxMessage(true);
        Object object = rawmaterialOutService.getRawmaterialOutByNo(outNo);
        if (object == null)
        {
            message.setSuccess(false);
            message.setMessage("原料出库申请单不存在");
        }
        else
        {
            message.setObject(object);
        }
        return message;
    }
    
    /**
     * 获取原料出库审核人<br>
     * */
    @ResponseBody
    @RequestMapping("/getAuditorByRawmaterialOut")
    public AjaxMessage getAuditorByRawmaterialOut(HttpServletRequest request, HttpServletResponse response)
    {
        AjaxMessage message = new AjaxMessage(true);
        User user = rawmaterialOutService.getAuditorByRawmaterialOut();
        if (user == null)
        {
            message.setSuccess(false);
            message.setMessage("获取原料出库(车间加工)审核人出错！请在配置字典[type=" + RawmaterialOutService.AUDITOR_DICT_TYPE + ",key="
                    + RawmaterialOutService.AUDITOR_DICT_KEY + "]的值是可用的用户ID！");
        }
        else
        {
            message.setObject(user);
        }
        return message;
    }
}
