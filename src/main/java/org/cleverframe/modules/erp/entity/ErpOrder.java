package org.cleverframe.modules.erp.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * 订单实体类<br>
 * @author LiZW
 * @version 2016年2月14日 上午11:58:36
 */
@Entity
@Table(name = "erp_order")
@DynamicInsert
@DynamicUpdate
public class ErpOrder extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单号 */
    private String orderCode;

    /** 批号 */
    private String orderNo;

    /** 订单类型 */
    private Character orderType;

    /** 登记日期 */
    private Date orderDate;

    /** 客户 */
    private String customer;

    /** 名称 */
    private String name;

    /** 规格 */
    private String specification;

    /** 订单数量 */
    private BigDecimal orderQuantity;

    /** 循环 */
    private String cycling;

    /** 实产数量 */
    private BigDecimal realQuantity;

    /** 是否刷毛 */
    private Character isBrush;

    /** 交货日期 */
    private Date deliveryDate;

    /** 交货要求 */
    private String deliveryClaim;

    /** 制表人 */
    private Long tabulationPeople;

    /** 排纱审核 */
    private String yarnAudit;

    /** 车间班长 */
    private String workshopClass;

    /** 审核 */
    private String auditor;

    /** 是否有合同 */
    private Character hasContracts;

    /** 订单登记 */
    private Long orderEnrollment;

    /** 色号 */
    private String colorNo;

    /** 白纱 */
    private String whiteGauze;

    /** 色纱 */
    private String coloredYarn;

    /** 纱种 */
    private String yarnType;

    /** 原料纱种批号 */
    private String yarnTypeNo;

    /** 是否先下订单后原料出库（1：是；2：不是） */
    private Character isFirstOrder;

    /** 定型幅宽 */
    private String stereotypesWidth;

    /** 定型浆度 */
    private String stereotypesFreeness;

    /** 厂长审核 */
    private Long directorAudit;

    /** 业务审核 */
    private Long businessAudit;
    
    /** 生产部（H） */
    private Long production;

    /** 材料（H） */
    private String material;

    /** 百分比（H） */
    private BigDecimal percentage;

    /** 数量/公斤（H） */
    private BigDecimal quantity;

    /** 审核人ID */
    private Long auditorId;

    /** 流程状态（1：业务登记；2：工艺登记；3：厂长审核；4：审核完成-通过；5：审核完成-驳回） */
    private Character auditorState;

    /** 是否是历史记录（1：不是历史记录；2：是历史记录） */
    private Character isHistory;

    /** 是否是变更后的数据（1：不是；2：是） */
    private Character isChange;

    /*--------------------------------------------------------------
    *          getter、setter
    * -------------------------------------------------------------*/

    public String getOrderCode()
    {
        return orderCode;
    }

    public void setOrderCode(String orderCode)
    {
        this.orderCode = orderCode;
    }

    public String getOrderNo()
    {
        return orderNo;
    }

    public void setOrderNo(String orderNo)
    {
        this.orderNo = orderNo;
    }

    public Character getOrderType()
    {
        return orderType;
    }

    public void setOrderType(Character orderType)
    {
        this.orderType = orderType;
    }

    public Date getOrderDate()
    {
        return orderDate;
    }

    public void setOrderDate(Date orderDate)
    {
        this.orderDate = orderDate;
    }

    public String getCustomer()
    {
        return customer;
    }

    public void setCustomer(String customer)
    {
        this.customer = customer;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSpecification()
    {
        return specification;
    }

    public void setSpecification(String specification)
    {
        this.specification = specification;
    }

    public BigDecimal getOrderQuantity()
    {
        return orderQuantity;
    }

    public void setOrderQuantity(BigDecimal orderQuantity)
    {
        this.orderQuantity = orderQuantity;
    }

    public String getCycling()
    {
        return cycling;
    }

    public void setCycling(String cycling)
    {
        this.cycling = cycling;
    }

    public BigDecimal getRealQuantity()
    {
        return realQuantity;
    }

    public void setRealQuantity(BigDecimal realQuantity)
    {
        this.realQuantity = realQuantity;
    }

    public Character getIsBrush()
    {
        return isBrush;
    }

    public void setIsBrush(Character isBrush)
    {
        this.isBrush = isBrush;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate)
    {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryClaim()
    {
        return deliveryClaim;
    }

    public void setDeliveryClaim(String deliveryClaim)
    {
        this.deliveryClaim = deliveryClaim;
    }

    public Long getTabulationPeople()
    {
        return tabulationPeople;
    }

    public void setTabulationPeople(Long tabulationPeople)
    {
        this.tabulationPeople = tabulationPeople;
    }

    public String getYarnAudit()
    {
        return yarnAudit;
    }

    public void setYarnAudit(String yarnAudit)
    {
        this.yarnAudit = yarnAudit;
    }

    public String getWorkshopClass()
    {
        return workshopClass;
    }

    public void setWorkshopClass(String workshopClass)
    {
        this.workshopClass = workshopClass;
    }

    public String getAuditor()
    {
        return auditor;
    }

    public void setAuditor(String auditor)
    {
        this.auditor = auditor;
    }

    public Character getHasContracts()
    {
        return hasContracts;
    }

    public void setHasContracts(Character hasContracts)
    {
        this.hasContracts = hasContracts;
    }

    public Long getOrderEnrollment()
    {
        return orderEnrollment;
    }

    public void setOrderEnrollment(Long orderEnrollment)
    {
        this.orderEnrollment = orderEnrollment;
    }

    public String getColorNo()
    {
        return colorNo;
    }

    public void setColorNo(String colorNo)
    {
        this.colorNo = colorNo;
    }

    public String getWhiteGauze()
    {
        return whiteGauze;
    }

    public void setWhiteGauze(String whiteGauze)
    {
        this.whiteGauze = whiteGauze;
    }

    public String getColoredYarn()
    {
        return coloredYarn;
    }

    public void setColoredYarn(String coloredYarn)
    {
        this.coloredYarn = coloredYarn;
    }

    public String getYarnType()
    {
        return yarnType;
    }

    public void setYarnType(String yarnType)
    {
        this.yarnType = yarnType;
    }

    public String getYarnTypeNo()
    {
        return yarnTypeNo;
    }

    public void setYarnTypeNo(String yarnTypeNo)
    {
        this.yarnTypeNo = yarnTypeNo;
    }

    public Character getIsFirstOrder()
    {
        return isFirstOrder;
    }

    public void setIsFirstOrder(Character isFirstOrder)
    {
        this.isFirstOrder = isFirstOrder;
    }

    public String getStereotypesWidth()
    {
        return stereotypesWidth;
    }

    public void setStereotypesWidth(String stereotypesWidth)
    {
        this.stereotypesWidth = stereotypesWidth;
    }

    public String getStereotypesFreeness()
    {
        return stereotypesFreeness;
    }

    public void setStereotypesFreeness(String stereotypesFreeness)
    {
        this.stereotypesFreeness = stereotypesFreeness;
    }

    public Long getDirectorAudit()
    {
        return directorAudit;
    }

    public void setDirectorAudit(Long directorAudit)
    {
        this.directorAudit = directorAudit;
    }

    public Long getBusinessAudit()
    {
        return businessAudit;
    }

    public void setBusinessAudit(Long businessAudit)
    {
        this.businessAudit = businessAudit;
    }

    public Long getProduction()
    {
        return production;
    }

    public void setProduction(Long production)
    {
        this.production = production;
    }

    public String getMaterial()
    {
        return material;
    }

    public void setMaterial(String material)
    {
        this.material = material;
    }

    public BigDecimal getPercentage()
    {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage)
    {
        this.percentage = percentage;
    }

    public BigDecimal getQuantity()
    {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity)
    {
        this.quantity = quantity;
    }

    public Long getAuditorId()
    {
        return auditorId;
    }

    public void setAuditorId(Long auditorId)
    {
        this.auditorId = auditorId;
    }

    public Character getAuditorState()
    {
        return auditorState;
    }

    public void setAuditorState(Character auditorState)
    {
        this.auditorState = auditorState;
    }

    public Character getIsHistory()
    {
        return isHistory;
    }

    public void setIsHistory(Character isHistory)
    {
        this.isHistory = isHistory;
    }

    public Character getIsChange()
    {
        return isChange;
    }

    public void setIsChange(Character isChange)
    {
        this.isChange = isChange;
    }
}
