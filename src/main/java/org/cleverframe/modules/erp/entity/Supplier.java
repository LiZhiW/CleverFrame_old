package org.cleverframe.modules.erp.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 供应商信息实体类<br>
 * 
 * @author LiZW
 * @version 2015年12月23日 下午8:51:53
 */
@Entity
@Table(name = "erp_supplier")
@DynamicInsert
@DynamicUpdate
public class Supplier extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 供应商编号数字部分长度 */
    public static final int NO_NUMBER_LENGTH = 6;

    /** 供应商编号前缀 */
    public static final String NO_PREFIX = "GYS";

    /** 供应商编号后缀 */
    public static final String NO_SUFFIX = "";
    
    /** 供应商编号 */
    @NotNull(message = "供应商编号")
    @Length(min = 0, max = 100, message = "供应商编号长度必须是1——100个字符")
    private String supplierNo;
    
    /** 供应商名称 */
    @NotNull(message = "供应商名称")
    @Length(min = 1, max = 255, message = "供应商名称长度必须是1——255个字符")
    private String supplierName;
    
    /** 供应商法人 */
    @Length(max = 100, message = "供应商法人长度必须是1——100个字符")
    private String legalPeople;
    
    /** 供应商法人联系方式 */
    @Length(max = 100, message = "供应商法人联系方式 长度必须是1——100个字符")
    private String legalPeopleContact;
    
    /** 供应商对接人 */
    @NotNull(message = "供应商对接人")
    @Length(min = 1, max = 100, message = "供应商对接人长度必须是1——100个字符")
    private String dockingPeople;
    
    /** 供应商对接人联系方式 */
    @NotNull(message = "供应商对接人联系方式")
    @Length(min = 1, max = 255, message = "供应商对接人联系方式长度必须是1——255个字符")
    private String dockingPeopleContact;
    
    /** 供应商开始供应日期 */
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    private Date startSupplyDate;

    /*--------------------------------------------------------------
    *          getter、setter
    * -------------------------------------------------------------*/
    
    public String getSupplierNo()
    {
        return supplierNo;
    }

    public void setSupplierNo(String supplierNo)
    {
        this.supplierNo = supplierNo;
    }

    public String getSupplierName()
    {
        return supplierName;
    }

    public void setSupplierName(String supplierName)
    {
        this.supplierName = supplierName;
    }

    public String getLegalPeople()
    {
        return legalPeople;
    }

    public void setLegalPeople(String legalPeople)
    {
        this.legalPeople = legalPeople;
    }

    public String getLegalPeopleContact()
    {
        return legalPeopleContact;
    }

    public void setLegalPeopleContact(String legalPeopleContact)
    {
        this.legalPeopleContact = legalPeopleContact;
    }

    public String getDockingPeople()
    {
        return dockingPeople;
    }

    public void setDockingPeople(String dockingPeople)
    {
        this.dockingPeople = dockingPeople;
    }

    public String getDockingPeopleContact()
    {
        return dockingPeopleContact;
    }

    public void setDockingPeopleContact(String dockingPeopleContact)
    {
        this.dockingPeopleContact = dockingPeopleContact;
    }

    public Date getStartSupplyDate()
    {
        return startSupplyDate;
    }

    public void setStartSupplyDate(Date startSupplyDate)
    {
        this.startSupplyDate = startSupplyDate;
    }
}
