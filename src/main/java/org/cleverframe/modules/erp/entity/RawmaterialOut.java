package org.cleverframe.modules.erp.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

/**
 * 原料出库申请实体类<br>
 * @author LiZW
 * @version 2016年1月19日 下午9:20:26
 */
@Entity
@Table(name = "erp_rawmaterial_out")
@DynamicInsert
@DynamicUpdate
public class RawmaterialOut extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 原料出库申请单编号数字部分长度 */
    public static final int NO_NUMBER_LENGTH = 6;

    /** 原料出库申请单编号前缀 */
    public static final String NO_PREFIX = "YLCK";

    /** 原料出库申请单编号后缀 */
    public static final String NO_SUFFIX = "";

    /** 原料出库申请单历史单的编号后缀 */
    public static final String HISTORY_NO_SUFFIX = "-LS";
    
    /** 1：等待审核 */
    public static final char PENDING_AUDIT = '1';
    /** 2：审核中-已经审核过但是没有完成  */
    public static final char AUDIT_ING = '2';
    /** 3：审核完成-通过 */
    public static final char AUDIT_PASS = '3';
    /** 4：审核完成-驳回 */
    public static final char AUDIT_REJECT = '4';
    
    /** 1：开票  */
    public static final char INVOICE_YES = '1';
    /** 2：不开票 */
    public static final char INVOICE_NO = '2';

    /** 1：不是历史记录  */
    public static final char HISTORY_NO = '1';
    /** 2：是历史记录 */
    public static final char HISTORY_YES = '2';

    /** 1：不是变更后的数据  */
    public static final char CHANGE_NO = '1';
    /** 2：是变更后的数据 */
    public static final char CHANGE_YES = '2';

    /** 1：车间加工 */
    public static final char WORK_SHOP = '1';
    /** 2：外发加工 */
    public static final char OUT_GOING = '2';
    
    /** 1:先下订单后原料出库 */
    public static final char FIRST_ORDER_YES = '1';
    /** 2:不是先下订单后原料出库 */
    public static final char FIRST_ORDER_NO = '2';
    
    /** 原料出库申请单编号 */
    private String outNo;

    /** 原料编码 */
    @NotNull(message = "原料编码不能为空")
    @Length(min = 1, max = 100, message = "原料编码长度必须是1——100个字符")
    private String rawmaterialCode;

    /** 原料名称 */
    @NotNull(message = "原料名称不能为空")
    @Length(min = 1, max = 100, message = "原料名称长度必须是1——100个字符")
    private String name;

    /** 原料规格 */
    @NotNull(message = "原料规格不能为空")
    @Length(min = 1, max = 100, message = "原料规格长度必须是1——100个字符")
    private String specification;

    /** 原料类型 */
    @Length(max = 100, message = "原料类型长度必须是0——100个字符")
    private String rawmaterialType;

    /** 原料色号 */
    @Length(max = 100, message = "原料色号长度必须是0——100个字符")
    private String colorNo;

    /** 出库数量 */
    @NotNull(message = "入库数量不能为空")
    @DecimalMin(value = "0.00", message = "入库数量不能小于0")
    private BigDecimal quantity;

    /** 出库日期 */
    @NotNull(message = "入库日期不能为空")
    private Date outDate;

    /** 出库类型（1：车间加工；2：外发加工） */
    @NotNull(message = "出库类型不能为空")
    private Character outType;

    /** 订单编号 */
    @Length(max = 100, message = "订单编号长度必须是0——100个字符")
    private String orderNo;
    
    /** 是否先下订单后原料出库（1：是；2：不是） */
    @NotNull(message = "是否先下订单后原料出库不能为空")
    private Character isFirstOrder;

    /** 审核人ID */
    @NotNull(message = "审核人不能为空")
    private Long auditorId;

    /** 审核状态（1：等待审核；2：审核中-已经审核过但是没有完成；3：审核完成-通过；4：审核完成-驳回） */
    @NotNull(message = "审核状态不能为空")
    private Character auditorState;

    /** 单价 */
    @NotNull(message = "单价不能为空")
    @DecimalMin(value = "0.00", message = "单价不能小于0")
    private BigDecimal unitPrice;

    /** 总额 */
    @NotNull(message = "应付金额不能为空")
    @DecimalMin(value = "0.00", message = "应付金额不能小于0")
    private BigDecimal aggregateAmount;

    /** 是否开票（1：开票；2：不开票） */
    @NotNull(message = "是否开票不能为空")
    private Character isInvoice;

    /** 是否是历史记录（1：不是历史记录；2：是历史记录） */
    @NotNull(message = "是否是历史记录不能为空")
    private Character isHistory;

    /** 是否是变更后的数据（1：不是；2：是） */
    @NotNull(message = "是否是变更后的数据不能为空")
    private Character isChange;

    /*--------------------------------------------------------------
    *          getter、setter
    * -------------------------------------------------------------*/

    public String getOutNo()
    {
        return outNo;
    }

    public void setOutNo(String outNo)
    {
        this.outNo = outNo;
    }

    public String getRawmaterialCode()
    {
        return rawmaterialCode;
    }

    public void setRawmaterialCode(String rawmaterialCode)
    {
        this.rawmaterialCode = rawmaterialCode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSpecification()
    {
        return specification;
    }

    public void setSpecification(String specification)
    {
        this.specification = specification;
    }

    public String getRawmaterialType()
    {
        return rawmaterialType;
    }

    public void setRawmaterialType(String rawmaterialType)
    {
        this.rawmaterialType = rawmaterialType;
    }

    public String getColorNo()
    {
        return colorNo;
    }

    public void setColorNo(String colorNo)
    {
        this.colorNo = colorNo;
    }

    public BigDecimal getQuantity()
    {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity)
    {
        this.quantity = quantity;
    }

    public Date getOutDate()
    {
        return outDate;
    }

    public void setOutDate(Date outDate)
    {
        this.outDate = outDate;
    }

    public Character getOutType()
    {
        return outType;
    }

    public void setOutType(Character outType)
    {
        this.outType = outType;
    }

    public Character getIsFirstOrder()
    {
        return isFirstOrder;
    }

    public void setIsFirstOrder(Character isFirstOrder)
    {
        this.isFirstOrder = isFirstOrder;
    }

    public Long getAuditorId()
    {
        return auditorId;
    }

    public void setAuditorId(Long auditorId)
    {
        this.auditorId = auditorId;
    }

    public Character getAuditorState()
    {
        return auditorState;
    }

    public void setAuditorState(Character auditorState)
    {
        this.auditorState = auditorState;
    }

    public BigDecimal getUnitPrice()
    {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice)
    {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getAggregateAmount()
    {
        return aggregateAmount;
    }

    public void setAggregateAmount(BigDecimal aggregateAmount)
    {
        this.aggregateAmount = aggregateAmount;
    }

    public Character getIsInvoice()
    {
        return isInvoice;
    }

    public void setIsInvoice(Character isInvoice)
    {
        this.isInvoice = isInvoice;
    }

    public Character getIsHistory()
    {
        return isHistory;
    }

    public void setIsHistory(Character isHistory)
    {
        this.isHistory = isHistory;
    }

    public Character getIsChange()
    {
        return isChange;
    }

    public void setIsChange(Character isChange)
    {
        this.isChange = isChange;
    }

    public String getOrderNo()
    {
        return orderNo;
    }

    public void setOrderNo(String orderNo)
    {
        this.orderNo = orderNo;
    }
}
