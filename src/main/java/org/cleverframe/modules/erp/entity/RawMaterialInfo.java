package org.cleverframe.modules.erp.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

/**
 * 原料资料实体类<br>
 * 
 * @author LiZW
 * @version 2015年12月23日 下午8:37:32
 */
@Entity
@Table(name = "erp_rawmaterial_info")
@DynamicInsert
@DynamicUpdate
public class RawMaterialInfo extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 原料编码数字部分长度 */
    public static final int CODE_NUMBER_LENGTH = 6;
    
    /** 原料编码前缀 */
    public static final String CODE_PREFIX = "YL";
    
    /** 原料编码后缀 */
    public static final String CODE_SUFFIX = "";
    
    /** 原料编码 */
    // @NotNull(message = "原料编码不能为空")
    // @Length(min = 1, max = 100, message = "原料编码长度必须是1——100个字符")
    private String rawmaterialCode;

    /** 原料名称 */
    @NotNull(message = "原料名称不能为空")
    @Length(min = 1, max = 100, message = "原料名称长度必须是1——100个字符")
    private String name;

    /** 原料规格 */
    @NotNull(message = "原料规格不能为空")
    @Length(min = 1, max = 100, message = "原料规格长度必须是1——100个字符")
    private String specification;

    /** 原料类型 */
    @Length(max = 100, message = "原料类型长度必须是0——100个字符")
    private String rawmaterialType;
    
    /** 原料色号 */
    @Length(max = 100, message = "原料类型长度必须是0——100个字符")
    private String colorNo;
    
    /*--------------------------------------------------------------
    *          getter、setter
    * -------------------------------------------------------------*/

    public String getRawmaterialCode()
    {
        return rawmaterialCode;
    }

    public void setRawmaterialCode(String rawmaterialCode)
    {
        this.rawmaterialCode = rawmaterialCode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSpecification()
    {
        return specification;
    }

    public void setSpecification(String specification)
    {
        this.specification = specification;
    }

    public String getRawmaterialType()
    {
        return rawmaterialType;
    }

    public void setRawmaterialType(String rawmaterialType)
    {
        this.rawmaterialType = rawmaterialType;
    }

    public String getColorNo()
    {
        return colorNo;
    }

    public void setColorNo(String colorNo)
    {
        this.colorNo = colorNo;
    }
}
