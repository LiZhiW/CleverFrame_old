package org.cleverframe.modules.erp.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.cleverframe.common.persistence.entity.IdEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;

/**
 * 原料外发加工返回原料信息<br>
 * @author LiZW
 * @version 2016年1月19日 下午9:53:49
 */
@Entity
@Table(name = "erp_rawmaterial_return")
@DynamicInsert
@DynamicUpdate
public class RawmaterialReturn extends IdEntity
{
    private static final long serialVersionUID = 1L;

    /** 原料出库申请单ID */
    @NotNull(message = "原料出库申请单ID")
    private Long outId;

    /** 原料编码 */
    @NotNull(message = "原料编码不能为空")
    @Length(min = 1, max = 100, message = "原料编码长度必须是1——100个字符")
    private String rawmaterialCode;

    /** 原料名称 */
    @NotNull(message = "原料名称不能为空")
    @Length(min = 1, max = 100, message = "原料名称长度必须是1——100个字符")
    private String name;

    /** 原料规格 */
    @NotNull(message = "原料规格不能为空")
    @Length(min = 1, max = 100, message = "原料规格长度必须是1——100个字符")
    private String specification;

    /** 原料类型 */
    @Length(max = 100, message = "原料类型长度必须是0——100个字符")
    private String rawmaterialType;

    /** 原料色号 */
    @Length(max = 100, message = "原料色号长度必须是0——100个字符")
    private String colorNo;

    /** 返回数量 */
    private BigDecimal returnQuantity;

    /** 损耗数量 */
    private BigDecimal lossQuantity;

    public Long getOutId()
    {
        return outId;
    }

    public void setOutId(Long outId)
    {
        this.outId = outId;
    }

    public String getRawmaterialCode()
    {
        return rawmaterialCode;
    }

    public void setRawmaterialCode(String rawmaterialCode)
    {
        this.rawmaterialCode = rawmaterialCode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSpecification()
    {
        return specification;
    }

    public void setSpecification(String specification)
    {
        this.specification = specification;
    }

    public String getRawmaterialType()
    {
        return rawmaterialType;
    }

    public void setRawmaterialType(String rawmaterialType)
    {
        this.rawmaterialType = rawmaterialType;
    }

    public String getColorNo()
    {
        return colorNo;
    }

    public void setColorNo(String colorNo)
    {
        this.colorNo = colorNo;
    }

    public BigDecimal getReturnQuantity()
    {
        return returnQuantity;
    }

    public void setReturnQuantity(BigDecimal returnQuantity)
    {
        this.returnQuantity = returnQuantity;
    }

    public BigDecimal getLossQuantity()
    {
        return lossQuantity;
    }

    public void setLossQuantity(BigDecimal lossQuantity)
    {
        this.lossQuantity = lossQuantity;
    }
}
