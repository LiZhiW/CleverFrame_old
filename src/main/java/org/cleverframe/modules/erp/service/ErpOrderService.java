package org.cleverframe.modules.erp.service;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.common.utils.DateUtils;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.dao.ErpOrderDao;
import org.cleverframe.modules.erp.entity.ErpOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository(ErpBeanNames.ErpOrderService)
public class ErpOrderService extends BaseService
{
    @Autowired
    @Qualifier(ErpBeanNames.ErpOrderDao)
    private ErpOrderDao erpOrderDao;
    
    /**
     * 分页查询订单<br>
     * @param customer 查询参数：客户
     * @param orderNo 查询参数：批号
     * @param name 查询参数：名称
     * @param specification 查询参数：规格
     * @param orderType 查询参数：订单类型
     * @param auditorState 查询参数：订单状态
     * @param startDate 查询参数：登记日期(小)
     * @param endDate 查询参数：登记日期(大)
     * @return
     */
    public Page<Map<Object, Object>> findOrderByPage(
            Page<Map<Object, Object>> page,
            String customer, 
            String orderNo, 
            String name, 
            String specification, 
            String orderType, 
            String auditorState,
            Date startDate, 
            Date endDate)
    {
        return erpOrderDao.findOrderByPage(page, customer, orderNo, name, specification, orderType, auditorState, startDate, endDate);
    }
    
    /**
     * 新增订单<br>
     * 1.初始化审核时填写的数据<br>
     * 2.生成订单号<br>
     * 3.生成批号<br>
     */
    public synchronized boolean addOrder(ErpOrder erpOrder, AjaxMessage message)
    {
        // 订单号码 ： NO20150910
        String orderCode = "NO" + DateUtils.getDate(new Date().getTime(), "yyyyMM") + "__";
        String maxOrderCode = erpOrderDao.getMaxOrderCode(orderCode, erpOrder.getOrderType());
        if (maxOrderCode != null && maxOrderCode.length() >= 10)
        {
            maxOrderCode = maxOrderCode.substring(8, maxOrderCode.length());
            int count = NumberUtils.toInt(maxOrderCode);
            count++;
            maxOrderCode = "00000000" + count;
            maxOrderCode = maxOrderCode.substring(maxOrderCode.length()-2, maxOrderCode.length());
            orderCode = orderCode.replace("__", maxOrderCode);
        }
        else
        {
            orderCode = orderCode.replace("__", "01");
        }
        erpOrder.setOrderCode(orderCode);
        
        // 批号 ：16A0001   1：A生产单；2：A生产样板单；3：X生产单；4：X生产样板单；5：H生产单；6：H生产样板单；
        String orderNo = null;
        switch (erpOrder.getOrderType())
        {
        case '1':
            orderNo = "A";
            break;
        case '2':
            // orderNo = "A";
            orderNo = "AY";
            break;
        case '3':
            orderNo = "X";
            break;
        case '4':
            // orderNo = "X";
            orderNo = "XY";
            break;
        case '5':
            orderNo = "H";
            break;
        case '6':
            // orderNo = "H";
            orderNo = "HY";
            break;
        default:
            message.setMessage("订单类型错误！");
            return false;
            // break;
        }
        String year = DateUtils.getYear(); // 获取年
        year = year.substring(2, 4);
        orderNo = year + orderNo + "____";
        String maxOrderNo = erpOrderDao.getMaxOrderNo(orderNo, erpOrder.getOrderType());
        if (maxOrderNo != null && maxOrderNo.length() >= 7)
        {
            maxOrderNo = maxOrderNo.substring(maxOrderNo.length() - 4, maxOrderNo.length());
            int count = NumberUtils.toInt(maxOrderNo);
            count++;
            maxOrderNo = "00000000" + count;
            maxOrderNo = maxOrderNo.substring(maxOrderNo.length() - 4, maxOrderNo.length());
            orderNo = orderNo.replace("____", maxOrderNo);
        }
        else
        {
            orderNo = orderNo.replace("____", "0001");
        }
        erpOrder.setOrderNo(orderNo);
        
        // 保存数据
        erpOrderDao.getHibernateDao().save(erpOrder);
        return true;
    }
    
    /**
     * 新增订单<br>
     */
    public boolean updateOrder(ErpOrder erpOrder, AjaxMessage message)
    {
        ErpOrder oldErpOrder = erpOrderDao.getHibernateDao().get(erpOrder.getId());
        if (oldErpOrder == null)
        {
            message.setMessage("订单不存在！");
            return false;
        }

        erpOrderDao.getHibernateDao().getSession().evict(oldErpOrder);
        erpOrderDao.getHibernateDao().update(erpOrder);
        return true;
    }
    
    /**
     * 删除订单<br>
     */
    public boolean deleteOrder(ErpOrder erpOrder, AjaxMessage message)
    {
        ErpOrder oldErpOrder = erpOrderDao.getHibernateDao().get(erpOrder.getId());
        if (oldErpOrder == null)
        {
            message.setMessage("订单不存在！");
            return false;
        }

        erpOrderDao.getHibernateDao().delete(oldErpOrder);
        return true;
    }
    
    
    
    
    
    
    
    
    
    
    
}
