package org.cleverframe.modules.erp.service;

import java.util.List;

import org.cleverframe.common.service.BaseService;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.dao.RawmaterialReturnDao;
import org.cleverframe.modules.erp.entity.RawmaterialReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 原料外发加工返回原料信息Service<br>
 * @author LiZW
 * @version 2016年1月20日 下午10:15:53
 */
@Service(ErpBeanNames.RawmaterialReturnService)
public class RawmaterialReturnService extends BaseService
{
    @Autowired
    @Qualifier(ErpBeanNames.RawmaterialReturnDao)
    private RawmaterialReturnDao rawmaterialReturnDao;

    /**
     * 根据外发加工出库单ID 查询所有外发加工返回原料信息<br>
     * @param outId 查询参数：外发加工出库单ID
     * @return 外发加工返回原料信息
     */
    public List<RawmaterialReturn> findRawmaterialReturnByOutId(Long outId)
    {
        return rawmaterialReturnDao.findRawmaterialReturnByOutId(outId);
    }

    /**
     * 增加原料外发加工返回原料信息<br>
     * @param rawmaterialReturn
     * @param message
     * @return
     */
    public boolean addRawmaterialReturn(RawmaterialReturn rawmaterialReturn, AjaxMessage message)
    {
        rawmaterialReturnDao.getHibernateDao().save(rawmaterialReturn);
        return true;
    }

    /**
     * 更新原料外发加工返回原料信息<br>
     * @param rawmaterialReturn
     * @param message
     * @return
     */
    public boolean updateRawmaterialReturn(RawmaterialReturn rawmaterialReturn, AjaxMessage message)
    {
        rawmaterialReturnDao.getHibernateDao().update(rawmaterialReturn);
        return true;
    }

    /**
     * 原料外发加工返回原料删除信息<br>
     * @param rawmaterialReturn
     * @param message
     * @return
     */
    public boolean deleteRawmaterialReturn(RawmaterialReturn rawmaterialReturn, AjaxMessage message)
    {
        rawmaterialReturnDao.getHibernateDao().delete(rawmaterialReturn);
        return true;
    }
}
