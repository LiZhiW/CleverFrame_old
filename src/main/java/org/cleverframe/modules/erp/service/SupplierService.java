package org.cleverframe.modules.erp.service;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.service.BaseService;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.dao.SupplierDao;
import org.cleverframe.modules.erp.entity.RawMaterialInfo;
import org.cleverframe.modules.erp.entity.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author LiZW
 * @version 2015年12月30日 下午10:25:20
 */
@Service(ErpBeanNames.SupplierService)
public class SupplierService extends BaseService
{
    @Autowired
    @Qualifier(ErpBeanNames.SupplierDao)
    private SupplierDao supplierDao;
    
    /**
     * 使用分页查询供应商信息<br>
     * @param page 分页数据
     * @param supplierNo 查询参数：供应商编号
     * @param supplierName 查询参数：供应商名称
     * @param legalPeople 查询参数：供应商法人
     * @param dockingPeople 查询参数：供应商对接人
     * @param startDate 查询参数：供应日期(最小)
     * @param endDate 查询参数：供应日期(最大)
     */
    public Page<Supplier> findSupplierByPage(Page<Supplier> page, String supplierNo, String supplierName, String legalPeople, String dockingPeople, Date startDate, Date endDate)
    {
        return supplierDao.findSupplierByPage(page, supplierNo, supplierName, legalPeople, dockingPeople, startDate, endDate);
    }
    
    /**
     * 新增供应商<br>
     * 1.供应商名称不能重复<br>
     * 2.自动生成供应商编号，需要同步<br>
     */
    public synchronized boolean addSupplier(Supplier supplier, AjaxMessage message)
    {
        /* ----------------------------------供应商验证---------------------------------- */
        String supplierName = StringUtils.trim(supplier.getSupplierName());
        supplier.setSupplierName(supplierName);
        if (supplierDao.supplierIsRepeat(null, supplierName))
        {
            message.setMessage("供应商名称重复");
            return false;
        }
        
        // 计算原料编码
        String supplierMaxNo = supplierDao.getSupplierMaxNO();
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < Supplier.NO_NUMBER_LENGTH; i++)
        {
            str.append('0');
        }
        if (StringUtils.isBlank(supplierMaxNo))
        {
            str.append('1');
        }
        else
        {
            supplierMaxNo = supplierMaxNo.replace(Supplier.NO_PREFIX, "").replace(Supplier.NO_SUFFIX, "");
            int max = NumberUtils.toInt(supplierMaxNo, 0) + 1;
            str.append(max);
        }
        supplierMaxNo = str.toString().substring(str.length() - Supplier.NO_NUMBER_LENGTH);
        supplierMaxNo = Supplier.NO_PREFIX + supplierMaxNo + Supplier.NO_SUFFIX;
        supplier.setSupplierNo(supplierMaxNo);
        
        /* ----------------------------------供应商保存---------------------------------- */
        supplierDao.getHibernateDao().save(supplier);
        return true;
    }
    
    /**
     * 更新供应商信息<br>
     * 1.判断更新供应商是否存在<br>
     * 2.供应商名称不能重复<br>
     * 3.不能修改“供应商编号”<br>
     */
    public boolean updateSupplier(Supplier supplier, AjaxMessage message)
    {
        /* ----------------------------------供应商更新验证---------------------------------- */
        Supplier oldSupplier = supplierDao.getHibernateDao().get(supplier.getId());
        if(oldSupplier == null)
        {
            message.setMessage("供应商不存在");
            return false;
        }
        if(oldSupplier.getSupplierNo().equals(supplier.getSupplierNo()) == false)
        {
            message.setMessage("不能修改供应商编号");
            return false;
        }
        if (oldSupplier.getSupplierName().equals(supplier.getSupplierName()) == false)
        {
            if (supplierDao.supplierIsRepeat(supplier.getId(), supplier.getSupplierName()))
            {
                message.setMessage("供应商名称重复");
                return false;
            }
        }
        
        /* ----------------------------------供应商更新---------------------------------- */
        supplierDao.getHibernateDao().getSession().evict(oldSupplier);
        supplierDao.getHibernateDao().update(supplier);
        return true;
    }
    
    /**
     * 删除供应商<br>
     * 1.供应商被其他地方引用不能删除<br>
     */
    public boolean deleteSupplier(Supplier supplier, AjaxMessage message)
    {
        /* ----------------------------------供应商删除验证---------------------------------- */
        Supplier oldSupplier = supplierDao.getHibernateDao().get(supplier.getId());
        if (oldSupplier == null)
        {
            message.setMessage("供应商不存在");
            return false;
        }

        // TODO 供应商被其他地方引用不能删除
        
        /* ----------------------------------供应商删除---------------------------------- */
        supplierDao.getHibernateDao().delete(oldSupplier);
        return true;
    }
    
    /**
     * 模糊查询供应商信息，使用分页<br>
     * */
    public Page<Supplier> findSupplierByParam(Page<Supplier> page, String q)
    {
        return supplierDao.findSupplierByParam(page, q);
    }
    
    /**
     * 增加供应商供应的原料<br>
     * @param supplierId 供应商ID
     * @param rawmaterialId 原料ID
     * @param message 错误信息
     * @return 成功返回true
     */
    public boolean addSupplierRawmaterial(Long supplierId, Long rawmaterialId, AjaxMessage message)
    {
        Supplier supplier = supplierDao.getHibernateDao().getEntity(Supplier.class, supplierId);
        if (supplier == null)
        {
            message.setMessage("供应商不存在");
            return false;
        }
        RawMaterialInfo rawmaterial = supplierDao.getHibernateDao().getEntity(RawMaterialInfo.class, rawmaterialId);
        if (rawmaterial == null)
        {
            message.setMessage("原料不存在");
            return false;
        }
        if (supplierDao.existsSupplierRawmaterial(supplierId, rawmaterialId))
        {
            message.setMessage("供应商[" + supplier.getSupplierName() + "]供应原料[" + rawmaterial.getName() + "]已存在");
            return true;
        }
        supplierDao.addSupplierRawmaterial(supplierId, rawmaterialId);
        return true;
    }
    
    /**
     * 删除供应商供应的原料<br>
     * @param supplierId 供应商ID
     * @param rawmaterialId 原料ID
     * @param message 错误信息
     * @return 成功返回true
     */
    public boolean deleteSupplierRawmaterial(Long supplierId, Long rawmaterialId, AjaxMessage message)
    {
        supplierDao.deleteSupplierRawmaterial(supplierId, rawmaterialId);
        return true;
    }
}
