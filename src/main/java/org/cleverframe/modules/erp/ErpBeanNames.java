package org.cleverframe.modules.erp;

/**
 * 定义当前erp模块定义的Spring Bean名称<br>
 * 
 * @author LiZW
 * @version 2015年12月21日 下午9:35:43
 */
public class ErpBeanNames
{
    // -------------------------------------------------------------------------------------------//
    // Dao
    // -------------------------------------------------------------------------------------------//
    public static final String RawMaterialInfoDao = "erp_RawMaterialInfoDao";
    public static final String SupplierDao = "erp_SupplierDao";
    public static final String RawmaterialWarehouseDao = "erp_RawmaterialWarehouseDao";
    public static final String RawmaterialReturnDao = "erp_RawmaterialReturnDao";
    public static final String RawmaterialOutDao = "erp_RawmaterialOutDao";
    public static final String ErpOrderDao = "erp_ErpOrderDao";
    
    // -------------------------------------------------------------------------------------------//
    // Service
    // -------------------------------------------------------------------------------------------//
    public static final String RawMaterialInfoService = "erp_RawMaterialInfoService";
    public static final String SupplierService = "erp_SupplierService";
    public static final String RawmaterialWarehouseService = "erp_RawmaterialWarehouseService";
    public static final String RawmaterialReturnService = "erp_RawmaterialReturnService";
    public static final String RawmaterialOutService = "erp_RawmaterialOutService";
    public static final String ErpOrderService = "erp_ErpOrderService";
    
    // -------------------------------------------------------------------------------------------//
    // Other
    // -------------------------------------------------------------------------------------------//

}
