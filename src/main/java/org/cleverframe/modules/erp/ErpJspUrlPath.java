package org.cleverframe.modules.erp;

/**
 * 当前erp模块对应的JSP文件URL路径<br>
 * 1.注意：此类只保存JSP文件的URL<br>
 * @author LiZW
 * @version 2015年12月21日 下午9:37:06
 */
public class ErpJspUrlPath
{
    /** ERP系统主页 */
    public static final String ErpMain = "modules/erp/ErpMain";

    /** 原料资料管理页面 */
    public static final String RawMaterialInfo = "modules/erp/RawMaterialInfo";

    /** 供应商管理页面 */
    public static final String Supplier = "modules/erp/Supplier";

    /** 供应商供应原料管理 */
    public static final String SupplierRawMaterial = "modules/erp/SupplierRawMaterial";

    /** 供应商供应原料查询 */
    public static final String SupplierRawMaterialQuery = "modules/erp/SupplierRawMaterialQuery";

    /** 原料入库申请页面 */
    public static final String RawmaterialWarehouse = "modules/erp/RawmaterialWarehouse";

    /** 原料入库审核页面 */
    public static final String RawmaterialWarehouseAudit = "modules/erp/RawmaterialWarehouseAudit";

    /** 原料出库申请-车间加工 */
    public static final String RawmaterialOutWorkshop = "modules/erp/RawmaterialOutWorkshop";

    /** 原料出库-车间审核 */
    public static final String RawmaterialOutWorkshopAudit = "modules/erp/RawmaterialOutWorkshopAudit";

    /** 原料出库申请-外发加工 */
    public static final String RawmaterialOutgoing = "modules/erp/RawmaterialOutgoing";

    /** 原料外发加工审核 */
    public static final String RawmaterialOutgoingAudit = "modules/erp/RawmaterialOutgoingAudit";

    /** 原料外发加工返回情况 */
    public static final String RawmaterialOutgoingReturn = "modules/erp/RawmaterialOutgoingReturn";

    /** 原料库统计 */
    public static final String RawMaterialInventory = "modules/erp/RawMaterialInventory";
    
    /** A生产订单-业务登记 */
    public static final String A_BusinessRegistration = "modules/erp/A_BusinessRegistration";
    
    /** A生产订单-工艺登记 */
    public static final String A_CraftsRegistration = "modules/erp/A_CraftsRegistration";
    
    /** A生产订单-厂长审核 */
    public static final String A_DirectorAudit = "modules/erp/A_DirectorAudit";
    
    
    
}
