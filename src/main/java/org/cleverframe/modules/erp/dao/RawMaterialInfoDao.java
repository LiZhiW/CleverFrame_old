package org.cleverframe.modules.erp.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.entity.RawMaterialInfo;
import org.cleverframe.modules.erp.entity.RawmaterialOut;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 * 原料资料Dao<br>
 * 
 * @author LiZW
 * @version 2015年12月23日 下午9:13:13
 */
@Repository(ErpBeanNames.RawMaterialInfoDao)
public class RawMaterialInfoDao extends BaseDao<RawMaterialInfo>
{
    /**
     * 分页查询原料数据<br>
     * 
     * @param page 分页对象
     * @param rawmaterialCode 查询参数：原料编码
     * @param name 查询参数：原料名称
     * @param specification 查询参数：原料规格
     * @param rawmaterialType 查询参数：原料类型
     * @param colorNo 查询参数：原料色号
     * @return
     */
    public Page<RawMaterialInfo> findRawMaterialInfoByPage(
            Page<RawMaterialInfo> page, 
            String rawmaterialCode, 
            String name, 
            String specification, 
            String rawmaterialType, 
            String colorNo)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("rawmaterialCode", rawmaterialCode);
        param.put("name", name);
        param.put("specification", specification);
        param.put("rawmaterialType", rawmaterialType);
        param.put("colorNo", colorNo);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawMaterialInfoDao.findRawMaterialInfoByPage");
        hibernateDao.findBySql(page, sql, param);
        return page;
    }
    
    /**
     * 原料的“原料名称、原料规格、原料类型、原料色号”组合数据不能重复<br>
     * @param id 查询参数：排除的数据ID，若不排除任何数据使用null
     * @param name 查询参数：原料名称
     * @param specification 查询参数：原料规格
     * @param rawmaterialType 查询参数：原料类型
     * @param colorNo 查询参数：原料色号
     * @return 原料重复返回true
     */
    public boolean rawMaterialInfoIsRepeat(Serializable id, String name, String specification, String rawmaterialType, String colorNo)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("id", id);
        param.put("name", name);
        param.put("specification", specification);
        param.put("rawmaterialType", rawmaterialType);
        param.put("colorNo", colorNo);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawMaterialInfoDao.rawMaterialInfoIsRepeat");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        int count = NumberUtils.toInt(object.toString(), 0);
        return count > 0;
    }
    
    /**
     * 得到最大的原料编码<br>
     * @return 不存在返回空字符串
     */
    public String getRawMaterialMaxCode()
    {
        Parameter param = new Parameter();
        param.put("companyId", userUtils.getCurrentCompanyId());
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawMaterialInfoDao.getRawMaterialMaxCode");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        return (object == null) ? "" : object.toString();
    }
    
    /**
     * 根据多个id查询数据
     * @param ids 查询参数：多个ID，如 12,19,15,255
     * @return 
     */
    public List<RawMaterialInfo> findRawMaterialInfoByIds(String ids)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        Map<String, Object> dataModel = new HashMap<String, Object>();
        dataModel.put("ids", ids);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawMaterialInfoDao.findRawMaterialInfoByIds", dataModel);
        return hibernateDao.findBySql(sql, param);
    }
    
    /**
     * 模糊查询原料信息，使用分页<br>
     * @return 分页数据
     */
    public Page<RawMaterialInfo> findRawMaterialInfoByParam(Page<RawMaterialInfo> page, String q)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("q", q);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawMaterialInfoDao.findRawMaterialInfoByParam");
        hibernateDao.findBySql(page, sql, param);
        return page;
    }
    
    /**
     * 根据原料编码查询原料<br>
     * @param rawmaterialCode 查询参数：原料编码
     */
    public RawMaterialInfo getRawMaterialInfoByCode(String rawmaterialCode)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("rawmaterialCode", rawmaterialCode);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawMaterialInfoDao.getRawMaterialInfoByCode");
        return hibernateDao.getBySql(RawMaterialInfo.class, sql, param);
    }
    
    /**
     * 分页查询供应商供应的原料信息<br>
     * @param page 分页数据
     * @param supplierId 查询参数：供应商ID
     * @return 分页数据
     */
    public Page<RawMaterialInfo> findSupplierRawmaterial(Page<RawMaterialInfo> page, Long supplierId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("supplierId", supplierId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawMaterialInfoDao.findSupplierRawmaterial");
        hibernateDao.findBySql(page, sql, param);
        return page;
    }
    
    /**
     * 查询原料库存<br>
     * @param rawmaterialCode 查询参数：原料编码
     * @param name 参询参数：原料名称
     * @param specification 查询参数：规格
     * @return
     */
    public Page<Map<Object, Object>> findRawmaterialInventory(Page<Map<Object, Object>> page, String rawmaterialCode, String name, String specification)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("workShop", RawmaterialOut.WORK_SHOP);
        param.put("outGoing", RawmaterialOut.OUT_GOING);
        param.put("auditorState", RawmaterialOut.AUDIT_PASS);
        param.put("historyNo", RawmaterialOut.HISTORY_NO);
        param.put("rawmaterialCode", rawmaterialCode);
        param.put("name", name);
        param.put("specification", specification);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawMaterialInfoDao.findRawmaterialInventory");
        hibernateDao.findMapBySql(page, sql, param);
        return page;
    }
}
