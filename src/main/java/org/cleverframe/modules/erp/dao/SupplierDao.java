package org.cleverframe.modules.erp.dao;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.math.NumberUtils;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.entity.Supplier;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 * 供应商信息Dao<br>
 * @author LiZW
 * @version 2015年12月30日 下午10:23:45
 */
@Repository(ErpBeanNames.SupplierDao)
public class SupplierDao extends BaseDao<Supplier>
{
    /**
     * 使用分页查询供应商信息<br>
     * @param page 分页数据
     * @param supplierNo 查询参数：供应商编号
     * @param supplierName 查询参数：供应商名称
     * @param legalPeople 查询参数：供应商法人
     * @param dockingPeople 查询参数：供应商对接人
     * @param startDate 查询参数：供应日期(最小)
     * @param endDate 查询参数：供应日期(最大)
     */
    public Page<Supplier> findSupplierByPage(Page<Supplier> page, String supplierNo, String supplierName, String legalPeople, String dockingPeople, Date startDate, Date endDate)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("supplierNo", supplierNo);
        param.put("supplierName", supplierName);
        param.put("legalPeople", legalPeople);
        param.put("dockingPeople", dockingPeople);
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.SupplierDao.findSupplierByPage");
        hibernateDao.findBySql(page, sql, param);
        return page;
    }
    
    /**
     * 判断供应商名称数据库中是否重复<br>
     * @param id 查询参数：排除的数据ID，若不排除任何数据使用null
     * @param supplierName 查询参数：供应商名称
     * @return 存在重复的数据返回true
     */
    public boolean supplierIsRepeat(Serializable id, String supplierName)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("id", id);
        param.put("supplierName", supplierName);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.SupplierDao.supplierIsRepeat");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        int count = NumberUtils.toInt(object.toString(), 0);
        return count > 0;
    }
    
    /**
     * 得到最大的供应商编号<br>
     * @return 不存在返回空字符串
     */
    public String getSupplierMaxNO()
    {
        Parameter param = new Parameter();
        param.put("companyId", userUtils.getCurrentCompanyId());
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.SupplierDao.getSupplierMaxCode");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        return (object == null) ? "" : object.toString();
    }
    
    /**
     * 模糊查询供应商信息，使用分页<br>
     * */
    public Page<Supplier> findSupplierByParam(Page<Supplier> page, String q)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("q", q);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.SupplierDao.findSupplierByParam");
        hibernateDao.findBySql(page, sql, param);
        return page;
    }
    
    /**
     * 供应商供应的原料是否存在<br>
     * @param supplierId 供应商ID
     * @param rawmaterialId 原料ID
     * @return 存在返回true
     */
    public boolean existsSupplierRawmaterial(Long supplierId, Long rawmaterialId)
    {
        Parameter param = new Parameter();
        param.put("supplierId", supplierId);
        param.put("rawmaterialId", rawmaterialId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.SupplierDao.existsSupplierRawmaterial");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        long count = NumberUtils.toLong(object.toString(), 0L);
        return count > 0;
    }
    
    /**
     * 增加供应商供应的原料<br>
     * @param supplierId 供应商ID
     * @param rawmaterialId 原料ID
     * @return 
     */
    public int addSupplierRawmaterial(Long supplierId, Long rawmaterialId)
    {
        Parameter param = new Parameter();
        param.put("supplierId", supplierId);
        param.put("rawmaterialId", rawmaterialId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.SupplierDao.addSupplierRawmaterial");
        Query query = hibernateDao.createSqlQuery(sql, param);
        return query.executeUpdate();
    }
    
    /**
     * 删除供应商供应的原料<br>
     * @param supplierId 供应商ID
     * @param rawmaterialId 原料ID
     * @return 
     */
    public int deleteSupplierRawmaterial(Long supplierId, Long rawmaterialId)
    {
        Parameter param = new Parameter();
        param.put("supplierId", supplierId);
        param.put("rawmaterialId", rawmaterialId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.SupplierDao.deleteSupplierRawmaterial");
        Query query = hibernateDao.createSqlQuery(sql, param);
        return query.executeUpdate();
    }
}
