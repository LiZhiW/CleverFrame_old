package org.cleverframe.modules.erp.dao;

import java.util.Date;
import java.util.Map;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.entity.ErpOrder;
import org.cleverframe.modules.erp.entity.RawmaterialOut;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 * @author LiZW
 * @version 2016年2月14日 下午8:53:09
 */
@Repository(ErpBeanNames.ErpOrderDao)
public class ErpOrderDao extends BaseDao<ErpOrder>
{
    /**
     * 分页查询订单<br>
     * @param customer 查询参数：客户
     * @param orderNo 查询参数：批号
     * @param name 查询参数：名称
     * @param specification 查询参数：规格
     * @param orderType 查询参数：订单类型
     * @param auditorState 查询参数：订单状态
     * @param startDate 查询参数：登记日期(小)
     * @param endDate 查询参数：登记日期(大)
     * @return
     */
    public Page<Map<Object, Object>> findOrderByPage(
            Page<Map<Object, Object>> page,
            String customer, 
            String orderNo, 
            String name, 
            String specification, 
            String orderType, 
            String auditorState,
            Date startDate, 
            Date endDate)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("customer", customer);
        param.put("orderNo", orderNo);
        param.put("name", name);
        param.put("specification", specification);
        param.put("orderType", orderType);
        param.put("auditorState", auditorState);
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        param.put("isHistory", RawmaterialOut.HISTORY_NO);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.ErpOrderDao.findOrderByPage");
        hibernateDao.findMapBySql(page, sql, param);
        return page;
    }
    
    /**
     * 获取最大的订单号<br>
     * @param orderCode 查询参数：订单号
     * @param orderType 查询参数：订单类型
     * @return
     */
    public String getMaxOrderCode(String orderCode, char orderType)
    {
        Parameter param = new Parameter();
        param.put("orderCode", orderCode);
        param.put("orderType", orderType);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.ErpOrderDao.getMaxOrderCode");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        return (object == null) ? "" : object.toString();
    }
    
    /**
     * 获取最大的订单批号<br>
     * @param orderNo 查询参数：订单批号
     * @param orderType 查询参数：订单类型
     * @return
     */
    public String getMaxOrderNo(String orderNo, char orderType)
    {
        Parameter param = new Parameter();
        param.put("orderNo", orderNo);
        param.put("orderType", orderType);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.ErpOrderDao.getMaxOrderNo");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        return (object == null) ? "" : object.toString();
    }
}
