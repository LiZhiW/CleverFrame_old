package org.cleverframe.modules.erp.dao;

import java.util.List;

import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.entity.RawmaterialReturn;
import org.springframework.stereotype.Repository;

/**
 * 原料外发加工返回原料信息DAO<br>
 * @author LiZW
 * @version 2016年1月20日 下午10:14:37
 */
@Repository(ErpBeanNames.RawmaterialReturnDao)
public class RawmaterialReturnDao extends BaseDao<RawmaterialReturn>
{
    /**
     * 根据外发加工出库单ID 查询所有外发加工返回原料信息<br>
     * @param outId 查询参数：外发加工出库单ID
     * @return 外发加工返回原料信息
     */
    public List<RawmaterialReturn> findRawmaterialReturnByOutId(Long outId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("outId", outId);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialReturnDao.findRawmaterialReturnByOutId");
        return hibernateDao.findBySql(sql, param);
    }
}
