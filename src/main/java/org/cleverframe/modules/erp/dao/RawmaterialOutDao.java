package org.cleverframe.modules.erp.dao;

import java.util.Date;
import java.util.Map;

import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.entity.RawmaterialOut;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 * 原料出库申请实体类DAO<br>
 * @author LiZW
 * @version 2016年1月20日 下午10:17:53
 */
@Repository(ErpBeanNames.RawmaterialOutDao)
public class RawmaterialOutDao extends BaseDao<RawmaterialOut>
{
    /**
     * 分页查询原料出库单<br>
     * @param page 分页数据
     * @param rawmaterialCode 查询参数：原料编码
     * @param outType 查询参数：出库类型
     * @param outNo 查询参数：出库单编号
     * @param name 查询参数：原料名称
     * @param specification 查询参数：原料规格
     * @param auditorState 查询参数：审核状态
     * @param startDate 查询参数：出库时间(最小)
     * @param endDate 查询参数：出库时间(最大)
     * @return 分页数据
     */
    public Page<Map<Object, Object>> findRawmaterialOutByPage(
            Page<Map<Object, Object>> page, 
            String rawmaterialCode, 
            String outType, 
            String outNo, 
            String name, 
            String specification, 
            String auditorState,
            Date startDate, 
            Date endDate)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("rawmaterialCode", rawmaterialCode);
        param.put("outType", outType);
        param.put("outNo", outNo);
        param.put("name", name);
        param.put("specification", specification);
        param.put("auditorState", auditorState);
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        param.put("isHistory", RawmaterialOut.HISTORY_NO);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialOutDao.findRawmaterialOutByPage");
        hibernateDao.findMapBySql(page, sql, param);
        return page;
    }
    
    /**
     * 根据原料出库单编号或原料名称，模糊查询原料出库单
     * @param page 
     * @param q 查询参数
     * @return
     */
    public Page<Map<Object, Object>> findRawmaterialOutByCombobox(Page<Map<Object, Object>> page, String q)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("q", q);
        param.put("outType", RawmaterialOut.OUT_GOING);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialOutDao.findRawmaterialOutByCombobox");
        hibernateDao.findMapBySql(page, sql, param);
        return page;
    }
    
    /**
     * 查询当前用户需要审核的原料出库单<br>
     * @param page 分页数据
     * @param outType 查询参数：出库类型
     * @param outNo 查询参数：出库单编号
     * @param name 查询参数：原料名称
     * @param specification 查询参数：原料规格
     * @param auditorState 查询参数：审核状态
     * @param startDate 查询参数：出库时间(最小)
     * @param endDate 查询参数：出库时间(最大)
     * @param userId 查询参数：用户ID
     * @return 分页数据
     */
    public Page<Map<Object, Object>> findAuditRawmaterialOutByPage(
            Page<Map<Object, Object>> page, 
            String outType, 
            String outNo, 
            String name, 
            String specification, 
            String auditorState,
            Date startDate, 
            Date endDate, 
            Long userId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("outType", outType);
        param.put("outNo", outNo);
        param.put("name", name);
        param.put("specification", specification);
        param.put("auditorState", auditorState);
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        param.put("userId", userId);
        param.put("pendingAudit", RawmaterialOut.PENDING_AUDIT);
        param.put("auditing", RawmaterialOut.AUDIT_ING);
        param.put("isHistory", RawmaterialOut.HISTORY_NO);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialOutDao.findAuditRawmaterialOutByPage");
        hibernateDao.findMapBySql(page, sql, param);
        return page;
    }
    
    
    
    
    /**
     * 查询原料出库申请单的最大的编码<br>
     * @return 不存在返回空字符串
     */
    public String getOutMaxNo()
    {
        Parameter param = new Parameter();
        param.put("companyId", userUtils.getCurrentCompanyId());
        param.put("noLength", RawmaterialOut.NO_NUMBER_LENGTH + RawmaterialOut.NO_PREFIX.length() + RawmaterialOut.NO_SUFFIX);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialOutDao.getOutMaxNo");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        return (object == null) ? "" : object.toString();
    }
    
    /**
     * 根据原料出库申请单编号查询申请单
     * @param outNo 查询参数：原料出库申请单编号
     * @return 原料出库申请单
     */
    public RawmaterialOut getRawmaterialOutByOutNo(String outNo)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("outNo", outNo);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialOutDao.getRawmaterialOutByOutNo");
        return hibernateDao.getBySql(RawmaterialOut.class, sql, param);
    }
    
    /**
     * 根据原料出库申请单编号查询申请单
     * @param outNo 查询参数：原料出库申请单编号
     * @return 原料出库申请单以及关联信息
     */
    public Map<String, Object> getRawmaterialOutByNo(String outNo)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("outNo", outNo);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialOutDao.getRawmaterialOutByNo");
        return hibernateDao.getMapBySql(sql, param);
    }
}
