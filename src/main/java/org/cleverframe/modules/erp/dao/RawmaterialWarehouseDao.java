package org.cleverframe.modules.erp.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.cleverframe.common.persistence.Page;
import org.cleverframe.common.persistence.Parameter;
import org.cleverframe.common.persistence.dao.BaseDao;
import org.cleverframe.common.persistence.entity.BaseEntity;
import org.cleverframe.core.qlscript.utils.QLScriptUtils;
import org.cleverframe.modules.erp.ErpBeanNames;
import org.cleverframe.modules.erp.entity.RawmaterialWarehouse;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 * 原料入库申请单DAO<br>
 * @author LiZW
 * @version 2016年1月6日 下午2:08:15
 */
@Repository(ErpBeanNames.RawmaterialWarehouseDao)
public class RawmaterialWarehouseDao extends BaseDao<RawmaterialWarehouse>
{
    /**
     * 分页查询原料入库单<br>
     * @param page 分页数据
     * @param warehouseNo 查询参数：入库单号
     * @param rawmaterialCode 查询参数：原料编码
     * @param name 查询参数：原料名称
     * @param auditorState 查询参数：审核状态
     * @param startDate 查询参数：入库时间最小
     * @param endDate 查询参数：入库时间最大
     * @param supplierName 查询参数：供应商名称
     * @return
     */
    public Page<Map<Object, Object>> findRawmaterialWarehouseByPage(
            Page<Map<Object, Object>> page, 
            String warehouseNo, 
            String rawmaterialCode, 
            String name,
            String auditorState, 
            Date startDate, 
            Date endDate, 
            String supplierName)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("warehouseNo", warehouseNo);
        param.put("rawmaterialCode", rawmaterialCode);
        param.put("name", name);
        param.put("auditorState", auditorState);
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        param.put("supplierName", supplierName);
        param.put("isHistory", RawmaterialWarehouse.HISTORY_NO);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialWarehouseDao.findRawmaterialWarehouseByPage");
        hibernateDao.findMapBySql(page, sql, param);
        return page;
    }
    
    /**
     * 查询当前用户需要审核的原料入库单<br>
     * @param page 分页数据
     * @param warehouseNo 查询参数：入库单号
     * @param rawmaterialCode 查询参数：原料编码
     * @param name 查询参数：原料名称
     * @param startDate 查询参数：入库时间最小
     * @param endDate 查询参数：入库时间最大
     * @param supplierName 查询参数：供应商名称
     * @param userId 查询参数：用户ID
     * @return
     */
    public Page<Map<Object, Object>> findAuditRawmaterialWarehouseByPage(
            Page<Map<Object, Object>> page, 
            String warehouseNo, 
            String rawmaterialCode, 
            String name,
            Date startDate, 
            Date endDate, 
            String supplierName,
            Long userId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("warehouseNo", warehouseNo);
        param.put("rawmaterialCode", rawmaterialCode);
        param.put("name", name);
        param.put("startDate", startDate);
        param.put("endDate", endDate);
        param.put("supplierName", supplierName);
        param.put("userId", userId);
        param.put("pendingAudit", RawmaterialWarehouse.PENDING_AUDIT);
        param.put("auditing", RawmaterialWarehouse.AUDIT_ING);
        param.put("isHistory", RawmaterialWarehouse.HISTORY_NO);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialWarehouseDao.findAuditRawmaterialWarehouseByPage");
        hibernateDao.findMapBySql(page, sql, param);
        return page;
    }
    
    /**
     * 查询原料入库申请单的最大的编码<br>
     * @return 不存在返回空字符串
     */
    public String getWarehouseMaxNo()
    {
        Parameter param = new Parameter();
        param.put("companyId", userUtils.getCurrentCompanyId());
        param.put("noLength", RawmaterialWarehouse.NO_NUMBER_LENGTH + RawmaterialWarehouse.NO_PREFIX.length() + RawmaterialWarehouse.NO_SUFFIX);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialWarehouseDao.getWarehouseMaxNo");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        return (object == null) ? "" : object.toString();
    }
    
    /**
     * 查询用户需要审核的原料入库申请单数量
     * @param userId 查询参数：用户ID
     */
    public int getCountByAuditorId(Serializable userId)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("userId", userId);
        param.put("pendingAudit", RawmaterialWarehouse.PENDING_AUDIT);
        param.put("auditing", RawmaterialWarehouse.AUDIT_ING);
        param.put("isHistory", RawmaterialWarehouse.HISTORY_NO);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialWarehouseDao.getCountByAuditorId");
        Query query = hibernateDao.createSqlQuery(sql, param);
        Object object = query.uniqueResult();
        object = (object == null) ? "0" : object;
        int count = NumberUtils.toInt(object.toString(), 0);
        return count;
    }
    
    /**
     * 根据原料入库申请单编号查询申请单
     * @param warehouseNo 查询参数：原料入库申请单编号
     * @return 原料入库申请单
     */
    public RawmaterialWarehouse getRawmaterialWarehouseByWarehouseNo(String warehouseNo)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("warehouseNo", warehouseNo);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialWarehouseDao.getRawmaterialWarehouseByWarehouseNo");
        return hibernateDao.getBySql(RawmaterialWarehouse.class, sql, param);
    }
    
    /**
     * 根据原料入库申请单编号查询申请单
     * @param warehouseNo 查询参数：原料入库申请单编号
     * @return 原料入库申请单以及关联信息
     */
    public Map<String, Object> getRawmaterialWarehouseByNo(String warehouseNo)
    {
        Parameter param = new Parameter(BaseEntity.DEL_FLAG_NORMAL, userUtils.getCurrentCompanyId());
        param.put("warehouseNo", warehouseNo);
        String sql = QLScriptUtils.getSQLScript("org.cleverframe.modules.erp.dao.RawmaterialWarehouseDao.getRawmaterialWarehouseByNo");
        return hibernateDao.getMapBySql(sql, param);
    }
}
