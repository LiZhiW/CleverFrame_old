package org.cleverframe.modules;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cleverframe.common.controller.BaseController;
import org.cleverframe.common.vo.AjaxMessage;
import org.cleverframe.core.qlscript.QLScriptBeanNames;
import org.cleverframe.core.qlscript.service.IQLScriptService;
import org.cleverframe.modules.sys.SysBeanNames;
import org.cleverframe.modules.sys.service.DictService;
import org.cleverframe.modules.sys.service.MDictService;
import org.cleverframe.modules.sys.service.MenuService;
import org.cleverframe.modules.sys.service.OrganizationService;
import org.cleverframe.modules.sys.service.PermissionService;
import org.cleverframe.modules.sys.service.RoleService;
import org.cleverframe.modules.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 初始化系统数据，系统部署时调用<br>
 * 1.重新创建数据库表<br>
 * 2.创建系统初始数据<br>
 * 
 * @author LiZW
 * @version 2015年7月24日 上午10:22:27
 */
@Controller
@RequestMapping("/${mvcPath}")
@Transactional(readOnly = true)
public class DataInitialization extends BaseController
{
    @Autowired
    @Qualifier(QLScriptBeanNames.MemoryQLScriptService)
    private IQLScriptService qLScriptService;

    @Autowired
    @Qualifier(SysBeanNames.DictService)
    private DictService dictService;

    @Autowired
    @Qualifier(SysBeanNames.OrganizationService)
    private OrganizationService organizationService;

    @Autowired
    @Qualifier(SysBeanNames.UserService)
    private UserService userService;

    @Autowired
    @Qualifier(SysBeanNames.RoleService)
    private RoleService roleService;

    @Autowired
    @Qualifier(SysBeanNames.MenuService)
    private MenuService menuService;

    @Autowired
    @Qualifier(SysBeanNames.PermissionService)
    private PermissionService permissionService;

    @Autowired
    @Qualifier(SysBeanNames.MDictService)
    private MDictService mDictService;

    /**
     * 初始化系统基础数据<br>
     * */
    @ResponseBody
    @RequestMapping("/intiData")
    @Transactional(readOnly = false)
    public AjaxMessage intiData(HttpServletRequest request, HttpServletResponse response)
    {
        AjaxMessage message = new AjaxMessage();

        return message;
    }

    /**
     * 导出数据到文件<br>
     * */
    @ResponseBody
    @RequestMapping("/exportData")
    @Transactional(readOnly = false)
    public AjaxMessage exportData(HttpServletRequest request, HttpServletResponse response)
    {
        AjaxMessage message = new AjaxMessage();
        // 数据保存的基础路径
        // String basePath = "C:\\Users\\LiZhiWei\\git\\CodeGenerator\\DataBase\\InitData";
        // List<QLScript> qLScriptList = qLScriptService.findAllQLScript();

        return message;
    }
}
