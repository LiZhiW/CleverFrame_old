<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<%@ taglib prefix="sys" tagdir="/WEB-INF/tags/sys" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>多级字典管理</title>
<style type="text/css">
#fm-search #fm {
	margin: 0;
	padding: 0px 5px;
}
#fm-search .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#fm-search .row {
	margin-bottom: 8px;
}
#fm-search .row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
#fm-search .row input,select {
	width: 160px;

}
#fm-search .row .column {
	margin-right: 10px;
}
#fm-search .row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:70px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="columnLast">
					<label for="type-search">字典类型</label> 
					<input id="type-search" name="type-search">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="id-search">数据ID</label>
					<input id="id-search" name="id-search" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="uuid-search">数据UUID</label> 
					<input id="uuid-search" name="uuid-search" class="easyui-textbox" style="width: 410px">
				</span> 
			</div>
		</form>
	</div>

	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="mDictData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'mdictKey',width:150,align:'left'">字典名称</th>
					<th data-options="field:'mdictType',width:120,align:'left'">字典类型</th>
					<th data-options="field:'mdictValue',width:200,align:'left'">字典值</th>
					<th data-options="field:'description',width:150,align:'left'">字典描述</th>
					<th data-options="field:'sort',width:60,align:'left'">排序</th>
					<th data-options="field:'fullPath',width:60,align:'left',hidden:true">全路径</th>
					<th data-options="field:'parentId',width:60,align:'left',hidden:true">父级编号</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:100,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchMDict();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="newMDict();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editMDict();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">编辑</a>
			<a onclick="delMDict();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>
	    </div>
	</div>

	<sys:MDictEdit mvcPath="${pageScope.mvcPath}" saveFunction="saveMDict()" dialogID="dlg" formID="fm"></sys:MDictEdit>

	<script type="text/javascript">
	// 获取分页MDict数据地址
	var mDictDataUrl = "${pageScope.mvcPath}/sys/findMDictByType";
	// 新增保存MDict地址
	var newMDictUrl = "${pageScope.mvcPath}/sys/addMDict";
	// 编辑保存MDict地址
	var editMDictUrl = "${pageScope.mvcPath}/sys/updateMDict";
	// 删除MDict地址
	var delMDictUrl = "${pageScope.mvcPath}/sys/deleteMDict";
	// 保存时地址
	var mDictSaveUrl = "";
	// 获取所有字典类型地址
	var findMDictAllTypeUrl = "${pageScope.mvcPath}/sys/findMDictAllType";
	// 获取字典树数据的地址
	var mDictTreeUrl = "${pageScope.mvcPath}/sys/getMDictTreeExcludeOneself";
	
	$(document).ready(function () {

		$("#type-search").combobox({
			url : findMDictAllTypeUrl,
			editable : true,
			valueField : "value",
			textField : "text"
		});
		
		// 设置mdictData数据显示表格
		$("#mDictData").treegrid({
			url : mDictDataUrl,
			idField: 'id',
			treeField: 'mdictKey',
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			nowrap : true,
			pagination : false,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				editMDict();
			},
			onBeforeLoad : function (row, param) {
				if(param == null){
					param = {};
				}
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			}
		});
	});

	// 保存多级字典
	function saveMDict() {
		$("#fm").form("submit", {
			url : mDictSaveUrl,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({
						title : '提示',
						msg : data.message,
						timeout : 5000,
						showType : 'slide'
					});
					$("#mDictData").treegrid('load');
				} else {
					// 保存失败

				}
			}
		});
	}
	
	// 查询多级字典数据
	function searchMDict() {
		$("#mDictData").treegrid('load');
	}

	// 新增多级字典
	function newMDict() {
		mDictSaveUrl = newMDictUrl;
		var param = {};
		param.fullPath = "XXX";
		param.type = "";
		$.post(mDictTreeUrl, param, function (data) {
			$("#parentId").combotree("clear");
			$("#parentId").combotree("loadData",data);
			
			$('#dlg').dialog('open').dialog('setTitle', '新增多级字典');
			$('#fm').form('reset');
		}, "json");
	}

	// 编辑多级字典
	function editMDict() {
		mDictSaveUrl = editMDictUrl;
		var row = $('#mDictData').treegrid('getSelected');
		if (row == null) {
			$.messager.alert("提示", "未选择要编辑的数据！", "info");
			return;
		}
		if (row) {
			var param = {};
			param.fullPath = row.fullPath;
			param.type = row.type;
			$.post(mDictTreeUrl, param, function (data) {
				$("#parentId").combotree("clear");
				$("#parentId").combotree("loadData",data);
				
				$('#dlg').dialog('open').dialog('setTitle', '编辑多级字典');
				$('#fm').form('load', row);
			}, "json");
		}
	}

	// 删除多级字典
	function delMDict() {
		var row = $("#mDictData").treegrid("getSelected");
		if (row == null) {
			$.messager.alert("提示", "未选择要删除的数据！", "info");
			return;
		}
		$.messager.confirm("确认删除", "确定要删除字典[类型：" + row.mdictType + "，名称：" + row.mdictKey + "]？", function (r) {
			if (r) {
				$.post(delMDictUrl, row, function (data) {
					if (data.success) {
						// 删除成功
						$.messager.show({
							title : '提示',
							msg : data.message,
							timeout : 5000,
							showType : 'slide'
						});
						$("#mDictData").treegrid('load');
					} else {
						// 删除失败
					}
				}, "json");
			}
		});
	}
	</script>
</body>
</html>