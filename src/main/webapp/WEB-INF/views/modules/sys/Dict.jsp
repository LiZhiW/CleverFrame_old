<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<%@ taglib prefix="sys" tagdir="/WEB-INF/tags/sys" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>系统字典管理</title>
<style type="text/css">
#fm-search #fm {
	margin: 0;
	padding: 0px 5px;
}
#fm-search .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#fm-search .row {
	margin-bottom: 8px;
}
#fm-search .row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
#fm-search .row input,select {
	width: 160px;

}
#fm-search .row .column {
	margin-right: 10px;
}
#fm-search .row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:70px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="dictType-search">字典类型</label> 
					<input id="dictType-search" name="dictType-search" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dictKey-search">字典名称</label> 
					<input id="dictKey-search" name="dictKey-search" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dictValue-search">字典值</label> 
					<input id="dictValue-search" name="dictValue-search" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="id-search">数据ID</label>
					<input id="id-search" name="id-search" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="uuid-search">数据UUID</label> 
					<input id="uuid-search" name="uuid-search" class="easyui-textbox" style="width: 410px">
				</span> 
			</div>
		</form>
	</div>

	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="dictData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'dictType',width:120,align:'left'">字典类型</th>
					<th data-options="field:'dictKey',width:150,align:'left'">字典名称</th>
					<th data-options="field:'dictValue',width:200,align:'left'">字典值</th>
					<th data-options="field:'description',width:150,align:'left'">字典描述</th>
					<th data-options="field:'sort',width:60,align:'left'">排序</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:100,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchDict();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="newDict();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editDict();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">编辑</a>
			<a onclick="delDict();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>
	    </div>
	</div>

	<sys:DictEdit mvcPath="${pageScope.mvcPath}" saveFunction="saveDict()" dialogID="dlg" formID="fm" />

	<script type="text/javascript">
	// 获取分页Dict数据地址
	var dictDataUrl = "${pageScope.mvcPath}/sys/findDictByPage";
	// 新增保存Dict地址
	var newDictUrl = "${pageScope.mvcPath}/sys/addDict";
	// 编辑保存Dict地址
	var editDictUrl = "${pageScope.mvcPath}/sys/updateDict";
	// 删除Dict地址
	var delDictUrl = "${pageScope.mvcPath}/sys/deleteDict";
	// 保存时地址
	var dictSaveUrl = "";
	// 获取所有字典类型地址
	var findDictAllTypeUrl = "${pageScope.mvcPath}/sys/findDictAllType";
	
	$(document).ready(function () {

		$("#dictType-search").combobox({
			url : findDictAllTypeUrl,
			editable : true,
			valueField : "value",
			textField : "text"
		});
		
		// 设置dictData数据显示表格
		$("#dictData").datagrid({
			url : dictDataUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				editDict();
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			}
		});
	});

	// 保存字典
	function saveDict() {
		$("#fm").form("submit", {
			url : dictSaveUrl,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({
						title : '提示',
						msg : data.message,
						timeout : 5000,
						showType : 'slide'
					});
					$("#dictData").datagrid('reload');
				} else {
					// 保存失败

				}
			}
		});
	}
	
	// 查询字典数据
	function searchDict() {
		$("#dictData").datagrid('load');
	}

	// 新增字典
	function newDict() {
		dictSaveUrl = newDictUrl;
		$('#dlg').dialog('open').dialog('setTitle', '新增字典');
		$('#fm').form('reset');
	}

	// 编辑字典
	function editDict() {
		dictSaveUrl = editDictUrl;
		var row = $('#dictData').datagrid('getSelected');
		if (row == null) {
			$.messager.alert("提示", "未选择要编辑的数据！", "info");
			return;
		}
		if (row) {
			$('#dlg').dialog('open').dialog('setTitle', '编辑字典');
			$('#fm').form('load', row);
		}
	}

	// 删除字典
	function delDict() {
		var row = $("#dictData").datagrid("getSelected");
		if (row == null) {
			$.messager.alert("提示", "未选择要删除的数据！", "info");
			return;
		}
		$.messager.confirm("确认删除", "确定要删除字典?<br/>类型：" + row.dictType + "<br/>名称：" + row.dictKey, function (r) {
			if (r) {
				$.post(delDictUrl, row, function (data) {
					if (data.success) {
						// 删除成功
						$.messager.show({
							title : '提示',
							msg : data.message,
							timeout : 5000,
							showType : 'slide'
						});
						$("#dictData").datagrid('reload');
					} else {
						// 删除失败
					}
				}, "json");
			}
		});
	}
	</script>
</body>
</html>