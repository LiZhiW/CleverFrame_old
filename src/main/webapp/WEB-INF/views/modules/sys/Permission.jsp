<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<%@ taglib prefix="sys" tagdir="/WEB-INF/tags/sys" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>权限管理</title>
<style type="text/css">
#fm-search #fm {
	margin: 0;
	padding: 0px 5px;
}
#fm-search .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#fm-search .row {
	margin-bottom: 8px;
}
#fm-search .row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
#fm-search .row input,select {
	width: 160px;

}
#fm-search .row .column {
	margin-right: 10px;
}
#fm-search .row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:70px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="menuId-search">所属菜单</label> 
					<input id="menuId-search" name="menuId-search" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="permission-search">权限标识</label> 
					<input id="permission-search" name="permission-search" class="easyui-textbox" style="width: 410px">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="id-search">数据ID</label>
					<input id="id-search" name="id-search" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="uuid-search">数据UUID</label> 
					<input id="uuid-search" name="uuid-search" class="easyui-textbox" style="width: 410px">
				</span> 
			</div>
		</form>
	</div>

	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="permissionData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'name',width:200,align:'left'">权限名称</th>
					<th data-options="field:'permission',width:200,align:'left'">权限标识字符串</th>
					<th data-options="field:'permissionType',width:80,align:'left'">权限类型</th>
					<th data-options="field:'menuId',width:150,align:'left'">所属菜单</th>
					<th data-options="field:'url',width:300,align:'left'">权限对应的请求地址</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:100,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchPermission();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="newPermission();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editPermission();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">编辑</a>
			<a onclick="delPermission();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>
	    </div>
	</div>

	<sys:PermissionEdit mvcPath="${pageScope.mvcPath}" saveFunction="savePermission()" dialogID="dlg" formID="fm"></sys:PermissionEdit>

	<script type="text/javascript">
	// 获取分页Permission数据地址
	var permissionDataUrl = "${pageScope.mvcPath}/sys/findPermissionByPage";
	// 新增保存Permission地址
	var newPermissionUrl = "${pageScope.mvcPath}/sys/addPermission";
	// 编辑保存Permission地址
	var editPermissionUrl = "${pageScope.mvcPath}/sys/updatePermission";
	// 删除Permission地址
	var delPermissionUrl = "${pageScope.mvcPath}/sys/deletePermission";
	// 保存时地址
	var permissionSaveUrl = "";
	
	$(document).ready(function () {
		
		// 设置permissionData数据显示表格
		$("#permissionData").datagrid({
			url : permissionDataUrl,
			idField : "name",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				editPermission();
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			}
		});
	});

	// 保存权限
	function savePermission() {
		$("#fm").form("submit", {
			url : permissionSaveUrl,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({
						title : '提示',
						msg : data.message,
						timeout : 5000,
						showType : 'slide'
					});
					$("#permissionData").datagrid('reload');
				} else {
					// 保存失败

				}
			}
		});
	}
	
	// 查询权限数据
	function searchPermission() {
		$("#permissionData").datagrid('load');
	}

	// 新增权限
	function newPermission() {
		permissionSaveUrl = newPermissionUrl;
		$('#dlg').dialog('open').dialog('setTitle', '新增权限');
		$('#fm').form('reset');
	}

	// 编辑权限
	function editPermission() {
		permissionSaveUrl = editPermissionUrl;
		var row = $('#permissionData').datagrid('getSelected');
		if (row == null) {
			$.messager.alert("提示", "未选择要编辑的数据！", "info");
			return;
		}
		if (row) {
			$('#dlg').dialog('open').dialog('setTitle', '编辑权限');
			$('#fm').form('load', row);
		}
	}

	// 删除字典
	function delPermission() {
		var row = $("#permissionData").datagrid("getSelected");
		if (row == null) {
			$.messager.alert("提示", "未选择要删除的数据！", "info");
			return;
		}
		$.messager.confirm("确认删除", "确定要删除权限[" + row.permission + "]？", function (r) {
			if (r) {
				$.post(delPermissionUrl, row, function (data) {
					if (data.success) {
						// 删除成功
						$.messager.show({
							title : '提示',
							msg : data.message,
							timeout : 5000,
							showType : 'slide'
						});
						$("#permissionData").datagrid('reload');
					} else {
						// 删除失败
					}
				}, "json");
			}
		});
	}
	</script>
</body>
</html>