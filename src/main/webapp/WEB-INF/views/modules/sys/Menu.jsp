<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<%@ taglib prefix="sys" tagdir="/WEB-INF/tags/sys" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>菜单管理</title>
<style type="text/css">
#fm-search #fm {
	margin: 0;
	padding: 0px 5px;
}
#fm-search .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#fm-search .row {
	margin-bottom: 8px;
}
#fm-search .row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
#fm-search .row input,select {
	width: 160px;

}
#fm-search .row .column {
	margin-right: 10px;
}
#fm-search .row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:70px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="name-search">菜单名称</label> 
					<input id="name-search" name="name-search" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="category-search">菜单类别</label> 
					<input id="category-search" name="label-search" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="parentId-search">上级菜单</label> 
					<input id="parentId-search" name="parentId-search" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="id-search">数据ID</label>
					<input id="id-search" name="id-search" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="uuid-search">数据UUID</label> 
					<input id="uuid-search" name="uuid-search" class="easyui-textbox" style="width: 410px">
				</span> 
			</div>
		</form>
	</div>

	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="menuData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'name',width:240,align:'left'">菜单名称</th>
					<th data-options="field:'category',width:100,align:'left'">菜单类别</th>
					<th data-options="field:'menuType',width:80,align:'left'">菜单类型</th>
					<th data-options="field:'href',width:300,align:'left'">链接</th>
					<th data-options="field:'icon',width:80,align:'left'">图标</th>
					<th data-options="field:'sort',width:60,align:'left'">排序</th>
                    <th data-options="field:'parentId',width:80,align:'left',hidden:true">父级编号</th>
                    <th data-options="field:'fullPath',width:80,align:'left',hidden:true">结构的全路径</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:100,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchMenu();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="newMenu();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editMenu();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">编辑</a>
			<a onclick="delMenu();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>
	    </div>
	</div>

	<sys:MenuEdit mvcPath="${pageScope.mvcPath}" saveFunction="saveMenu()" dialogID="dlg" formID="fm"></sys:MenuEdit>

	<script type="text/javascript">
	// 获取所有菜单数据地址
	var menuDataUrl = "${pageScope.mvcPath}/sys/findMenu";
	// 新增保存Menu地址
	var newMenuUrl = "${pageScope.mvcPath}/sys/addMenu";
	// 编辑保存Menu地址
	var editMenuUrl = "${pageScope.mvcPath}/sys/updateMenu";
	// 删除Menu地址
	var delMenuUrl = "${pageScope.mvcPath}/sys/deleteMenu";
	// 保存时地址
	var menuSaveUrl = "";
	// 获取菜单树数据的地址
	var menuTreeUrl = "${pageScope.mvcPath}/sys/getMenuTreeExcludeOneself";
	
	$(document).ready(function () {
		// 设置menuData数据显示表格
		$("#menuData").treegrid({
			url : menuDataUrl,
			idField: 'id',
			treeField: 'name',
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			nowrap : true,
			pagination : false,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function(rowIndex, rowData){
				editMenu();
			},
			onBeforeLoad : function (row, param) {
				if(param == null){
					param = {};
				}
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			}
		});
	});

	// 保存字典
	function saveMenu() {
		$("#fm").form("submit", {
			url : menuSaveUrl,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({
						title : '提示',
						msg : data.message,
						timeout : 5000,
						showType : 'slide'
					});
					$("#menuData").treegrid('load');
				} else {
					// 保存失败

				}
			}
		});
	}
	
	// 查询字典数据
	function searchMenu() {
		$("#menuData").treegrid('load');
	}

	// 新增菜单
	function newMenu() {
		menuSaveUrl = newMenuUrl;
		var param = {};
		param.fullPath = "XXX";
		$.post(menuTreeUrl, param, function (data) {
			$("#parentId").combotree("clear");
			$("#parentId").combotree("loadData",data);
			
			$('#dlg').dialog('open').dialog('setTitle', '新增菜单');
			$('#fm').form('reset');
		}, "json");
	}

	// 编辑菜单
	function editMenu() {
		menuSaveUrl = editMenuUrl;
		var row = $('#menuData').treegrid('getSelected');
		if (row == null) {
			$.messager.alert("提示", "未选择要编辑的数据！", "info");
			return;
		}
		if (row) {
         	var param = {};
        	param.fullPath = row.fullPath;
			$.post(menuTreeUrl, param, function (data) {
				$("#parentId").combotree("clear");
				$("#parentId").combotree("loadData",data);
				
				$('#dlg').dialog('open').dialog('setTitle', '编辑菜单');
				$('#fm').form('load', row);
			}, "json");
		}
	}

	// 删除菜单
	function delMenu() {
		var row = $("#menuData").treegrid("getSelected");
		if (row == null) {
			$.messager.alert("提示", "未选择要删除的数据！", "info");
			return;
		}
		$.messager.confirm("确认删除", "确定要删除菜单[类别：" + row.category + "，名称：" + row.name + "]？", function (r) {
			if (r) {
				$.post(delMenuUrl, row, function (data) {
					if (data.success) {
						// 删除成功
						$.messager.show({
							title : '提示',
							msg : data.message,
							timeout : 5000,
							showType : 'slide'
						});
						$("#menuData").treegrid('load');
					} else {
						// 删除失败
					}
				}, "json");
			}
		});
	}
	</script>
</body>
</html>