<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<%@ taglib prefix="sys" tagdir="/WEB-INF/tags/sys" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>用户管理</title>
<style type="text/css">
#fm-search #fm {
	margin: 0;
	padding: 0px 5px;
}
#fm-search .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#fm-search .row {
	margin-bottom: 8px;
}
#fm-search .row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
#fm-search .row input,select {
	width: 160px;

}
#fm-search .row .column {
	margin-right: 10px;
}
#fm-search .row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">

	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:70px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="name-search">用户姓名</label> 
					<input id="name-search" name="name-search" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="logiName-search">登入名</label> 
					<input id="logiName-search" name="logiName-search" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="userType-search">用户类型</label> 
					<input id="userType-search" name="userType-search" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="id-search">数据ID</label>
					<input id="id-search" name="id-search" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="uuid-search">数据UUID</label> 
					<input id="uuid-search" name="uuid-search" class="easyui-textbox" style="width: 410px">
				</span> 
			</div>
		</form>
	</div>
	
	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="userData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'name',width:120,align:'left'">姓名</th>
                    <th data-options="field:'loginName',width:120,align:'left'">登录名</th>
					<th data-options="field:'homeCompany',width:180,align:'left'">归属公司</th>
                    <th data-options="field:'homeOrg',width:180,align:'left'">直属机构</th>
                    <th data-options="field:'jobNo',width:120,align:'left'">工号</th>
                    <th data-options="field:'userType',width:80,align:'left'">用户类型</th>
                    <th data-options="field:'accountState',width:80,align:'left'">帐号状态</th>
                    <th data-options="field:'userState',width:80,align:'left'">用户状态</th>
                    <th data-options="field:'password',width:80,align:'left',hidden:true">密码</th>
                    <th data-options="field:'email',width:80,align:'left',hidden:true">邮箱</th>
                    <th data-options="field:'phone',width:80,align:'left',hidden:true">电话</th>
                    <th data-options="field:'mobile',width:80,align:'left',hidden:true">手机</th>
                    <th data-options="field:'loginIp',width:80,align:'left',hidden:true">最后登陆IP</th>
                    <th data-options="field:'loginDate',width:80,align:'left',hidden:true">最后登陆时间</th>
                    
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:260,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchUser();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="newUser();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editUser();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">编辑</a>
			<a onclick="delUser();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>
	    </div>
	</div>

	<sys:UserEdit mvcPath="${pageScope.mvcPath}" saveFunction="saveUser()" dialogID="dlg" formID="fm"></sys:UserEdit>
	
	<script type="text/javascript">
    // 查询用户数据地址
    var userDataUrl = "${pageScope.mvcPath}/sys/findUserByPage";
    // 新增保存User地址
    var newUserUrl = "${pageScope.mvcPath}/sys/addUser";
    // 编辑保存User地址
    var editUserUrl = "${pageScope.mvcPath}/sys/updateUser";
    // 删除User地址
    var delUserUrl = "${pageScope.mvcPath}/sys/deleteUser";
    // 保存时地址
    var userSaveUrl = "";
    
    $(document).ready(function() {
        // 设置userData数据显示表格
        $("#userData").datagrid({
            url : userDataUrl,
            idField : "name",
            fit : true,
            fitColumns : false,
            striped : true,
            rownumbers : true,
            singleSelect : true,
            nowrap : true,
            pagination : true,
            loadMsg : "正在加载，请稍候...",
            toolbar : "#tb",
            onDblClickRow : function (rowIndex, rowData) {
            	editUser();
            },
            onBeforeLoad : function (param) {
                // 增加查询参数
                var paramArray = $("#fm-search").serializeArray();
                $(paramArray).each(function () {
                    if (param[this.name]) {
                        if ($.isArray(param[this.name])) {
                            param[this.name].push(this.value);
                        } else {
                            param[this.name] = [param[this.name], this.value];
                        }
                    } else {
                        param[this.name] = this.value;
                    }
                });
            }
        });
    });
    
    // 查询用户
    function searchUser(){
        $("#userData").datagrid('load');
    }
    
    // 保存用户
    function saveUser(){
        $("#fm").form("submit", {
            url : userSaveUrl,
            success : function (data) {
                var data = $.parseJSON(data);
                if (data.success) {
                    // 保存成功
                    $('#dlg').dialog('close')
                    $.messager.show({
                        title : '提示',
                        msg : data.message,
                        timeout : 5000,
                        showType : 'slide'
                    });
                    $("#userData").datagrid('reload');
                } else {
                    // 保存失败

                }
            }
        });
    }
    
    // 新增用户
    function newUser(){
    	userSaveUrl = newUserUrl;
        $('#dlg').dialog('open').dialog('setTitle','新增用户');
        $('#fm').form('reset');
        
/*         var param = {};
        param.fullPath = "XXX";
        $.post(areaTreeUrl, param, function (data) {
            $("#parentId").combotree("clear");
            $("#parentId").combotree("loadData",data);
            
            $('#dlg').dialog('open').dialog('setTitle','新增区域');
            $('#fm').form('reset');
        }, "json"); */
    }
    
    // 编辑用户
    function editUser(){
        userSaveUrl = editUserUrl;
        var row = $("#userData").datagrid("getSelected");
        if(row == null){
            $.messager.alert("提示","未选择要编辑的数据！","info");
            return ;
        }
        if (row){
            $('#dlg').dialog('open').dialog('setTitle','编辑用户');
            $('#fm').form('load',row);
        	
/*             var param = {};
            param.fullPath = row.fullPath;
            $.post(areaTreeUrl, param, function (data) {
                $("#parentId").combotree("clear");
                $("#parentId").combotree("loadData",data);
                
                $('#dlg').dialog('open').dialog('setTitle','编辑区域');
                $('#fm').form('load',row);
            }, "json"); */
        }
    }
    
    // 删除用户
    function delUser(){
        var row = $("#userData").datagrid("getSelected");
        if (row == null) {
            $.messager.alert("提示", "未选择要删除的数据！", "info");
            return;
        }
        $.messager.confirm("确认删除", "确定要删除用户[" + row.name + "]？", function (r) {
            if (r) {
                $.post(delUserUrl, row, function (data) {
                    if (data.success) {
                        // 删除成功
                        $.messager.show({
                            title : '提示',
                            msg : data.message,
                            timeout : 5000,
                            showType : 'slide'
                        });
                        $("#userData").datagrid('reload');
                    } else {
                        // 删除失败
                    }
                }, "json");
            }
        });
    }
	</script>
</body>
</html>