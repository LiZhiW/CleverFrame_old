<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<%@ taglib prefix="sys" tagdir="/WEB-INF/tags/sys" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>机构管理</title>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<%--
		<!-- 页面上部 -->
		<div data-options="region:'north',border:true" style="height:70px;">
			<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
	
			</form>
		</div>
	--%>
	
	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="orgData" data-options="border:false">
			<thead>
				<tr>
                    <th data-options="field:'name',width:200,align:'left'">机构名称</th>
					<th data-options="field:'code',width:180,align:'left'">机构编码</th>
                    <th data-options="field:'orgType',width:100,align:'left'">机构类型</th>
                    <th data-options="field:'address',width:100,align:'left'">机构地址</th>
                    <th data-options="field:'zipCode',width:100,align:'left'">邮政编码</th>
                    <th data-options="field:'master',width:100,align:'left'">负责人</th>
                    <th data-options="field:'phone',width:100,align:'left'">电话</th>
                    <th data-options="field:'fax',width:100,align:'left'">传真</th>
					<th data-options="field:'email',width:100,align:'left'">邮箱</th>
                    <th data-options="field:'parentId',width:80,align:'left',hidden:true">父级编号</th>
                    <th data-options="field:'fullPath',width:80,align:'left',hidden:true">结构的全路径</th>
                    
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的ID</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的ID</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:260,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchOrg();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="newOrg();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editOrg();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">编辑</a>
			<a onclick="delOrg();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>
	    </div>
	</div>
	
	<sys:OrgEdit mvcPath="${pageScope.mvcPath}" saveFunction="saveOrg()" dialogID="dlg" formID="fm" />
	
	<script type="text/javascript">
		// 获取所有机构数据地址
		var orgDataUrl = "${pageScope.mvcPath}/sys/getAllOrganization";
		// 新增保存Org地址
		var newOrgUrl = "${pageScope.mvcPath}/sys/addOrganization";
		// 编辑保存Org地址
		var editOrgUrl = "${pageScope.mvcPath}/sys/updateOrganization";
		// 删除Org地址
		var delOrgUrl = "${pageScope.mvcPath}/sys/deleteOrg";
		// 保存时地址
		var orgSaveUrl = "";
		// 获取机构树数据的地址
		var orgTreeUrl = "${pageScope.mvcPath}/sys/getOrgTreeExcludeOneself";
		// 查询机构的直接子机构可选类型
		var findOrgTypeUrl	= "${mvcPath}/sys/getChildOrgType";
		
		$(document).ready(function() {
			// 设置orgData数据显示表格
			$("#orgData").treegrid({
				url : orgDataUrl,
				idField: 'id',
				treeField: 'name',
				fit : true,
				fitColumns : false,
				striped : true,
				rownumbers : true,
				singleSelect : true,
				nowrap : true,
				pagination : false,
				loadMsg : "正在加载，请稍候...",
				toolbar : "#tb",
				onDblClickRow : function(rowIndex, rowData){
					editOrg();
				}
			});
		});
		
		// 查询机构
		function searchOrg(){
			$("#orgData").treegrid('load');
		}
		
		// 保存机构
		function saveOrg(){
			$("#fm").form("submit", {
				url : orgSaveUrl,
				success : function (data) {
					var data = $.parseJSON(data);
					if (data.success) {
						// 保存成功
						$('#dlg').dialog('close')
						$.messager.show({
							title : '提示',
							msg : data.message,
							timeout : 5000,
							showType : 'slide'
						});
						$("#orgData").treegrid('load');
					} else {
						// 保存失败

					}
				}
			});
		}
		
		// 新增机构
        function newOrg(){
        	orgSaveUrl = newOrgUrl;
			var param = {};
			param.fullPath = "XXX";
			param.orgId = -1;
			$.post(orgTreeUrl, param, function (data) {
				$("#parentId").combotree("clear");
				$("#parentId").combotree("loadData",data);
				
				$("#orgType").combobox("loadData",{});
				
	            $('#dlg').dialog('open').dialog('setTitle','新增机构');
	            $('#fm').form('reset');
			}, "json");
        }
		
		// 编辑机构
        function editOrg(){
        	orgSaveUrl = editOrgUrl;
            var row = $('#orgData').treegrid('getSelected');
			if(row == null){
				$.messager.alert("提示","未选择要编辑的数据！","info");
				return ;
			}
            if (row){
             	var param = {};
            	param.fullPath = row.fullPath;
    			$.post(orgTreeUrl, param, function (data) {
    				$("#parentId").combotree("clear");
    				$("#parentId").combotree("loadData",data);
    				
    				$("#orgType").combobox("loadData",{});
    				
                    $('#dlg').dialog('open').dialog('setTitle','编辑机构');
                    $('#fm').form('load',row);
    			}, "json");
            }
        }
		
		// 删除机构
        function delOrg(){
    		var row = $("#orgData").treegrid("getSelected");
    		if (row == null) {
    			$.messager.alert("提示", "未选择要删除的数据！", "info");
    			return;
    		}
    		$.messager.confirm("确认删除", "确定要删除机构[" + row.name + "]？", function (r) {
    			if (r) {
    				$.post(delOrgUrl, row, function (data) {
    					if (data.success) {
    						// 删除成功
    						$.messager.show({
    							title : '提示',
    							msg : data.message,
    							timeout : 5000,
    							showType : 'slide'
    						});
    						$("#orgData").treegrid('load');
    					} else {
    						// 删除失败
    					}
    				}, "json");
    			}
    		});
        }
	</script>
	
</body>
</html>