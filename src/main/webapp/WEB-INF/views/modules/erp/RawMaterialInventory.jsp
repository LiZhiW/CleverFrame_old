<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>原料库统计</title>
<style type="text/css">
.row {
	margin-bottom: 8px;
}
.row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
.row input,select {
	width: 160px;

}
.row .column {
	margin-right: 10px;
}
.row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:45px;">
		<form id="fm-search" method="post" style="margin-top: 10px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="search_rawmaterialCode">原料编码</label> 
					<input id="search_rawmaterialCode" name="rawmaterialCode" class="easyui-textbox">
				</span>
				
				<span class="column">
					<label for="search_name">原料名称</label> 
					<input id="search_name" name="name" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="search_specification">原料规格</label> 
					<input id="search_specification" name="specification" class="easyui-textbox">
				</span>
			</div>
		</form>
	</div>

	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="rawMaterialData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'check',checkbox:true">选择</th>
					<th data-options="field:'rawmaterial_code',width:100,align:'left'">原料编码</th>
					<th data-options="field:'name',width:150,align:'left'">原料名称</th>
					<th data-options="field:'specification',width:100,align:'left'">原料规格</th>
					<th data-options="field:'rawmaterial_type',width:100,align:'left'">原料类型</th>
					<th data-options="field:'color_no',width:100,align:'left'">原料色号</th>
					
					<th data-options="field:'warehouse_quantity',width:100,align:'left',formatter:seeWarehouseQuantity">入库数量(m)</th>
					<th data-options="field:'out_workshop_quantity',width:100,align:'left',formatter:seeOutWorkshopQuantity">车间出库(m)</th>
					<th data-options="field:'out_goning_quantity',width:100,align:'left',formatter:seeOutGoningQuantity">外发出库(m)</th>
					<th data-options="field:'inventory',width:100,align:'left'">库存(m)</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'company_id',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'org_id',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'create_by',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'create_date',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'update_by',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'update_date',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:150,align:'left',hidden:true">备注信息</th>
					<th data-options="field:'del_flag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchRawMaterialInventory();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="clearSearchForm();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-edit-clear-2',plain:true">清空查询条件</a>
	    </div>
	</div>

	<!-- 查看入库数量 -->
	<div id="see_warehouse_quantity" style="width: 765px; height: 365px;">
		<table id="rawmaterialWarehouseData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'check',checkbox:true,hidden:true">选择</th>
					<th data-options="field:'warehouse_no',width:80,align:'left'">申请单编号</th>
					<th data-options="field:'createName',width:80,align:'left'">申请人</th>
					<th data-options="field:'warehouse_date',width:80,align:'left',formatter:formatDate">入库日期</th>
					<th data-options="field:'rawmaterial_code',width:80,align:'left'">原料编码</th>
					<th data-options="field:'name',width:100,align:'left'">原料名称</th>
					<th data-options="field:'specification',width:80,align:'left'">原料规格</th>
					<th data-options="field:'rawmaterial_type',width:80,align:'left'">原料类型</th>
					<th data-options="field:'color_no',width:80,align:'left'">原料色号</th>
					<th data-options="field:'quantity',width:80,align:'left'">入库数量</th>
					<th data-options="field:'supplierName',width:100,align:'left'">供应商</th>
					<th data-options="field:'auditor_state',width:80,align:'left',formatter:formatAuditorState">审核状态</th>
					<th data-options="field:'auditorName',width:80,align:'left'">审核人</th>
					<th data-options="field:'unit_price',width:60,align:'left'">单价</th>
					<th data-options="field:'aggregate_amount',width:70,align:'left'">应付金额</th>
					<th data-options="field:'is_invoice',width:80,align:'left',formatter:formatIsInvoice">是否开发票</th>
					
					<th data-options="field:'is_history',width:80,align:'left',hidden:true">是否是历史记录</th>
					<th data-options="field:'auditor_id',width:100,align:'left',hidden:true">审核人ID</th>
					<th data-options="field:'supplier_id',width:50,align:'left',hidden:true">供应商ID</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'company_id',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'org_id',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'create_by',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'create_date',width:100,align:'left',hidden:true">申请时间</th>
					<th data-options="field:'update_by',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'update_date',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:150,align:'left'">备注信息</th>
					<th data-options="field:'del_flag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
	</div>

	<!-- 查看出库数量 -->
	<div id="see_out_quantity" style="width: 765px; height: 365px;">
		<table id="rawmaterialOutData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'check',checkbox:true,hidden:true">选择</th>
					<th data-options="field:'out_no',width:80,align:'left'">申请单编号</th>
					<th data-options="field:'createName',width:80,align:'left'">申请人</th>
					<th data-options="field:'out_date',width:80,align:'left',formatter:formatDate">出库日期</th>
					<th data-options="field:'out_type',width:80,align:'left',formatter:formatOutTypeData">出库类型</th>
					<th data-options="field:'rawmaterial_code',width:80,align:'left'">原料编码</th>
					<th data-options="field:'name',width:100,align:'left'">原料名称</th>
					<th data-options="field:'specification',width:80,align:'left'">原料规格</th>
					<th data-options="field:'rawmaterial_type',width:80,align:'left'">原料类型</th>
					<th data-options="field:'color_no',width:80,align:'left'">原料色号</th>
					<th data-options="field:'quantity',width:80,align:'left'">出库数量</th>
					<th data-options="field:'auditor_state',width:80,align:'left',formatter:formatAuditorState">审核状态</th>
					<th data-options="field:'auditorName',width:80,align:'left'">审核人</th>
					<th data-options="field:'unit_price',width:60,align:'left'">单价</th>
					<th data-options="field:'aggregate_amount',width:70,align:'left'">应付金额</th>
					<th data-options="field:'is_invoice',width:80,align:'left',formatter:formatIsInvoice">是否开发票</th>
					
					<th data-options="field:'order_no',width:80,align:'left',hidden:true">订单编号</th>
					<th data-options="field:'is_first_order',width:80,align:'left',hidden:true">是否先下订单后原料出库</th>
					<th data-options="field:'is_history',width:80,align:'left',hidden:true">是否是历史记录</th>
					<th data-options="field:'auditor_id',width:100,align:'left',hidden:true">审核人ID</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'company_id',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'org_id',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'create_by',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'create_date',width:100,align:'left',hidden:true">申请时间</th>
					<th data-options="field:'update_by',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'update_date',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:150,align:'left'">备注信息</th>
					<th data-options="field:'del_flag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
	</div>

	<script type="text/javascript">
	// 查询库存
	var findRawmaterialInventoryUrl = "${pageScope.mvcPath}/erp/findRawmaterialInventory";
	// 查询入库
	var findRawmaterialWarehouseByPageUrl = "${pageScope.mvcPath}/erp/findRawmaterialWarehouseByPage";
	// 查询出库
	var findRawmaterialOutByPageUrl = "${pageScope.mvcPath}/erp/findRawmaterialOutByPage";
	// 原料编码
	var rawmaterialCode = null;
	// 审核状态
	var auditorState = "3";
	// 出库类型
	var outType = null;
	
	// 流程审核状态 - 字典
	var findAuditorStateDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findAuditorStateDictUrl += encodeURIComponent("流程审核状态");
	var auditorStateDictData = null;
	$.ajax({
		url : findAuditorStateDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			auditorStateDictData = data;
		}
	});
	
	// 是否开发票 - 字典
	var findIsInvoiceDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findIsInvoiceDictUrl += encodeURIComponent("是否开发票");
	var IsInvoiceDictData = null;
	$.ajax({
		url : findIsInvoiceDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			IsInvoiceDictData = data;
		}
	});
	
	// 出库类型 - 字典
	var findOutTypeDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findOutTypeDictUrl += encodeURIComponent("原料出库-出库类型");
	var OutTypeData = null;
	$.ajax({
		url : findOutTypeDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			OutTypeData = data;
		}
	});
	
	$(document).ready(function () {
		// 设置rawMaterialData数据显示表格
		$("#rawMaterialData").datagrid({
			url : findRawmaterialInventoryUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			},
			onLoadSuccess : function (data) {
				
			},
			onCheckAll : function (rows) {
				isCheckAll = true;
			},
			onUncheckAll : function (rows) {
				isCheckAll = false;
			}
		});
		
		// 入库记录对话框
		$("#see_warehouse_quantity").dialog({
			title : "原料入库记录",
			closed : true,
			resizable : false,
			minWidth : 765,
			minHeight : 365,
			maxWidth : 765,
			maxHeight : 365,
			modal : true
		});
		
		// 入库记录表格
		$("#rawmaterialWarehouseData").datagrid({
			url : findRawmaterialWarehouseByPageUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			onDblClickRow : function (rowIndex, rowData) {
				
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				if(rawmaterialCode == null) {
					return false;
				}
				param.rawmaterialCode = rawmaterialCode;
				param.auditorState = auditorState;
			}
		});
		
		// 出库记录对话框
		$("#see_out_quantity").dialog({
			title : "原料出库记录",
			closed : true,
			resizable : false,
			minWidth : 765,
			minHeight : 365,
			maxWidth : 765,
			maxHeight : 365,
			modal : true
		});
		
		// 出库记录表格
		$("#rawmaterialOutData").datagrid({
			url : findRawmaterialOutByPageUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			onDblClickRow : function (rowIndex, rowData) {
				
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				if(rawmaterialCode == null || outType == null) {
					return false;
				}
				param.rawmaterialCode = rawmaterialCode;
				param.auditorState = auditorState;
				param.outType = outType;
			}
		});
	});
	
	// 清空查询条件
	function clearSearchForm() {
		$("#fm-search").form("clear");
	}
	
	// 查询
	function searchRawMaterialInventory() {
		$("#rawMaterialData").datagrid("load");
	}
	
	// 打开原料入库单信息
	function openWarehouseQuantity(code) {
		rawmaterialCode = code;
		$("#rawmaterialWarehouseData").datagrid("load");
		$("#see_warehouse_quantity").dialog("open");
	}
	
	// 打开原料出库单信息
	function openOutQuantity(code, type) {
		rawmaterialCode = code;
		outType = type;
		$("#rawmaterialOutData").datagrid("load");
		$("#see_out_quantity").dialog("open");
	}
	
	function seeWarehouseQuantity(value,row,index) {
		if($.isNumeric(value) && value > 0) {
			return "<a href='javascript:void(0)' onclick='openWarehouseQuantity(\"" + row.rawmaterial_code + "\");' >" + value +"</a>";
		} else {
			return value;
		}
	}
	
	function seeOutWorkshopQuantity(value,row,index) {
		if($.isNumeric(value) && value > 0) {
			return "<a href='javascript:void(0)' onclick='openOutQuantity(\"" + row.rawmaterial_code + "\",\"1\");' >" + value +"</a>";
		} else {
			return value;
		}
	}
	
	function seeOutGoningQuantity(value,row,index) {
		if($.isNumeric(value) && value > 0) {
			return "<a href='javascript:void(0)' onclick='openOutQuantity(\"" + row.rawmaterial_code + "\",\"2\");' >" + value +"</a>";
		} else {
			return value;
		}
	}
	
	function formatOutTypeData(value,row,index) {
		var text = null;
		$(OutTypeData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatIsInvoice(value,row,index) {
		var text = null;
		$(IsInvoiceDictData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatAuditorState(value,row,index) {
		var text = null;
		$(auditorStateDictData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
    function formatDate(value,row,index) {
    	var date = new Date(value);
    	return date.format("yyyy-MM-dd");
    }
	
    // 日期格式化
    Date.prototype.format = function (fmt) {
    	var o = {
    		"M+" : this.getMonth() + 1, //月份
    		"d+" : this.getDate(), //日
    		"h+" : this.getHours(), //小时
    		"m+" : this.getMinutes(), //分
    		"s+" : this.getSeconds(), //秒
    		"q+" : Math.floor((this.getMonth() + 3) / 3), //季度
    		"S" : this.getMilliseconds() //毫秒
    	};
    	if (/(y+)/.test(fmt)){
    		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    	}
    	for (var k in o) {
    		if (new RegExp("(" + k + ")").test(fmt)) {
    			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    		}
    	}
    	return fmt;
    }
	</script>
</body>
</html>