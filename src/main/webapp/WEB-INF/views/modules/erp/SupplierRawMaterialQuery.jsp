<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>供应商供应原料查询</title>
<style type="text/css">
.row {
	margin-bottom: 8px;
}
.row label {
	display: inline-block;
	width: 90px;
	text-align: center;
}
.row input,select {
	width: 160px;

}
.row .column {
	margin-right: 20px;
}
.row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:100px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="search_supplierNo">供应商编号</label> 
					<input id="search_supplierNo" name="supplierNo" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="search_supplierName">供应商名称</label> 
					<input id="search_supplierName" name="supplierName" class="easyui-textbox" style="width: 410px">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="search_legalPeople">供应商法人</label> 
					<input id="search_legalPeople" name="legalPeople" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="search_dockingPeople">供应商对接人</label>
					<input id="search_dockingPeople" name="dockingPeople" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="search_startDate">供应日期(最小)</label> 
					<input id="search_startDate" name="startDate" class="easyui-datebox">
				</span>
				<span class="columnLast">
					<label for="search_endDate">供应日期(最大)</label> 
					<input id="search_endDate" name="endDate" class="easyui-datebox">
				</span>
			</div>
		</form>
	</div>
	
	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',fit:false,border:false">
		<!-- 供应商面板 -->
		<div id="supplierPanel" class="easyui-panel" data-options="height:'55%',width:'100%',title:'供应商信息',border:true">
			<table id="supplierData" data-options="border:false">
				<thead>
					<tr>
						<th data-options="field:'check',checkbox:true,hidden:true">选择</th>
						<th data-options="field:'supplierNo',width:80,align:'left'">供应商编号</th>
						<th data-options="field:'supplierName',width:200,align:'left'">供应商名称</th>
						<th data-options="field:'legalPeople',width:100,align:'left'">供应商法人</th>
						<th data-options="field:'legalPeopleContact',width:150,align:'left'">供应商法人联系方式</th>
						<th data-options="field:'dockingPeople',width:100,align:'left'">供应商对接人</th>
						<th data-options="field:'dockingPeopleContact',width:150,align:'left'">供应商对接人联系方式</th>
						<th data-options="field:'startSupplyDate',width:130,align:'left'">供应商开始供应日期</th>
						
						<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
						<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
	                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
						<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
						<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
						<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
						<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
						<th data-options="field:'remarks',width:250,align:'left'">备注信息</th>
						<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
						<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
					</tr>
				</thead>
			</table>
			<div id="tb">
				<a onclick="searchSupplier();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
				<a onclick="clearSearchForm();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-edit-clear-2',plain:true">清空查询条件</a>
		    </div>
		</div>
		
		<!-- 原料面板 -->
		<div id="rawmaterialPanel" class="easyui-panel" data-options="height:'45%',width:'100%',title:'原料信息-选择供应商刷新',border:true">
			<table id="rawMaterialData" data-options="border:false">
				<thead>
					<tr>
						<th data-options="field:'check',checkbox:true,hidden:true">选择</th>
						<th data-options="field:'rawmaterialCode',width:150,align:'left'">原料编码</th>
						<th data-options="field:'name',width:150,align:'left'">原料名称</th>
						<th data-options="field:'specification',width:100,align:'left'">原料规格</th>
						<th data-options="field:'rawmaterialType',width:100,align:'left'">原料类型</th>
						<th data-options="field:'colorNo',width:100,align:'left'">原料色号</th>
						
						<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
						<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
	                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
						<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
						<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
						<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
						<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
						<th data-options="field:'remarks',width:150,align:'left'">备注信息</th>
						<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
						<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>

	<script type="text/javascript">
	// 查询数据
	var findSupplierByPageUrl = "${pageScope.mvcPath}/erp/findSupplierByPage";
	// 查询数据
	var findSupplierRawmaterialUrl = "${pageScope.mvcPath}/erp/findSupplierRawmaterial";
	// 当前选择的供应商
	var supplier = null;
	
	$(document).ready(function () {
		$("#supplierData").datagrid({
			url : findSupplierByPageUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onSelect : function (index,row) {
				supplier = row;
				findSupplierRawmaterial();
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			},
			onLoadSuccess : function (data) {
				// 加载成功，清空原料信息
				supplier = null;
				$("#rawMaterialData").datagrid("loadData", { total: 0, rows: [] });
			}
		});
		
		$("#rawMaterialData").datagrid({
			url : findSupplierRawmaterialUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			onBeforeLoad : function (param) {
				// 增加查询参数
				if(supplier == null) {
					return false;
				} else {
					param.supplierId = supplier.id;
				}
			}
		});
	});
	
	// 清空查询条件
	function clearSearchForm() {
		$("#fm-search").form("clear");
	}
	
	// 查询
	function searchSupplier() {
		$("#supplierData").datagrid("load");
	}
	
	// 查询供应商供应的原料信息
	function findSupplierRawmaterial() {
		$("#rawMaterialData").datagrid("load");
	}
	</script>
</body>
</html>