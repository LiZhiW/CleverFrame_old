<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>原料资料管理</title>
<style type="text/css">
.row {
	margin-bottom: 8px;
}
.row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
.row input,select {
	width: 160px;

}
.row .column {
	margin-right: 10px;
}
.row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:70px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="search_rawmaterialCode">原料编码</label> 
					<input id="search_rawmaterialCode" name="rawmaterialCode" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="search_name">原料名称</label> 
					<input id="search_name" name="name" class="easyui-textbox" style="width: 410px">
				</span>

			</div>
			<div class="row">
				<span class="column">
					<label for="search_specification">原料规格</label> 
					<input id="search_specification" name="specification" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="search_rawmaterialType">原料类型</label>
					<input id="search_rawmaterialType" name="rawmaterialType" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="search_colorNo">原料色号</label> 
					<input id="search_colorNo" name="colorNo" class="easyui-textbox">
				</span>
			</div>
		</form>
	</div>

	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="rawMaterialData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'check',checkbox:true">选择</th>
					<th data-options="field:'rawmaterialCode',width:150,align:'left'">原料编码</th>
					<th data-options="field:'name',width:150,align:'left'">原料名称</th>
					<th data-options="field:'specification',width:100,align:'left'">原料规格</th>
					<th data-options="field:'rawmaterialType',width:100,align:'left'">原料类型</th>
					<th data-options="field:'colorNo',width:100,align:'left'">原料色号</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:150,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchRawMaterial();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="clearSearchForm();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-edit-clear-2',plain:true">清空查询条件</a>
			<a onclick="newRawMaterial();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editRawMaterial();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">修改</a>
			<a onclick="delRawMaterial();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>
			<a onclick="dataExport();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-blue-document-export',plain:true">导出</a>
			<a onclick="showImport();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-blue-document-import',plain:true">导入</a>
			<a onclick="downloadTemplateFile();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-download',plain:true">下载导入模版</a>
	    </div>
	</div>
	
	<!-- 新增/编辑对话框 -->
	<div id="dlg" style="width: 520px; height: 260px; padding: 5px 10px">
		<form id="fm" method="post">
			<div class="row" style="margin-top: 10px;">
				<span class="column">
					<label for="dialog_rawmaterialCode">原料编码</label> 
					<input id="dialog_rawmaterialCode" name="rawmaterialCode" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_name">原料名称</label> 
					<input id="dialog_name" name="name" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="dialog_specification">原料规格</label> 
					<input id="dialog_specification" name="specification" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_rawmaterialType">原料类型</label>
					<input id="dialog_rawmaterialType" name="rawmaterialType" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="columnLast">
					<label for="dialog_colorNo">原料色号</label> 
					<input id="dialog_colorNo" name="colorNo" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="dialog_remarks">备注信息</label> 
					<input id="dialog_remarks" name="remarks" style="width:400px;height:60px" class="easyui-textbox">
				</span>
			</div>
			
			<!-- 隐藏属性值 -->
			<div style="display: none;">
				<input name="id">
				<input name="delFlag">
				<input name="companyId">
				<input name="orgId">
				<input name="createBy">
				<input name="createDate">
				<input name="updateBy">
				<input name="updateDate">
				<input name="uuid">
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="saveRawMaterial();" style="width:90px">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">取消</a>
	</div>
	
	<!-- 数据导入面板 -->
	<div id="dataImport_dlg" style="width: 370px; height: 140px; padding: 5px 10px">
		<form id="dataImport_fm" method="post" enctype="multipart/form-data">
			<div style="margin-top: 15px;">
				<input id="dataImport_file" type="file" name="dataImport_file" style="margin-right: 10px;">
			</div>
		</form>
	</div>
	<div id="dataImport_dlg_buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="dataImport();" style="width:90px">导入</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#dataImport_dlg').dialog('close')" style="width:90px">取消</a>
	</div>
	
	<script type="text/javascript">
	// 查询原料
	var findRawMaterialInfoByPageUrl = "${pageScope.mvcPath}/erp/findRawMaterialInfoByPage";
	// 保存原料
	var addRawMaterialInfoUrl = "${pageScope.mvcPath}/erp/addRawMaterialInfo";
	// 更新原料
	var updateRawMaterialInfoUrl = "${pageScope.mvcPath}/erp/updateRawMaterialInfo";
	// 删除原料
	var deleteRawMaterialInfoUrl = "${pageScope.mvcPath}/erp/deleteRawMaterialInfo";
	var rawMaterialInfoSaveUrl;
	// 下载导入模版数据的模版文件
	var downloadTemplateFileUrl = "${pageScope.mvcPath}/sys/downloadTemplateFile/";
	downloadTemplateFileUrl += encodeURIComponent("原料资料导入模版");
	
	// 是否全选
	var isCheckAll = false;
	// 数据导出Url
	var dataExportUrl = "${pageScope.mvcPath}/erp/exportRawMaterialInfo";
	// 数据导入Url
	var dataImportUrl = "${pageScope.mvcPath}/erp/importRawMaterialInfo";
	
	$(document).ready(function () {
		// 设置rawMaterialData数据显示表格
		$("#rawMaterialData").datagrid({
			url : findRawMaterialInfoByPageUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				editRawMaterial();
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			},
			onLoadSuccess : function (data) {
				
			},
			onCheckAll : function (rows) {
				isCheckAll = true;
			},
			onUncheckAll : function (rows) {
				isCheckAll = false;
			}
		});
		
		$("#dlg").dialog({
			title : "原料资料",
			closed : true,
			resizable : true,
			minWidth : 520,
			minHeight : 260,
			maxWidth : 520,
			maxHeight : 260,
			modal : true,
			buttons : "#dlg-buttons"
		});
		
		$("#dialog_rawmaterialCode").textbox({
			required : false,
			readonly : true,
			prompt : "编码自动生成",
			validType : 'length[1,100]'
		});
		
		$("#dialog_name").textbox({
			required : true,
			validType : 'length[1,100]'
		});
		
		$("#dialog_specification").textbox({
			required : true,
			validType : 'length[1,100]'
		});
		
		$("#dialog_rawmaterialType").textbox({
			required : false,
			validType : 'length[0,100]'
		});
		
		$("#dialog_colorNo").textbox({
			required : false,
			validType : 'length[0,100]'
		});
		
		$("#dialog_remarks").textbox({
			validType : 'length[3,255]',
			multiline:true
		});
		
		$("#dataImport_dlg").dialog({
			title : "原料资料数据导入",
			closed : true,
			resizable : true,
			minWidth : 370,
			minHeight : 140,
			maxWidth : 370,
			maxHeight : 140,
			modal : true,
			buttons : "#dataImport_dlg_buttons"
		});
	});
	
	// 清空查询条件
	function clearSearchForm() {
		$("#fm-search").form("clear");
	}
	
	// 查询原料信息
	function searchRawMaterial() {
		$("#rawMaterialData").datagrid("load");
	}
	
	// 新增原料
	function newRawMaterial() {
		rawMaterialInfoSaveUrl = addRawMaterialInfoUrl;
		$('#dlg').dialog('open').dialog('setTitle', '新增原料资料');
		$('#fm').form('reset');
	}
	
	// 编辑原料
	function editRawMaterial() {
		rawMaterialInfoSaveUrl = updateRawMaterialInfoUrl;
		var row = $('#rawMaterialData').datagrid('getSelected');
		if (row == null) {
			$.messager.alert("提示", "未选择要编辑的数据！", "info");
			return;
		}
		if (row) {
			$('#dlg').dialog('open').dialog('setTitle', '编辑原料资料');
			$('#fm').form('load', row);
		}
	}
	
	// 删除原料
	function delRawMaterial() {
		var row = $("#rawMaterialData").datagrid("getSelected");
		if (row == null) {
			$.messager.alert("提示", "未选择要删除的数据！", "info");
			return;
		}
		$.messager.confirm("确认删除", "确定要删除原料资料?<br/>名称：" + row.name + "<br/>规格：" + row.specification, function (r) {
			if (r) {
				$.post(deleteRawMaterialInfoUrl, row, function (data) {
					if (data.success) {
						// 删除成功
						$.messager.show({
							title : '提示',
							msg : data.message,
							timeout : 5000,
							showType : 'slide'
						});
						$("#rawMaterialData").datagrid('reload');
					} else {
						// 删除失败
						$.messager.alert("提示", data.message, "warning");
					}
				}, "json");
			}
		});
	}
	
	// 保存原料
	function saveRawMaterial() {
		$("#fm").form("submit", {
			url : rawMaterialInfoSaveUrl,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({
						title : '提示',
						msg : data.message,
						timeout : 5000,
						showType : 'slide'
					});
					$("#rawMaterialData").datagrid('reload');
				} else {
					// 保存失败
					$.messager.alert("提示", data.message, "warning");
				}
			}
		});
	}
	
	// 下载模版文件
	function downloadTemplateFile() {
		window.open(downloadTemplateFileUrl);
	}
	
	// 数据导出
	function dataExport() {
		if(isCheckAll == true) {
			window.open(dataExportUrl + "?ids=ALL");
		} else {
			var rows = $("#rawMaterialData").datagrid("getChecked");
			if(rows.length <= 0) {
				$.messager.alert("提示", "请选择要导出的数据!", "info");
				return;
			} else {
				var ids = "";
				$(rows).each(function(index, row){
					if(ids == ""){
						ids += row.id;
					} else {
						ids += ("," + row.id);
					}
				});
				window.open(dataExportUrl + "?ids=" + ids);
			}
		}
	}
	
	// 显示数据导入面板
	function showImport() {
		$("#dataImport_dlg").dialog("open");
	}
	
	// 数据导入
	function dataImport() {
		if($("#dataImport_file").val() == "") {
			$("#dataImport_file").focus();
			$.messager.alert("提示", "请选择要导入的数据文件!", "info");
			return ;
		}
		$("#dataImport_fm").attr("action", dataImportUrl).submit();
		$("#dataImport_dlg").dialog("close");
		$("#rawMaterialData").datagrid("reload");
	}
	</script>
</body>
</html>