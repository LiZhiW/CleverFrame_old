<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>原料外发加工审核</title>
<style type="text/css">
.row {
	margin-bottom: 8px;
}
.row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
.row input,select {
	width: 160px;
}
.row .column {
	margin-right: 10px;
}
.row .columnLast {
	margin-right: 0px;
}
.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
.radiobutton{
	cursor: pointer;
    overflow: hidden;
    vertical-align: middle;
    width: 14px;
    height: 14px;
    margin: 0;
    padding: 0;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:70px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="search_outNo">申请单编号</label> 
					<input id="search_outNo" name="outNo" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="search_name">原料名称</label> 
					<input id="search_name" name="name" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="search_specification">原料规格</label> 
					<input id="search_specification" name="specification" class="easyui-textbox">
				</span>
			</div>
			 
			<div class="row">
<!-- 			
				<span class="column">
					<label for="search_auditorState">审核状态</label> 
					<input id="search_auditorState" name="auditorState" class="easyui-combobox">
				</span>
-->
				<span class="columnLast">
					<label for="search_startDate">出库日期</label>
					<input id="search_startDate" name="startDate" class="easyui-datebox">
				</span>
				<span class="columnLast">
					<label for="search_endDate" style="width: 20px;">至</label> 
					<input id="search_endDate" name="endDate" class="easyui-datebox">
				</span>
			</div>
		</form>
	</div>
	
	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="rawmaterialOutData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'check',checkbox:true,hidden:true">选择</th>
					<th data-options="field:'out_no',width:80,align:'left'">申请单编号</th>
					<th data-options="field:'createName',width:80,align:'left'">申请人</th>
					<th data-options="field:'out_date',width:80,align:'left',formatter:formatDate">出库日期</th>
					<th data-options="field:'out_type',width:80,align:'left',formatter:formatOutTypeData">出库类型</th>
					<th data-options="field:'rawmaterial_code',width:80,align:'left'">原料编码</th>
					<th data-options="field:'name',width:100,align:'left'">原料名称</th>
					<th data-options="field:'specification',width:80,align:'left'">原料规格</th>
					<th data-options="field:'rawmaterial_type',width:80,align:'left'">原料类型</th>
					<th data-options="field:'color_no',width:80,align:'left'">原料色号</th>
					<th data-options="field:'quantity',width:80,align:'left'">出库数量</th>
					<th data-options="field:'auditor_state',width:80,align:'left',formatter:formatAuditorState">审核状态</th>
					<th data-options="field:'auditorName',width:80,align:'left'">审核人</th>
					<th data-options="field:'unit_price',width:60,align:'left'">单价</th>
					<th data-options="field:'aggregate_amount',width:70,align:'left'">应付金额</th>
					<th data-options="field:'is_invoice',width:80,align:'left',formatter:formatIsInvoice">是否开发票</th>
					
					<th data-options="field:'order_no',width:80,align:'left',hidden:true">订单编号</th>
					<th data-options="field:'is_first_order',width:80,align:'left',hidden:true">是否先下订单后原料出库</th>
					<th data-options="field:'is_history',width:80,align:'left',hidden:true">是否是历史记录</th>
					<th data-options="field:'auditor_id',width:100,align:'left',hidden:true">审核人ID</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'company_id',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'org_id',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'create_by',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'create_date',width:100,align:'left',hidden:true">申请时间</th>
					<th data-options="field:'update_by',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'update_date',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:150,align:'left'">备注信息</th>
					<th data-options="field:'del_flag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
					<th data-options="field:'operate',width:100,align:'left',formatter:formatOperate">操作</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchRawmaterialOut();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="clearSearchForm();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-edit-clear-2',plain:true">清空查询条件</a>
			<a onclick="audit();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-mail-signed-verified-2',plain:true">审核</a>
	    </div>
	</div>
	
	<!-- 新增/编辑对话框 -->
	<div id="dlg" style="width: 765px; height: 390px; padding: 5px 10px">
		<form id="fm" method="post">
			<div class="row" style="margin-top: 10px;">
				<span class="column">
					<label for="dialog_outNo">申请单编号</label> 
					<input id="dialog_outNo" name="outNo" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_outDate">出库日期</label>
					<input id="dialog_outDate" name="outDate" class="easyui-datebox">
				</span>
				<span class="columnLast">
					<label for="dialog_orderNo">订单编号</label> 
					<input id="dialog_orderNo" name="orderNo" class="easyui-textbox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_rawmaterialCode">原料编码</label> 
					<input id="dialog_rawmaterialCode" name="rawmaterialCode" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_name">原料名称</label> 
					<input id="dialog_name" name="name" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_specification">原料规格</label> 
					<input id="dialog_specification" name="specification" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="dialog_rawmaterialType">原料类型</label>
					<input id="dialog_rawmaterialType" name="rawmaterialType" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_colorNo">原料色号</label>
					<input id="dialog_colorNo" name="colorNo" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_quantity">出库数量</label>
					<input id="dialog_quantity" name="quantity" class="easyui-numberbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="dialog_isInvoice">是否开票</label>
					<input id="dialog_isInvoice" name="isInvoice" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_unitPrice">原料单价</label>
					<input id="dialog_unitPrice" name="unitPrice" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_aggregateAmount">应付金额</label>
					<input id="dialog_aggregateAmount" name="aggregateAmount" class="easyui-textbox">
				</span>
			</div>
			<div style="margin-bottom:8px">
				<label>是否先下订单后原料出库：</label>
				<label for="dialog_firstOrderByYes" style="width: 15px;cursor: pointer;margin-right: 10px;vertical-align: middle;">
					<input id="dialog_firstOrderByYes" class="radiobutton" name="isFirstOrder" type="radio" value="1"/>是
				</label>
				
				<label for="dialog_firstOrderByNo" style="width: 15px;cursor: pointer;margin-right: 0px;vertical-align: middle;">
					<input id="dialog_firstOrderByNo" class="radiobutton" name="isFirstOrder" type="radio" value="2" checked="checked"/>否
				</label>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="dialog_remarks">备注信息</label> 
					<input id="dialog_remarks" name="remarks" style="width:655px;height:80px" class="easyui-textbox">
				</span>
			</div>
			<div class="ftitle">后续流程：</div>
			<div>
				<span>
					<label for="dialog_auditResultPass" style="width: 120px;cursor: pointer;margin-right: 20px;vertical-align: middle;">
						<input id="dialog_auditResultPass" class="radiobutton" name="auditResult" type="radio" value="auditPass" checked="checked"/>审核通过，仓管录入返回情况
					</label>
				</span>
				<span>
					<label for="dialog_auditResultReject" style="width: 120px;cursor: pointer;margin-right: 55px;vertical-align: middle;">
						<input id="dialog_auditResultReject" class="radiobutton" name="auditResult" type="radio" value="auditReject"/>驳回修改
					</label>
					<label for="dialog_auditorId" style="margin-left: 10px">处理人：</label>
					<input id="dialog_auditorId" name="auditorId" class="easyui-combobox">
				</span>
			</div>
			
			<!-- 隐藏属性值 -->
			<div style="display: none;">
				<input id="dialog_outType" name="outType" value="2">
				<input id="dialog_auditorState" name="auditorState" value="1">
				<input id="dialog_isHistory" name="isHistory" value="1">
				<input id="dialog_isChange" name="isChange" value="1">
				
				<input name="id">
				<input name="delFlag">
				<input name="companyId">
				<input name="orgId">
				<input name="createBy">
				<input name="createDate">
				<input name="updateBy">
				<input name="updateDate">
				<input name="uuid">
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="saveRawmaterialOut();" style="width:90px">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">取消</a>
	</div>
	
	<script type="text/javascript">
	// 模糊查询用户信息，使用分页
	var findUserByParamUrl = "${pageScope.mvcPath}/sys/findUserByParam";
	// 查询
	var findAuditRawmaterialOutByPageUrl = "${pageScope.mvcPath}/erp/findAuditRawmaterialOutByPage";
	// 审核通过
	var auditResultPassSaveUrl = "${pageScope.mvcPath}/erp/rawmaterialOut/auditResultPass";
	// 审核驳回
	var auditResultRejectSaveUrl = "${pageScope.mvcPath}/erp/rawmaterialOut/auditResultReject";
	
	// 流程审核状态 - 字典
	var findAuditorStateDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findAuditorStateDictUrl += encodeURIComponent("流程审核状态");
	var auditorStateDictData = null;
	$.ajax({
		url : findAuditorStateDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			auditorStateDictData = data;
		}
	});
	
	// 是否开发票 - 字典
	var findIsInvoiceDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findIsInvoiceDictUrl += encodeURIComponent("是否开发票");
	var IsInvoiceDictData = null;
	$.ajax({
		url : findIsInvoiceDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			IsInvoiceDictData = data;
		}
	});
	
	// 出库类型 - 字典
	var findOutTypeDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findOutTypeDictUrl += encodeURIComponent("原料出库-出库类型");
	var OutTypeData = null;
	$.ajax({
		url : findOutTypeDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			OutTypeData = data;
		}
	});
	
	$(document).ready(function () {
		// 设置rawmaterialOutData数据显示表格
		$("#rawmaterialOutData").datagrid({
			url : findAuditRawmaterialOutByPageUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
				param.outType = "2";
			}
		});
		
		$("#search_auditorState").combobox({
			required : false,
			editable : true,
			panelHeight : "auto",
			valueField : "value",
			textField : "text",
		});
		$("#search_auditorState").combobox("loadData", auditorStateDictData);
		
		$("#dlg").dialog({
			title : "原料出库申请单-外发加工",
			closed : true,
			resizable : false,
			minWidth : 765,
			minHeight : 390,
			maxWidth : 765,
			maxHeight : 390,
			modal : true,
			buttons : "#dlg-buttons"
		});
		
		$("#dialog_outNo").textbox({
			required : false,
			readonly : true
		});
		
		$("#dialog_outDate").datebox({    
			required : true,
			readonly : true
		}); 
		
		$("#dialog_orderNo").textbox({
			required : false,
			readonly : true
		});
		
		$("#dialog_rawmaterialCode").textbox({
			required : true,
			readonly : true
		});
		
		$("#dialog_name").textbox({
			required : false,
			readonly : true
		}); 
		
		$("#dialog_specification").textbox({
			required : false,
			readonly : true
		});
		
		$("#dialog_rawmaterialType").textbox({
			required : false,
			readonly : true
		});
		
		$("#dialog_colorNo").textbox({
			required : false,
			readonly : true
		});
		
		$("#dialog_quantity").numberbox({
			required : true,
			readonly : true,
			precision : 2,
			min : 0,
			prompt : "单位：米",
			suffix : "米"
		});

		$("#dialog_auditorId").combobox({
			required : true,
			prompt : "输入用户名或登登录名检索",
			width : 190,
			editable : true,
			delay : 200,
			panelHeight : 150,
			mode : "remote",
			url : findUserByParamUrl,
			valueField : "value",
			textField : "text",
			formatter: function(row) {
				var div = $("<div/>");
				// 用户编号：jobNo；登录名称：loginName
				var tmp = $("<span style='color:#080'/>").html(row.object.jobNo);
				div.append(tmp);
				div.append("<br>");
				tmp = $("<span style='color:#008'/>").html(row.text + "(" + row.object.loginName + ")");
				div.append(tmp);
				return div.html();
			},
			onHidePanel : function() {
				var value = $("#dialog_auditorId").combobox("getValue");
				var text = $("#dialog_auditorId").combobox("getText");
				var data = $("#dialog_auditorId").combobox("getData");
				var flag = false;
				$(data).each(function (index, row){
					if(value == row.value && text == row.text) {
						flag = true;
						return false;
					}
				});
				if(flag == false) {
					$("#dialog_auditorId").combobox("clear");
				}
			}
		});
		
		$("#dialog_remarks").textbox({
			validType : 'length[3,255]',
			multiline:true
		});
		
		$("#dialog_unitPrice").numberbox({
			required : true,
			min : 0,
			precision : 2,
			prefix : "￥",
			//suffix : "元/米",
			prompt : "单位：元/米",
			onChange : function(newValue, oldValue) {
				var unitPrice = parseFloat(newValue);
				if($.isNumeric(unitPrice) && unitPrice > 0) {
					var quantity = $("#dialog_quantity").numberbox("getValue");
					quantity = parseFloat(quantity);
					if($.isNumeric(quantity) && quantity <= 0) {
						$.messager.alert("提示", "出库数量的数据类型错误，不是数字！", "warning");
					} else {
						$("#dialog_aggregateAmount").numberbox("setValue",(quantity * unitPrice));
					}
				} else {
					$("#dialog_aggregateAmount").numberbox("clear");
				}
			}
		});
		
		$("#dialog_aggregateAmount").numberbox({
			required : true,
			readonly : true,
			min : 0,
			precision : 2,
			prefix : "￥",
			//suffix : "元",
			prompt : "单位：元"
		});
		
		$("#dialog_isInvoice").combobox({
			url : findIsInvoiceDictUrl,
			editable : false,
			panelHeight : "auto",
			required : true
		});
	});
	
	// 表单初始化
	function initForm() {
		// 是否先下订单后原料出库 禁用
		$("#dialog_firstOrderByYes").removeAttr("disabled");
		$("#dialog_firstOrderByNo").removeAttr("disabled");
		if($("#dialog_firstOrderByYes").prop("checked") == true) {
			$("#dialog_firstOrderByNo").attr("disabled","disabled");
		}
		if($("#dialog_firstOrderByNo").prop("checked") == true) {
			$("#dialog_firstOrderByYes").attr("disabled","disabled");
		}
		
		// 默认审核通过
		$("#dialog_auditResultPass").prop("checked", true);
		
		// 原料单价默认为空
		var unitPrice = $("#dialog_unitPrice").numberbox("getValue");
		unitPrice = parseFloat(unitPrice);
		if($.isNumeric(unitPrice) && unitPrice <= 0) {
			$("#dialog_unitPrice").numberbox("clear");
		}
		// 应付金额默认为空
		var aggregateAmount = $("#dialog_aggregateAmount").numberbox("getValue");
		aggregateAmount = parseFloat(aggregateAmount);
		if($.isNumeric(aggregateAmount) && aggregateAmount <= 0) {
			$("#dialog_aggregateAmount").numberbox("clear");
		}
	}
	
	// 清空查询条件
	function clearSearchForm() {
		$("#fm-search").form("clear");
	}
	
	// 查询
	function searchRawmaterialOut() {
		$("#rawmaterialOutData").datagrid("load");
	}

	// 审核
	function audit(rowId) {
		var row = null;
		if(typeof(rowId) == "undefined") {
			row = $('#rawmaterialOutData').datagrid('getSelected');
		} else if($.trim(rowId) != "") {
			var data = $("#rawmaterialOutData").datagrid("getData");
			$(data.rows).each(function(index, r){
				if(r.id == rowId) {
					row = r;
					return false;
				}
			});
		}
		
		if (row == null) {
			$.messager.alert("提示", "未选择要审核的数据！", "info");
			return;
		}
		if (row) {
			$("#dlg").dialog('open').dialog('setTitle', '原料出库申请单-外发加工-审核');
			$('#fm').form('load', dataRowConvert(row));
			
			// 设置下拉列表的值
			$("#dialog_auditorId").combobox("setValue", row.create_by);
			$("#dialog_auditorId").combobox("setText", row.createName);
			
			initForm();
		}
	}
	
	// 保存
	function saveRawmaterialOut() {
		var url = null;
		if($("#dialog_auditResultPass").prop("checked") == true){
			url = auditResultPassSaveUrl;
		}
		if($("#dialog_auditResultReject").prop("checked") == true){
			url = auditResultRejectSaveUrl;
		}
		if(url == null) {
			$.messager.alert("提示", "请选择审核结果：通过或驳回", "info");
			return;
		}
		
		$("#fm").form("submit", {
			url : url,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({ title : '提示', msg : data.message, timeout : 5000, showType : 'slide' });
					$("#rawmaterialOutData").datagrid('reload');
				} else {
					// 保存失败
					$.messager.alert("提示", data.message, "warning");
				}
			}
		});
	}
	
	// 数据转换
	function dataRowConvert(row)
	{
		if(row == null) {
			return null;
		}
		var newRow = {};
		newRow.outNo = row.out_no;
		newRow.outDate = new Date(row.out_date).format("yyyy-MM-dd hh:mm:ss");
        newRow.outType = row.out_type;
        newRow.orderNo = row.order_no;
        newRow.isFirstOrder = row.is_first_order;
		newRow.rawmaterialCode = row.rawmaterial_code;
		newRow.name = row.name;
		newRow.specification = row.specification;
		newRow.rawmaterialType = row.rawmaterial_type;
		newRow.colorNo = row.color_no;
		newRow.quantity = row.quantity;
		newRow.remarks = row.remarks;
		newRow.auditorId = row.auditor_id;
		newRow.auditorState = row.auditor_state;
		newRow.unitPrice = row.unit_price;
		newRow.aggregateAmount = row.aggregate_amount;
		newRow.isInvoice = row.is_invoice;
		newRow.isHistory = row.is_history;
		newRow.id = row.id;
		newRow.delFlag = row.del_flag;
		newRow.companyId = row.company_id;
		newRow.orgId = row.org_id;
		newRow.createBy = row.create_by;
		newRow.createDate = new Date(row.create_date).format("yyyy-MM-dd hh:mm:ss");
		newRow.updateBy = row.update_by;
		newRow.updateDate = new Date(row.update_date).format("yyyy-MM-dd hh:mm:ss");
		newRow.uuid = row.uuid;
		return newRow;
	}
	
	function formatOutTypeData(value,row,index) {
		var text = null;
		$(OutTypeData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatIsInvoice(value,row,index) {
		var text = null;
		$(IsInvoiceDictData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatAuditorState(value,row,index) {
		var text = null;
		$(auditorStateDictData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatOperate(value,row,index) {
		var div = $("<div />");
		var tmp = null;
		if(row.auditor_state == "1" || row.auditor_state == "2") {
			tmp = $("<a href='javascript:void(0)' onclick='audit(\"" + row.id + "\");' />");
			tmp.html("审核");
			div.append(tmp);
		}
		return div.html();
	}
	
    function formatDate(value,row,index) {
    	var date = new Date(value);
    	return date.format("yyyy-MM-dd");
    }
	
    // 日期格式化
    Date.prototype.format = function (fmt) {
    	var o = {
    		"M+" : this.getMonth() + 1, //月份
    		"d+" : this.getDate(), //日
    		"h+" : this.getHours(), //小时
    		"m+" : this.getMinutes(), //分
    		"s+" : this.getSeconds(), //秒
    		"q+" : Math.floor((this.getMonth() + 3) / 3), //季度
    		"S" : this.getMilliseconds() //毫秒
    	};
    	if (/(y+)/.test(fmt)){
    		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    	}
    	for (var k in o) {
    		if (new RegExp("(" + k + ")").test(fmt)) {
    			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    		}
    	}
    	return fmt;
    }
	</script>
</body>
</html>