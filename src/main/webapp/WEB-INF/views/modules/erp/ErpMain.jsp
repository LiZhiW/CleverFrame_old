<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>广州航达纺织ERP系统</title>
<style type="text/css">
html, body{
	margin:0;padding:0;border:0;width:100%;height:100%;overflow:hidden;
}
.logo
{
    font-family:"微软雅黑",	"Helvetica Neue",​Helvetica,​Arial,​sans-serif;
    font-size:22px;
    font-weight:bold;
    color:#444;        
    cursor:default;
    position:absolute;
    top:10px;
    left:15px; 
    line-height:28px;       
}    
.topNav
{
    position:absolute;right:8px;top:10px;        
    font-size:12px;
    line-height:25px;
}
.topNav a
{
    text-decoration:none;
    color:#222;
    font-weight:normal;
    font-size:12px;
    line-height:25px;
    margin-left:3px;
    margin-right:3px;
}
.topNav a:hover
{
    text-decoration:underline;
    color:blue;
}
</style>
</head>
<body id="mainPanel" class="easyui-layout">

	<!-- 页面上部 -->
    <div data-options="region:'north',border:false" >
		<div class="tabs-header" style="overflow: hidden; height: 50px;">
			<!-- 广州航达纺织ERP系统 -->
			<div class="logo"></div>
			<div class="topNav">
				当前用户：<a href="javascript:void(0)"><span id="currentUser"></span></a> |
				<a href="javascript:void(0)">个人主页</a> |
				<a href="javascript:void(0)">系统消息(0)</a> |
				<a href="javascript:void(0)"><span id="erpToDo">代办(0)</span></a> |
				<a href="${pageScope.mvcPath}/sys/logout">安全退出</a>
			</div>
		</div>
    </div>

	<!-- 页面左部 -->
    <div data-options="region:'west',title:'功能菜单',split:true,border:true" style="width:200px;">
    	<ul id="menuTree"></ul>
    </div>

    <!-- 页面中部 -->
    <div data-options="region:'center',border:true,minWidth:800,minHeight:300">
	    <div id="tabsCenter" class="easyui-tabs" data-options="fit:true,border:'false',tools:'#tabsCenter-tools'">
			<div title="个人主页" data-options="border:'false'">

		    </div>
	    </div>
		<div id="tabsCenter-tools">
			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reload'" onclick="refreshTab()">刷新</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'ExpandIcon-cross'" onclick="closeTab()">关闭</a>
		</div>
	</div>
	
    <!-- 页面底部 -->
    <div data-options="region:'south',border:false" align="center" class="tabs-header" style="font-size: 12px;">
    	请使用最新的 
    	<a href="javascript:void(0)">Chrome</a> /
    	<a href="javascript:void(0)">Firefox</a> /
    	<a href="javascript:void(0)">Safari</a>
    	浏览器&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;2015版权所有&copy;
    </div>
    
	<!-- 多页签右键菜单 -->
	<div id="menuByTabs" class="easyui-menu">
		<div data-options="name:'refresh',iconCls:'icon-reload'">刷新</div>
		<div data-options="name:'openInBrowserNewTab'">在浏览器新窗口打开</div>
		<div class="menu-sep"></div>
		<div data-options="name:'close'">关闭</div>
		<div data-options="name:'closeOther'">关闭其他</div>
		<div data-options="name:'closeAll'">关闭全部</div>
	</div>
	
	<script type="text/javascript">
	// 获取菜单信息地址
	var menuUrl = "${pageScope.mvcPath}/sys/findMenuByCurrentUserAndCategory?category=";
	menuUrl += encodeURIComponent("ERP模块菜单");
	// 获取代办信息
	var getErpToDoUrl = "${pageScope.mvcPath}/erp/getErpToDo";
	// 获取当前登录的用户
	var getCurrentUserUrl = "${pageScope.mvcPath}/sys/getCurrentUser";
	
	$(document).ready(function() {
		// 页面左部，菜单区域
		$("#mainPanel").layout("panel", "west").panel({
			region : "west",
			width : 200,
			title : "功能菜单",
			border : true,
			minWidth : 150,
			maxWidth : 300,
			split : true,
			tools : [ {
				iconCls : "icon-reload",
				handler : function() {
					$("#menuTree").tree("reload");
				}
			} ]
		});

		// 构造菜单树
		$("#menuTree").tree({
			url : menuUrl,
			animate : false,
			checkbox : false,
			cascadeCheck : true,
			onlyLeafCheck : false,
			lines : true,
			dnd : false,
			onClick : function(node){
				if(node.attributes.href.indexOf("/") != 0)
				{
					return ;
				}
				var url = "${pageScope.appPath}" + node.attributes.href;
				addTab(node.text,url);
			}
		});
		
		// 菜单设置
		$('#menuByTabs').menu({
			onClick : function (item) {
				var selectIndex = $(this).data("selectIndex");
				var tablist = $('#tabsCenter').tabs('tabs');
				if(item.name == "close"){
					$('#tabsCenter').tabs('close', selectIndex);
				} else if(item.name == "closeOther"){
					for (var i = tablist.length-1; i >= 0; i--) {
						if( i == selectIndex){
							continue;
						}
						$('#tabsCenter').tabs('close', i);
					}
				} else if(item.name == "closeAll") {
					for (var i = tablist.length-1; i >= 0; i--) {
						$('#tabsCenter').tabs('close', i);
					}
				} else if(item.name == "refresh") {
					refreshTab();
				} else if("openInBrowserNewTab" == item.name) {
					openInBrowserNewTab();
				}
			}
		});
		
		// 页签集合
		$("#tabsCenter").tabs({
			fit : true,
			border : 'false',
			tools : '#tabsCenter-tools',
			onContextMenu : function (e, title, index) {
				e.preventDefault();
				if (index >= 0) {
					$("#tabsCenter").tabs("select",index);
					$('#menuByTabs').menu('show', {
						left : e.pageX - 3,
						top : e.pageY -3
					}).data("selectIndex", index);
				}
			}
		});
		
		getCurrentUser();
		// 不断重复调用刷新代办信息
		getErpToDo();
		setInterval("getErpToDo()", 1000 * 30);
	});
	
	//以iframe方式增加页面
	function addTab(tabName, tabUrl) {
		if ($("#tabsCenter").tabs("exists", tabName)) {
			$("#tabsCenter").tabs("select", tabName);
		} else {
			if (tabUrl) {
				var id = "id" + Math.random();
				id = id.replace(".","");
				var content = "<iframe id='" + id + "' scrolling='auto' style='width:100%;height:100%;' frameborder='0' src='" + tabUrl + "'></iframe>";
			} else {
				var content = "未定义页面路径！";
			}
			$("#tabsCenter").tabs("add", {
				title : tabName,
				closable : true,
				content : content,
				tools : [{
                       iconCls : "icon-mini-refresh",
                       handler : function(){
                       	if(id){
                       		document.getElementById(id).contentWindow.location.reload(true);
                       	}
                       }
                   }]
			});
		}
	}
	
	// 刷新页面
	function refreshTab() {
		var tab = $('#tabsCenter').tabs('getSelected');
		if (tab) {
			var content = tab.panel('options')['content'];
			if(content){
				content = $('<div></div>').html(content);
				var id = content.find("iframe").attr("id");
				document.getElementById(id).contentWindow.location.reload(true);
				content.remove();
			}
		}
	}
	
	// 在浏览器新窗口中打开
	function openInBrowserNewTab() {
		var tab = $('#tabsCenter').tabs('getSelected');
		if (tab) {
			var content = tab.panel('options')['content'];
			if(content){
				content = $('<div></div>').html(content);
				var url = content.find("iframe").attr("src");
				content.remove();
				window.open(url);
			}
		}
	}
	
	// 关闭页面
	function closeTab() {
		var tab = $('#tabsCenter').tabs('getSelected');
		if (tab) {
			var index = $('#tabsCenter').tabs('getTabIndex', tab);
			$('#tabsCenter').tabs('close', index);
		}
	}
	
	// 获取代办信息
	function getErpToDo() {
		$.ajax({
			url : getErpToDoUrl,
			dataType : "json",
			type : "POST",
			success : function (data) {
				if(data.message != "0") {
					$("#erpToDo").css("color", "red");
					$("#erpToDo").css("font-weight", "bold");
				} else {
					$("#erpToDo").css("color", "black");
					$("#erpToDo").css("font-weight", "normal");
				}
				$("#erpToDo").html("代办("+ data.message +")");
				//console.log(data);
			}
		});
	}
	
	// 获取当前登录的用户
	function getCurrentUser(){
		$.ajax({
			url : getCurrentUserUrl,
			dataType : "json",
			type : "POST",
			success : function (data) {
				$("#currentUser").html(data.object.name);
				// console.log(data);
			}
		});
	}
	</script>
</body>
</html>













