<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>A生产订单-业务登记</title>
<style type="text/css">
.row {
	margin-bottom: 8px;
}
.row label {
	display: inline-block;
	width: 75px;
	text-align: center;
}
.row input,select {
	width: 160px;

}
.row .column {
	margin-right: 10px;
}
.row .columnLast {
	margin-right: 0px;
}
.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:100px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="search_customer">客户</label> 
					<input id="search_customer" name="customer" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="search_orderNo">批号</label> 
					<input id="search_orderNo" name="orderNo" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="search_name">名称</label> 
					<input id="search_name" name="name" class="easyui-textbox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="search_specification">规格</label> 
					<input id="search_specification" name="specification" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="search_orderType">订单类型</label> 
					<input id="search_orderType" name="orderType" class="easyui-combobox">
				</span>
				<span class="columnLast">
					<label for="search_auditorState">订单状态</label> 
					<input id="search_auditorState" name="auditorState" class="easyui-combobox">
				</span>
			</div>
			
			<div class="row">
				<span class="columnLast">
					<label for="search_startDate">登记日期</label>
					<input id="search_startDate" name="startDate" class="easyui-datebox">
				</span>
				<span class="columnLast">
					<label for="search_endDate" style="width: 20px;">至</label> 
					<input id="search_endDate" name="endDate" class="easyui-datebox">
				</span>
			</div>
		</form>
	</div>
	
	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="orderData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'check',checkbox:true,hidden:true">选择</th>
					<th data-options="field:'order_code',width:80,align:'left'">NO</th>
					<th data-options="field:'customer',width:80,align:'left'">客户</th>
					<th data-options="field:'order_no',width:80,align:'left'">批号</th>
					<th data-options="field:'name',width:80,align:'left'">名称</th>
					<th data-options="field:'specification',width:80,align:'left'">规格</th>
					<th data-options="field:'order_quantity',width:80,align:'left'">订单数量</th>
					<th data-options="field:'delivery_date',width:80,align:'left',formatter:formatDate">交货日期</th>
					<th data-options="field:'delivery_claim',width:80,align:'left'">交货要求</th>
					<th data-options="field:'order_date',width:80,align:'left',formatter:formatDate">登记日期</th>
					<th data-options="field:'order_type',width:80,align:'left',formatter:formatOrderType">订单类型</th>
					<th data-options="field:'auditor_state',width:80,align:'left',formatter:formatAuditorState">订单状态</th>
					<th data-options="field:'auditorName',width:80,align:'left'">审核人</th>
					<th data-options="field:'auditor_id',width:80,align:'left',hidden:true">审核人ID</th>
					
					<th data-options="field:'cycling',width:80,align:'left',hidden:true">循环</th>
					<th data-options="field:'real_quantity',width:80,align:'left',hidden:true">实产数量</th>
					<th data-options="field:'is_brush',width:80,align:'left',hidden:true">是否刷毛</th>
					<th data-options="field:'tabulation_people',width:80,align:'left',hidden:true">制表人</th>
					<th data-options="field:'yarn_audit',width:80,align:'left',hidden:true">排纱审核</th>
					<th data-options="field:'workshop_class',width:80,align:'left',hidden:true">车间班长</th>
					<th data-options="field:'auditor',width:80,align:'left',hidden:true">审核</th>
					<th data-options="field:'has_contracts',width:80,align:'left',hidden:true">是否有合同</th>
					<th data-options="field:'order_enrollment',width:80,align:'left',hidden:true">订单登记</th>
					<th data-options="field:'color_no',width:80,align:'left',hidden:true">色号</th>
					<th data-options="field:'white_gauze',width:80,align:'left',hidden:true">白纱</th>
					<th data-options="field:'colored_yarn',width:80,align:'left',hidden:true">色纱</th>
					<th data-options="field:'yarn_type',width:80,align:'left',hidden:true">纱种</th>
					<th data-options="field:'yarn_type_no',width:80,align:'left',hidden:true">原料纱种批号</th>
					<th data-options="field:'is_first_order',width:80,align:'left',hidden:true">是否先下订单后原料出库</th>
					<th data-options="field:'stereotypes_width',width:80,align:'left',hidden:true">定型幅宽</th>
					<th data-options="field:'stereotypes_freeness',width:80,align:'left',hidden:true">定型浆度</th>
					<th data-options="field:'director_audit',width:80,align:'left',hidden:true">厂长审核</th>
					<th data-options="field:'production',width:80,align:'left',hidden:true">生产部</th>
					<th data-options="field:'material',width:80,align:'left',hidden:true">材料</th>
					<th data-options="field:'percentage',width:80,align:'left',hidden:true">百分比</th>
					<th data-options="field:'quantity',width:80,align:'left',hidden:true">数量(公斤)</th>
					<th data-options="field:'is_history',width:80,align:'left',hidden:true">是否是历史记录</th>
					<th data-options="field:'is_change',width:80,align:'left',hidden:true">是否变更</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'company_id',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'org_id',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'create_by',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'create_date',width:100,align:'left',hidden:true">申请时间</th>
					<th data-options="field:'update_by',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'update_date',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:150,align:'left'">备注信息</th>
					<th data-options="field:'del_flag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
					<th data-options="field:'operate',width:100,align:'left',formatter:formatOperate">操作</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchOrder();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="clearSearchForm();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-edit-clear-2',plain:true">清空查询条件</a>
			<a onclick="newOrder();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editOrder();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">修改</a>
	    </div>
	</div>
	<!-- 新增/编辑对话框 -->
	<div id="dlg" style="width: 810px; height: 530px; padding: 5px 10px">
		<div class="ftitle" style="text-align: center;">
			广州市航达纺织品有限公司
			<span id="dialog_div_orderCode" style="margin-left: 15px;">NO20150910</span>
		</div>
		<form id="fm" method="post">
			<div class="row" style="margin-top: 10px;">
				<span class="column">
					<label for="dialog_orderType">订单类型</label>
					<input id="dialog_orderType" name="orderType" class="easyui-combobox">
				</span>
				<span class="column">
					<label for="dialog_orderDate">登记日期</label>
					<input id="dialog_orderDate" name="orderDate" class="easyui-datebox">
				</span>
				<span class="columnLast">
					<label for="dialog_customer">客户</label> 
					<input id="dialog_customer" name="customer" class="easyui-textbox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_orderNo">批号</label>
					<input id="dialog_orderNo" name="orderNo" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_name">名称</label>
					<input id="dialog_name" name="name" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_specification">规则</label> 
					<input id="dialog_specification" name="specification" class="easyui-textbox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_orderQuantity">订单数量</label>
					<input id="dialog_orderQuantity" name="orderQuantity" class="easyui-numberbox">
				</span>
				<span class="column">
					<label for="dialog_cycling">循环</label>
					<input id="dialog_cycling" name="cycling" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_realQuantity">实产数量</label> 
					<input id="dialog_realQuantity" name="realQuantity" class="easyui-textbox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_isBrush">是否刷毛</label>
					<input id="dialog_isBrush" name="isBrush" class="easyui-combobox">
				</span>
				<span class="column">
					<label for="dialog_colorNo">色号</label>
					<input id="dialog_colorNo" name="colorNo" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_whiteGauze">白纱</label> 
					<input id="dialog_whiteGauze" name="whiteGauze" class="easyui-textbox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_yarnType">纱种</label>
					<input id="dialog_yarnType" name="yarnType" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_coloredYarn">色纱</label>
					<input id="dialog_coloredYarn" name="coloredYarn" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_stereotypesWidth">定型幅宽</label> 
					<input id="dialog_stereotypesWidth" name="stereotypesWidth" class="easyui-textbox">
				</span>
			</div>
			
			<div class="row">
				<span class="columnLast"> 
					<label for="dialog_remarks">备注信息</label> 
					<input id="dialog_remarks" name="remarks" style="width:685px;" class="easyui-textbox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_deliveryDate">交货日期</label>
					<input id="dialog_deliveryDate" name="deliveryDate" class="easyui-datebox">
				</span>
				<span class="column">
					<label for="dialog_stereotypesFreeness">定型浆度</label>
					<input id="dialog_stereotypesFreeness" name="stereotypesFreeness" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_tabulationPeople">制表</label> 
					<input id="dialog_tabulationPeople" name="tabulationPeople" class="easyui-combobox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_yarnAudit">排纱审核</label>
					<input id="dialog_yarnAudit" name="yarnAudit" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_workshopClass">车间班长</label>
					<input id="dialog_workshopClass" name="workshopClass" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_auditor">审核</label> 
					<input id="dialog_auditor" name="auditor" class="easyui-textbox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_yarnTypeNo">原料纱种批号</label>
					<input id="dialog_yarnTypeNo" name="yarnTypeNo" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_deliveryClaim">交货要求</label>
					<input id="dialog_deliveryClaim" name="deliveryClaim" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_hasContracts">是否有合同</label> 
					<input id="dialog_hasContracts" name="hasContracts" class="easyui-combobox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_orderEnrollment">订单登记</label>
					<input id="dialog_orderEnrollment" name="orderEnrollment" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_directorAudit">厂长审核</label>
					<input id="dialog_directorAudit" name="directorAudit" class="easyui-combobox">
				</span>
				<span class="columnLast">
					<label for="dialog_businessAudit">业务审核</label> 
					<input id="dialog_businessAudit" name="businessAudit" class="easyui-combobox">
				</span>
			</div>
			
			<div class="row">
				<span class="columnLast">
					<label for="dialog_isFirstOrder">是否先下订单后原料出库</label> 
					<input id="dialog_isFirstOrder" name="isFirstOrder" class="easyui-textbox">
				</span>
			</div>
			
			<div class="ftitle">后续流程：</div>
			<div class="row">
				<span class="columnLast">
					<label for="dialog_auditorId" style="width: 200px;">订单业务登记完成，发送工艺登记</label>
					<input id="dialog_auditorId" name="auditorId" class="easyui-combobox">
				</span>
			</div>
			
			<!-- 隐藏属性值 -->
			<div style="display: none;">
				<input id="dialog_orderCode" name="orderCode">
				<input id="dialog_auditorState" name="auditorState" value="2">
				<input id="dialog_isHistory" name="isHistory" value="1">
				<input id="dialog_isChange" name="isChange" value="1">
				
				<input name="id">
				<input name="delFlag">
				<input name="companyId">
				<input name="orgId">
				<input name="createBy">
				<input name="createDate">
				<input name="updateBy">
				<input name="updateDate">
				<input name="uuid">
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="saveOrder();" style="width:90px">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">取消</a>
	</div>
	
	<script type="text/javascript">
	// 获取当前登录的用户
	var getCurrentUserUrl = "${pageScope.mvcPath}/sys/getCurrentUser";
	// 模糊查询用户信息，使用分页
	var findUserByParamUrl = "${pageScope.mvcPath}/sys/findUserByParam";
	// 查询
	var findOrderByPageUrl = "${pageScope.mvcPath}/erp/findOrderByPage";
	// 新增
	var addOrderUrl = "${pageScope.mvcPath}/erp/addOrder";
	// 更新
	var updateOrderUrl = "${pageScope.mvcPath}/erp/updateOrder";
	// 保存
	var orderSaveUrl = null;
	// 删除
	var deleteOrderUrl = "${pageScope.mvcPath}/erp/deleteOrder";
	// 变更
	var changeOrderUrl = "${pageScope.mvcPath}/erp/changeOrder";
	// 历史数据
	var getOrderByNoUrl = "${pageScope.mvcPath}/erp/getOrderByNo";
	// 默认审核人
	var getAuditorByOrderUrl = "${pageScope.mvcPath}/erp/getAuditorByOrder";

	// 获取当前用户
	var CurrentUser = null;
	$.ajax({
		url : getCurrentUserUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			CurrentUser = data.object;
		}
	});
	
	// A订单类型 - 字典
	var findAOrderTypeDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findAOrderTypeDictUrl += encodeURIComponent("A订单类型");
	var AOrderTypeData = null;
	$.ajax({
		url : findAOrderTypeDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			AOrderTypeData = data;
		}
	});

	// 流程审核状态 - 字典
	var findAuditorStateDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findAuditorStateDictUrl += encodeURIComponent("订单状态");
	var auditorStateDictData = null;
	$.ajax({
		url : findAuditorStateDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			auditorStateDictData = data;
		}
	});
	
	// 是否刷毛 - 字典
	var findIsBrushDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findIsBrushDictUrl += encodeURIComponent("是否刷毛");
	var IsBrushDictData = null;
	$.ajax({
		url : findIsBrushDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			IsBrushDictData = data;
		}
	});
	
	// 是否有合同 - 字典
	var findHasContractsDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findHasContractsDictUrl += encodeURIComponent("是否有合同");
	var HasContractsDictData = null;
	$.ajax({
		url : findHasContractsDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			HasContractsDictData = data;
		}
	});
	
	// 是否先下订单后原料出库 - 字典
	var findIsFirstOrderDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findIsFirstOrderDictUrl += encodeURIComponent("是否先下订单后原料出库");
	var IsFirstOrderDictData = null;
	$.ajax({
		url : findIsFirstOrderDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			IsFirstOrderDictData = data;
		}
	});
	
	$(document).ready(function () {
		// 订单类型
		$("#search_orderType").combobox({
			required : false,
			editable : true,
			panelHeight : "auto",
			valueField : "value",
			textField : "text",
		});
		$("#search_orderType").combobox("loadData", AOrderTypeData);
		
		// 订单状态
		$("#search_auditorState").combobox({
			required : false,
			editable : true,
			panelHeight : "auto",
			valueField : "value",
			textField : "text",
		});
		$("#search_auditorState").combobox("loadData", auditorStateDictData);
		
		$("#orderData").datagrid({
			url : findOrderByPageUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				editOrder();
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			}
		});
		
		$("#dlg").dialog({
			title : "A生产订单-业务登记",
			closed : true,
			resizable : false,
			minWidth : 810,
			minHeight : 530,
			maxWidth : 810,
			maxHeight : 530,
			modal : true,
			buttons : "#dlg-buttons"
		});
		
		$("#dialog_orderType").combobox({
			required : true,
			editable : false,
			panelHeight : "auto",
			valueField : "value",
			textField : "text",
		});
		$("#dialog_orderType").combobox("loadData", AOrderTypeData);
		
		$("#dialog_orderDate").datebox({    
			required : true
		});
		
		$("#dialog_customer").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_orderNo").textbox({
			required : false,
			readonly : true,
			prompt : "批号自动生成",
			validType : 'length[1,100]'
		});
		
		$("#dialog_name").textbox({
			required : true,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_specification").textbox({
			required : true,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_orderQuantity").numberbox({
			required : true,
			precision : 2,
			min : 0,
			prompt : "单位：米",
			suffix : "米",
		});
		
		$("#dialog_cycling").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_realQuantity").numberbox({
			required : false,
			precision : 2,
			min : 0,
			prompt : "单位：米",
			suffix : "米",
		});
		
		$("#dialog_isBrush").combobox({
			required : false,
			editable : false,
			panelHeight : "auto",
			valueField : "value",
			textField : "text",
		});
		$("#dialog_isBrush").combobox("loadData", IsBrushDictData);
		
		$("#dialog_colorNo").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_whiteGauze").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_yarnType").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_coloredYarn").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_stereotypesWidth").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_remarks").textbox({
			required : true,
			readonly : false,
			validType : 'length[1,2000]'
		});
		
		$("#dialog_stereotypesFreeness").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_deliveryDate").datebox({    
			required : false
		});
		
		$("#dialog_tabulationPeople").combobox({
			required : false,
			prompt : "输入用户名或登登录名检索",
			editable : true,
			delay : 200,
			panelHeight : 150,
			mode : "remote",
			url : findUserByParamUrl,
			valueField : "value",
			textField : "text",
			formatter: function(row) {
				var div = $("<div/>");
				// 用户编号：jobNo；登录名称：loginName
				var tmp = $("<span style='color:#080'/>").html(row.object.jobNo);
				div.append(tmp);
				div.append("<br>");
				tmp = $("<span style='color:#008'/>").html(row.text + "(" + row.object.loginName + ")");
				div.append(tmp);
				return div.html();
			},
			onHidePanel : function() {
				var value = $("#dialog_tabulationPeople").combobox("getValue");
				var text = $("#dialog_tabulationPeople").combobox("getText");
				var data = $("#dialog_tabulationPeople").combobox("getData");
				var flag = false;
				$(data).each(function (index, row){
					if(value == row.value && text == row.text) {
						flag = true;
						return false;
					}
				});
				if(flag == false) {
					$("#dialog_tabulationPeople").combobox("clear");
				}
			}
		});
		
		$("#dialog_yarnAudit").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_workshopClass").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_auditor").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_yarnTypeNo").textbox({
			required : false,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_deliveryClaim").textbox({
			required : true,
			readonly : false,
			validType : 'length[1,100]'
		});
		
		$("#dialog_hasContracts").combobox({
			required : false,
			editable : false,
			panelHeight : "auto",
			valueField : "value",
			textField : "text",
		});
		$("#dialog_hasContracts").combobox("loadData", HasContractsDictData);
		
		// 自动生成当前记录人员的名字
		$("#dialog_orderEnrollment").textbox({
			required : true,
			readonly : true,
			prompt : "当前用户",
			validType : 'length[1,100]'
		});
		
		$("#dialog_directorAudit").combobox({
			required : false,
			prompt : "输入用户名或登登录名检索",
			editable : true,
			delay : 200,
			panelHeight : 150,
			mode : "remote",
			url : findUserByParamUrl,
			valueField : "value",
			textField : "text",
			formatter: function(row) {
				var div = $("<div/>");
				// 用户编号：jobNo；登录名称：loginName
				var tmp = $("<span style='color:#080'/>").html(row.object.jobNo);
				div.append(tmp);
				div.append("<br>");
				tmp = $("<span style='color:#008'/>").html(row.text + "(" + row.object.loginName + ")");
				div.append(tmp);
				return div.html();
			},
			onHidePanel : function() {
				var value = $("#dialog_directorAudit").combobox("getValue");
				var text = $("#dialog_directorAudit").combobox("getText");
				var data = $("#dialog_directorAudit").combobox("getData");
				var flag = false;
				$(data).each(function (index, row){
					if(value == row.value && text == row.text) {
						flag = true;
						return false;
					}
				});
				if(flag == false) {
					$("#dialog_directorAudit").combobox("clear");
				}
			}
		});
		
		$("#dialog_businessAudit").combobox({
			required : false,
			prompt : "输入用户名或登登录名检索",
			editable : true,
			delay : 200,
			panelHeight : 150,
			mode : "remote",
			url : findUserByParamUrl,
			valueField : "value",
			textField : "text",
			formatter: function(row) {
				var div = $("<div/>");
				// 用户编号：jobNo；登录名称：loginName
				var tmp = $("<span style='color:#080'/>").html(row.object.jobNo);
				div.append(tmp);
				div.append("<br>");
				tmp = $("<span style='color:#008'/>").html(row.text + "(" + row.object.loginName + ")");
				div.append(tmp);
				return div.html();
			},
			onHidePanel : function() {
				var value = $("#dialog_businessAudit").combobox("getValue");
				var text = $("#dialog_businessAudit").combobox("getText");
				var data = $("#dialog_businessAudit").combobox("getData");
				var flag = false;
				$(data).each(function (index, row){
					if(value == row.value && text == row.text) {
						flag = true;
						return false;
					}
				});
				if(flag == false) {
					$("#dialog_businessAudit").combobox("clear");
				}
			}
		});
		
		$("#dialog_isFirstOrder").combobox({
			required : false,
			editable : false,
			panelHeight : "auto",
			valueField : "value",
			textField : "text",
		});
		$("#dialog_isFirstOrder").combobox("loadData", IsFirstOrderDictData);
		
		$("#dialog_auditorId").combobox({
			required : true,
			prompt : "输入用户名或登登录名检索",
			editable : true,
			delay : 200,
			panelHeight : 150,
			mode : "remote",
			url : findUserByParamUrl,
			valueField : "value",
			textField : "text",
			formatter: function(row) {
				var div = $("<div/>");
				// 用户编号：jobNo；登录名称：loginName
				var tmp = $("<span style='color:#080'/>").html(row.object.jobNo);
				div.append(tmp);
				div.append("<br>");
				tmp = $("<span style='color:#008'/>").html(row.text + "(" + row.object.loginName + ")");
				div.append(tmp);
				return div.html();
			},
			onHidePanel : function() {
				var value = $("#dialog_auditorId").combobox("getValue");
				var text = $("#dialog_auditorId").combobox("getText");
				var data = $("#dialog_auditorId").combobox("getData");
				var flag = false;
				$(data).each(function (index, row){
					if(value == row.value && text == row.text) {
						flag = true;
						return false;
					}
				});
				if(flag == false) {
					$("#dialog_auditorId").combobox("clear");
				}
			}
		});
		
		
	});
	
	// 表单初始化
	function initForm() {
		$("#dialog_orderDate").datebox("setValue", new Date().format("yyyy-MM-dd"));
		if(CurrentUser != null) {
			$("#dialog_orderEnrollment").textbox("setValue", CurrentUser.id);
			$("#dialog_orderEnrollment").textbox("setText", CurrentUser.name);
		}
	}
	
	// 清空查询条件
	function clearSearchForm() {
		$("#fm-search").form("clear");
	}
	
	// 查询
	function searchOrder() {
		$("#orderData").datagrid('load');
	}
	
	// 新建
	function newOrder() {
		orderSaveUrl = addOrderUrl;
		$("#dlg").dialog('open').dialog('setTitle', 'A生产订单-业务登记[新增]');
		$("#fm").form('reset');
		initForm();
	}
	
	// 编辑
	function editOrder() {
		orderSaveUrl = updateOrderUrl;
		var row = $("#orderData").datagrid("getSelected");
		if (row == null) {
			$.messager.alert("提示", "未选择要编辑的数据！", "info");
			return;
		}
		if (row) {
			if(row.auditor_state != "1" && row.auditor_state != "2" && row.auditor_state != "5") {
				$.messager.alert("提示", "审核状态必须是“业务登记”、“工艺登记”、“驳回”才能修改!", "info");
				return;
			}
			$('#dlg').dialog('open').dialog('setTitle', 'A生产订单-业务登记[修改]');
			$('#fm').form('load', dataRowConvert(row));
			// 设置下拉列表的值
			$("#dialog_businessAudit").combobox("setValue", row.business_audit);
			$("#dialog_businessAudit").combobox("setText", row.businessAuditName);
			
			$("#dialog_directorAudit").combobox("setValue", row.director_audit);
			$("#dialog_directorAudit").combobox("setText", row.directorAuditName);
			
			$("#dialog_tabulationPeople").combobox("setValue", row.tabulation_people);
			$("#dialog_tabulationPeople").combobox("setText", row.tabulationPeopleName);
			
			$("#dialog_auditorId").combobox("setValue", row.auditor_id);
			$("#dialog_auditorId").combobox("setText", row.auditorName);
		}
	}
	
	// 保存
	function saveOrder() {
		$("#fm").form("submit", {
			url : orderSaveUrl,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({ title : '提示', msg : data.message, timeout : 5000, showType : 'slide' });
					$("#orderData").datagrid('reload');
				} else {
					// 保存失败
					$.messager.alert("提示", data.message, "warning");
				}
			}
		});
	}
	
	// 数据转换
	function dataRowConvert(row)
	{
		if(row == null) {
			return null;
		}
		var newRow = {};
        
        newRow.orderCode = row.order_code;
        newRow.orderNo = row.order_no;
        newRow.orderType = row.order_type;
        if(row.order_date != null && row.order_date != "") {
        	newRow.orderDate = new Date(row.order_date).format("yyyy-MM-dd hh:mm:ss");
        } else {
        	// newRow.orderDate = "";
        }
        newRow.customer = row.customer;
        newRow.name = row.name;
        newRow.specification = row.specification;
        newRow.orderQuantity = row.order_quantity;
        newRow.cycling = row.cycling;
        newRow.realQuantity = row.real_quantity;
        newRow.isBrush = row.is_brush;
        if(row.delivery_date != null && row.delivery_date != "") {
        	newRow.deliveryDate = new Date(row.delivery_date).format("yyyy-MM-dd hh:mm:ss");
        } else {
        	newRow.deliveryDate = "";
        }
        newRow.deliveryClaim = row.delivery_claim;
        newRow.tabulationPeople = row.tabulation_people;
        newRow.yarnAudit = row.yarn_audit;
        newRow.workshopClass = row.workshop_class;
        newRow.auditor = row.auditor;
        newRow.hasContracts = row.has_contracts;
        newRow.orderEnrollment = row.order_enrollment;
        newRow.colorNo = row.color_no;
        newRow.whiteGauze = row.white_gauze;
        newRow.coloredYarn = row.colored_yarn;
        newRow.yarnType = row.yarn_type;
        newRow.yarnTypeNo = row.yarn_type_no;
        newRow.isFirstOrder = row.is_first_order;
        newRow.stereotypesWidth = row.stereotypes_width;
        newRow.stereotypesFreeness = row.stereotypes_freeness;
        newRow.directorAudit = row.director_audit;
        newRow.businessAudit = row.business_audit;
        newRow.production = row.production;
        newRow.material = row.material;
        newRow.percentage = row.percentage;
        newRow.quantity = row.quantity;
        newRow.auditorId = row.auditorId;
        newRow.auditorState = row.auditor_state;
        newRow.isHistory = row.is_history;
        newRow.isChange = row.is_change;
        
		newRow.id = row.id;
		newRow.delFlag = row.del_flag;
		newRow.companyId = row.company_id;
		newRow.orgId = row.org_id;
		newRow.createBy = row.create_by;
		newRow.createDate = new Date(row.create_date).format("yyyy-MM-dd hh:mm:ss");
		newRow.updateBy = row.update_by;
		newRow.updateDate = new Date(row.update_date).format("yyyy-MM-dd hh:mm:ss");
		newRow.uuid = row.uuid;
        newRow.remarks = row.remarks;
		return newRow;
	}
	
	function formatOrderType(value,row,index) {
		var text = null;
		$(AOrderTypeData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatIsInvoice(value,row,index) {
		var text = null;
		$(IsInvoiceDictData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatAuditorState(value,row,index) {
		var text = null;
		$(auditorStateDictData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatOperate(value,row,index) {
		var div = $("<div />");
		var tmp = null;
		if(row.auditor_state == "3") {
			tmp = $("<a href='javascript:void(0)' onclick='changeRawmaterialWarehouse(\"" + row.id + "\");' />");
			tmp.html("变更");
			div.append(tmp);
		}

		if(row.is_change == "2") {
			if(tmp != null) {
				div.append(" | ");
			}
			
			tmp = $("<a href='javascript:void(0)' onclick='getHistoryData(\"" + row.id + "\");' />");
			tmp.html("历史记录");
			div.append(tmp);
		}
		
		return div.html();
	}
	
    function formatDate(value,row,index) {
    	if(value == "" || value == null) {
    		return "";
    	}
    	var date = new Date(value);
    	return date.format("yyyy-MM-dd");
    }
	
    // 日期格式化
    Date.prototype.format = function (fmt) {
    	var o = {
    		"M+" : this.getMonth() + 1, //月份
    		"d+" : this.getDate(), //日
    		"h+" : this.getHours(), //小时
    		"m+" : this.getMinutes(), //分
    		"s+" : this.getSeconds(), //秒
    		"q+" : Math.floor((this.getMonth() + 3) / 3), //季度
    		"S" : this.getMilliseconds() //毫秒
    	};
    	if (/(y+)/.test(fmt)){
    		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    	}
    	for (var k in o) {
    		if (new RegExp("(" + k + ")").test(fmt)) {
    			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    		}
    	}
    	return fmt;
    }
	</script>
</body>
</html>