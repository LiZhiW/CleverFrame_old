<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>供应商供应原料管理</title>
<style type="text/css">
.row {
	margin-bottom: 8px;
}
.row label {
	display: inline-block;
	width: 120px;
	text-align: center;
}
.row input,select {
	width: 160px;

}
.row .column {
	margin-right: 20px;
}
.row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:130px;">
		<div class="row" style="margin-top: 5px;">
			<span class="column">
				<label for="dialog_supplierName">供应商名称</label> 
				<input id="dialog_supplierName" name="supplierName" class="easyui-combobox">
			</span>
			<span class="column">
				<label for="dialog_supplierNo">供应商编号</label> 
				<input id="dialog_supplierNo" name="supplierNo" class="easyui-textbox" data-options="readonly:true">
			</span>
			<span class="columnLast">
				<label for="dialog_startSupplyDate">供应商开始供应日期</label>
				<input id="dialog_startSupplyDate" name="startSupplyDate" class="easyui-datebox" data-options="readonly:true">
			</span>
		</div>
		<div class="row">
			<span class="column">
				<label for="dialog_legalPeople">供应商法人</label> 
				<input id="dialog_legalPeople" name="legalPeople" class="easyui-textbox" data-options="readonly:true">
			</span>
			<span class="columnLast">
				<label for="dialog_legalPeopleContact">供应商法人联系方式</label>
				<input id="dialog_legalPeopleContact" name="legalPeopleContact" class="easyui-textbox" data-options="readonly:true">
			</span>
		</div>
		<div class="row">
			<span class="column">
				<label for="dialog_dockingPeople">供应商对接人</label> 
				<input id="dialog_dockingPeople" name="dockingPeople" class="easyui-textbox" data-options="readonly:true">
			</span>
			<span class="columnLast">
				<label for="dialog_dockingPeopleContact">供应商对接人联系方式</label>
				<input id="dialog_dockingPeopleContact" name="dockingPeopleContact" class="easyui-textbox" data-options="readonly:true">
			</span>
		</div>
		<div class="row">
			<span class="columnLast"> 
				<label for="dialog_remarks">备注信息</label> 
				<input id="dialog_remarks" name="remarks" style="width:795px" class="easyui-textbox" data-options="readonly:true">
			</span>
		</div>
	</div>
	
	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="rawMaterialData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'check',checkbox:true,hidden:true">选择</th>
					<th data-options="field:'rawmaterialCode',width:150,align:'left'">原料编码</th>
					<th data-options="field:'name',width:150,align:'left'">原料名称</th>
					<th data-options="field:'specification',width:100,align:'left'">原料规格</th>
					<th data-options="field:'rawmaterialType',width:100,align:'left'">原料类型</th>
					<th data-options="field:'colorNo',width:100,align:'left'">原料色号</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:150,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="newSupplierRawmaterial();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">增加供应原料</a>
			<a onclick="delSupplierRawmaterial();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">移除供应原料</a>
	    </div>
	</div>

	<!-- 增加供应原料-对话框 -->
	<div id="dlg" style="width: 520px; height: 260px; padding: 5px 10px">
		<form id="fm" method="post">
			<div class="row" style="margin-top: 10px;">
				<span class="column">
					<label for="dialog_rawmaterialCode" style="width: 60px;">原料编码</label> 
					<input id="dialog_rawmaterialCode" name="rawmaterialCode" class="easyui-combogrid">
				</span>
				<span class="columnLast">
					<label for="dialog_name" style="width: 60px;">原料名称</label> 
					<input id="dialog_name" name="name" class="easyui-textbox" data-options="readonly:true">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="dialog_specification" style="width: 60px;">原料规格</label> 
					<input id="dialog_specification" name="specification" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="columnLast">
					<label for="dialog_rawmaterialType" style="width: 60px;">原料类型</label>
					<input id="dialog_rawmaterialType" name="rawmaterialType" class="easyui-textbox" data-options="readonly:true">
				</span>
			</div>
			<div class="row">
				<span class="columnLast">
					<label for="dialog_colorNo" style="width: 60px;">原料色号</label> 
					<input id="dialog_colorNo" name="colorNo" class="easyui-textbox" data-options="readonly:true">
				</span>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="dialog_remarks" style="width: 60px;">备注信息</label> 
					<input id="dialog_remarks" name="remarks" style="width:400px;height:60px" class="easyui-textbox" data-options="readonly:true,multiline:true">
				</span>
			</div>
			
			<!-- 隐藏属性值 -->
			<div style="display: none;">
				<input id="dialog_rawmaterialId" name="id">
				<input name="delFlag">
				<input name="companyId">
				<input name="orgId">
				<input name="createBy">
				<input name="createDate">
				<input name="updateBy">
				<input name="updateDate">
				<input name="uuid">
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="saveSupplierRawmaterial();" style="width:90px">增加</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">取消</a>
	</div>

	<script type="text/javascript">
	// 根据原料编码模糊查询原料信息，使用分页
	var findRawMaterialInfoByParamUrl = "${pageScope.mvcPath}/erp/findRawMaterialInfoByParam";
	// 模糊查询供应商信息，使用分页
	var findSupplierByParamUrl = "${pageScope.mvcPath}/erp/findSupplierByParam";
	// 当前供应商信息
	var supplier = null;
	// 增加供应商供应的原料
	var addSupplierRawmaterialUrl = "${pageScope.mvcPath}/erp/addSupplierRawmaterial";
	// 删除供应商供应的原料
	var deleteSupplierRawmaterialUrl = "${pageScope.mvcPath}/erp/deleteSupplierRawmaterial";
	// 查询数据
	var findSupplierRawmaterialUrl = "${pageScope.mvcPath}/erp/findSupplierRawmaterial";
	
	$(document).ready(function () {
		
		$("#dialog_supplierName").combobox({
			required : true,
			prompt : "输入编码或名称检索",
			editable : true,
			delay : 200,
			panelHeight : 200,
			mode : "remote",
			url : findSupplierByParamUrl,
			valueField : "value",
			textField : "text",
			formatter: function(row) {
				var div = $("<div/>");
				var tmp = $("<span style='color:#080'/>").html(row.object.supplierNo);
				div.append(tmp);
				div.append("<br>");
				tmp = $("<span style='color:#008'/>").html(row.text);
				div.append(tmp);
				return div.html();
			},
			onHidePanel : function() {
				var value = $("#dialog_supplierName").combobox("getValue");
				var text = $("#dialog_supplierName").combobox("getText");
				var data = $("#dialog_supplierName").combobox("getData");
				var flag = false;
				$(data).each(function (index, row){
					if(value == row.value && text == row.text) {
						flag = true;
						supplier = row.object;
						return false;
					}
				});
				if(flag == false) {
					supplier = null;
					$("#dialog_supplierName").combobox("clear");
					$("#dialog_supplierNo").textbox("clear");
					$("#dialog_startSupplyDate").datebox("clear");
					$("#dialog_legalPeople").textbox("clear");
					$("#dialog_legalPeopleContact").textbox("clear");
					$("#dialog_dockingPeople").textbox("clear");
					$("#dialog_dockingPeopleContact").textbox("clear");
					$("#dialog_remarks").textbox("clear");
					// 清除数据
					$("#rawMaterialData").datagrid("loadData", { total: 0, rows: [] });
				} else {
					$("#dialog_supplierNo").textbox("setValue",supplier.supplierNo);
					$("#dialog_startSupplyDate").datebox("setValue",supplier.startSupplyDate);
					$("#dialog_legalPeople").textbox("setValue",supplier.legalPeople);
					$("#dialog_legalPeopleContact").textbox("setValue",supplier.legalPeopleContact);
					$("#dialog_dockingPeople").textbox("setValue",supplier.dockingPeople);
					$("#dialog_dockingPeopleContact").textbox("setValue",supplier.dockingPeopleContact);
					$("#dialog_remarks").textbox("setValue",supplier.remarks);
					// 加载数据
					$("#rawMaterialData").datagrid('reload');
				}
			}
		});
		
		$("#rawMaterialData").datagrid({
			url : findSupplierRawmaterialUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onBeforeLoad : function (param) {
				// 增加查询参数
				if(supplier == null) {
					return false;
				} else {
					param.supplierId = supplier.id;
				}
				$("#rawMaterialData").datagrid("clearSelections");
			}
		});
		
		$("#dlg").dialog({
			title : "供应商的供应原料",
			closed : true,
			resizable : true,
			minWidth : 520,
			minHeight : 260,
			maxWidth : 520,
			maxHeight : 260,
			modal : true,
			buttons : "#dlg-buttons"
		});
		
		$("#dialog_rawmaterialCode").combogrid({
			required : true,
			prompt : "输入编码或名称检索",
			loadMsg : "正在加载，请稍候...",
			editable : true,
			delay : 200,
			panelWidth : 500,
			panelHeight : 245,
			fitColumns : true,
			mode : "remote",
			url : findRawMaterialInfoByParamUrl,
			idField : "rawmaterialCode",
			textField : "rawmaterialCode",
			columns: [[
			           {field:"rawmaterialCode",title:"原料编码",width:120},
			           {field:"name",title:"原料名称",width:120},
			           {field:"specification",title:"原料规格",width:120},
			           {field:"rawmaterialType",title:"原料类型",width:120},
			           {field:"colorNo",title:"原料色号",width:120}
			       ]],
			onSelect : function(index, row) {
				$("#dialog_rawmaterialId").val(row.id);
				$("#dialog_name").textbox("setValue", row.name);
				$("#dialog_specification").textbox("setValue", row.specification);
				$("#dialog_rawmaterialType").textbox("setValue", row.rawmaterialType);
				$("#dialog_colorNo").textbox("setValue", row.colorNo);
			},
	        onChange : function(newValue, oldValue) {
	        	if($.trim(newValue) == "") {
	        		$("#dialog_rawmaterialId").val("");
	        		$("#dialog_name").textbox("clear");
		     	    $("#dialog_specification").textbox("clear");
		     	    $("#dialog_rawmaterialType").textbox("clear");
		     	    $("#dialog_colorNo").textbox("clear");
	     	    }
	        },
	        onHidePanel : function() {
	        	   var value = $("#dialog_rawmaterialCode").combogrid("getValue");
	        	   var text = $("#dialog_rawmaterialCode").combogrid("getText");
	        	   var data = $("#dialog_rawmaterialCode").combogrid("grid").datagrid("getData");
	        	   var flag = false;
		    		$(data.rows).each(function (index, row){
		    			if(value == row.rawmaterialCode && text == row.rawmaterialCode) {
		    				flag = true;
		    				return false;
		    			}
		    		});
		    		if(flag == false) {
		    			$("#dialog_rawmaterialCode").combogrid("clear");
		    		}
	        }
		});
	});

	// 增加供应原料
	function newSupplierRawmaterial() {
		if($("#dialog_supplierName").combobox("isValid") == false) {
			$("#dialog_supplierName").combobox("validate");
			$.messager.alert("提示", "请先选择供应商！", "info");
			return ;
		}
		$('#dlg').dialog('open').dialog('setTitle', '增加供应商的供应原料');
		$('#fm').form('reset');
	}
	
	// 移除供应原料
	function delSupplierRawmaterial() {
		if($("#dialog_supplierName").combobox("isValid") == false || supplier == null) {
			$("#dialog_supplierName").combobox("validate");
			$.messager.alert("提示", "请先选择供应商！", "info");
			return ;
		}
		var row = $("#rawMaterialData").datagrid("getSelected");
		if (row == null) {
			$.messager.alert("提示", "未选择要移除的原料信息！", "info");
			return;
		}
		$.messager.confirm("确认移除", "确定要移除供应商供应的原料?<br/>原料名称：" + row.name + "<br/>原料规格：" + row.specification, function (r) {
			if (r) {
				var param = {};
				param.supplierId = supplier.id;
				param.rawmaterialId = row.id;
				$.post(deleteSupplierRawmaterialUrl, param, function (data) {
					if (data.success) {
						// 成功
						$('#dlg').dialog('close')
						$.messager.show({ title : '提示', msg : data.message, timeout : 5000, showType : 'slide' });
						$("#rawMaterialData").datagrid('reload');
					} else {
						// 失败
						$.messager.alert("提示", data.message, "warning");
					}
				}, "json");
			}
		});
	}
	
	// 增加供应原料-真实
	function saveSupplierRawmaterial() {
		if(supplier == null) {
			$.messager.alert("提示", "请先选择供应商！", "info");
			return ;
		}
		var param = {};
		param.supplierId = supplier.id;
		param.rawmaterialId = $("#dialog_rawmaterialId").val();
		$.post(addSupplierRawmaterialUrl, param, function (data) {
			if (data.success) {
				// 成功
				$('#dlg').dialog('close')
				$.messager.show({ title : '提示', msg : data.message, timeout : 5000, showType : 'slide' });
				$("#rawMaterialData").datagrid('reload');
			} else {
				// 失败
				$.messager.alert("提示", data.message, "warning");
			}
		}, "json");
	}
	</script>
</body>
</html>