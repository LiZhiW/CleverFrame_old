<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>供应商管理</title>
<style type="text/css">
.row {
	margin-bottom: 8px;
}
.row label {
	display: inline-block;
	width: 90px;
	text-align: center;
}
.row input,select {
	width: 160px;

}
.row .column {
	margin-right: 20px;
}
.row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:100px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="search_supplierNo">供应商编号</label> 
					<input id="search_supplierNo" name="supplierNo" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="search_supplierName">供应商名称</label> 
					<input id="search_supplierName" name="supplierName" class="easyui-textbox" style="width: 410px">
				</span>

			</div>
			<div class="row">
				<span class="column">
					<label for="search_legalPeople">供应商法人</label> 
					<input id="search_legalPeople" name="legalPeople" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="search_dockingPeople">供应商对接人</label>
					<input id="search_dockingPeople" name="dockingPeople" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="search_startDate">供应日期(最小)</label> 
					<input id="search_startDate" name="startDate" class="easyui-datebox">
				</span>
				<span class="columnLast">
					<label for="search_endDate">供应日期(最大)</label> 
					<input id="search_endDate" name="endDate" class="easyui-datebox">
				</span>
			</div>
		</form>
	</div>

	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="supplierData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'check',checkbox:true,hidden:true">选择</th>
					<th data-options="field:'supplierNo',width:80,align:'left'">供应商编号</th>
					<th data-options="field:'supplierName',width:200,align:'left'">供应商名称</th>
					<th data-options="field:'legalPeople',width:100,align:'left'">供应商法人</th>
					<th data-options="field:'legalPeopleContact',width:150,align:'left'">供应商法人联系方式</th>
					<th data-options="field:'dockingPeople',width:100,align:'left'">供应商对接人</th>
					<th data-options="field:'dockingPeopleContact',width:150,align:'left'">供应商对接人联系方式</th>
					<th data-options="field:'startSupplyDate',width:130,align:'left'">供应商开始供应日期</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:250,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchSupplier();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="clearSearchForm();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-edit-clear-2',plain:true">清空查询条件</a>
			<a onclick="newSupplier();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editSupplier();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">修改</a>
			<a onclick="delSupplier();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>
	    </div>
	</div>
	
	<!-- 新增/编辑对话框 -->
	<div id="dlg" style="width: 720px; height: 285px; padding: 5px 10px">
		<form id="fm" method="post">
			<div class="row" style="margin-top: 10px;">
				<span class="column">
					<label for="dialog_supplierNo" style="width: 125px">供应商编号</label> 
					<input id="dialog_supplierNo" name="supplierNo" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_supplierName" style="width: 125px">供应商名称</label> 
					<input id="dialog_supplierName" name="supplierName" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="dialog_legalPeople" style="width: 125px">供应商法人</label> 
					<input id="dialog_legalPeople" name="legalPeople" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_legalPeopleContact" style="width: 125px">供应商法人联系方式</label>
					<input id="dialog_legalPeopleContact" name="legalPeopleContact" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="dialog_dockingPeople" style="width: 125px">供应商对接人</label> 
					<input id="dialog_dockingPeople" name="dockingPeople" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_dockingPeopleContact" style="width: 125px">供应商对接人联系方式</label>
					<input id="dialog_dockingPeopleContact" name="dockingPeopleContact" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="columnLast">
					<label for="dialog_startSupplyDate" style="width: 125px">供应商开始供应日期</label>
					<input id="dialog_startSupplyDate" name="startSupplyDate" class="easyui-datebox">
				</span>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="dialog_remarks">备注信息</label> 
					<input id="dialog_remarks" name="remarks" style="width:570px;height:60px" class="easyui-textbox">
				</span>
			</div>
			
			<!-- 隐藏属性值 -->
			<div style="display: none;">
				<input name="id">
				<input name="delFlag">
				<input name="companyId">
				<input name="orgId">
				<input name="createBy">
				<input name="createDate">
				<input name="updateBy">
				<input name="updateDate">
				<input name="uuid">
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="saveSupplier();" style="width:90px">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">取消</a>
	</div>
	
	
	<script type="text/javascript">
	// 查询数据
	var findSupplierByPageUrl = "${pageScope.mvcPath}/erp/findSupplierByPage";
	// 新增
	var addSupplierUrl = "${pageScope.mvcPath}/erp/addSupplier";
	// 更新
	var updateSupplierUrl = "${pageScope.mvcPath}/erp/updateSupplier";
	// 删除
	var deleteSupplierUrl = "${pageScope.mvcPath}/erp/deleteSupplier";
	// 保存
	var supplierSaveUrl;
	
	$(document).ready(function () {
		// 设置supplierData数据显示表格
		$("#supplierData").datagrid({
			url : findSupplierByPageUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				editSupplier();
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			}
		});
		
		$("#dlg").dialog({
			title : "供应商信息",
			closed : true,
			resizable : false,
			minWidth : 720,
			minHeight : 285,
			maxWidth : 720,
			maxHeight : 285,
			modal : true,
			buttons : "#dlg-buttons"
		});
		
		$("#dialog_supplierNo").textbox({
			required : false,
			readonly : true,
			prompt : "编码自动生成",
			validType : 'length[1,100]',
		});
		
		$("#dialog_supplierName").textbox({
			required : true,
			width : 218,
			validType : 'length[1,255]',
		});

		$("#dialog_legalPeople").textbox({
			required : false,
			validType : 'length[1,100]',
		});

		$("#dialog_legalPeopleContact").textbox({
			required : false,
			width : 218,
			validType : 'length[1,255]',
		});

		$("#dialog_dockingPeople").textbox({
			required : true,
			validType : 'length[1,100]',
		});

		$("#dialog_dockingPeopleContact").textbox({
			required : true,
			width : 218,
			validType : 'length[1,100]',
		});

		$("#dialog_startSupplyDate").textbox({
			required : false
		});

		$("#dialog_remarks").textbox({
			validType : 'length[3,255]',
			multiline:true
		});
	});
	
	// 清空查询条件
	function clearSearchForm() {
		$("#fm-search").form("clear");
	}
	
	// 查询
	function searchSupplier() {
		$("#supplierData").datagrid("load");
	}
	
	// 新增
	function newSupplier() {
		supplierSaveUrl = addSupplierUrl;
		$('#dlg').dialog('open').dialog('setTitle', '新增供应商');
		$('#fm').form('reset');
	}
	
	// 修改
	function editSupplier() {
		supplierSaveUrl = updateSupplierUrl;
		var row = $('#supplierData').datagrid('getSelected');
		if (row == null) {
			$.messager.alert("提示", "未选择要编辑的数据！", "info");
			return;
		}
		if (row) {
			$('#dlg').dialog('open').dialog('setTitle', '编辑供应商');
			$('#fm').form('load', row);
		}
	}
	
	// 删除
	function delSupplier() {
		var row = $("#supplierData").datagrid("getSelected");
		if (row == null) {
			$.messager.alert("提示", "未选择要删除的数据！", "info");
			return;
		}
		$.messager.confirm("确认删除", "确定要删除供应商?<br/>编号：" + row.supplierNo + "<br/>名称：" + row.supplierName, function (r) {
			if (r) {
				$.post(deleteSupplierUrl, row, function (data) {
					if (data.success) {
						// 删除成功
						$.messager.show({ title : '提示', msg : data.message, timeout : 5000, showType : 'slide' });
						$("#supplierData").datagrid('reload');
					} else {
						// 删除失败
						$.messager.alert("提示", data.message, "warning");
					}
				}, "json");
			}
		});
	}
	
	// 保存
	function saveSupplier() {
		$("#fm").form("submit", {
			url : supplierSaveUrl,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({ title : '提示', msg : data.message, timeout : 5000, showType : 'slide' });
					$("#supplierData").datagrid('reload');
				} else {
					// 保存失败
					$.messager.alert("提示", data.message, "warning");
				}
			}
		});
	}
	</script>
</body>
</html>