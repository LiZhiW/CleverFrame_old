<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>原料出库申请-车间加工</title>
<style type="text/css">
.row {
	margin-bottom: 8px;
}
.row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
.row input,select {
	width: 160px;
}
.row .column {
	margin-right: 10px;
}
.row .columnLast {
	margin-right: 0px;
}
.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
.radiobutton{
	cursor: pointer;
    overflow: hidden;
    vertical-align: middle;
    width: 14px;
    height: 14px;
    margin: 0;
    padding: 0;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:70px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="search_outNo">申请单编号</label> 
					<input id="search_outNo" name="outNo" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="search_name">原料名称</label> 
					<input id="search_name" name="name" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="search_specification">原料规格</label> 
					<input id="search_specification" name="specification" class="easyui-textbox">
				</span>
			</div>
			 
			<div class="row">
				<span class="column">
					<label for="search_auditorState">审核状态</label> 
					<input id="search_auditorState" name="auditorState" class="easyui-combobox">
				</span>
				<span class="columnLast">
					<label for="search_startDate">出库日期</label>
					<input id="search_startDate" name="startDate" class="easyui-datebox">
				</span>
				<span class="columnLast">
					<label for="search_endDate" style="width: 20px;">至</label> 
					<input id="search_endDate" name="endDate" class="easyui-datebox">
				</span>
			</div>
		</form>
	</div>

	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="rawmaterialOutData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'check',checkbox:true,hidden:true">选择</th>
					<th data-options="field:'out_no',width:80,align:'left'">申请单编号</th>
					<th data-options="field:'createName',width:80,align:'left'">申请人</th>
					<th data-options="field:'out_date',width:80,align:'left',formatter:formatDate">出库日期</th>
					<th data-options="field:'out_type',width:80,align:'left',formatter:formatOutTypeData">出库类型</th>
					<th data-options="field:'rawmaterial_code',width:80,align:'left'">原料编码</th>
					<th data-options="field:'name',width:100,align:'left'">原料名称</th>
					<th data-options="field:'specification',width:80,align:'left'">原料规格</th>
					<th data-options="field:'rawmaterial_type',width:80,align:'left'">原料类型</th>
					<th data-options="field:'color_no',width:80,align:'left'">原料色号</th>
					<th data-options="field:'quantity',width:80,align:'left'">出库数量</th>
					<th data-options="field:'auditor_state',width:80,align:'left',formatter:formatAuditorState">审核状态</th>
					<th data-options="field:'auditorName',width:80,align:'left'">审核人</th>
					<th data-options="field:'unit_price',width:60,align:'left'">单价</th>
					<th data-options="field:'aggregate_amount',width:70,align:'left'">应付金额</th>
					<th data-options="field:'is_invoice',width:80,align:'left',formatter:formatIsInvoice">是否开发票</th>
					
					<th data-options="field:'order_no',width:80,align:'left',hidden:true">订单编号</th>
					<th data-options="field:'is_first_order',width:80,align:'left',hidden:true">是否先下订单后原料出库</th>
					<th data-options="field:'is_history',width:80,align:'left',hidden:true">是否是历史记录</th>
					<th data-options="field:'auditor_id',width:100,align:'left',hidden:true">审核人ID</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'company_id',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'org_id',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'create_by',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'create_date',width:100,align:'left',hidden:true">申请时间</th>
					<th data-options="field:'update_by',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'update_date',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:150,align:'left'">备注信息</th>
					<th data-options="field:'del_flag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
					<th data-options="field:'operate',width:100,align:'left',formatter:formatOperate">操作</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchRawmaterialOut();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="clearSearchForm();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'ExpandIcon-edit-clear-2',plain:true">清空查询条件</a>
			<a onclick="newRawmaterialOut();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editRawmaterialOut();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">修改</a>
	    </div>
	</div>

	<!-- 新增/编辑对话框 -->
	<div id="dlg" style="width: 765px; height: 365px; padding: 5px 10px">
		<form id="fm" method="post">
			<div class="row" style="margin-top: 10px;">
				<span class="column">
					<label for="dialog_outNo">申请单编号</label> 
					<input id="dialog_outNo" name="outNo" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_outDate">出库日期</label>
					<input id="dialog_outDate" name="outDate" class="easyui-datebox">
				</span>
				<span class="columnLast">
					<label for="dialog_orderNo">订单编号</label> 
					<input id="dialog_orderNo" name="orderNo" class="easyui-textbox">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_rawmaterialCode">原料编码</label> 
					<input id="dialog_rawmaterialCode" name="rawmaterialCode" class="easyui-combogrid">
				</span>
				<span class="column">
					<label for="dialog_name">原料名称</label> 
					<input id="dialog_name" name="name" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_specification">原料规格</label> 
					<input id="dialog_specification" name="specification" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="dialog_rawmaterialType">原料类型</label>
					<input id="dialog_rawmaterialType" name="rawmaterialType" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="dialog_colorNo">原料色号</label>
					<input id="dialog_colorNo" name="colorNo" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_quantity">出库数量</label>
					<input id="dialog_quantity" name="quantity" class="easyui-numberbox">
				</span>
			</div>
			<div style="margin-bottom:8px">
				<label>是否先下订单后原料出库：</label>
				<label for="dialog_firstOrderByYes" style="width: 15px;cursor: pointer;margin-right: 10px;vertical-align: middle;">
					<input id="dialog_firstOrderByYes" class="radiobutton" name="isFirstOrder" type="radio" value="1"/>是
				</label>
				
				<label for="dialog_firstOrderByNo" style="width: 15px;cursor: pointer;margin-right: 0px;vertical-align: middle;">
					<input id="dialog_firstOrderByNo" class="radiobutton" name="isFirstOrder" type="radio" value="2" checked="checked"/>否
				</label>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="dialog_remarks">备注信息</label> 
					<input id="dialog_remarks" name="remarks" style="width:655px;height:80px" class="easyui-textbox">
				</span>
			</div>
			<div class="ftitle">后续流程：</div>
			<div class="row">
				<span class="columnLast">
					<label for="dialog_auditorId" style="width: 160px;">财务部审核入库，审核人</label>
					<input id="dialog_auditorId" name="auditorId" class="easyui-combobox">
				</span>
			</div>
			
			<!-- 隐藏属性值 -->
			<div style="display: none;">
				<input id="dialog_outType" name="outType" value="1">
				<input id="dialog_auditorState" name="auditorState" value="1">
				<input id="dialog_unitPrice" name="unitPrice" value="0">
				<input id="dialog_aggregateAmount" name="aggregateAmount" value="0">
				<input id="dialog_isInvoice" name="isInvoice" value="1">
				<input id="dialog_isHistory" name="isHistory" value="1">
				<input id="dialog_isChange" name="isChange" value="1">
				
				<input name="id">
				<input name="delFlag">
				<input name="companyId">
				<input name="orgId">
				<input name="createBy">
				<input name="createDate">
				<input name="updateBy">
				<input name="updateDate">
				<input name="uuid">
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="saveRawmaterialOut();" style="width:90px">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">取消</a>
	</div>

	<!-- 历史对话框 -->
	<div id="dlg_history" style="width: 765px; height: 365px; padding: 5px 10px">
		<form id="fm_history" method="post">
			<div class="row" style="margin-top: 10px;">
				<span class="column">
					<label for="dialog_history_outNo">申请单编号</label> 
					<input id="dialog_history_outNo" name="outNo" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="column">
					<label for="dialog_history_outDate">出库日期</label>
					<input id="dialog_history_outDate" name="outDate" class="easyui-datebox" data-options="readonly:true">
				</span>
				<span class="columnLast">
					<label for="dialog_history_orderNo">订单编号</label> 
					<input id="dialog_history_orderNo" name="orderNo" class="easyui-textbox" data-options="readonly:true">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="dialog_history_rawmaterialCode">原料编码</label> 
					<input id="dialog_history_rawmaterialCode" name="rawmaterialCode" class="easyui-combobox" data-options="readonly:true">
				</span>
				<span class="column">
					<label for="dialog_history_name">原料名称</label> 
					<input id="dialog_history_name" name="name" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="columnLast">
					<label for="dialog_history_specification">原料规格</label> 
					<input id="dialog_history_specification" name="specification" class="easyui-textbox" data-options="readonly:true">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="dialog_history_rawmaterialType">原料类型</label>
					<input id="dialog_history_rawmaterialType" name="rawmaterialType" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="column">
					<label for="dialog_history_colorNo">原料色号</label>
					<input id="dialog_history_colorNo" name="colorNo" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="columnLast">
					<label for="dialog_history_quantity">出库数量</label>
					<input id="dialog_history_quantity" name="quantity" class="easyui-numberbox" data-options="readonly:true">
				</span>
			</div>
			<div style="margin-bottom:8px">
				<label>是否先下订单后原料出库：</label>
				<label for="dialog_history_firstOrderByYes" style="width: 15px;cursor: pointer;margin-right: 10px;vertical-align: middle;">
					<input id="dialog_history_firstOrderByYes" class="radiobutton" name="isFirstOrder" type="radio" value="1" disabled="disabled"/>是
				</label>
				
				<label for="dialog_history_firstOrderByNo" style="width: 15px;cursor: pointer;margin-right: 0px;vertical-align: middle;">
					<input id="dialog_history_firstOrderByNo" class="radiobutton" name="isFirstOrder" type="radio" value="2" checked="checked" disabled="disabled"/>否
				</label>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="dialog_history_remarks">备注信息</label> 
					<input id="dialog_history_remarks" name="remarks" style="width:655px;height:80px" class="easyui-textbox" data-options="readonly:true,multiline:true">
				</span>
			</div>
			<div class="ftitle">后续流程：</div>
			<div class="row">
				<span class="columnLast">
					<label for="dialog_history_auditorId" style="width: 160px;">财务部审核入库，审核人</label>
					<input id="dialog_history_auditorId" name="auditorId" class="easyui-combobox" data-options="readonly:true">
				</span>
			</div>
		</form>
	</div>
	<div id="dlg-history-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#dlg_history').dialog('close')" style="width:90px">关闭</a>
	</div>

	<script type="text/javascript">
	// 根据原料编码模糊查询原料信息，使用分页
	var findRawMaterialInfoByParamUrl = "${pageScope.mvcPath}/erp/findRawMaterialInfoByParam";
	// 模糊查询用户信息，使用分页
	var findUserByParamUrl = "${pageScope.mvcPath}/sys/findUserByParam";
	// 查询
	var findRawmaterialOutByPageUrl = "${pageScope.mvcPath}/erp/findRawmaterialOutByPage";
	// 新增
	var addRawmaterialOutUrl = "${pageScope.mvcPath}/erp/addRawmaterialOut";
	// 更新
	var updateRawmaterialOutUrl = "${pageScope.mvcPath}/erp/updateRawmaterialOut";
	// 保存
	var rawmaterialOutSaveUrl;
	// 删除
	var deleteRawmaterialOutUrl = "${pageScope.mvcPath}/erp/deleteRawmaterialOut";
	// 变更
	var changeRawmaterialOutUrl = "${pageScope.mvcPath}/erp/changeRawmaterialOut";
	// 历史数据
	var getRawmaterialOutByNoUrl = "${pageScope.mvcPath}/erp/getRawmaterialOutByNo";
	// 默认审核人
	var getAuditorByRawmaterialOutUrl = "${pageScope.mvcPath}/erp/getAuditorByRawmaterialOut";
	
	// 流程审核状态 - 字典
	var findAuditorStateDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findAuditorStateDictUrl += encodeURIComponent("流程审核状态");
	var auditorStateDictData = null;
	$.ajax({
		url : findAuditorStateDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			auditorStateDictData = data;
		}
	});
	
	// 是否开发票 - 字典
	var findIsInvoiceDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findIsInvoiceDictUrl += encodeURIComponent("是否开发票");
	var IsInvoiceDictData = null;
	$.ajax({
		url : findIsInvoiceDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			IsInvoiceDictData = data;
		}
	});
	
	// 出库类型 - 字典
	var findOutTypeDictUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findOutTypeDictUrl += encodeURIComponent("原料出库-出库类型");
	var OutTypeData = null;
	$.ajax({
		url : findOutTypeDictUrl,
		async : false,
		dataType : "json",
		type : "POST",
		success : function (data) {
			OutTypeData = data;
		}
	});
	
	// 默认审核人
	var auditorUser = null;
	
	$(document).ready(function () {
		// 设置rawmaterialOutData数据显示表格
		$("#rawmaterialOutData").datagrid({
			url : findRawmaterialOutByPageUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				editRawmaterialOut();
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
				param.outType = "1";
			}
		});
		
		$("#search_auditorState").combobox({
			required : false,
			editable : true,
			panelHeight : "auto",
			valueField : "value",
			textField : "text",
		});
		$("#search_auditorState").combobox("loadData", auditorStateDictData);
		
		$("#dlg").dialog({
			title : "原料出库申请单-车间加工",
			closed : true,
			resizable : false,
			minWidth : 765,
			minHeight : 365,
			maxWidth : 765,
			maxHeight : 365,
			modal : true,
			buttons : "#dlg-buttons"
		});
		
		$("#dlg_history").dialog({
			title : "原料出库申请单-车间加工-历史单",
			closed : true,
			resizable : false,
			minWidth : 765,
			minHeight : 365,
			maxWidth : 765,
			maxHeight : 365,
			modal : true,
			buttons : "#dlg-history-buttons"
		});
		
		$("#dialog_outNo").textbox({
			required : false,
			readonly : true,
			prompt : "编号自动生成",
			validType : 'length[1,100]'
		});
		
		$("#dialog_outDate").datebox({    
			required : true
		}); 
		
		$("#dialog_orderNo").textbox({
			required : false,
			readonly : true,
			prompt : "选择订单"
		});
		
		$("#dialog_rawmaterialCode").combogrid({
			required : true,
			prompt : "输入编码或名称检索",
			loadMsg : "正在加载，请稍候...",
			editable : true,
			delay : 200,
			panelWidth : 500,
			panelHeight : 245,
			fitColumns : true,
			mode : "remote",
			url : findRawMaterialInfoByParamUrl,
			idField : "rawmaterialCode",
			textField : "rawmaterialCode",
			columns: [[
			           {field:"rawmaterialCode",title:"原料编码",width:120},
			           {field:"name",title:"原料名称",width:120},
			           {field:"specification",title:"原料规格",width:120},
			           {field:"rawmaterialType",title:"原料类型",width:120},
			           {field:"colorNo",title:"原料色号",width:120}
			       ]],
			onSelect : function(index, row) {
				$("#dialog_name").textbox("setValue", row.name);
				$("#dialog_specification").textbox("setValue", row.specification);
				$("#dialog_rawmaterialType").textbox("setValue", row.rawmaterialType);
				$("#dialog_colorNo").textbox("setValue", row.colorNo);
	       },
	       onChange : function(newValue, oldValue) {
	    	   if($.trim(newValue) == "") {
		    	   $("#dialog_name").textbox("clear");
		    	   $("#dialog_specification").textbox("clear");
		    	   $("#dialog_rawmaterialType").textbox("clear");
		    	   $("#dialog_colorNo").textbox("clear");
	    	   }
	       },
	       onHidePanel : function() {
	    	   var value = $("#dialog_rawmaterialCode").combogrid("getValue");
	    	   var text = $("#dialog_rawmaterialCode").combogrid("getText");
	    	   var data = $("#dialog_rawmaterialCode").combogrid("grid").datagrid("getData"); 
	    	   var flag = false;
				$(data.rows).each(function (index, row){
					if(value == row.rawmaterialCode && text == row.rawmaterialCode) {
						flag = true;
						return false;
					}
				});
				if(flag == false) {
					$("#dialog_rawmaterialCode").combogrid("clear");
				}
	       }
		});
		
		$("#dialog_name").textbox({
			required : false,
			readonly : true,
			prompt : "自动带出",
			validType : 'length[1,100]'
		}); 
		
		$("#dialog_specification").textbox({
			required : false,
			readonly : true,
			prompt : "自动带出",
			validType : 'length[1,100]'
		});
		
		$("#dialog_rawmaterialType").textbox({
			required : false,
			readonly : true,
			prompt : "自动带出",
			validType : 'length[1,100]'
		});
		
		$("#dialog_colorNo").textbox({
			required : false,
			readonly : true,
			prompt : "自动带出",
			validType : 'length[0,100]'
		});
		
		$("#dialog_quantity").numberbox({
			required : true,
			precision : 2,
			min : 0,
			prompt : "单位：米",
			suffix : "米",
		});

		$("#dialog_auditorId").combobox({
			required : true,
			prompt : "输入用户名或登登录名检索",
			width : 190,
			editable : true,
			delay : 200,
			panelHeight : 150,
			mode : "remote",
			url : findUserByParamUrl,
			valueField : "value",
			textField : "text",
			formatter: function(row) {
				var div = $("<div/>");
				// 用户编号：jobNo；登录名称：loginName
				var tmp = $("<span style='color:#080'/>").html(row.object.jobNo);
				div.append(tmp);
				div.append("<br>");
				tmp = $("<span style='color:#008'/>").html(row.text + "(" + row.object.loginName + ")");
				div.append(tmp);
				return div.html();
			},
			onHidePanel : function() {
				var value = $("#dialog_auditorId").combobox("getValue");
				var text = $("#dialog_auditorId").combobox("getText");
				var data = $("#dialog_auditorId").combobox("getData");
				var flag = false;
				$(data).each(function (index, row){
					if(value == row.value && text == row.text) {
						flag = true;
						return false;
					}
				});
				if(flag == false) {
					$("#dialog_auditorId").combobox("clear");
				}
			}
		});
		
		$("#dialog_remarks").textbox({
			validType : 'length[3,255]',
			multiline:true
		});
		
		// 获取默认审核人
		$.ajax({
			url : getAuditorByRawmaterialOutUrl,
			async : true,
			dataType : "json",
			type : "POST",
			success : function (data) {
				if (data.success) {
					auditorUser = data.object;
				} else {
					$.messager.alert("提示", data.message, "warning");
				}
			}
		});
	});
	
	// 表单初始化
	function initForm() {
		$("#dialog_outDate").datebox("setValue", new Date().format("yyyy-MM-dd"));
		$("#dialog_auditorState").val("1");
		$("#dialog_unitPrice").val("0");
		$("#dialog_aggregateAmount").val("0");
		$("#dialog_isInvoice").val("1");
		$("#dialog_isHistory").val("1");
		$("#dialog_isChange").val("1");
		
		// 设置默认审核人
		if (auditorUser != null) {
			$("#dialog_auditorId").combobox("setValue", auditorUser.id);
			$("#dialog_auditorId").combobox("setText", auditorUser.name);
		}
	}
	
	// 清空查询条件
	function clearSearchForm() {
		$("#fm-search").form("clear");
	}
	
	// 查询
	function searchRawmaterialOut() {
		$("#rawmaterialOutData").datagrid("load");
	}

	// 新增
	function newRawmaterialOut() {
		rawmaterialOutSaveUrl = addRawmaterialOutUrl;
		$("#dlg").dialog('open').dialog('setTitle', '新增原料出库申请单-车间加工');
		$("#fm").form('reset');
		initForm();
	}
	
	// 编辑
	function editRawmaterialOut() {
		rawmaterialOutSaveUrl = updateRawmaterialOutUrl;
		var row = $("#rawmaterialOutData").datagrid("getSelected");
		if (row == null) {
			$.messager.alert("提示", "未选择要编辑的数据！", "info");
			return;
		}
		if (row) {
			if(row.auditor_state != "1" && row.auditor_state != "4") {
				$.messager.alert("提示", "原料出库申请单审核状态必须是“等待审核”、“驳回”才能修改!", "info");
				return;
			}
			$('#dlg').dialog('open').dialog('setTitle', '编辑原料出库申请单-车间加工');
			$('#fm').form('load', dataRowConvert(row));
			// 设置下拉列表的值
			$("#dialog_rawmaterialCode").combogrid("setValue", row.rawmaterial_code);
			$("#dialog_rawmaterialCode").combogrid("setText", row.rawmaterial_code);
			
			$("#dialog_auditorId").combobox("setValue", row.auditor_id);
			$("#dialog_auditorId").combobox("setText", row.auditorName);
		}
	}
	
	// 保存
	function saveRawmaterialOut() {
		$("#fm").form("submit", {
			url : rawmaterialOutSaveUrl,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({ title : '提示', msg : data.message, timeout : 5000, showType : 'slide' });
					$("#rawmaterialOutData").datagrid('reload');
				} else {
					// 保存失败
					$.messager.alert("提示", data.message, "warning");
				}
			}
		});
	}
	
	// 变更
	function changeRawmaterialOut(rowId) {
		if($.trim(rowId) == "") {
			$.messager.alert("提示", "未选择要变更的数据！", "info");
			return;
		}
		var data = $("#rawmaterialOutData").datagrid("getData");
		var row = null;
		$(data.rows).each(function(index, r){
			if(r.id == rowId) {
				row = r;
				return false;
			}
		});
		rawmaterialOutSaveUrl = changeRawmaterialOutUrl;
		if (row == null) {
			$.messager.alert("提示", "未选择要变更的数据！", "info");
			return;
		}
		if (row) {
			if(row.auditor_state != "3") {
				$.messager.alert("提示", "原料出库申请单审核状态必须是“通过”才能变更!", "info");
				return;
			}
			$('#dlg').dialog('open').dialog('setTitle', '变更原料出库申请单-车间加工');
			$('#fm').form('load', dataRowConvert(row));
			// 设置下拉列表的值
			$("#dialog_rawmaterialCode").combogrid("setValue", row.rawmaterial_code);
			$("#dialog_rawmaterialCode").combogrid("setText", row.rawmaterial_code);
			
			$("#dialog_auditorId").combobox("setValue", row.auditor_id);
			$("#dialog_auditorId").combobox("setText", row.auditorName);
		}
	}
	
	// 历史数据
	function getHistoryData(rowId) {
		if($.trim(rowId) == "") {
			$.messager.alert("提示", "未选择要查看的历史数据！", "info");
			return;
		}
		var data = $("#rawmaterialOutData").datagrid("getData");
		var row = null;
		$(data.rows).each(function(index, r){
			if(r.id == rowId) {
				row = r;
				return false;
			}
		});
		if (row == null) {
			$.messager.alert("提示", "未选择要查看的历史数据！", "info");
			return;
		}
		var param = {};
		param.outNo = row.out_no + "-LS";
		$.ajax({
			url : getRawmaterialOutByNoUrl,
			async : false,
			data : param,
			dataType : "json",
			type : "POST",
			success : function (data) {
				if(data.success == true) {
					var row = data.object;
					$('#dlg_history').dialog('open');
					$('#fm_history').form('load', dataRowConvert(row));
					$("#dialog_history_rawmaterialCode").combobox("setText", row.rawmaterial_code);
					$("#dialog_history_auditorId").combobox("setText", row.auditorName);
				} else {
					// 失败
					$.messager.alert("提示", data.message, "warning");
				}
			}
		});
	}
	
	// 数据转换
	function dataRowConvert(row)
	{
		if(row == null) {
			return null;
		}
		var newRow = {};
		newRow.outNo = row.out_no;
		newRow.outDate = new Date(row.out_date).format("yyyy-MM-dd hh:mm:ss");
        newRow.outType = row.out_type;
        newRow.orderNo = row.order_no;
        newRow.isFirstOrder = row.is_first_order;
		newRow.rawmaterialCode = row.rawmaterial_code;
		newRow.name = row.name;
		newRow.specification = row.specification;
		newRow.rawmaterialType = row.rawmaterial_type;
		newRow.colorNo = row.color_no;
		newRow.quantity = row.quantity;
		newRow.remarks = row.remarks;
		newRow.auditorId = row.auditor_id;
		newRow.auditorState = row.auditor_state;
		newRow.unitPrice = row.unit_price;
		newRow.aggregateAmount = row.aggregate_amount;
		newRow.isInvoice = row.is_invoice;
		newRow.isHistory = row.is_history;
		newRow.id = row.id;
		newRow.delFlag = row.del_flag;
		newRow.companyId = row.company_id;
		newRow.orgId = row.org_id;
		newRow.createBy = row.create_by;
		newRow.createDate = new Date(row.create_date).format("yyyy-MM-dd hh:mm:ss");
		newRow.updateBy = row.update_by;
		newRow.updateDate = new Date(row.update_date).format("yyyy-MM-dd hh:mm:ss");
		newRow.uuid = row.uuid;
		return newRow;
	}
	
	function formatOutTypeData(value,row,index) {
		var text = null;
		$(OutTypeData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatIsInvoice(value,row,index) {
		var text = null;
		$(IsInvoiceDictData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatAuditorState(value,row,index) {
		var text = null;
		$(auditorStateDictData).each(function(index, row) {
			if(value == row.value) {
				text = row.text;
				return false;
			}
		});
		if(text == null) {
			text = value;
		}
		return text;
	}
	
	function formatOperate(value,row,index) {
		var div = $("<div />");
		var tmp = null;
		if(row.auditor_state == "3") {
			tmp = $("<a href='javascript:void(0)' onclick='changeRawmaterialOut(\"" + row.id + "\");' />");
			tmp.html("变更");
			div.append(tmp);
		}

		if(row.is_change == "2") {
			if(tmp != null) {
				div.append(" | ");
			}
			
			tmp = $("<a href='javascript:void(0)' onclick='getHistoryData(\"" + row.id + "\");' />");
			tmp.html("历史记录");
			div.append(tmp);
		}
		
		return div.html();
	}
	
    function formatDate(value,row,index) {
    	var date = new Date(value);
    	return date.format("yyyy-MM-dd");
    }
	
    // 日期格式化
    Date.prototype.format = function (fmt) {
    	var o = {
    		"M+" : this.getMonth() + 1, //月份
    		"d+" : this.getDate(), //日
    		"h+" : this.getHours(), //小时
    		"m+" : this.getMinutes(), //分
    		"s+" : this.getSeconds(), //秒
    		"q+" : Math.floor((this.getMonth() + 3) / 3), //季度
    		"S" : this.getMilliseconds() //毫秒
    	};
    	if (/(y+)/.test(fmt)){
    		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    	}
    	for (var k in o) {
    		if (new RegExp("(" + k + ")").test(fmt)) {
    			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    		}
    	}
    	return fmt;
    }
	</script>
</body>
</html>