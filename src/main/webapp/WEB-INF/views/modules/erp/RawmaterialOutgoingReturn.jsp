<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>原料外发加工返回情况</title>
<style type="text/css">
.row {
	margin-bottom: 8px;
}
.row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
.row input,select {
	width: 160px;
}
.row .column {
	margin-right: 10px;
}
.row .columnLast {
	margin-right: 0px;
}
.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
.radiobutton{
	cursor: pointer;
    overflow: hidden;
    vertical-align: middle;
    width: 14px;
    height: 14px;
    margin: 0;
    padding: 0;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:245px;">
		<form id="fm" method="post" style="margin-left: 10px;">
			<div class="row" style="margin-top: 10px;">
				<span class="column">
					<label for="fm_outNo">申请单编号</label> 
					<input id="fm_outNo" name="outNo" class="easyui-textbox">
				</span>
				<span class="column">
					<label for="fm_outDate">出库日期</label>
					<input id="fm_outDate" name="outDate" class="easyui-datebox" data-options="readonly:true">
				</span>
				<span class="columnLast">
					<label for="fm_orderNo">订单编号</label> 
					<input id="fm_orderNo" name="orderNo" class="easyui-textbox" data-options="readonly:true">
				</span>
			</div>
			
			<div class="row">
				<span class="column">
					<label for="fm_rawmaterialCode">原料编码</label> 
					<input id="fm_rawmaterialCode" name="rawmaterialCode" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="column">
					<label for="fm_name">原料名称</label> 
					<input id="fm_name" name="name" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="columnLast">
					<label for="fm_specification">原料规格</label> 
					<input id="fm_specification" name="specification" class="easyui-textbox" data-options="readonly:true">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="fm_rawmaterialType">原料类型</label>
					<input id="fm_rawmaterialType" name="rawmaterialType" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="column">
					<label for="fm_colorNo">原料色号</label>
					<input id="fm_colorNo" name="colorNo" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="columnLast">
					<label for="fm_quantity">出库数量</label>
					<input id="fm_quantity" name="quantity" class="easyui-numberbox" data-options="readonly:true">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="fm_isInvoice">是否开票</label>
					<input id="fm_isInvoice" name="isInvoice" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="column">
					<label for="fm_unitPrice">原料单价</label>
					<input id="fm_unitPrice" name="unitPrice" class="easyui-textbox" data-options="readonly:true">
				</span>
				<span class="columnLast">
					<label for="fm_aggregateAmount">应付金额</label>
					<input id="fm_aggregateAmount" name="aggregateAmount" class="easyui-textbox" data-options="readonly:true">
				</span>
			</div>
			<div style="margin-bottom:8px">
				<label>是否先下订单后原料出库：</label>
				<label for="fm_firstOrderByYes" style="width: 15px;cursor: pointer;margin-right: 10px;vertical-align: middle;">
					<input id="fm_firstOrderByYes" class="radiobutton" name="isFirstOrder" type="radio" value="1" disabled="disabled"/>是
				</label>
				
				<label for="fm_firstOrderByNo" style="width: 15px;cursor: pointer;margin-right: 0px;vertical-align: middle;">
					<input id="fm_firstOrderByNo" class="radiobutton" name="isFirstOrder" type="radio" value="2" disabled="disabled"/>否
				</label>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="fm_remarks">备注信息</label> 
					<input id="fm_remarks" name="remarks" style="width:655px;height:80px" class="easyui-textbox" data-options="readonly:true,multiline:true">
				</span>
			</div>
			
			<!-- 隐藏属性值 -->
			<div style="display: none;">
				<input id="fm_outType" name="outType" value="2">
				<input id="fm_auditorState" name="auditorState" value="1">
				<input id="fm_isHistory" name="isHistory" value="1">
				<input id="fm_isChange" name="isChange" value="1">
				
				<input name="id">
				<input name="delFlag">
				<input name="companyId">
				<input name="orgId">
				<input name="createBy">
				<input name="createDate">
				<input name="updateBy">
				<input name="updateDate">
				<input name="uuid">
			</div>
		</form>
	</div>
	
	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="rawmaterialReturnData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'check',checkbox:true,hidden:true">选择</th>
					<th data-options="field:'rawmaterialCode',width:150,align:'left'">原料编码</th>
					<th data-options="field:'name',width:150,align:'left'">原料名称</th>
					<th data-options="field:'specification',width:100,align:'left'">原料规格</th>
					<th data-options="field:'rawmaterialType',width:100,align:'left'">原料类型</th>
					<th data-options="field:'colorNo',width:100,align:'left'">原料色号</th>
					<th data-options="field:'returnQuantity',width:100,align:'left'">返回数量</th>
					<th data-options="field:'lossQuantity',width:100,align:'left'">损耗数量</th>
					
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:150,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchRawmaterialReturn();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="newRawmaterialReturn();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增返回原料</a>
			<a onclick="editRawmaterialReturn();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">修改返回原料</a>
			<a onclick="delRawmaterialReturn();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除返回原料</a>
	    </div>
	</div>

	<!-- 增加返回原料-对话框 -->
	<div id="dlg" style="width: 520px; height: 280px; padding: 5px 10px">
		<form id="dlg-fm" method="post">
			<div class="row" style="margin-top: 10px;">
				<span class="column">
					<label for="dialog_rawmaterialCode" style="width: 60px;">原料编码</label> 
					<input id="dialog_rawmaterialCode" name="rawmaterialCode" class="easyui-combogrid">
				</span>
				<span class="columnLast">
					<label for="dialog_name" style="width: 60px;">原料名称</label> 
					<input id="dialog_name" name="name" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="dialog_specification" style="width: 60px;">原料规格</label> 
					<input id="dialog_specification" name="specification" class="easyui-textbox">
				</span>
				<span class="columnLast">
					<label for="dialog_rawmaterialType" style="width: 60px;">原料类型</label>
					<input id="dialog_rawmaterialType" name="rawmaterialType" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="dialog_colorNo" style="width: 60px;">原料色号</label> 
					<input id="dialog_colorNo" name="colorNo" class="easyui-textbox" >
				</span>
				<span class="columnLast">
					<label for="dialog_returnQuantity" style="width: 60px;">返回数量</label> 
					<input id="dialog_returnQuantity" name="returnQuantity" class="easyui-numberbox">
				</span>
			</div>
			<div class="row">
				<span class="columnLast">
					<label for="dialog_lossQuantity" style="width: 60px;">损耗数量</label> 
					<input id="dialog_lossQuantity" name="lossQuantity" class="easyui-numberbox">
				</span>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="dialog_remarks" style="width: 60px;">备注信息</label> 
					<input id="dialog_remarks" name="remarks" style="width:400px;height:60px" class="easyui-textbox">
				</span>
			</div>
			
			<!-- 隐藏属性值 -->
			<div style="display: none;">
				<input id="dialog_outId" name="outId">
				
				<input name="id">
				<input name="delFlag">
				<input name="companyId">
				<input name="orgId">
				<input name="createBy">
				<input name="createDate">
				<input name="updateBy">
				<input name="updateDate">
				<input name="uuid">
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="saveRawmaterialReturn();" style="width:90px">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">取消</a>
	</div>

	<script type="text/javascript">
	// 根据原料编码模糊查询原料信息，使用分页
	var findRawMaterialInfoByParamUrl = "${pageScope.mvcPath}/erp/findRawMaterialInfoByParam";
	// 使用模糊查询外发加工出库单
	var findRawmaterialOutByPageUrl = "${pageScope.mvcPath}/erp/findRawmaterialOutByCombobox";
	// 查询
	var findRawmaterialReturnByOutIdUrl = "${pageScope.mvcPath}/erp/findRawmaterialReturnByOutId";
	// 新增 
	var addRawmaterialReturnUrl = "${pageScope.mvcPath}/erp/addRawmaterialReturn";
	// 更新
	var updateRawmaterialReturnUrl = "${pageScope.mvcPath}/erp/updateRawmaterialReturn";
	// 删除
	var deleteRawmaterialReturnUrl = "${pageScope.mvcPath}/erp/deleteRawmaterialReturn";
	// 保存
	var saveRawmaterialReturnUrl = "";
	
	// 原料出库单
	var rawmaterialOut = null;
	
	$(document).ready(function () {
		$("#fm_outNo").combobox({
			required : true,
			prompt : "输入原料出库申请单编号",
			editable : true,
			delay : 200,
			panelHeight : 200,
			mode : "remote",
			url : findRawmaterialOutByPageUrl,
			valueField : "out_no",
			textField : "out_no",
			formatter: function(row) {
				var div = $("<div/>");
				var tmp = $("<span style='color:#080'/>").html(row.out_no);
				div.append(tmp);
				div.append("<br>");
				tmp = $("<span style='color:#008'/>").html(row.name);
				div.append(tmp);
				return div.html();
			},
			onHidePanel : function() {
				var value = $("#fm_outNo").combobox("getValue");
				var text = $("#fm_outNo").combobox("getText");
				var data = $("#fm_outNo").combobox("getData");
				var flag = false;
				$(data).each(function (index, row){
					if(value == row.out_no && text == row.out_no) {
						flag = true;
						rawmaterialOut = dataRowConvert(row);
						return false;
					}
				});
				if(flag == false) {
					rawmaterialOut = null;
					$("#fm").form('reset');
					// 清除数据
					$("#rawmaterialReturnData").datagrid("loadData", { total: 0, rows: [] });
				} else {
					$('#fm').form('load', rawmaterialOut);
					// 加载数据
					$("#rawmaterialReturnData").datagrid('load');
				}
			}
		});
		
		$("#rawmaterialReturnData").datagrid({
			url : findRawmaterialReturnByOutIdUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			checkOnSelect : false,
			selectOnCheck : false,
			nowrap : true,
			pagination : false,
			showFooter : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onBeforeLoad : function (param) {
				// 增加查询参数
				if(rawmaterialOut == null) {
					return false;
				} else {
					param.outId = rawmaterialOut.id;
				}
				$("#rawmaterialReturnData").datagrid("clearSelections");
			}
		});
		
		$("#dlg").dialog({
			title : "外发加工返回原料",
			closed : true,
			resizable : true,
			minWidth : 520,
			minHeight : 290,
			maxWidth : 520,
			maxHeight : 290,
			modal : true,
			buttons : "#dlg-buttons"
		});
		
		$("#dialog_rawmaterialCode").combogrid({
			required : true,
			prompt : "输入编码或名称检索",
			loadMsg : "正在加载，请稍候...",
			editable : true,
			delay : 200,
			panelWidth : 500,
			panelHeight : 245,
			fitColumns : true,
			mode : "remote",
			url : findRawMaterialInfoByParamUrl,
			idField : "rawmaterialCode",
			textField : "rawmaterialCode",
			columns: [[
			           {field:"rawmaterialCode",title:"原料编码",width:120},
			           {field:"name",title:"原料名称",width:120},
			           {field:"specification",title:"原料规格",width:120},
			           {field:"rawmaterialType",title:"原料类型",width:120},
			           {field:"colorNo",title:"原料色号",width:120}
			       ]],
			onSelect : function(index, row) {
				$("#dialog_name").textbox("setValue", row.name);
				$("#dialog_specification").textbox("setValue", row.specification);
				$("#dialog_rawmaterialType").textbox("setValue", row.rawmaterialType);
				$("#dialog_colorNo").textbox("setValue", row.colorNo);
			},
	        onChange : function(newValue, oldValue) {
	        	if($.trim(newValue) == "") {
	        		$("#dialog_name").textbox("clear");
		     	    $("#dialog_specification").textbox("clear");
		     	    $("#dialog_rawmaterialType").textbox("clear");
		     	    $("#dialog_colorNo").textbox("clear");
	     	    }
	        },
	        onHidePanel : function() {
	        	   var value = $("#dialog_rawmaterialCode").combogrid("getValue");
	        	   var text = $("#dialog_rawmaterialCode").combogrid("getText");
	        	   var data = $("#dialog_rawmaterialCode").combogrid("grid").datagrid("getData");
	        	   var flag = false;
		    		$(data.rows).each(function (index, row){
		    			if(value == row.rawmaterialCode && text == row.rawmaterialCode) {
		    				flag = true;
		    				return false;
		    			}
		    		});
		    		if(flag == false) {
		    			$("#dialog_rawmaterialCode").combogrid("clear");
		    		}
	        }
		});
		
		$("#dialog_name").textbox({
			prompt : "自动带出",
			required : false,
			readonly : true
		});
		
		$("#dialog_specification").textbox({
			prompt : "自动带出",
			required : false,
			readonly : true
		});
		
		$("#dialog_rawmaterialType").textbox({
			prompt : "自动带出",
			required : false,
			readonly : true
		});
		
		$("#dialog_colorNo").textbox({
			prompt : "自动带出",
			required : false,
			readonly : true
		});
		
		$("#dialog_remarks").textbox({
			multiline : true,
			required : false,
			readonly : false
		});
		
		$("#dialog_returnQuantity").numberbox({
			required : false,
			readonly : false,
			precision : 2,
			min : 0,
			prompt : "单位：米",
			suffix : "米"
		});
		
		$("#dialog_lossQuantity").numberbox({
			required : false,
			readonly : false,
			precision : 2,
			min : 0,
			prompt : "单位：米",
			suffix : "米"
		});
	});
	
	// 查询 
	function searchRawmaterialReturn(){
		$("#rawmaterialReturnData").datagrid('reload');
	}
	
	// 新增
	function newRawmaterialReturn() {
		if($("#fm_outNo").combobox("isValid") == false) {
			$("#fm_outNo").combobox("validate");
			$.messager.alert("提示", "请先选择原料出库单！", "info");
			return ;
		}
		saveRawmaterialReturnUrl = addRawmaterialReturnUrl;
		$('#dlg').dialog('open').dialog('setTitle', '增加外发加工返回原料');
		$('#dlg-fm').form('reset');
		$("#dialog_outId").val(rawmaterialOut.id);
	}
	
	// 编辑
	function editRawmaterialReturn() {
		if($("#fm_outNo").combobox("isValid") == false) {
			$("#fm_outNo").combobox("validate");
			$.messager.alert("提示", "请先选择原料出库单！", "info");
			return ;
		}
		saveRawmaterialReturnUrl = updateRawmaterialReturnUrl;
		var row = $('#rawmaterialReturnData').datagrid('getSelected');
		if (row == null) {
			$.messager.alert("提示", "未选择要编辑的数据！", "info");
			return;
		}
		if (row) {
			$('#dlg').dialog('open').dialog('setTitle', '编辑外发加工返回原料');
			$('#dlg-fm').form('load', row);
		}
	}
	
	// 删除
	function delRawmaterialReturn() {
		if($("#fm_outNo").combobox("isValid") == false) {
			$("#fm_outNo").combobox("validate");
			$.messager.alert("提示", "请先选择原料出库单！", "info");
			return ;
		}
		
		var row = $("#rawmaterialReturnData").datagrid("getSelected");
		if (row == null) {
			$.messager.alert("提示", "未选择要删除的数据！", "info");
			return;
		}
		$.messager.confirm("确认删除", "确定要删除外发加工返回原料信息?<br/>外发加工单号：" + rawmaterialOut.outNo + "<br/>原料名称：" + row.name, function (r) {
			if (r) {
				$.post(deleteRawmaterialReturnUrl, row, function (data) {
					if (data.success) {
						// 删除成功
						$.messager.show({ title : '提示', msg : data.message, timeout : 5000, showType : 'slide' });
						$("#rawmaterialReturnData").datagrid('reload');
					} else {
						// 删除失败
						$.messager.alert("提示", data.message, "warning");
					}
				}, "json");
			}
		});
	}
	
	// 保存
	function saveRawmaterialReturn() {
		$("#dlg-fm").form("submit", {
			url : saveRawmaterialReturnUrl,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({ title : '提示', msg : data.message, timeout : 5000, showType : 'slide' });
					$("#rawmaterialReturnData").datagrid('reload');
				} else {
					// 保存失败
					$.messager.alert("提示", data.message, "warning");
				}
			}
		});
	}
	
	// 数据转换
	function dataRowConvert(row)
	{
		if(row == null) {
			return null;
		}
		var newRow = {};
		newRow.outNo = row.out_no;
		newRow.outDate = new Date(row.out_date).format("yyyy-MM-dd hh:mm:ss");
        newRow.outType = row.out_type;
        newRow.orderNo = row.order_no;
        newRow.isFirstOrder = row.is_first_order;
		newRow.rawmaterialCode = row.rawmaterial_code;
		newRow.name = row.name;
		newRow.specification = row.specification;
		newRow.rawmaterialType = row.rawmaterial_type;
		newRow.colorNo = row.color_no;
		newRow.quantity = row.quantity;
		newRow.remarks = row.remarks;
		newRow.auditorId = row.auditor_id;
		newRow.auditorState = row.auditor_state;
		newRow.unitPrice = row.unit_price;
		newRow.aggregateAmount = row.aggregate_amount;
		newRow.isInvoice = row.is_invoice;
		newRow.isHistory = row.is_history;
		newRow.id = row.id;
		newRow.delFlag = row.del_flag;
		newRow.companyId = row.company_id;
		newRow.orgId = row.org_id;
		newRow.createBy = row.create_by;
		newRow.createDate = new Date(row.create_date).format("yyyy-MM-dd hh:mm:ss");
		newRow.updateBy = row.update_by;
		newRow.updateDate = new Date(row.update_date).format("yyyy-MM-dd hh:mm:ss");
		newRow.uuid = row.uuid;
		return newRow;
	}
	
    function formatDate(value,row,index) {
    	var date = new Date(value);
    	return date.format("yyyy-MM-dd");
    }
	
    // 日期格式化
    Date.prototype.format = function (fmt) {
    	var o = {
    		"M+" : this.getMonth() + 1, //月份
    		"d+" : this.getDate(), //日
    		"h+" : this.getHours(), //小时
    		"m+" : this.getMinutes(), //分
    		"s+" : this.getSeconds(), //秒
    		"q+" : Math.floor((this.getMonth() + 3) / 3), //季度
    		"S" : this.getMilliseconds() //毫秒
    	};
    	if (/(y+)/.test(fmt)){
    		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    	}
    	for (var k in o) {
    		if (new RegExp("(" + k + ")").test(fmt)) {
    			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    		}
    	}
    	return fmt;
    }
	</script>
</body>
</html>