<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<!-- ECharts单文件引入 -->
<script type="text/javascript" src="${pageScope.staticPath}/ECharts/dist/echarts.js"></script>
<title>JSP视图测试</title>
</head>
<body>
	<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
	<div id="main" style="height:400px;width: 400px;"></div>
	
	<script type="text/javascript">
		var imgURL = null; 
	
		 // 路径配置
		require.config({
			paths: {
				echarts: '${pageScope.staticPath}/ECharts/dist'
			}
		});
		 // 使用
		require(
			[
				'echarts',
				'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
			],
			function(ec) {
				// 基于准备好的dom，初始化echarts图表
				var myChart = ec.init(document.getElementById('main'));
				var option = {
					tooltip: {
						show: true
					},
					legend: {
						data: ['销量']
					},
					xAxis: [{
						type: 'category',
						data: ["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"]
					}],
					yAxis: [{
						type: 'value'
					}],
					series: [{
						"name": "销量",
						"type": "bar",
						"data": [1, 2, 3, 4, 5, 45]
					}],
					animation: false,
				};
				// 为echarts对象加载数据 
				myChart.setOption(option);
				imgURL = myChart.getDataURL('png'); //获取base64编码
				
				// var imgObj = myChart.getImage("png");
				// var ss=imgObj.outerHTML;
				// $("#Q123").append(ss);
			}
		);
		 
		function getImg() {
			var data = {};
			data.imagedata = imgURL;
			$.post("${pageScope.mvcPath}/sys/imagedata",data);
		}
	</script>

	<a id="Q123" href="javascript:void(0)" onclick="getImg()">图片</a>
</body>
</html>