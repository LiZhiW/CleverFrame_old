<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>上传文件管理</title>
<style type="text/css">
#fm-search #fm {
	margin: 0;
	padding: 0px 5px;
}
#fm-search .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#fm-search .row {
	margin-bottom: 8px;
}
#fm-search .row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
#fm-search .row input,select {
	width: 160px;

}
#fm-search .row .column {
	margin-right: 10px;
}
#fm-search .row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:70px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="startTime-search">开始时间</label> 
					<input id="startTime-search" name="startTime-search" class="easyui-datetimebox">
				</span>
				<span class="column">
					<label for="endTime-search">结束时间</label> 
					<input id="endTime-search" name="endTime-search" class="easyui-datetimebox">
				</span>
				<span class="columnLast">
					<label for="fileName-search">上传文件名</label> 
					<input id="fileName-search" name="fileName-search" class="easyui-textbox">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="digest-search">文件签名</label>
					<input id="digest-search" name="digest-search" class="easyui-textbox" style="width: 410px">
				</span>
				<span class="columnLast">
					<label for="newName-search">文件名</label> 
					<input id="newName-search" name="newName-search" class="easyui-textbox" style="width: 410px">
				</span> 
			</div>
		</form>
	</div>
	
	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="fileData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'fileName',width:260,align:'left',formatter:fileNameFormatter">上传文件名</th>
					<th data-options="field:'newName',width:300,align:'left'">文件名</th>
					<th data-options="field:'uploadTime',width:60,align:'left',formatter:timeFormatter">上传用时</th>
					<th data-options="field:'storedTime',width:60,align:'left',formatter:timeFormatter">存储用时</th>
                    <th data-options="field:'fileSize',width:80,align:'left',formatter:fileSizeFormatter">文件大小</th>
					<th data-options="field:'digestType',width:60,align:'left'">签名类型</th>
                    <th data-options="field:'digest',width:240,align:'left'">文件签名</th>
                    <th data-options="field:'filePath',width:200,align:'left'">文件路径</th>
                    <th data-options="field:'storedType',width:60,align:'left'">存储类型</th>
                    <th data-options="field:'download',width:60,align:'left',formatter:downloadFormatter">下载地址</th>
                    
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的编码</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的编码</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'createDate',width:100,align:'left',hidden:true">创建时间</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:130,align:'left',hidden:false">更新时间</th>
					<th data-options="field:'remarks',width:260,align:'left',hidden:true">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchFileInfo();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="delFile();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除文件</a>
	    </div>
	</div>
	
	<script type="text/javascript">
    // 文件数据地址
    var fileDataUrl = "${pageScope.mvcPath}/fileupload/findFileInfo";
    // 下载文件地址. url + 文件UUID
    var downloadFileUrl = "${pageScope.mvcPath}/fileupload/fileDownload/";
    // 删除文件地址. url + 文件UUID
    var delFileUrl = "${pageScope.mvcPath}/fileupload/delete/";
    // 获取某一个文件的信息. url + 文件UUID
    var fileInfoUrl = "${pageScope.mvcPath}/fileupload/getFileInfo/";
    
    $(document).ready(function() {
        // 设置userData数据显示表格
        $("#fileData").datagrid({
            url : fileDataUrl,
            idField : "name",
            fit : true,
            fitColumns : false,
            striped : true,
            rownumbers : true,
            singleSelect : true,
            nowrap : true,
            pagination : true,
            loadMsg : "正在加载，请稍候...",
            toolbar : "#tb",
            onDblClickRow : function (rowIndex, rowData) {
            	// 双击查看明细
            },
            onBeforeLoad : function (param) {
                // 增加查询参数
                var paramArray = $("#fm-search").serializeArray();
                $(paramArray).each(function () {
                    if (param[this.name]) {
                        if ($.isArray(param[this.name])) {
                            param[this.name].push(this.value);
                        } else {
                            param[this.name] = [param[this.name], this.value];
                        }
                    } else {
                        param[this.name] = this.value;
                    }
                });
            }
        });
    });
    
    // 查询文件信息
    function searchFileInfo(){
        $("#fileData").datagrid('load');
    }
    
    // 删除文件
    function delFile(){
        var row = $("#fileData").datagrid("getSelected");
        if(row == null){
            $.messager.alert("提示","未选择要删除的文件！","info");
            return ;
        }
        $.messager.confirm("确认删除", "确定要删除文件:<br>[" + row.fileName + "]？", function (r) {
        	var param = {};
        	param.lazy = "false";
            if (r) {
                $.post(delFileUrl + row.uuid, param, function (data) {
                    if (data.success) {
                        // 删除成功
                        $.messager.show({
                            title : '提示',
                            msg : data.message,
                            timeout : 5000,
                            showType : 'slide'
                        });
                        $("#fileData").datagrid('reload');
                    } else {
                        // 删除失败
                    }
                }, "json");
            }
        });
    }
    
    function downloadFormatter(value,row,index){
    	row.download = downloadFileUrl + row.uuid
    	value = row.download;
    	return "<a target='_blank' href='" + value + "'>下载</a>";
    }
    
    function fileNameFormatter(value,row,index){
    	var url = downloadFileUrl + row.uuid;
    	return "<a target='_blank' href='" + url + "'>" + value + "</a>";
    }
    
    function fileSizeFormatter(value,row,index){
    	var sizeByte = value;
    	var sizeKB = (sizeByte / 1024.0).toFixed(2);
    	if(sizeKB < 1){
    		return sizeByte + "B";
    	}
    	
    	var sizeMB = (sizeKB / 1024.0).toFixed(2);
    	if(sizeMB < 1){
    		return sizeKB + "KB";
    	}
    	
    	var sizeGB = (sizeMB / 1024.0).toFixed(2);
    	if(sizeGB < 1){
    		return sizeMB + "MB";
    	}
    	return sizeGB + "GB";
    }
    
    function timeFormatter(value,row,index){
    	var timeS = value / 1000.0
    	return timeS + "秒"
    }
	</script>
</body>
</html>