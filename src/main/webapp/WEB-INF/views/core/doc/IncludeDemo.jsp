<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%-- 加入JSTL标准库 --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- 加入自定义标签库 --%>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>

<%-- 自定义page范围的属性 --%>
<c:set scope="page" var="appPath" value="${pageContext.request.contextPath}" />
<c:set scope="page" var="staticPath" value="${pageContext.request.contextPath}/${fns:getStaticPath()}" />
<c:set scope="page" var="docPath" value="${pageContext.request.contextPath}/${fns:getDocPath()}" />
<c:set scope="page" var="viewsPath" value="${pageContext.request.contextPath}/${fns:getViewsPath()}" />
<c:set scope="page" var="mvcPath" value="${pageContext.request.contextPath}/${fns:getMvcPath()}" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="${pageScope.staticPath}/Bootstrap/css/bootstrap.min.css">
<!-- 可选的Bootstrap主题文件（一般不用引入） -->
<link rel="stylesheet" href="${pageScope.staticPath}/Bootstrap/css/bootstrap-theme.min.css">
<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="${pageScope.staticPath}/JQuery/jquery-1.11.2.min.js"></script>
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="${pageScope.staticPath}/Bootstrap/js/bootstrap.min.js"></script>

<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>

<!-- JQuery -->
<script type="text/javascript" src="${pageScope.staticPath}/JQuery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/JQuery/jquery-2.1.3.min.js"></script>

<!-- ECharts单文件引入 -->
<script type="text/javascript" src="${pageScope.staticPath}/ECharts/dist/echarts.js"></script>

<!-- zTree -->


<title>系统引用文件Demo</title>
</head>
<body>

pageScope.appPath: ${pageScope.appPath}<br/>
pageScope.staticPath: ${pageScope.staticPath}<br/>
pageScope.docPath: ${pageScope.docPath}<br/>
pageScope.viewsPath: ${pageScope.viewsPath}<br/>
pageScope.mvcPath: ${pageScope.mvcPath}<br/>
<hr/>

</body>
</html>