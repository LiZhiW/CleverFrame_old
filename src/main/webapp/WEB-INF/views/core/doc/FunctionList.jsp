<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%-- 加入JSTL标准库 --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- 加入自定义标签库 --%>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>

<%-- 自定义page范围的属性 --%>
<c:set scope="page" var="appPath" value="${pageContext.request.contextPath}" />
<c:set scope="page" var="staticPath" value="${pageContext.request.contextPath}/${fns:getStaticPath()}" />
<c:set scope="page" var="docPath" value="${pageContext.request.contextPath}/${fns:getDocPath()}" />
<c:set scope="page" var="viewsPath" value="${pageContext.request.contextPath}/${fns:getViewsPath()}" />
<c:set scope="page" var="mvcPath" value="${pageContext.request.contextPath}/${fns:getMvcPath()}" />

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>系统功能列表</title>
<!-- 引用基本CSS、JavaScript文件 -->

</head>
<body>

<a target="_blank" href="${pageScope.mvcPath}/sys/loginJsp">用户登入</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/sys/logout">用户退出</a><br/><br/>

<a target="_blank" href="${pageScope.mvcPath}/sys/getSysMainJsp">sys模块管理页面</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/common/getQLScriptJsp">数据库脚本管理</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/sys/getDictJsp">字典管理</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/sys/getSysOrganizationJsp">机构管理</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/sys/getSysUserJsp">用户管理</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/sys/getSysRoleJsp">角色管理</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/sys/getSysMenuJsp">菜单管理</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/sys/getSysPermissionJsp">权限管理</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/sys/getSysMDictJsp">多级字典管理</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/sys/getSysUserRoleMenuPermission">用户角色权限管理</a><br/><br/>

<a target="_blank" href="${pageScope.viewsPath}/modules/codebuild/CodeBuildMain.html">代码生成主界面</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/monitor/monitorMain">服务器监控</a><br/><br/>

<a target="_blank" href="${pageScope.mvcPath}/fileupload/demo">文件上传Demo</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/fileupload/fileDownload/1">文件下载</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/fileupload/delete/1?lazy=true">文件删除</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/fileupload/getFileInfo/1">文件信息</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/fileupload/fileuploadProgress">上传文件进度信息地址</a><br/>
<a target="_blank" href="${pageScope.mvcPath}/fileupload/fileManager">文件管理</a><br/>

<hr/>
<!-- ERP系统页面 -->
<a target="_blank" href="${pageScope.mvcPath}/erp/getErpMainJsp">ERP系统主页</a><br/>


</body>
</html>