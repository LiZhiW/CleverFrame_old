<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Cache-Control" content="no-store"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>流程设计</title>
<!-- Raphael-1.5.2(Snaker Web设计器依赖js库) -->
<script type="text/javascript" src="${pageScope.staticPath}/snaker/raphael-min.js"></script>
<!-- jQuery -->
<script type="text/javascript" src="${pageScope.staticPath}/JQuery/jquery-1.11.2.min.js"></script>
<!-- jQueryUI(Snaker Web设计器依赖js库) -->
<link rel="stylesheet" href="${pageScope.staticPath}/jQueryUI/jquery-ui.min.css" />
<script type="text/javascript" src="${pageScope.staticPath}/jQueryUI/jquery-ui.min.js"></script>
<!-- Snaker Web设计器 -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/snaker/snaker.css">
<script type="text/javascript" src="${pageScope.staticPath}/snaker/snaker.designer.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/snaker/snaker.model.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/snaker/snaker.editors.js"></script>
<script type="text/javascript">
	var basePath = "${pageScope.staticPath}/snaker/";
	var ctxPath = "${pageScope.staticPath}";
	var formPath = "forms/";
	
	var saveModelUrl = "${pageScope.mvcPath}/wfsnaker/deployXml";
	var saveModelSuccessUrl = "${ctx}/snaker/process/list";
	
	$(document).ready(function () {
		var json = "${process }";
		var model;
		if (json) {
			//json.replace(new RegExp("@@","gm"), "\"")
			model = eval("(" + json + ")");
		} else {
			model = "";
		}
		$('#snakerflow').snakerflow({
			basePath : basePath,
			ctxPath : ctxPath,
			restore : model,
			formPath : formPath,
			tools : {
				save : {
					onclick : function (data) {
						saveModel(data);
					}
				}
			}
		});
	});
	
	function saveModel(data) {
		alert(data);
		$.ajax({
			type : 'POST',
			url : saveModelUrl,
			data : "model=" + data + "&processId=${processId}",
			async : false,
			globle : false,
			error : function () {
				alert('数据处理错误！');
				return false;
			},
			success : function (data) {
				if (data.success == true) {
					// window.location.href = saveModelSuccessUrl;
				} else {
					alert('数据处理错误！');
				}
			}
		});
	}
</script>					
</head>
<body>
	<div id="toolbox">
		<div id="toolbox_handle">工具集-(可拖动)</div>
		<div class="node" id="save"><img src="${pageScope.staticPath}/snaker/images/save.gif" />&nbsp;&nbsp;保存</div>
		<div>
			<hr />
		</div>
		<div class="node selectable" id="pointer">
		    <img src="${pageScope.staticPath}/snaker/images/select16.gif" />&nbsp;&nbsp;Select
		</div>
		<div class="node selectable" id="path">
		    <img src="${pageScope.staticPath}/snaker/images/16/flow_sequence.png" />&nbsp;&nbsp;transition
		</div>
		<div>
			<hr/>
		</div>
		<div class="node state" id="start" type="start">
			<img src="${pageScope.staticPath}/snaker/images/16/start_event_empty.png" />&nbsp;&nbsp;start
		</div>
		<div class="node state" id="end" type="end">
			<img src="${pageScope.staticPath}/snaker/images/16/end_event_terminate.png" />&nbsp;&nbsp;end
		</div>
		<div class="node state" id="task" type="task">
			<img src="${pageScope.staticPath}/snaker/images/16/task_empty.png" />&nbsp;&nbsp;task
		</div>
		<div class="node state" id="task" type="custom">
			<img src="${pageScope.staticPath}/snaker/images/16/task_empty.png" />&nbsp;&nbsp;custom
		</div>
		<div class="node state" id="task" type="subprocess">
			<img src="${pageScope.staticPath}/snaker/images/16/task_empty.png" />&nbsp;&nbsp;subprocess
		</div>
		<div class="node state" id="fork" type="decision">
			<img src="${pageScope.staticPath}/snaker/images/16/gateway_exclusive.png" />&nbsp;&nbsp;decision
		</div>
		<div class="node state" id="fork" type="fork">
			<img src="${pageScope.staticPath}/snaker/images/16/gateway_parallel.png" />&nbsp;&nbsp;fork
		</div>
		<div class="node state" id="join" type="join">
			<img src="${pageScope.staticPath}/snaker/images/16/gateway_parallel.png" />&nbsp;&nbsp;join
		</div>
	</div>
	
	<div id="properties">
		<div id="properties_handle">属性-(可拖动)</div>
		<table class="properties_all" cellpadding="0" cellspacing="0"></table>
		<div>&nbsp;</div>
	</div>
	
	<div id="snakerflow"></div>
</body>
</html>
