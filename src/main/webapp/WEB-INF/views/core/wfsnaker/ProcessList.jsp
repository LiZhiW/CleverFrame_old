<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>流程定义管理</title>
<style type="text/css">
#fm-search #fm {
	margin: 0;
	padding: 0px 5px;
}
#fm-search .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#fm-search .row {
	margin-bottom: 8px;
}
#fm-search .row label {
	display: inline-block;
	width: 80px;
	text-align: center;
}
#fm-search .row input,select {
	width: 160px;

}
#fm-search .row .column {
	margin-right: 10px;
}
#fm-search .row .columnLast {
	margin-right: 0px;
}
a.btnStart {
    background: url(${pageScope.staticPath}/Icon/16x16/flow-start.png) no-repeat;
    display: block;
    width: 22px;
    height: 20px;
    text-indent: -1000px;
    overflow: hidden;
    float: left;
    margin-right: 3px;
}
a.btnEdit {
    background: url(${pageScope.staticPath}/Icon/16x16/edit.png) no-repeat;
    display: block;
    width: 22px;
    height: 20px;
    text-indent: -1000px;
    overflow: hidden;
    float: left;
    margin-right: 3px;
}
a.btnDesigner {
    background: url(${pageScope.staticPath}/Icon/16x16/application-x-designer.png) no-repeat;
    display: block;
    width: 22px;
    height: 20px;
    text-indent: -1000px;
    overflow: hidden;
    float: left;
    margin-right: 3px;
}
a.btnEnable {
    background: url(${pageScope.staticPath}/Icon/16x16/dialog-ok-apply-2.png) no-repeat;
    display: block;
    width: 22px;
    height: 20px;
    text-indent: -1000px;
    overflow: hidden;
    float: left;
    margin-right: 3px;
}
a.btnDisabled {
    background: url(${pageScope.staticPath}/Icon/16x16/lightbulb_off_disabled.png) no-repeat;
    display: block;
    width: 22px;
    height: 20px;
    text-indent: -1000px;
    overflow: hidden;
    float: left;
    margin-right: 3px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:40px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="columnLast">
					<label for="displayName_search">流程显示名称</label> 
					<input id="displayName_search" name="displayName" class="easyui-textbox" style="width: 420px">
				</span>
			</div>
		</form>
	</div>
	
	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="processData" data-options="border:false">
			<thead>
				<tr>
                    <th data-options="field:'name',width:100,align:'left'">流程名称</th>
					<th data-options="field:'displayName',width:150,align:'left'">流程显示名称</th>
                    <th data-options="field:'type',width:80,align:'left'">流程类型</th>
                    <th data-options="field:'state',width:80,align:'left'">流程是否可用</th>
                    <th data-options="field:'version',width:80,align:'left'">版本</th>
                    <th data-options="field:'instanceUrl',width:80,align:'left'">实例url</th>
 					<th data-options="field:'createTime',width:80,align:'left',hidden:true">创建时间</th>
                    <th data-options="field:'creator',width:80,align:'left',hidden:true">创建人</th>
                    <th data-options="field:'operate',width:120,formatter:operateFormatter">操作</th>
                    
                    <th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
                    <th data-options="field:'content',width:80,align:'left',hidden:true">流程模型定义</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchProcess();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="newProcess();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
	    </div>
	</div>
	
	
	<script type="text/javascript">
	// 查询流程URL
	var findProcessByPageUrl = "${pageScope.mvcPath}/wfsnaker/findProcessByPage";
	
	$(document).ready(function () {
		// 设置processData数据显示表格
		$("#processData").datagrid({
			url : findProcessByPageUrl,
			idField : "id",
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				//双击查看流程图
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			}
		});
	});
	
	// 查询
	function searchProcess() {
		$("#processData").datagrid("load");
	}
	
	// 操作流程：启动流程、编辑流程、设计流程、启用/禁用流程
	function operateFormatter(value, row, index) {
		var designUrl = "${pageScope.mvcPath}/wfsnaker/getProcessDesignerJsp?processId=" + row.id;
		
		var startUp = "<a class='btnStart' target='_blank' href='javascript:void(0)' title='启动'>启动<a>";
		var edit = "<a class='btnEdit' target='_blank' href='javascript:void(0)' title='编辑'>编辑<a>";
		var design = "<a class='btnDesigner' target='_blank' href='" + designUrl + "' title='设计'>设计<a>";
		var state = "";
		if (1 == row.state) {
			state = "<a class='btnDisabled' target='_blank' href='javascript:void(0)' title='禁用'>禁用<a>";
		} else {
			state = "<a class='btnEnable' target='_blank' href='javascript:void(0)' title='启用'>启用<a>";
		}
		return startUp + edit + design + state;
	}
	</script>
</body>
</html>