<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%@ taglib prefix="monitor" tagdir="/WEB-INF/tags/core/monitor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%-- <%@ include file="/WEB-INF/views/include/EasyUI.jsp"%> --%>
<script type="text/javascript" src="${pageScope.staticPath}/JQuery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/ECharts/dist/echarts.js"></script>
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="${pageScope.staticPath}/Bootstrap/css/bootstrap.min.css">
<title>Redis监控</title>
</head>
<body>
	<monitor:RedisMonitorHead 
		currentUrl="chartsUrl"
		keysUrl="/CodeGenerator/mvc/monitor/redis/redisKeysMonitor" 
		infoUrl="/CodeGenerator/mvc/monitor/redis/redisInfoMonitor" 
		mainUrl="/CodeGenerator/mvc/monitor/redis/redisChartsMonitor" 
		confUrl="/CodeGenerator/mvc/monitor/redis/redisConfMonitor" 
		chartsUrl="/CodeGenerator/mvc/monitor/redis/redisChartsMonitor" />
	
	<!-- 内容区域 -->
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="col-md-6">
				<!-- Redis当前数据库中key的数量 -->
				<div id="keyCountCharts" style="width: 100%;height: 400px;"></div>
			</div>
			
			<div class="col-md-6">
				<!-- Redis服务器CPU监控 -->
				<div id="redisCpuCharts" style="width: 100%;height: 400px;"></div>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-6">
				<!-- Redis服务器Memory监控 -->
				<div id="redisMemoryCharts" style="width: 100%;height: 400px;"></div>
			</div>
			
			<div class="col-md-6">
				<!-- Redis服务器处理请求速度监控 -->
				<div id="redisCmdVCharts" style="width: 100%;height: 400px;"></div>
			</div>
		</div>
	</div>
	
    <script type="text/javascript">
    var keyCountUrl = "/CodeGenerator/mvc/monitor/redis/getKeyCount";
    var keyCountTimeTicket = null;
    var keyCountCharts = null;
    
    var redisCpuUrl = "/CodeGenerator/mvc/monitor/redis/getRedisInfo?section=Cpu";
    var redisCpuTimeTicket = null;
    var redisCpuCharts = null;
    
    var redisMemoryUrl = "/CodeGenerator/mvc/monitor/redis/getRedisInfo?section=Memory";
    var redisMemoryTimeTicket = null;
    var redisMemoryCharts = null;

    var redisCmdVUrl = "/CodeGenerator/mvc/monitor/redis/getRedisInfo?section=Stats";
    var redisCmdVTimeTicket = null;
    var redisCmdVCharts = null;
    
    require.config({
        paths : {
            echarts : '${pageScope.staticPath}/ECharts/dist'
        }
    });
    require(
        [
            'echarts',
            'echarts/chart/line'
        ],
        function (ec) {
        // 初始化 keyCountCharts
        keyCountCharts = ec.init(document.getElementById('keyCountCharts'));
        keyCountOption = {
			title : {
               	x : 'left',
               	y : 'top',
                   text : 'Redis当前数据库中key的数量'
               },
               tooltip : {
                   trigger : 'axis'
               },
               legend: {
                   data:['key数量']
               },
               xAxis : [{
            	   	   name : '时间',
                       type : 'category',
                       boundaryGap : false,
                       splitNumber : 20,
                       axisTick : {
                           show : true,
                           interval : 0
                       },
                       data : (function () {
                           var now = new Date();
                           var res = [];
                           var len = 20;
                           while (len--) {
                               res.unshift(now.toLocaleTimeString().replace(/^\D*/, ''));
                               now = new Date(now - 2000);
                           }
                           return res;
                       })()
                   }
               ],
               yAxis : [{
            	   	   name : '数量',
                       type : 'value',
                       axisLabel : {
                           formatter : '{value} 个'
                       },
                       splitNumber : 10,
                       axisTick : {
                           show : true,
                           interval : 0
                       },
                   }
               ],
               series : [{
                       name : 'key数量',
                       type : 'line',
                       smooth : true,
                       itemStyle: {normal: {areaStyle: {type: 'default'}}},
                       data : [0, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']
                   }
               ]
           };
            clearInterval(keyCountTimeTicket);
            keyCountTimeTicket = setInterval(function () {
                    $.post(keyCountUrl, function (data) {
                    	var params = new Array();
                        var lastData = new Array();
                        lastData[0] = 0; // seriesIdx
                        lastData[1] = data.object.value; // data
                        lastData[2] = false; // isHead
                       	lastData[3] = false; // dataGrow
                   		lastData[4] = (new Date()).toLocaleTimeString().replace(/^\D*/, ''); // additionData
                   		params[0] = lastData;
                        keyCountCharts.addData(params);
                    }, "json");
                }, 2000);
            keyCountCharts.setOption(keyCountOption);
            
            // 初始化 redisCpuCharts
            redisCpuCharts = ec.init(document.getElementById('redisCpuCharts'));
            redisCpuOption = {
        			title : {
                       	x : 'left',
                       	y : 'top',
                           text : 'Redis服务器CPU监控'
                       },
                       tooltip : {
                           trigger : 'axis'
                       },
                       legend: {
                           data:['used_cpu_sys_children','used_cpu_user_children']
                       },
                       xAxis : [{
                    	       name : '时间',
                               type : 'category',
                               boundaryGap : false,
                               splitNumber : 20,
                               axisTick : {
                                   show : true,
                                   interval : 0
                               },
                               data : (function () {
                                   var now = new Date();
                                   var res = [];
                                   var len = 20;
                                   while (len--) {
                                       res.unshift(now.toLocaleTimeString().replace(/^\D*/, ''));
                                       now = new Date(now - 2000);
                                   }
                                   return res;
                               })()
                           }
                       ],
                       yAxis : [{
                    	   	   name : 'CPU使用率',
                               type : 'value',
                               //max : 150,
                               //min : 0,
                               axisLabel : {
                                   formatter : '{value} %'
                               },
                               splitNumber : 10,
                               axisTick : {
                                   show : true,
                                   interval : 0
                               },
                           }
                       ],
                       series : [
                           {
                               name : 'used_cpu_sys_children',
                               type : 'line',
                               smooth : true,
                               itemStyle: {normal: {areaStyle: {type: 'default'}}},
                               data : [0, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']
                           },
                           {
                               name : 'used_cpu_user_children',
                               type : 'line',
                               smooth : true,
                               itemStyle: {normal: {areaStyle: {type: 'default'}}},
                               data : [0, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']
                           }
                       ]
                   };
            clearInterval(redisCpuTimeTicket);
            redisCpuTimeTicket = setInterval(function () {
                    $.post(redisCpuUrl, function (data) {
                    	var params = new Array();
                    	$(data.object).each(function (index, info){
                    		var lastData = new Array();
                    		// seriesIdx
                    		if(info.key == "used_cpu_sys_children"){
                    			lastData[0] = 0;
                    		} else if(info.key == "used_cpu_user_children"){
                    			lastData[0] = 1;
                    		} else {
                    			return true;
                    		}
                    		lastData[1] = info.value; // data
                            lastData[2] = false; // isHead
                           	lastData[3] = false; // dataGrow
                           	lastData[4] = (new Date()).toLocaleTimeString().replace(/^\D*/, '');
                    		params[lastData[0]] = lastData;
                    	});
                   		redisCpuCharts.addData(params);
                    }, "json");
                }, 2000);
            redisCpuCharts.setOption(redisCpuOption);
            
            // 初始化 redisMemoryCharts
            redisMemoryCharts = ec.init(document.getElementById('redisMemoryCharts'));
            redisMemoryOption = {
        			title : {
                       	x : 'left',
                       	y : 'top',
                           text : 'Redis服务器Memory监控'
                       },
                       tooltip : {
                           trigger : 'axis'
                       },
                       legend: {
                           data:['used_memory','used_memory_rss']
                       },
                       xAxis : [{
                    	       name : '时间',
                               type : 'category',
                               boundaryGap : false,
                               splitNumber : 20,
                               axisTick : {
                                   show : true,
                                   interval : 0
                               },
                               data : (function () {
                                   var now = new Date();
                                   var res = [];
                                   var len = 20;
                                   while (len--) {
                                       res.unshift(now.toLocaleTimeString().replace(/^\D*/, ''));
                                       now = new Date(now - 2000);
                                   }
                                   return res;
                               })()
                           }
                       ],
                       yAxis : [{
                    	   	   name : '内存大小',
                               type : 'value',
                               axisLabel : {
                                   formatter : '{value} MB'
                               },
                               splitNumber : 10,
                               axisTick : {
                                   show : true,
                                   interval : 0
                               },
                           }
                       ],
                       series : [{
                               name : 'used_memory',
                               type : 'line',
                               smooth : true,
                               itemStyle: {normal: {areaStyle: {type: 'default'}}},
                               data : [0, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']
                           },
                           {
                               name : 'used_memory_rss',
                               type : 'line',
                               smooth : true,
                               itemStyle: {normal: {areaStyle: {type: 'default'}}},
                               data : [0, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']
                           }
                       ]
                   };
            clearInterval(redisMemoryTimeTicket);
            redisMemoryTimeTicket = setInterval(function () {
                    $.post(redisMemoryUrl, function (data) {
                    	var params = new Array();
                    	$(data.object).each(function (index, info){
                    		var lastData = new Array();
                    		// seriesIdx
                    		if(info.key == "used_memory"){
                    			lastData[0] = 0;
                    		} else if (info.key == "used_memory_rss"){
                    			lastData[0] = 1;
                    		} else {
                    			return true;
                    		}
                    		lastData[1] = (info.value / 1024.0 / 1024.0).toFixed(2); // data
                            lastData[2] = false; // isHead
                           	lastData[3] = false; // dataGrow
                           	lastData[4] = (new Date()).toLocaleTimeString().replace(/^\D*/, '');
                    		params[lastData[0]] = lastData;
                    	});
                    	redisMemoryCharts.addData(params);
                    }, "json");
                }, 2000);
            redisMemoryCharts.setOption(redisMemoryOption);
            
            // 初始化 redisCmdVCharts
            redisCmdVCharts = ec.init(document.getElementById('redisCmdVCharts'));
            redisCmdVOption = {
        			title : {
                       	x : 'left',
                       	y : 'top',
                           text : 'Redis服务器处理请求速度监控'
                       },
                       tooltip : {
                           trigger : 'axis'
                       },
                       legend: {
                           data:['处理请求速度']
                       },
                       xAxis : [{
                    	       name : '时间',
                               type : 'category',
                               boundaryGap : false,
                               splitNumber : 20,
                               axisTick : {
                                   show : true,
                                   interval : 0
                               },
                               data : (function () {
                                   var now = new Date();
                                   var res = [];
                                   var len = 20;
                                   while (len--) {
                                       res.unshift(now.toLocaleTimeString().replace(/^\D*/, ''));
                                       now = new Date(now - 2000);
                                   }
                                   return res;
                               })()
                           }
                       ],
                       yAxis : [{
                    	   	   name : '速度',
                               type : 'value',
                               axisLabel : {
                                   formatter : '{value} 个/秒'
                               },
                               splitNumber : 10,
                               axisTick : {
                                   show : true,
                                   interval : 0
                               },
                           }
                       ],
                       series : [{
                               name : '处理请求速度',
                               type : 'line',
                               smooth : true,
                               itemStyle: {normal: {areaStyle: {type: 'default'}}},
                               data : [0, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']
                           }
                       ]
                   };
            clearInterval(redisCmdVTimeTicket);
            redisCmdVTimeTicket = setInterval(function () {
                    $.post(redisCmdVUrl, function (data) {
                    	var params = new Array();
                    	$(data.object).each(function (index, info){
                    		var lastData = new Array();
                    		// seriesIdx
                    		if(info.key == "instantaneous_ops_per_sec"){
                    			lastData[0] = 0;
                    		} else {
                    			return true;
                    		}
                    		lastData[1] = info.value; // data
                            lastData[2] = false; // isHead
                           	lastData[3] = false; // dataGrow
                           	lastData[4] = (new Date()).toLocaleTimeString().replace(/^\D*/, '');
                    		params[lastData[0]] = lastData;
                    	});
                    	redisCmdVCharts.addData(params);
                    }, "json");
                }, 2000);
            redisCmdVCharts.setOption(redisCmdVOption);
    });
    </script>
    
</body>
</html>