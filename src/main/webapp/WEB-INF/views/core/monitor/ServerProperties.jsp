<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>系统配置信息</title>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:false" style="height: 60px">
		1234
	</div>

	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="propertiesData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'checkBox',align:'center',checkbox:true">选择</th>
					<th data-options="field:'name',width:100,align:'left'">配置名称</th>
					<th data-options="field:'value',width:500,align:'left'">配置值(String)</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="$('#propertiesData').datagrid('reload');" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" style="margin-left:10px" >刷新</a>
			<input name="attributeName" class="easyui-searchbox" type="text" style="width: 200px" ></input>
	    </div>
	</div>

	<script type="text/javascript">
		var propertiesUrl = "/CodeGenerator/mvc/monitor/sys/getProperties";
	
		//设置SessionAttribute数据显示表格
		$("#propertiesData").datagrid({
			url : propertiesUrl,
			idField : "name",
			fit : true,
			fitColumns : true,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			nowrap : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb"
		});
	</script>

</body>
</html>