<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%@ taglib prefix="monitor" tagdir="/WEB-INF/tags/core/monitor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%-- <%@ include file="/WEB-INF/views/include/EasyUI.jsp"%> --%>
<script type="text/javascript" src="${pageScope.staticPath}/JQuery/jquery-1.11.2.min.js"></script>
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="${pageScope.staticPath}/Bootstrap/css/bootstrap.min.css">
<script src="${pageScope.staticPath}/AngularJS/angular.min-1.4.6.js"></script>
<title>Redis监控</title>
</head>
<body>
	<monitor:RedisMonitorHead 
		currentUrl="keysUrl"
		keysUrl="/CodeGenerator/mvc/monitor/redis/redisKeysMonitor" 
		infoUrl="/CodeGenerator/mvc/monitor/redis/redisInfoMonitor" 
		mainUrl="/CodeGenerator/mvc/monitor/redis/redisChartsMonitor" 
		confUrl="/CodeGenerator/mvc/monitor/redis/redisConfMonitor" 
		chartsUrl="/CodeGenerator/mvc/monitor/redis/redisChartsMonitor" />
		
	<div class="container-fluid" ng-app="redisKeysApp" ng-controller="redisKeysCtrl">
		<div class="row-fluid">
			<!-- 右边列 -->
			<div class="col-md-3">
				<!-- 显示输入key及查询按钮行 -->
				<div class="row-fluid" style="margin-bottom: 45px;">
					<span class="col-md-7" padding="0px" style="margin-left: -15px;margin-right: -15px">
						<input type="text" class="form-control" placeholder="请输入key" ng-model="key" ng-keypress="search($event)">
					</span>
					<span class="col-md-2">
						<button class="btn btn-small btn-primary" ng-click="getKeys()">搜索</button>
					</span>
					<span class="col-md-3">
						<input type="text" class="form-control" placeholder="数量" value="100" ng-model="size" ng-keypress="search($event)">
					</span>
				</div>
				<!-- 显示Keys表格行 -->
				<div class="row-fluid">
					<table class="table table-striped table-bordered table-condensed well">
						<tbody>
							<tr ng-repeat="key in keys">
								<td>{{ $index + 1 }}</td>
								<td><a href="javascript:void(0)" ng-click="getValue(key)" >{{ key }}</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
			<!-- 左边列 -->
			<div class="col-md-9">
				<div class="row-fluid">
				
				</div>
				<div class="row-fluid">
					<textarea class="form-control" rows="20">{{ value }}</textarea>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
	// 获取Keys
	var redisKeyUrl = "/CodeGenerator/mvc/monitor/redis/getKeys?pattern=";
	// 获取Value
	var redisValueUrl = "/CodeGenerator/mvc/monitor/redis/getValue?key=";
	
    var app = angular.module('redisKeysApp', []);
    app.controller('redisKeysCtrl', function($scope, $http) {
    	$scope.size = "100";
    	
    	$scope.getKeys = function() {
            $http.get(redisKeyUrl + $scope.key + "&size=" + $scope.size).success(function (response){
            	if(response.success == true){
            		$scope.keys = response.object;
            	}
    		});
    	};
    	
    	$scope.search = function($event) {
    		if($event.keyCode==13){
    			$scope.getKeys() ;
    		}
    	};
    	
    	$scope.getValue = function(key){
            $http.get(redisValueUrl + key).success(function (response){
            	if(response.success == true){
            		$scope.value = response.object;
            	}
    		});
        };
    });
	</script>
</body>
</html>