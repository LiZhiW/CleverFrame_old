<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%@ taglib prefix="monitor" tagdir="/WEB-INF/tags/core/monitor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%-- <%@ include file="/WEB-INF/views/include/EasyUI.jsp"%> --%>
<script type="text/javascript" src="${pageScope.staticPath}/JQuery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/ECharts/dist/echarts.js"></script>
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="${pageScope.staticPath}/Bootstrap/css/bootstrap.min.css">
<script src="${pageScope.staticPath}/AngularJS/angular.min-1.4.6.js"></script>
<title>Memcached监控</title>
</head>
<body>
	<!-- 导航栏 -->
	<monitor:MemcachedMonitorHead/>
	
	<!-- 内容区域 -->
	<div class="container-fluid">
		<div class="row-fluid">
			<div id="hitsCharts" style="width: 800px;height: 400px;"></div>		
		
		</div>
	</div>
	
	<script type="text/javascript">
	// 得到命中信息URL
	var getHitUrl = "/CodeGenerator/mvc/monitor/memcached/getStats?ipAddress=192.168.110.100&port=11211";
	var hitsCharts = null;
	
	var hitData = new Array();
	var missesData = new Array();
	$.ajax(getHitUrl, {
		type: "POST",
		dataType : "json",
		async : false,
		success : function (data) {
			var getHit = null;
			var getMisses = null;
			var deleteHit = null;
			var deleteMisses = null;
			var decrHit = null;
			var decrMisses = null;
			var incrHit = null;
			var incrMisses = null;
			var casHit = null;
			var casMisses = null;
			if(data.success == true) {
				$(data.object).each(function(index, stat) {
					stat.value = parseFloat(stat.value);
					if("get_hits" == stat.key) {
						getHit = stat;
					} else if("get_misses" == stat.key) {
						getMisses = stat;
					} else if("delete_hits" == stat.key) {
						deleteHit = stat;
					} else if("delete_misses" == stat.key) {
						deleteMisses = stat;
					} else if("decr_hits" == stat.key) {
						decrHit = stat;
					} else if("decr_misses" == stat.key) {
						decrMisses = stat;
					} else if("incr_hits" == stat.key) {
						incrHit = stat;
					} else if("incr_misses" == stat.key) {
						incrMisses = stat;
					} else if("cas_hits" == stat.key) {
						casHit = stat;
					} else if("cas_misses" == stat.key) {
						casMisses = stat;
					}
				});
				var hit = 0;
				var misses = 0;
				if(getHit == null || getMisses == null) {
					hitData[0] = '-';
					missesData[0] = '-';
				} else {
                    hit += getHit.value;
					misses += getMisses.value;
					hitData[0] = (getHit.value * 100.0 / (getHit.value + getMisses.value)).toFixed(2);
					missesData[0] = (getMisses.value * 100.0 / (getHit.value + getMisses.value)).toFixed(2);
				}
				
				if(deleteHit == null || deleteMisses == null) {
					hitData[1] = '-';
					missesData[1] = '-';
				} else {
					hit += deleteHit.value;
					misses += deleteMisses.value;
					hitData[1] = (deleteHit.value * 100.0 / (deleteHit.value + deleteMisses.value)).toFixed(2);
					missesData[1] = (deleteMisses.value * 100.0 / (deleteHit.value + deleteMisses.value)).toFixed(2);
				}
				
				if(decrHit == null || decrMisses == null) {
					hitData[2] = '-';
					missesData[2] = '-';
				} else {
					hit += decrHit.value;
					misses += decrMisses.value;
					hitData[2] = (decrHit.value * 100.0 / (decrHit.value + decrMisses.value)).toFixed(2);
					missesData[2] = (decrMisses.value * 100.0 / (decrHit.value + decrMisses.value)).toFixed(2);
				}
				
				if(incrHit == null || incrMisses == null) {
					hitData[3] = '-';
					missesData[3] = '-';
				} else {
					hit += incrHit.value;
					misses += incrMisses.value;
					hitData[3] = (incrHit.value * 100.0 / (incrHit.value + incrMisses.value)).toFixed(2);
					missesData[3] = (incrMisses.value * 100.0 / (incrHit.value + incrMisses.value)).toFixed(2);
				}
				
				if(casHit == null || casMisses == null) {
					hitData[4] = '-';
					missesData[4] = '-';
				} else {
					hit += casHit.value;
					misses += casMisses.value;
					hitData[4] = (casHit.value * 100.0 / (casHit.value + casMisses.value)).toFixed(2);
					missesData[4] = (casMisses.value * 100.0 / (casHit.value + casMisses.value)).toFixed(2);
				}
				
	            hitData[5] = (hit * 100.0 / (hit + misses)).toFixed(2);
	            missesData[5] = (misses * 100.0 / (hit + misses)).toFixed(2);
			}
		}
	});
    require.config({
        paths : {
            echarts : '${pageScope.staticPath}/ECharts/dist'
        }
    });
    require(
        [
            'echarts',
            'echarts/chart/bar'
        ],
        function (ec) {
        	hitsCharts = ec.init(document.getElementById('hitsCharts'));
        	hitsOption = {
	  		    title: {
	  		        text: 'Memcached命中率统计',
	  		    },
	  		    tooltip: {
	  		        trigger: 'axis',
	  		    },
	  		    legend: {
	  		        x: 'right',
	  		        data:['命中','未命中']
	  		    },
	  		    xAxis: [
	  		        {
	  		        	name : '类型',
	  		            type: 'category',
	  		            data: ['get命令','delete命令','decr命令','incr命令','cas命令','总计']
	  		        }
	  		    ],
	  		    yAxis: [
	  		        {
	  		        	name : '百分比',
	  		            type: 'value',
                        axisLabel : {
                            formatter : '{value} %'
                        },
                        max : 100,
                        splitNumber : 10,
                        axisTick : {
                            show : true,
                            interval : 0
                        }
	  		        }
	  		    ],
	  		    series: [
	  		        {
	  		            name: '命中',
	  		            type: 'bar',
	  		            data: hitData
	  		        },
	  		        {
	  		            name: '未命中',
	  		            type: 'bar',
	  		         	data: missesData
	  		        },
	  		    ]	
        	};
        	hitsCharts.setOption(hitsOption);
        });
	</script>
</body>
</html>