<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>当前Session属性范围值</title>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:false" style="height: 60px">
		1234
	</div>

	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="sessionData" data-options="border:false">
			<thead>
				<tr>
					<th data-options="field:'checkBox',align:'center',checkbox:true">选择</th>
					<th data-options="field:'name',width:100,align:'left'">属性名称</th>
					<th data-options="field:'type',width:150,align:'left'">属性类型</th>
					<th data-options="field:'value',width:500,align:'left'">属性值(Json)</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="$('#sessionData').datagrid('reload');" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" style="margin-left:10px" >刷新</a>
			<a href="javascript:void(0)" onclick="removeSessionAttribute();" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" style="margin-left:10px;margin-right:20px;">删除</a>
			<input name="attributeName" class="easyui-searchbox" type="text" style="width: 200px" ></input>
	    </div>
	</div>

	<script type="text/javascript">
		var attributeUrl = "/CodeGenerator/mvc/monitor/sys/getAttribute";
	
		//设置SessionAttribute数据显示表格
		$("#sessionData").datagrid({
			url : attributeUrl + "?scope=session",
			idField : "name",
			fit : true,
			fitColumns : true,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			nowrap : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb"
		});
		
		//移除SessionAttribute
		function removeSessionAttribute(){
			var rows = $("#sessionData").datagrid("getSelections");
			if(rows.length<=0){
				$.messager.alert("提示","未选择要移除的属性！","info");
			}
			for(var i=0; i<rows.length; i++){
				var row = rows[0];
				$.messager.confirm("确认删除", "确定要从当前Session属性中移除属性["+row.name+"]？", function(flag){
					if (flag){
						$.ajax({
							url : attributeUrl + "?scope=session&name=" + row.name, 
							type : "POST",
							success: function(data){
								if (data.success) {
									$("#sessionData").datagrid("reload");
									$.messager.alert("成功",data.message,"info");
								} else {
									$.messager.alert("失败",data.message,"error");
								}
							},
							error : function(data, status, e) {
								$.messager.alert("请求失败","状态：" + status,"error");
							}
						});
					}
				});
			}
		};
	</script>

</body>
</html>