<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%@ taglib prefix="monitor" tagdir="/WEB-INF/tags/core/monitor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%-- <%@ include file="/WEB-INF/views/include/EasyUI.jsp"%> --%>
<script type="text/javascript" src="${pageScope.staticPath}/JQuery/jquery-1.11.2.min.js"></script>
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="${pageScope.staticPath}/Bootstrap/css/bootstrap.min.css">
<script src="${pageScope.staticPath}/AngularJS/angular.min-1.4.6.js"></script>
<title>Redis监控</title>
</head>
<body>
	<monitor:RedisMonitorHead 
		currentUrl="confUrl"
		keysUrl="/CodeGenerator/mvc/monitor/redis/redisKeysMonitor" 
		infoUrl="/CodeGenerator/mvc/monitor/redis/redisInfoMonitor" 
		mainUrl="/CodeGenerator/mvc/monitor/redis/redisChartsMonitor" 
		confUrl="/CodeGenerator/mvc/monitor/redis/redisConfMonitor" 
		chartsUrl="/CodeGenerator/mvc/monitor/redis/redisChartsMonitor" />
		
	<!-- 内容区域 -->
	<div class="container-fluid" ng-app="redisConfApp" ng-controller="redisConfCtrl">
		
		<table class="table table-striped table-bordered table-condensed well">
			<caption style="font-size: 18px;font-weight: bold;text-align: center;">Redis 配置信息</caption>
			<thead>
				<tr>
					<th width="20px">#</th>
					<th width="200px">配置名称</th>
					<th width="400px">配置值</th>
					<th>配置说明</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="conf in redisConfs">
					<td>{{ $index + 1 }}</td>
					<td>{{ conf.key }}</td>
					<td>{{ conf.value }}</td>
					<td>{{ conf.note }}</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<script type="text/javascript">
	// 获取Redis Conf信息地址
    var redisConf = "/CodeGenerator/mvc/monitor/redis/getConfig";
    
    var app = angular.module('redisConfApp', []);
    app.controller('redisConfCtrl', function($scope, $http) {
        $http.get(redisConf).success(function (response){
        	if(response.success == true){
        		$scope.redisConfs = response.object;
        	}
		});
    });
	</script>
	
</body>
</html>