<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%@ taglib prefix="monitor" tagdir="/WEB-INF/tags/core/monitor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%-- <%@ include file="/WEB-INF/views/include/EasyUI.jsp"%> --%>
<script type="text/javascript" src="${pageScope.staticPath}/JQuery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/ECharts/dist/echarts.js"></script>
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="${pageScope.staticPath}/Bootstrap/css/bootstrap.min.css">
<script src="${pageScope.staticPath}/AngularJS/angular.min-1.4.6.js"></script>
<title>Memcached监控</title>
</head>
<body>
	<!-- 导航栏 -->
	<monitor:MemcachedMonitorHead/>

	<!-- 内容区域 -->
	<div class="container-fluid" ng-app="memcachedStatsApp" ng-controller="memcachedStatsCtrl">
		
		<table class="table table-striped table-bordered table-condensed well">
			<caption style="font-size: 18px;font-weight: bold;text-align: center;">Memcached Stats信息</caption>
			<thead>
				<tr>
					<th width="20px">#</th>
					<th width="200px">名称</th>
					<th width="200px">值</th>
					<th>说明</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="stat in stats">
					<td>{{ $index + 1 }}</td>
					<td>{{ stat.key }}</td>
					<td>{{ stat.value }}</td>
					<td>{{ stat.note }}</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<script type="text/javascript">
	// 获取Memcached Stats信息地址
    var statsUrl = "/CodeGenerator/mvc/monitor/memcached/getStats?ipAddress=192.168.110.100&port=11211";
    
    var app = angular.module('memcachedStatsApp', []);
    app.controller('memcachedStatsCtrl', function($scope, $http) {
        $http.get(statsUrl).success(function (response){
        	if(response.success == true){
        		$scope.stats = response.object;
        	}
		});
    });
	</script>
</body>
</html>