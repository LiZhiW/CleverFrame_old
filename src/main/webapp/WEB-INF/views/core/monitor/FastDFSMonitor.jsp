<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>FastDFS集群监控</title>
</head>
<body>

	<div id="infoHead" style="margin-left: 20px;font-size: 20px;font-weight: bold;">
		FastDFS集群信息:
	</div>
	<br>
	<!-- 动态加入集群信息DIV -->
	<div id="groups" style="margin-left:50px;">
		
	</div>

	<script type="text/javascript">
		// FastDFS集群group信息
		var groupStatsUrl = "/CodeGenerator/mvc/monitor/fastdfs/getGroupStats";
		// FastDFS集群Storage信息，参数：groupName=***
		var storageStatsUrl = "/CodeGenerator/mvc/monitor/fastdfs/getStorageStats";
		
		$(document).ready(function () {
			$.post(groupStatsUrl, function (data) {
				if(data.success == true){
					showGroups(data.object);
				} else {
					$.messager.alert("出错", "错误信息：" + data.exceptionMessage, "error");
				}
			}, "json");
		});
		
		function showGroups (groups){
			$(groups).each(function (index, group) {
				var groupText = "<b>Group " + (index + 1) + "</b><br/>";
				groupText += "group name = " + group.groupName + "<br/>";
				groupText += "disk total space = " + group.totalMB + "<br/>";
				groupText += "disk free space = " + group.freeMB + "<br/>";
				groupText += "trunk free space = " + group.trunkFreeMB + "<br/>";
				groupText += "storage server count = " + group.storageCount + "<br/>";
				groupText += "active server count = " + group.activeCount + "<br/>";
				groupText += "storage server port = " + group.storagePort + "<br/>";
				groupText += "storage HTTP port = " + group.storageHttpPort + "<br/>";
				groupText += "store path count = " + group.storePathCount + "<br/>";
				groupText += "subdir count per path = " + group.subdirCountPerPath + "<br/>";
				groupText += "current write server index = " + group.currentWriteServer + "<br/>";
				groupText += "current trunk file id = " + group.currentTrunkFileId + "<br/>";
				
				var properties = {};
				properties.id = group.groupName;
				var groupDiv = $("<div/>").attr(properties);
				groupDiv.html(groupText);
				groupDiv.appendTo($("#groups"));
				
				$.post(storageStatsUrl, {groupName : group.groupName}, function (data) {
					if(data.success == true){
						showStorages(group.groupName, data.object);
					} else {
						$.messager.alert("出错", "错误信息：" + data.exceptionMessage, "error");
					}
				}, "json");
				
				$("<hr/>").appendTo($("#groups"));
				$("<br/>").appendTo($("#groups"));
			});
		}
		
		function showStorages(groupName, storages){
			$(storages).each(function (index, storage) {
				var storageText = "<b>Storage " + (index + 1) + "</b><br/>";
				storageText += "storage id = " + storage.id + "<br/>";
				storageText += "ip_addr = " + storage.ipAddr + "<br/>";
				storageText += "http domain = " + storage.domainName + "<br/>";
				storageText += "version = " + storage.version + "<br/>";
				storageText += "join time = " + storage.joinTime + "<br/>";
				storageText += "up time = " + storage.upTime + "<br/>";
				storageText += "total storage = " + storage.totalMB + "<br/>";
				storageText += "free storage = " + storage.freeMB + "<br/>";
				storageText += "upload priority = " + storage.uploadPriority + "<br/>";
				storageText += "store_path_count = " + storage.storePathCount + "<br/>";
				storageText += "subdir_count_per_path = " + storage.subdirCountPerPath + "<br/>";
				storageText += "storage_port = " + storage.storagePort + "<br/>";
				storageText += "storage_http_port = " + storage.storageHttpPort + "<br/>";
				storageText += "current_write_path = " + storage.currentWritePath + "<br/>";
				storageText += "source ip_addr = " + storage.srcIpAddr + "<br/>";
				storageText += "if_trunk_server = " + storage.ifTrunkServer + "<br/>";
				storageText += "conntion.alloc_count  = " + storage.connectionAllocCount + "<br/>";
				storageText += "conntion.current_count  = " + storage.connectionCurrentCount + "<br/>";
				storageText += "conntion.max_count  = " + storage.connectionMaxCount + "<br/>";
				storageText += "total_upload_count = " + storage.totalUploadCount + "<br/>";
				storageText += "success_upload_count = " + storage.successUploadCount + "<br/>";
				storageText += "total_append_count = " + storage.totalAppendCount + "<br/>";
				storageText += "success_append_count = " + storage.successAppendCount + "<br/>";
				storageText += "total_modify_count = " + storage.totalModifyCount + "<br/>";
				storageText += "success_modify_count = " + storage.successModifyCount + "<br/>";
				storageText += "total_truncate_count = " + storage.totalTruncateCount + "<br/>";
				storageText += "success_truncate_count = " + storage.successTruncateCount + "<br/>";
				storageText += "total_set_meta_count = " + storage.totalSetMetaCount + "<br/>";
				storageText += "success_set_meta_count = " + storage.successSetMetaCount + "<br/>";
				storageText += "total_delete_count = " + storage.totalDeleteCount + "<br/>";
				storageText += "success_delete_count = " + storage.successDeleteCount + "<br/>";
				storageText += "total_download_count = " + storage.totalDownloadCount + "<br/>";
				storageText += "success_download_count = " + storage.successDownloadCount + "<br/>";
				storageText += "total_get_meta_count = " + storage.totalGetMetaCount + "<br/>";
				storageText += "success_get_meta_count = " + storage.successGetMetaCount + "<br/>";
				storageText += "total_create_link_count = " + storage.totalCreateLinkCount + "<br/>";
				storageText += "success_create_link_count = " + storage.successCreateLinkCount + "<br/>";
				storageText += "total_delete_link_count = " + storage.totalDeleteLinkCount + "<br/>";
				storageText += "success_delete_link_count = " + storage.successDeleteLinkCount + "<br/>";
				storageText += "total_upload_bytes = " + storage.totalUploadBytes + "<br/>";
				storageText += "success_upload_bytes = " + storage.successUploadBytes + "<br/>";
				storageText += "total_append_bytes = " + storage.totalAppendBytes + "<br/>";
				storageText += "success_append_bytes = " + storage.successAppendBytes + "<br/>";
				storageText += "total_modify_bytes = " + storage.totalModifyBytes + "<br/>";
				storageText += "success_modify_bytes = " + storage.successModifyBytes + "<br/>";
				storageText += "total_download_bytes = " + storage.totalDownloadloadBytes + "<br/>";
				storageText += "success_download_bytes = " + storage.successDownloadloadBytes + "<br/>";
				storageText += "total_sync_in_bytes = " + storage.totalSyncInBytes + "<br/>";
				storageText += "success_sync_in_bytes = " + storage.successSyncInBytes + "<br/>";
				storageText += "total_sync_out_bytes = " + storage.totalSyncOutBytes + "<br/>";
				storageText += "success_sync_out_bytes = " + storage.successSyncOutBytes + "<br/>";
				storageText += "total_file_open_count = " + storage.totalFileOpenCount + "<br/>";
				storageText += "success_file_open_count = " + storage.successFileOpenCount + "<br/>";
				storageText += "total_file_read_count = " + storage.totalFileReadCount + "<br/>";
				storageText += "success_file_read_count = " + storage.successFileReadCount + "<br/>";
				storageText += "total_file_write_count = " + storage.totalFileWriteCount + "<br/>";
				storageText += "success_file_write_count = " + storage.successFileWriteCount + "<br/>";
				storageText += "last_heart_beat_time = " + storage.lastHeartBeatTime + "<br/>";
				storageText += "last_source_update = " + storage.lastSourceUpdate + "<br/>";
				storageText += "last_sync_update = " + storage.lastSyncUpdate + "<br/>";
				storageText += "last_synced_timestamp = " + storage.lastSyncedTimestamp + "<br/>";
				
				$("<br/>").appendTo($("#" + groupName));
				var properties = {};
				properties.id = groupName + "_storage" + (index + 1);
				var storageDiv = $("<div style='margin-left: 50px;' />").attr(properties);
				storageDiv.html(storageText);
				storageDiv.appendTo($("#" + groupName));
				
			});
		}
		
	</script>
</body>
