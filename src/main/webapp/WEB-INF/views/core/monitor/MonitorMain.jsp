<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>系统监控</title>
</head>
<body class="easyui-layout" data-options="fit:true">
	<!-- 页面上部 -->
    <div data-options="region:'north',border:false" >
    	系统内部属性配置监控
    </div>
    
    <!-- 页面底部 -->
    <div data-options="region:'south',border:false" align="center">
    	系统内部属性配置监控&ensp;&ensp;&ensp;&ensp;版权所有&copy;
    </div>
    
    <!-- 页面左部 -->
	<div id="westPanel" data-options="region:'west',split:false,border:true,title:'系统监控类别'" style="width: 200px">
		<div id="monitorType" class="easyui-accordion" data-options="fit : true,border : false,animate : false">
			<div id="serverPanel" title="服务器" data-options="border : false" align="center">
				<a onclick="addTab('Session属性值', sessionUrl);" 
					href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 160px">当前Session属性范围值</a>
					
				<a onclick="addTab('Application属性值', applicationUrl);"
					href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 160px">Application属性范围里的值</a>
					
				<a onclick="addTab('系统配置信息', serverPropertiesUrl);"
					href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 160px">系统配置信息</a>
									
				<a onclick="addTab('Druid', druidUrl);"
					href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 160px">服务器数据库连接池</a>
			</div>
			<div id="springPanel" title="Spring" data-options="border : false" align="center">
				<a onclick="addTab('SpringBeans', springBeansUrl);"
					href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 180px">Spring容器Bean值</a>
				<a onclick="addTab('FastDFS集群监控', fastDFSUrl);"
					href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 180px">FastDFS集群监控</a>
				<a onclick="addTab('Redis监控', redisUrl);"
					href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 180px">Redis监控</a>
				<a onclick="addTab('Memcached监控', memcachedUrl);"
					href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 180px">Memcached监控</a>
			</div>
			<div id="otherPanel" title="其他" data-options="border : false" align="center">
				<a href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 180px">测试1</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 180px">测试2</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" style="margin-top:5px;width: 180px">其他</a>
			</div>
		</div>
	</div>

	<!-- 页面中部 -->
    <div id="centerPanel" data-options="region:'center',border:true">
	    <div id="tabsCenter" class="easyui-tabs" data-options="fit:true,border:false">
			<div title="帮助" data-options="border:false">
		        tab1
		    </div>
	    </div>
    </div>

	<script type="text/javascript">
		// 数据库连接池监控
		var druidUrl = "/CodeGenerator/druid/index.html";
		// 当前Session属性范围值
		var sessionUrl = "/CodeGenerator/mvc/monitor/sys/sessionAttribute";
		// Application属性范围里的值
		var applicationUrl = "/CodeGenerator/mvc/monitor/sys/applicationAttribute";
		// 系统配置信息
		var serverPropertiesUrl = "/CodeGenerator/mvc/monitor/sys/serverProperties";
		// Spring容器Bean值
		var springBeansUrl = "/CodeGenerator/mvc/monitor/spring/springBeans";
		// FastDFS集群监控
		var fastDFSUrl = "/CodeGenerator/mvc/monitor/fastdfs/fastDFSMonitor";
		// Redis监控
		var redisUrl = "/CodeGenerator/mvc/monitor/redis/redisChartsMonitor";
		// Memcached监控
		var memcachedUrl = "/CodeGenerator/mvc/monitor/memcached/memcachedStatsMonitor";
		
		$(document).ready(function() {

		});

		//以iframe方式增加页面
		function addTab(tabName, tabUrl) {
			if ($("#tabsCenter").tabs("exists", tabName)) {
				$("#tabsCenter").tabs("select", tabName);
			} else {
				if (tabUrl) {
					var id = "id" + Math.random();
					id = id.replace(".","");
					//var content = "<iframe id='" + id + "' scrolling='auto' style='width:100%;height:99%;' frameborder='0' src='" + tabUrl + "'></iframe>";
					var content = "<iframe id='" + id + "' scrolling='auto' style='width:100%;height:100%;' frameborder='0' src='" + tabUrl + "'></iframe>";
				} else {
					var content = "未定义页面路径！";
				}
				$("#tabsCenter").tabs("add", {
					title : tabName,
					closable : true,
					content : content,
					tools : [{
                        iconCls : "icon-mini-refresh",
                        handler : function(){
                        	if(id){
                        		document.getElementById(id).contentWindow.location.reload(true);
                        	}
                        	//window.open(tabUrl); // 在新窗口中打开
                        }
                    }]
				});
			}
		};
		
		//以div方式增加页面，此方式慎用
		function addTabByDiv(tabName, tabUrl) {
			if ($("#tabsCenter").tabs("exists", tabName)) {
				$("#tabsCenter").tabs("select", tabName);
			} else {
				$("#tabsCenter").tabs("add",{
					title:tabName,
					href:tabUrl,
					closable:true,
					extractor:function(data){
						data = $.fn.panel.defaults.extractor(data);
						//var tmp = $('<div></div>').html(data);
						//data = tmp.find('#content').html();
						//tmp.remove();
						return data;
					}
				});
			}
		};
	</script>
</body>
</html>