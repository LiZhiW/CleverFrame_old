<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 引入公用JSP --%>
<%@ include file="/WEB-INF/include/taglib.jsp"%>
<%-- 加入自定义UI标签库 --%>
<%@ taglib prefix="qlscript" tagdir="/WEB-INF/tags/qlscript" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageScope.staticPath}/jQueryEasyUI/themes/ExpandIcon.css">
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageScope.staticPath}/jQueryEasyUI/locale/easyui-lang-zh_CN.js"></script>
<title>数据库脚本管理</title>
<style type="text/css">
#fm-search #fm {
	margin: 0;
	padding: 0px 5px;
}
#fm-search .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#fm-search .row {
	margin-bottom: 8px;
}
#fm-search .row label {
	display: inline-block;
	width: 60px;
	text-align: center;
}
#fm-search .row input,select {
	width: 160px;

}
#fm-search .row .column {
	margin-right: 10px;
}
#fm-search .row .columnLast {
	margin-right: 0px;
}
</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<!-- 页面上部 -->
	<div data-options="region:'north',border:true" style="height:70px;">
		<form id="fm-search" method="post" style="margin-top: 5px;margin-left: 20px">
			<div class="row">
				<span class="column">
					<label for="type-search">脚本类型</label> 
					<input id="type-search" name="type-search" class="easyui-combobox">
				</span>
				<span class="columnLast">
					<label for="name-search">脚本名称</label> 
					<input id="name-search" name="name-search" class="easyui-textbox" style="width: 420px">
				</span>
			</div>
			<div class="row">
				<span class="column">
					<label for="id-search">数据ID</label> 
					<input id="id-search" name="id-search" class="easyui-numberbox">
				</span>
				<span class="columnLast">
					<label for="uuid-search">数据UUID</label> 
					<input id="uuid-search" name="uuid-search" class="easyui-textbox" style="width: 420px">
				</span> 
			</div>
		</form>
	</div>
	
	<!-- 页面中部 -->
	<div id="centerPanel" data-options="region:'center',border:false">
		<table id="qlScriptData" data-options="border:false">
			<thead>
				<tr>
                    <th data-options="field:'name',width:550,align:'left'">脚本名称</th>
					<th data-options="field:'scriptType',width:70,align:'left'">脚本类型</th>
					<th data-options="field:'createDate',width:130,align:'left',hidden:false">创建时间</th>
                    <th data-options="field:'description',width:450,align:'left'">脚本说明</th>
                    <th data-options="field:'script',width:100,align:'left'">脚本</th>
                    
					<th data-options="field:'id',width:30,align:'left',hidden:true">编号</th>
					<th data-options="field:'companyId',width:80,align:'left',hidden:true">所属公司的ID</th>
                    <th data-options="field:'orgId',width:80,align:'left',hidden:true">所属机构的ID</th>
					<th data-options="field:'createBy',width:50,align:'left',hidden:true">创建者</th>
					<th data-options="field:'updateBy',width:50,align:'left',hidden:true">更新者</th>
					<th data-options="field:'updateDate',width:100,align:'left',hidden:true">更新时间</th>
					<th data-options="field:'remarks',width:100,align:'left'">备注信息</th>
					<th data-options="field:'delFlag',width:50,align:'left',hidden:true">删除标记</th>
					<th data-options="field:'uuid',width:80,align:'left',hidden:true">数据UUID</th>
				</tr>
			</thead>
		</table>
		<div id="tb">
			<a onclick="searchQLScript();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a onclick="newQLScript();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">新增</a>
			<a onclick="editQLScript();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">编辑</a>
			<a onclick="delQLScript();" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>
	    </div>
	</div>
	
	<%-- 数据库脚本编辑表单 --%>
	<qlscript:QLScriptEdit mvcPath="${pageScope.mvcPath}" saveFunction="saveQLScript()" dialogID="dlg" formID="fm"/>
	
	<script type="text/javascript">
	// 获取分页QLScript数据地址
	var qlScriptDataUrl = "${pageScope.mvcPath}/common/findQLScriptByPage";
	// 新增保存QLScript地址
	var newQLScriptUrl = "${pageScope.mvcPath}/common/addQLScript";
	// 编辑保存QLScript地址
	var editQLScriptUrl = "${pageScope.mvcPath}/common/updateQLScript";
	// 删除QLScript地址
	var delQLScriptUrl = "${pageScope.mvcPath}/common/deleteQLScript";
	// 数据库脚本编辑表单--提交地址
	var qlScriptSaveUrl = "";
	// 根据字典类别查询字典地址
	var findDictTypeUrl	= "${mvcPath}/sys/findDictByType?dict-type=";
	findDictTypeUrl += encodeURIComponent("数据库脚本类型");
	
	$(document).ready(function () {
		// 设置qlScriptData数据显示表格
		$("#qlScriptData").datagrid({
			url : qlScriptDataUrl,
			idField : 'name',
			fit : true,
			fitColumns : false,
			striped : true,
			rownumbers : true,
			singleSelect : true,
			nowrap : true,
			pagination : true,
			loadMsg : "正在加载，请稍候...",
			toolbar : "#tb",
			onDblClickRow : function (rowIndex, rowData) {
				editQLScript();
			},
			onBeforeLoad : function (param) {
				// 增加查询参数
				var paramArray = $("#fm-search").serializeArray();
				$(paramArray).each(function () {
					if (param[this.name]) {
						if ($.isArray(param[this.name])) {
							param[this.name].push(this.value);
						} else {
							param[this.name] = [param[this.name], this.value];
						}
					} else {
						param[this.name] = this.value;
					}
				});
			}
		});
		
		$("#type-search").combobox({
			required : false,
			url : findDictTypeUrl,
			editable : true,
			valueField : 'value',
			textField : 'value',//text
			panelHeight : 50
		});
	});

	// 数据库脚本编辑表单--保存
	function saveQLScript() {
		$("#fm").form("submit", {
			url : qlScriptSaveUrl,
			success : function (data) {
				var data = $.parseJSON(data);
				if (data.success) {
					// 保存成功
					$('#dlg').dialog('close')
					$.messager.show({
						title : '提示',
						msg : data.message,
						timeout : 5000,
						showType : 'slide'
					});
					$("#qlScriptData").datagrid('reload');
				} else {
					// 保存失败
				}
			}
		});
	}

	// 查询
	function searchQLScript() {
		$("#qlScriptData").datagrid('load');
	}

	// 新增
	function newQLScript() {
		qlScriptSaveUrl = newQLScriptUrl;
		$('#dlg').dialog('open').dialog('setTitle', '新增数据库脚本');
		$('#fm').form('reset');
	}

	// 编辑
	function editQLScript() {
		qlScriptSaveUrl = editQLScriptUrl;
		var row = $('#qlScriptData').datagrid('getSelected');
		if (row == null) {
			$.messager.alert("提示", "请选择要编辑的数据！", "info");
			return;
		}
		if (row) {
			$('#dlg').dialog('open').dialog('setTitle', '编辑数据库脚本');
			$('#fm').form('load', row);
		}
	}

	// 删除
	function delQLScript() {
		var row = $('#qlScriptData').datagrid('getSelected');
		if (row == null) {
			$.messager.alert("提示", "请选择要删除的数据！", "info");
			return;
		}
		$.messager.confirm("确认删除", "您确定删除数据库脚本?<br/>" + row.name, function (r) {
			if (r) {
				$.post(delQLScriptUrl, row, function (data) {
					if (data.success) {
						// 删除成功
						$.messager.show({
							title : '提示',
							msg : data.message,
							timeout : 5000,
							showType : 'slide'
						});
						$("#qlScriptData").datagrid('reload');
					} else {
						// 删除失败
					}
				}, "json");
			}
		});
	}
	</script>
	
</body>
</html>