<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute type="java.lang.String" required="true" name="currentUrl" description="当前URL地址，如：mainUrl、chartsUrl、infoUrl、keysUrl..."%>
<%@ attribute type="java.lang.String" required="true" name="mainUrl" description="Redis监控首页地址"%>
<%@ attribute type="java.lang.String" required="true" name="chartsUrl" description="监控图表地址"%>
<%@ attribute type="java.lang.String" required="true" name="infoUrl" description="Redis Info地址"%>
<%@ attribute type="java.lang.String" required="true" name="keysUrl" description="Redis Info地址"%>
<%@ attribute type="java.lang.String" required="true" name="confUrl" description="Redis配置地址"%>

<!-- 顶部导航栏 -->
<nav class="navbar navbar-inverse" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="${mainUrl}">Redis监控</a>
	</div>
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav navbar-left">
			<li ${ currentUrl == "chartsUrl" ? "class='active'" : "" } ><a href="${chartsUrl}">监控图表</a></li>
			<li ${ currentUrl == "infoUrl" ? "class='active'" : "" } ><a href="${infoUrl}">Redis Info</a></li>
			<li ${ currentUrl == "confUrl" ? "class='active'" : "" } ><a href="${confUrl}">Redis配置</a></li>
			<li ${ currentUrl == "keysUrl" ? "class='active'" : "" } ><a href="${keysUrl}">搜索Keys</a></li>
			<li><a href="javascript:void(0)">节点管理</a></li>
			<li class="dropdown">
				<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
					选择节点 
					<span class="caret"></span>
	            </a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)">192.168.110.100</a></li>
					<li><a href="javascript:void(0)">192.168.110.101</a></li>
					<li><a href="javascript:void(0)">192.168.110.102</a></li>
					<li><a href="javascript:void(0)">192.168.110.103</a></li>
				</ul>
			</li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
				<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
					操作当前Redis
					<span class="caret"></span>
	            </a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)">flushall</a></li>
					<li><a href="javascript:void(0)">flushDB</a></li>
				</ul>
			</li>
			<li class="active"><p class="navbar-text">192.168.110.100</p></li>
		</ul>
	</div>
</nav>
