<%@ tag language="java" pageEncoding="UTF-8"%>

<!-- 顶部导航栏 -->
<nav class="navbar navbar-inverse" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="/CodeGenerator/mvc/monitor/memcached/memcachedStatsMonitor">Memcached监控</a>
	</div>
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav navbar-left">
			<li><a href="/CodeGenerator/mvc/monitor/memcached/memcachedChartsMonitor">图标监控</a></li>
			<li><a href="/CodeGenerator/mvc/monitor/memcached/memcachedStatsMonitor">统计信息</a></li>
			<li><a href="javascript:void(0)">节点管理</a></li>
			<li class="dropdown">
				<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
					选择节点 
					<span class="caret"></span>
	            </a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)">192.168.110.100</a></li>
					<li><a href="javascript:void(0)">192.168.110.101</a></li>
					<li><a href="javascript:void(0)">192.168.110.102</a></li>
					<li><a href="javascript:void(0)">192.168.110.103</a></li>
				</ul>
			</li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
				<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
					操作当前Memcached
					<span class="caret"></span>
	            </a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)">flushall</a></li>
					<li><a href="javascript:void(0)">flushDB</a></li>
				</ul>
			</li>
			<li class="active"><p class="navbar-text">192.168.110.100</p></li>
		</ul>
	</div>
</nav>