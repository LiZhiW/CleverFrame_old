<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute type="java.lang.String" required="true" name="mvcPath" description="系统mvc根路径"%>
<%@ attribute type="java.lang.String" required="true" name="dialogID" description="弹出的easyui-dialog对话框的ID"%>
<%@ attribute type="java.lang.String" required="true" name="formID" description="用户编辑表单的ID"%>
<%@ attribute type="java.lang.String" required="true" name="saveFunction" description="点击保存按钮时调用的方法"%>
<style type="text/css">
#${dialogID} #${formID} {
	margin: 0;
	padding: 0px 5px;
}
#${dialogID} .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#${dialogID} .row {
	margin-bottom: 8px;
}
#${dialogID} .row label {
	display: inline-block;
	width: 80px;
	text-align: right;
}
#${dialogID} .row input,select {
	width: 160px;
}
#${dialogID} .row .column {
	margin-right: 10px;
}
#${dialogID} .row .columnLast {
	margin-right: 0px;
}
</style>
<div id="${dialogID}" style="width: 830px; height: 370px; padding: 5px 10px">
	<form id="${formID}" method="post">
		<div class="ftitle">编辑字段</div>
		<div class="row">
			<span class="column"> 
				<label for="name">用户姓名</label> 
				<input id="name" name="name">
			</span> 
			<span class="column"> 
				<label for="loginName">登录名</label> 
				<input id="loginName" name="loginName">
			</span> 
			<span class="columnLast"> 
				<label for="userType">用户类型</label> 
				<input id="userType" name="userType">
			</span>
		</div>
		<div class="row">
			<span class="column"> 
				<label for="jobNo">工号</label> 
				<input id="jobNo" name="jobNo">
			</span>
			<span class="column"> 
				<label for="homeCompany">归属公司</label> 
				<input id="homeCompany" name="homeCompany">
			</span> 
			<span class="columnLast"> 
				<label for="homeOrg">直属机构</label> 
				<input id="homeOrg" name="homeOrg">
			</span>
		</div>
		<div class="row">
			<span class="column"> 
				<label for="email">邮箱</label> 
				<input id="email" name="email">
			</span> 
			<span class="column"> 
				<label for="phone">电话</label> 
				<input id="phone" name="phone">
			</span> 
			<span class="columnLast"> 
				<label for="mobile">手机</label> 
				<input id="mobile" name="mobile">
			</span>
		</div>
		<div class="row">
			<span class="column"> 
				<label for="userState">用户状态</label> 
				<input id="userState" name="userState">
			</span>
			<span class="columnLast"> 
				<label for="password">密码</label> 
				<input id="password" name="password" style="width:430px;">
			</span>
		</div>
		<div class="row">
			<span class="columnLast"> 
				<label for="remarks">备注信息</label> 
				<input id="remarks" name="remarks" style="width:690px;height:40px">
			</span>
		</div>
		
		<div class="ftitle"><a href="javascript:void(0)" onclick='showOrHide_fc0bf3626706();'>只读字段</a></div>
		<div id="readonly_c67d13c82a87">
			<div class="row">
				<span class="column"> 
					<label for="accountState">帐号状态</label> 
					<input id="accountState" name="accountState">
				</span> 
				<span class="column"> 
					<label for="loginIp">最后登陆IP</label> 
					<input id="loginIp" name="loginIp">
				</span> 
				<span class="columnLast"> 
					<label for="loginDate">最后登陆时间</label> 
					<input id="loginDate" name="loginDate">
				</span>
			</div>
			<div class="row">
				<span class="column"> 
					<label for="id">数据ID</label> 
					<input id="id" name="id">
				</span> 
				<span class="column"> 
					<label for="delFlag">删除标记</label> 
					<input id="delFlag" name="delFlag">
				</span>
				<span class="columnLast"> 
					<label for="companyId">数据所属公司</label> 
					<input id="companyId" name="companyId">
				</span> 
			</div>
			<div class="row">
				<span class="column"> 
					<label for="orgId">数据直属机构</label> 
					<input id="orgId" name="orgId">
				</span> 
				<span class="column"> 
					<label for="createBy">创建者</label> 
					<input id="createBy" name="createBy">
				</span>
				<span class="columnLast"> 
					<label for="createDate">创建时间</label> 
					<input id="createDate" name="createDate">
				</span>
			</div>
			<div class="row">
				 <span class="column"> 
				 	<label for="updateBy">更新者</label> 
				 	<input id="updateBy" name="updateBy">
				</span> 
				<span class="columnLast"> 
					<label for="updateDate">更新时间</label> 
					<input id="updateDate" name="updateDate">
				</span>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="uuid">数据UUID</label> 
					<input id="uuid" name="uuid" style="width: 430px;">
				</span>
			</div>
		</div>
	</form>
</div>
<div id="${dialogID}-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="${saveFunction}" style="width:90px">保存</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#${dialogID}').dialog('close')" style="width:90px">取消</a>
</div>

<script type="text/javascript">
	var findDictTypeUrlb90d0647378b	= "${mvcPath}/sys/findDictByType?dict-type=";
	findDictTypeUrlb90d0647378b += encodeURIComponent("用户类型");
	
	var findDictUserStateUrle968477be06c = "${mvcPath}/sys/findDictByType?dict-type=";
	findDictUserStateUrle968477be06c += encodeURIComponent("用户状态");
	
	// 获取所有公司地址
	var findAllCompanyUrl52bf2ed0ba53	= "${mvcPath}/sys/getAllCompany";
	
	// 获取某公司的所有机构地址
	var findOrgByCompanyUrl9285dfdeeb0f	= "${mvcPath}/sys/getOrgByCompany";
	
	// homeCompany是否改变
	var homeCompanyOldValue5fce914122f6 = "";
	
	//设置用户表单
	$(document).ready(function() {
		$("#${dialogID}").dialog({
			title : "用户信息",
			closed : true,
			resizable : true,
			minWidth : 850,
			minHeight : 350,
			maxWidth : 850,
			maxHeight : 480,
			modal : true,
			buttons : "#${dialogID}-buttons"
		});
		$("#name").textbox({
			
		});
		$("#loginName").textbox({
	
		});
		$("#userType").combobox({
			required : true,
			url : findDictTypeUrlb90d0647378b,
			editable : false,
			valueField : 'value',
			textField : 'text',
			panelHeight : 80
		});
		$("#jobNo").textbox({
			readonly : true
		});
		$("#homeCompany").combobox({
			required : true,
			url : findAllCompanyUrl52bf2ed0ba53,
			editable : false,
			valueField : 'value',
			textField : 'text',
			panelHeight : 120,
			onChange : function(newValue, oldValue) {
				$('#homeOrg').combotree('setValue', '');
			}
		});
		$("#homeOrg").combotree({
			required : true,
			editable : false,
			animate : false,
			checkbox : false,
			cascadeCheck : true,
			onlyLeafCheck : false,
			lines : true,
			dnd : false,
			onShowPanel : function(){
				var homeCompany = $("#homeCompany").combobox("getValue");
				if($.trim(homeCompany) == "" || homeCompanyOldValue5fce914122f6 == $.trim(homeCompany)){
					return ;
				}
				
				$('#homeOrg').combotree('loadData', {});
				var param = {};
				param.companyId = homeCompany;
		        $.post(findOrgByCompanyUrl9285dfdeeb0f, param, function (data) {
		        	homeCompanyOldValue5fce914122f6 = homeCompany;
	                $("#homeOrg").combotree("clear");
	                $("#homeOrg").combotree("loadData",data);
	            }, "json");
			}
		});
		$("#email").textbox({

		});
		$("#phone").textbox({

		});
		$("#mobile").textbox({

		});
		$("#userState").combobox({
			required : true,
			url : findDictUserStateUrle968477be06c,
			editable : false,
			valueField : 'value',
			textField : 'text',
			panelHeight : 80
		});
		$("#password").textbox({

		});
		$("#remarks").textbox({
			validType : 'length[3,255]',
			multiline:true
		});
		
		$("#accountState").textbox({
			readonly : true
		});
		$("#loginIp").textbox({
			readonly : true
		});
		$("#loginDate").textbox({
			readonly : true
		});
		$("#id").textbox({
			readonly : true
		});
		$("#delFlag").textbox({
			readonly : true
		});
		$("#companyId").textbox({
			readonly : true
		});
		$("#orgId").textbox({
			readonly : true
		});
		$("#createBy").textbox({
			readonly : true
		});
		$("#createDate").textbox({
			readonly : true
		});
		$("#updateBy").textbox({
			readonly : true
		});
		$("#updateDate").textbox({
			readonly : true
		});
		$("#uuid").textbox({
			readonly : true
		});
		
		// 隐藏只读字段
		$("#readonly_c67d13c82a87").hide();
	});
	
	// 控制只读字段显示或隐藏
	function showOrHide_fc0bf3626706() {
		if ($("#readonly_c67d13c82a87").is(":hidden")) {
			$("#readonly_c67d13c82a87").show();
		} else {
			$("#readonly_c67d13c82a87").hide();
		}
	}
</script>




















