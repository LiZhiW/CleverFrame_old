<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute type="java.lang.String" required="true" name="mvcPath" description="系统mvc根路径"%>
<%@ attribute type="java.lang.String" required="true" name="dialogID" description="弹出的easyui-dialog对话框的ID"%>
<%@ attribute type="java.lang.String" required="true" name="formID" description="菜单编辑表单的ID"%>
<%@ attribute type="java.lang.String" required="true" name="saveFunction" description="点击保存按钮时调用的方法"%>
<style type="text/css">
#${dialogID} #${formID} {
	margin: 0;
	padding: 0px 5px;
}
#${dialogID} .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#${dialogID} .row {
	margin-bottom: 8px;
}
#${dialogID} .row label {
	display: inline-block;
	width: 80px;
	text-align: right;
}
#${dialogID} .row input,select {
	width: 160px;
}
#${dialogID} .row .column {
	margin-right: 10px;
}
#${dialogID} .row .columnLast {
	margin-right: 0px;
}
</style>
<div id="${dialogID}" style="width: 850px; height: 350px; padding: 5px 10px">
	<form id="${formID}" method="post">
		<div class="ftitle">编辑字段</div>
		<div class="row">
			<span class="column">
				<label for="parentId">上级菜单</label> 
				<input id="parentId" name="parentId" >
			</span>
			<span class="column">
				<label for="menuType">菜单类型</label>
				<input id="menuType" name="menuType" >
			</span>
			<span class="columnLast">
				<label for="category">菜单类别</label> 
				<input id="category" name="category" >
			</span>
		</div>
		<div class="row">
			<span class="columnLast"> 
				<label for="href">菜单地址</label> 
				<input id="href" name="href" style="width:690px;">
			</span>
		</div>
		<div class="row">
			<span class="column">
				<label for="name">菜单名称</label>
				<input id="name" name="name" >
			</span>
			<span class="column">
				<label for="icon">菜单图标</label> 
				<input id="icon" name="icon" >
			</span>
			<span class="columnLast">
				<label for="sort">菜单排序</label>
				<input id="sort" name="sort" >
			</span>
		</div>
		<div class="row">
			<span class="columnLast">
				<label for="fullPath">菜单树路径</label>
				<input id="fullPath" name="fullPath" style="width: 425px;">
			</span>
		</div>
		<div class="row">
			<span class="columnLast"> 
				<label for="remarks">备注信息</label> 
				<input id="remarks" name="remarks" style="width:690px;height:40px">
			</span>
		</div>
		
		<div class="ftitle"><a href="javascript:void(0)" onclick='showOrHide_a28f24282609();'>只读字段</a></div>
		<div id="readonly_dd91f92fb763">
			<div class="row">
				<span class="column"> 
					<label for="id">数据ID</label> 
					<input id="id" name="id">
				</span> 
				<span class="column"> 
					<label for="delFlag">删除标记</label> 
					<input id="delFlag" name="delFlag">
				</span>
				<span class="columnLast"> 
					<label for="companyId">数据所属公司</label> 
					<input id="companyId" name="companyId">
				</span> 
			</div>
			<div class="row">
				<span class="column"> 
					<label for="orgId">数据直属机构</label> 
					<input id="orgId" name="orgId">
				</span> 
				<span class="column"> 
					<label for="createBy">创建者</label> 
					<input id="createBy" name="createBy">
				</span>
				<span class="columnLast"> 
					<label for="createDate">创建时间</label> 
					<input id="createDate" name="createDate">
				</span>
			</div>
			<div class="row">
				 <span class="column"> 
				 	<label for="updateBy">更新者</label> 
				 	<input id="updateBy" name="updateBy">
				</span> 
				<span class="columnLast"> 
					<label for="updateDate">更新时间</label> 
					<input id="updateDate" name="updateDate">
				</span>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="uuid">数据UUID</label> 
					<input id="uuid" name="uuid" style="width: 430px;">
				</span>
			</div>
		</div>
	</form>
</div>
<div id="${dialogID}-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="${saveFunction}" style="width:90px">保存</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#${dialogID}').dialog('close')" style="width:90px">取消</a>
</div>

<script type="text/javascript">
	// 设置字典表单
	$(document).ready(function() {
		var findDictTypeUrl75a51f0e096f	= "${mvcPath}/sys/findDictByType?dict-type=";
		findDictTypeUrl75a51f0e096f += encodeURIComponent("菜单类型");
		
		$("#${dialogID}").dialog({
			title : "字典信息",
			closed : true,
			resizable : true,
			minWidth : 850,
			minHeight : 350,
			maxWidth : 850,
			maxHeight : 450,
			modal : true,
			buttons : "#${dialogID}-buttons"
		});
		$("#parentId").combotree({
			required : false,
			editable : false,
			animate : false,
			checkbox : false,
			cascadeCheck : true,
			onlyLeafCheck : false,
			lines : true,
			dnd : false
		});
		$("#category").textbox({
			required : true,
			validType : 'length[1,100]'
		});
		$("#menuType").combobox({
			required : true,
			url : findDictTypeUrl75a51f0e096f,
			editable : false,
			valueField : 'value',
			textField : 'text',
			panelHeight : 50
		});
		$("#name").textbox({
			required : true,
			validType : 'length[1,50]'
		});
		$("#href").textbox({
			required : true,
			validType : 'length[1,255]'
		});
		$("#icon").textbox({
			required : true,
			validType : 'length[1,50]'
		});
		$("#sort").numberbox({
			value : 0,
			min : 0,
			max : 999999999999,
			precision : 0,
			required : true
		});
		$("#fullPath").textbox({
			readonly : true
		});
		$("#remarks").textbox({
			validType : 'length[3,255]',
			multiline:true
		});
		
		$("#id").textbox({
			readonly : true
		});
		$("#delFlag").textbox({
			readonly : true
		});
		$("#companyId").textbox({
			readonly : true
		});
		$("#orgId").textbox({
			readonly : true
		});
		$("#createBy").textbox({
			readonly : true
		});
		$("#createDate").textbox({
			readonly : true
		});
		$("#updateBy").textbox({
			readonly : true
		});
		$("#updateDate").textbox({
			readonly : true
		});
		$("#uuid").textbox({
			readonly : true
		});
		
		// 隐藏只读字段
		$("#readonly_dd91f92fb763").hide();
	});
	
	// 控制只读字段显示或隐藏
	function showOrHide_a28f24282609() {
		if ($("#readonly_dd91f92fb763").is(":hidden")) {
			$("#readonly_dd91f92fb763").show();
		} else {
			$("#readonly_dd91f92fb763").hide();
		}
	}
</script>
