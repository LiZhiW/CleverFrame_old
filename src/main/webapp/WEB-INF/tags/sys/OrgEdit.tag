<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute type="java.lang.String" required="true" name="mvcPath" description="系统mvc根路径"%>
<%@ attribute type="java.lang.String" required="true" name="dialogID" description="弹出的easyui-dialog对话框的ID"%>
<%@ attribute type="java.lang.String" required="true" name="formID" description="机构编辑表单的ID"%>
<%@ attribute type="java.lang.String" required="true" name="saveFunction" description="点击保存按钮时调用的方法"%>
<style type="text/css">
#${dialogID} #${formID} {
	margin: 0;
	padding: 0px 5px;
}
#${dialogID} .ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#${dialogID} .row {
	margin-bottom: 8px;
}
#${dialogID} .row label {
	display: inline-block;
	width: 80px;
	text-align: right;
}
#${dialogID} .row input,select {
	width: 160px;
}
#${dialogID} .row .column {
	margin-right: 10px;
}
#${dialogID} .row .columnLast {
	margin-right: 0px;
}
</style>
<div id="${dialogID}" style="width: 830px; height: 350px; padding: 5px 10px">
	<form id="${formID}" method="post">
		<div class="ftitle">编辑字段</div>
		<div class="row">
			<span class="column"> 
				<label for="parentId">上级机构</label> 
				<input id="parentId" name="parentId">
			</span> 
			<span class="column"> 
				<label for="orgType">机构类型</label> 
				<input id="orgType" name="orgType">
			</span>
		</div>
		<div class="row">
			<span class="column"> 
				<label for="name">机构名称</label> 
				<input id="name" name="name">
			</span> 
			<span class="columnLast"> 
				<label for="master">机构负责人</label> 
				<input id="master" name="master">
			</span>
		</div>
		<div class="row">
			<span class="column"> 
				<label for="phone">电话</label> 
				<input id="phone" name="phone">
			</span> 
			<span class="column"> 
				<label for="fax">传真</label> 
				<input id="fax" name="fax">
			</span> 
			<span class="columnLast"> 
				<label for="email">邮箱</label> 
				<input id="email" name="email">
			</span>
		</div>
		<div class="row">
			<span class="column">
				<label for="address">机构地址</label>
				<input id="address" name="address" style="width: 430px;">
			</span>
			<span class="columnLast"> 
				<label for="zipCode">邮政编码</label> 
				<input id="zipCode" name="zipCode">
			</span>
		</div>
		<div class="row">
			<span class="columnLast"> 
				<label for="remarks">备注信息</label> 
				<input id="remarks" name="remarks" style="width:690px;height:40px">
			</span>
		</div>
		
		<div class="ftitle">保存时自动生成字段</div>
		<div class="row">
			<span class="column">
				<label for="code">机构编码</label>
				<input id="code" name="code">
			</span>
			<span class="columnLast"> 
				<label for="fullPath">机构路径</label> 
				<input id="fullPath" name="fullPath" style="width: 430px;">
			</span>
		</div>
		
		<div class="ftitle"><a href="javascript:void(0)" onclick='showOrHide_39a32f34fd48();'>只读字段</a></div>
		<div id="readonly_5701cafa7c40">
			<div class="row">
				<span class="column"> 
					<label for="id">数据ID</label> 
					<input id="id" name="id">
				</span> 
				<span class="column"> 
					<label for="delFlag">删除标记</label> 
					<input id="delFlag" name="delFlag">
				</span>
				<span class="columnLast"> 
					<label for="companyId">数据所属公司</label> 
					<input id="companyId" name="companyId">
				</span> 
			</div>
			<div class="row">
				<span class="column"> 
					<label for="orgId">数据直属机构</label> 
					<input id="orgId" name="orgId">
				</span> 
				<span class="column"> 
					<label for="createBy">创建者</label> 
					<input id="createBy" name="createBy">
				</span>
				<span class="columnLast"> 
					<label for="createDate">创建时间</label> 
					<input id="createDate" name="createDate">
				</span>
			</div>
			<div class="row">
				 <span class="column"> 
				 	<label for="updateBy">更新者</label> 
				 	<input id="updateBy" name="updateBy">
				</span> 
				<span class="columnLast"> 
					<label for="updateDate">更新时间</label> 
					<input id="updateDate" name="updateDate">
				</span>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="uuid">数据UUID</label> 
					<input id="uuid" name="uuid" style="width: 430px;">
				</span>
			</div>
		</div>
	</form>
</div>
<div id="${dialogID}-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="${saveFunction}" style="width:90px">保存</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#${dialogID}').dialog('close')" style="width:90px">取消</a>
</div>

<script type="text/javascript">
	//设置机构表单
	$(document).ready(function() {
		var findDictTypeUrla734702c1038	= "${mvcPath}/sys/getChildOrgType";
		
		$("#${dialogID}").dialog({
			title : "机构信息",
			closed : true,
			resizable : true,
			minWidth : 850,
			minHeight : 350,
			maxWidth : 850,
			maxHeight : 510,
			modal : true,
			buttons : "#${dialogID}-buttons"
		});
 		$("#parentId").combotree({
			required : false,
			editable : false,
			animate : false,
			checkbox : false,
			cascadeCheck : true,
			onlyLeafCheck : false,
			lines : true,
			dnd : false,
			onSelect : function(record){
				var param = {};
            	param.orgId = record.id;
    			$.post(findDictTypeUrla734702c1038, param, function (data) {
    				$("#orgType").combobox("clear");
    				$("#orgType").combobox("loadData",data);
    			}, "json");
			},
			onUnselect : function(record){
				$("#orgType").combotree("clear");
			}
		});
		$("#orgType").combobox({
			required : true,
			editable : false,
			valueField : 'value',
			textField : 'text',
			panelHeight : 80
		});
		$("#name").textbox({
			required : true,
			validType : 'length[1,100]'
		});
		$("#master").textbox({
			required : false,
			validType : 'length[1,100]'
		});
		$("#phone").textbox({
			required : false,
			validType : 'length[1,100]'
		});
		$("#fax").textbox({
			required : false,
			validType : 'length[1,100]'
		});
		$("#email").textbox({
			required : false,
			validType : 'length[1,100]'
		});
		$("#address").textbox({
			required : false,
			validType : 'length[1,255]'
		});
		$("#zipCode").textbox({
			required : false,
			validType : 'length[1,50]'
		});
		$("#remarks").textbox({
			required : false,
			validType : 'length[3,255]',
			multiline:true
		});
		
		$("#code").textbox({
			readonly : true
		});
		$("#fullPath").textbox({
			readonly : true
		});
		
		$("#id").textbox({
			readonly : true
		});
		$("#delFlag").textbox({
			readonly : true
		});
		$("#companyId").textbox({
			readonly : true
		});
		$("#orgId").textbox({
			readonly : true
		});
		$("#createBy").textbox({
			readonly : true
		});
		$("#createDate").textbox({
			readonly : true
		});
		$("#updateBy").textbox({
			readonly : true
		});
		$("#updateDate").textbox({
			readonly : true
		});
		$("#uuid").textbox({
			readonly : true
		});
		
		// 隐藏只读字段
		$("#readonly_a105429041a5").hide();
	});
	
	// 控制只读字段显示或隐藏
	function showOrHide_39a32f34fd48() {
		if ($("#readonly_5701cafa7c40").is(":hidden")) {
			$("#readonly_5701cafa7c40").show();
		} else {
			$("#readonly_5701cafa7c40").hide();
		}
	}
</script>



















