<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute type="java.lang.String" required="true" name="areaTreeID" description="区域树easyui-tree的ID"%>
<%-- 其他属性 --%>

<ul id="${areaTreeID}"></ul>

<script type="text/javascript">
	$(document).ready(function() {
		// 构造区域树
		$("#${areaTreeID}").tree({
			url : "/CodeGenerator/mvc/sys/getAreaTree",
			animate : false,
			checkbox : false,
			cascadeCheck : true,
			onlyLeafCheck : false,
			lines : true,
			dnd : false,
			onClick : function(node){
				alert(node);
			}
		});
	});
</script>