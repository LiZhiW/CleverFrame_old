<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute type="java.lang.String" required="true" name="formID" description="登入表单的ID"%>

<div id="loginPanela09a13e36b98" class="easyui-panel" align="center">
	<form id="${formID}" method="post">
		<div id="errorMessage0a86c64a0f8f" style="height: 20px;color: red;" align="center"></div>
		<table>
			<tr>
				<td style="text-align:center">用户名:</td>
				<td><input id="userName_0eba080e1897" name="userName" type="text" style="width: 200px"></input></td>
			</tr>
			<tr>
				<td style="text-align:center">密&ensp;&ensp;码:</td>
				<td><input name="password" type="password" style="width: 200px"></input></td>
			</tr>
			<tr id="validateCodeId9fb9ff701a2f">
				<td style="text-align:center">验证码:</td>
				<td>
					<input name="validateCode" type="text" maxlength="4" style="width: 80px"></input>
					<img id="validateCodeImg3c029f6267a3" alt="验证码" title="看不清可单击图片刷新" style="vertical-align:middle;cursor:pointer;" onclick="refreshvalidateCode();"/>
				</td>
			</tr>
		</table>
	</form>
	
    <div style="text-align:center;padding:5px">
    	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitFormcf50385902cc()">登&ensp;&ensp;录</a>
    	&ensp;&ensp;&ensp;&ensp;
    	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#${formID}').form('clear');">清&ensp;&ensp;空</a>
    </div>
</div>

<script type="text/javascript">
	// 验证码获取地址
	var validateCodeUrl = "/CleverFrame/servlet/validateCodeServlet";
	// 用户登入地址
	var loginUrl = "/CleverFrame/mvc/sys/login";
	// 登入成功地址
	var loginSuccessUrl = "/CleverFrame/mvc/sys/getSysMainJsp";
	// 验证码是否显示
	var validateCodeIsShow = false;
	
	$(document).ready(function() {
		//设置登录面板
		$("#loginPanela09a13e36b98").panel({
			title : '登入系统',
			width : '300px',
			height : '180px'
		});
	
		//设置表单-----------------------------------------------
		$(":text[name='userName']").textbox({
			iconCls : 'icon-man',
			prompt : "用 户 名",
			required : true,
			validType : 'length[3,20]',
			invalidMessage : '用户名长度必须是3—20个字符',
			missingMessage : '用户名长度必须是3—20个字符'
		});
	
		$(":password[name='password']").textbox({
			iconCls : 'icon-lock',
			prompt : '密    码',
			required : true,
			validType : 'length[6,20]',
			invalidMessage : '密码长度必须是6—20个字符',
			missingMessage : '密码长度必须是6—20个字符'
		});
		
		$("#validateCodeId9fb9ff701a2f").css("visibility", "hidden");
		
		// 按下回车就请求登入
		$("#${formID} input").keydown(function (event) {
			if (event.keyCode == 13) {
				submitFormcf50385902cc();
			}
		});
		
		// 获取焦点
		$("#userName_0eba080e1897").next("span").find("input").focus();
	});

	//表单登入、验证逻辑----------------------------------------------
	function submitFormcf50385902cc() {
		$('#${formID}').form('submit',{
			url : loginUrl,
			onSubmit : function() {
				return $(this).form('enableValidation').form('validate');
			},
			success : function(data) {
				var data = $.parseJSON(data);
				if (data.success) {
					//登录成功
					location.href = loginSuccessUrl;
				} else {
					//登入失败
					$("#errorMessage0a86c64a0f8f").text(data.message);
					// 登入失败次数过多，需要验证码
					if(data.needValidateCode && !validateCodeIsShow) {
						//登入失败刷新验证码
						$("#validateCodeId9fb9ff701a2f").css("visibility", "visible");
						$(":text[name='validateCode']").textbox({
							prompt : "验证码",
							required : true,
							validType : 'length[4,4]',
							invalidMessage : '验证码无效',
							tipPosition : "left"
						});
						validateCodeIsShow = true;
					}
					if(data.needValidateCode) {
						refreshvalidateCode();
					}
				}
			}
		});
	};
	
	// 刷新验证码
	function refreshvalidateCode() {
		$("#validateCodeImg3c029f6267a3").attr("src", validateCodeUrl + "?d=" + Math.random());
	}
</script>




