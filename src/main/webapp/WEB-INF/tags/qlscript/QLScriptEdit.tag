<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute type="java.lang.String" required="true" name="mvcPath" description="系统mvc根路径"%>
<%@ attribute type="java.lang.String" required="true" name="dialogID" description="弹出的easyui-dialog对话框的ID"%>
<%@ attribute type="java.lang.String" required="true" name="formID" description="QL脚本编辑表单的ID"%>
<%@ attribute type="java.lang.String" required="true" name="saveFunction" description="点击保存按钮时调用的方法"%>
<style type="text/css">
#${dialogID} #${formID} {
	margin: 0;
	padding: 0px 5px;
}
#${dialogID} .ftitle {
	font-weight: bold;
	margin-bottom: 5px;
	border-bottom: 1px solid #ccc;
}
#${dialogID} .row {
	margin-bottom: 8px;
}
#${dialogID} .row label {
	display: inline-block;
	width: 80px;
	text-align: right;
}
#${dialogID} .row input,select {
	width: 160px;
}
#${dialogID} .row .column {
	margin-right: 10px;
}
#${dialogID} .row .columnLast {
	margin-right: 0px;
}
</style>
<div id="${dialogID}" style="width: 850px; height: 350px; padding: 5px 10px">
	<form id="${formID}" method="post">
		<div class="ftitle">编辑字段</div>
		<div class="row">
			<span class="column"> 
				<label for="parentId">脚本名称</label> 
				<input id="name" name="name" style="width: 425px;">
			</span>
			<span class="columnLast"> 
				<label for="scriptType">脚本类型</label> 
				<input id="scriptType" name="scriptType">
			</span> 
		</div>
		<div class="row">
			<span class="columnLast"> 
				<label for="script">脚本</label> 
				<input id="script" name="script" style="width: 690px; height: 120px">
			</span>
		</div>
		<div class="row">
			<span class="columnLast"> 
				<label for="description">脚本说明</label>
				<input id="description" name="description" style="width: 690px; height: 60px">
			</span>
		</div>
		<div class="row">
			<span class="columnLast"> 
				<label for="remarks">备注信息</label> 
				<input id="remarks" name="remarks" style="width: 690px; height: 60px">
			</span>
		</div>
		
		<div class="ftitle"><a href="javascript:void(0)" onclick="showOrHide_b32f4f15e389();">只读字段</a></div>
		<div id="readonly_e438b28ab6f2">
			<div class="row">
				<span class="column"> 
					<label for="id">数据ID</label> 
					<input id="id" name="id">
				</span> 
				<span class="column"> 
					<label for="delFlag">删除标记</label> 
					<input id="delFlag" name="delFlag">
				</span>
				<span class="columnLast"> 
					<label for="companyId">数据所属公司</label> 
					<input id="companyId" name="companyId">
				</span> 
			</div>
			<div class="row">
				<span class="column"> 
					<label for="orgId">数据直属机构</label> 
					<input id="orgId" name="orgId">
				</span> 
				<span class="column"> 
					<label for="createBy">创建者</label> 
					<input id="createBy" name="createBy">
				</span>
				<span class="columnLast"> 
					<label for="createDate">创建时间</label> 
					<input id="createDate" name="createDate">
				</span>
			</div>
			<div class="row">
				 <span class="column"> 
				 	<label for="updateBy">更新者</label> 
				 	<input id="updateBy" name="updateBy">
				</span> 
				<span class="columnLast"> 
					<label for="updateDate">更新时间</label> 
					<input id="updateDate" name="updateDate">
				</span>
			</div>
			<div class="row">
				<span class="columnLast"> 
					<label for="uuid">数据UUID</label> 
					<input id="uuid" name="uuid" style="width: 430px;">
				</span>
			</div>
		</div>
	</form>
</div>
<div id="${dialogID}-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" onclick="${saveFunction}" style="width:90px">保存</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#${dialogID}').dialog('close')" style="width:90px">取消</a>
</div>

<script type="text/javascript">
	var findDictTypeUrl4899a7f3a59c	= "${mvcPath}/sys/findDictByType?dict-type=";
	findDictTypeUrl4899a7f3a59c += encodeURIComponent("数据库脚本类型");
	// 设置数据库脚本表单
	$(document).ready(function() {
		
		$("#${dialogID}").dialog({
			title : "区域信息",
			closed : true,
			maximizable : false,
			resizable : true,
			minWidth : 850,
			minHeight : 350,
			maxWidth : 850,
			maxHeight : 550,
			modal : true,
			buttons : "#${dialogID}-buttons"
		});

		$("#name").textbox({
			required : true,
			validType : 'length[5,100]',
		});
		$("#scriptType").combobox({
			required : true,
			validType : 'length[1,10]',
			url : findDictTypeUrl4899a7f3a59c,
			editable : false,
			valueField : 'value',
			textField : 'text',
			panelHeight : 50
		});
		$("#description").textbox({
			required : true,
			validType : 'length[2,1000]',
			multiline : true
		});
		$("#remarks").textbox({
			validType : 'length[0,255]',
			multiline : true
		});
		$("#script").textbox({
			required : true,
			validType : 'length[5,2000]',
			multiline : true
		});

		$("#id").textbox({
			readonly : true
		});
		$("#delFlag").textbox({
			readonly : true
		});
		$("#companyId").textbox({
			readonly : true
		});
		$("#orgId").textbox({
			readonly : true
		});
		$("#createBy").textbox({
			readonly : true
		});
		$("#createDate").textbox({
			readonly : true
		});
		$("#updateBy").textbox({
			readonly : true
		});
		$("#updateDate").textbox({
			readonly : true
		});
		$("#uuid").textbox({
			readonly : true
		});
		
		// 隐藏只读字段
		$("#readonly_e438b28ab6f2").hide();
	});
	
	// 控制只读字段显示或隐藏
	function showOrHide_b32f4f15e389() {
		if ($("#readonly_e438b28ab6f2").is(":hidden")) {
			$("#readonly_e438b28ab6f2").show();
		} else {
			$("#readonly_e438b28ab6f2").hide();
		}
	}
</script>






