// 全局ajax事件
$(document).ajaxStart(function () {
	// alert("ajaxStart");
});

$(document).ajaxSend(function (evt, request, settings) {
	// alert("ajaxSend");
});

$(document).ajaxSuccess(function (evt, request, settings) {
	// alert("ajaxSuccess");
});

$(document).ajaxError(function (evt, request, settings) {
	// alert("ajaxError");
	if(request.status == 200){
		// loginPanela09a13e36b98
		// TODO 用户重新登入，不应该使用 "/CodeGenerator/mvc/sys/loginJsp"
		var url = "/CodeGenerator/mvc/sys/loginJsp"; //转向网页的地址
		var name = "登录"; //网页名称
		var iWidth = 600; //弹出窗口的宽度
		var iHeight = 400; //弹出窗口的高度
		var iTop = (window.screen.availHeight - 30 - iHeight) / 2; //获得窗口的垂直位置;
		var iLeft = (window.screen.availWidth - 10 - iWidth) / 2; //获得窗口的水平位置;
		window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no');
	}
});

$(document).ajaxComplete(function () {
	// alert("ajaxComplete");
});

$(document).ajaxStop(function () {
	// alert("ajaxStop");
});
