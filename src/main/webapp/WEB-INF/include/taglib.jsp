<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 加入JSTL标准库 --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- 加入自定义标签库 --%>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>

<%-- 自定义page范围的属性 --%>
<c:set scope="page" var="appPath" value="${pageContext.request.contextPath}" />
<c:set scope="page" var="staticPath" value="${pageContext.request.contextPath}/${fns:getStaticPath()}" />
<c:set scope="page" var="docPath" value="${pageContext.request.contextPath}/${fns:getDocPath()}" />
<c:set scope="page" var="viewsPath" value="${pageContext.request.contextPath}/${fns:getViewsPath()}" />
<c:set scope="page" var="mvcPath" value="${pageContext.request.contextPath}/${fns:getMvcPath()}" />
<%--
项目名称：CleverFrame
	pageScope.appPath:		/CleverFrame
	pageScope.staticPath:	/CleverFrame/static
	pageScope.docPath:		/CleverFrame/views/core/doc
	pageScope.viewsPath:	/CleverFrame/views
	pageScope.mvcPath:		/CleverFrame/mvc
--%>